export function e2eTranslate(fixture: Record<string, unknown>, translationString: string):  string | undefined{

	const [first, ...rest] = translationString.split(".")
	console.log(fixture[first],first, rest)
	if (rest.length === 0) return fixture[first] as string | undefined
	return e2eTranslate(fixture[first] as Record<string, unknown>, rest.join("."))

}
