import cypress = require("cypress")
import {e2eTranslate} from "../e2e-utils"

describe('Doc Version E2E Testing', () => {
/* Open app/doc in a browser on your first device (app/doc) (if you use dark mode, the colors below will not match); you should see the home screen with a button labeled ‘Start new Session’
*
*/


it('should display the questionnaire correctly', () => {
	cy.fixture('translations/en.json').then((en) => {

		cy.visit('https://localhost:4201')
		cy.get('#rcc-e2e-menu-button').should('be.visible').click()
		cy.get('#rcc-e2e-questionnaire-menu-entry').should('be.visible').click({ force: true }).wait(500).click({ force: true })
		cy.log(cy.get('#rcc-e2e-questionnaire-store>ion-list>ion-item-group').eq(0).children("rcc-item-tag").length)
		// cy.get('#rcc-e2e-questionnaire-store>ion-list>ion-item-group').eq(0).children("rcc-item-tag").length
		cy.get('#rcc-e2e-questionnaire-store>ion-list>ion-item-group').eq(0).children("rcc-item-tag").its('length').then(e => {
			cy.get('#rcc-e2e-curated-questions-curated-default-name').should('have.text', `${e2eTranslate(en, "CURATED.QUESTIONS.CURATED_DEFAULT_NAME")} (${e})`).and('be.visible')
		})
		cy.get('#rcc-e2e-questionnaire-store>ion-list>ion-item-group').eq(1).children("rcc-item-tag").its('length').then(e => {
			cy.get('#rcc-e2e-curated-questions-curated-extended-name').should('have.text', `${e2eTranslate(en, 'CURATED.QUESTIONS.CURATED_EXTENDED_NAME')} (${e})`).and('be.visible')
		})
			cy.get('#rcc-e2e-questionnaire-name-actions').should('be.visible').click()

		})
	})

})
