import {	e2eTranslate	} 	from '../e2e-utils'

describe('E2E Testing', () => {
/* Open app/doc in a browser on your first device (app/doc) (if you use dark mode, the colors below will not match) you should see the home screen with a button labeled ‘Start New Session’
*  Command to create updated version of translation JSONs: node rcc_merge_translations.js extract=./lib output=./cypress/fixtures/translations
*  Create a CSV file for easier lookup: node rcc_merge_translations.js extract=./lib csv-export=translations.csv
*/

const symptomCheckName:		string	= 'My first symptom check'
const questionName: 		string	= 'My customized question'

// Open the doc version of the app in a browser on your first device. You should see the home screen with a button labeled ‘Start New Session’.


	it('should run through the first testing part', () => {

		cy.fixture('translations/en.json').then((en: Record<string, string>) => {
			cy.visit('https://localhost:4201')

	// Switch to English first

			cy.get('#rcc-e2e-menu-button').click()
			cy.get('#rcc-e2e-settings-menu-entry').click({ force: true }).wait(500).click({ force: true })
			cy.get('#setting-config-activeLanguage').children().click({ force: true, multiple: true })
			cy.get('ion-radio').eq(0).shadow().should('have.text', ` ${ e2eTranslate(en, 'TRANSLATIONS.LANGUAGES.EN') } `)
			cy.get('ion-radio').eq(0).click({ force: true }).click({ force: true })
			cy.get('ion-back-button').click()
			cy.title().should('eq', e2eTranslate(en, 'APP_NAME')).and('not.be.empty')
			// cy.get('#rcc-e2e-app-name').should('have.text', ` ${e2eTranslate(en, 'APP_NAME} ${e2eTranslate(en, 'APP_NAME_ADD_ON} `)
			cy.get('#rcc-e2e-app-name').should('have.text', ` ${ e2eTranslate(en, 'APP_NAME') }` + ' – ' + 'Health Care Practitioners ')

			cy.get('#rcc-e2e-sessions-open-sessions-actions-new-session-description').should('have.text', ` ${ e2eTranslate(en, 'SESSIONS.OPEN_SESSIONS.ACTIONS.NEW_SESSION.DESCRIPTION') } `).and('be.visible')
			cy.get('#rcc-e2e-sessions-open-sessions-actions-new-session-label').should('contain', e2eTranslate(en, 'SESSIONS.OPEN_SESSIONS.ACTIONS.NEW_SESSION.LABEL')).and('be.visible').click()

// Click on/tap the button, labeled ‘Start New Session’. you should see a new screen/overlay with the title 'Template Selection' listing question templates - each one for a different diagnoses.

			cy.get('#rcc-e2e-sessions-open-sessions-edit-with-template-header').should('have.text', ` ${ e2eTranslate(en, 'SESSIONS.OPEN_SESSIONS.EDIT_WITH_TEMPLATE.HEADER') } `).and('not.be.empty')
			cy.get('rcc-item-tag').should('have.length', 5)

// Select the one for depression.

			cy.get('rcc-item-tag').eq(1).contains(' Symptom check – Template - Depression ').wait(500).click({ scrollBehavior:false })

			cy.get('.rcc-e2e-checkmark-button').click()

// A new screen(smartphone) or an overlay (laptop/tablet) should appear, titled ‘Edit Symptom Check’, with a cancel button, an apply button and various input fields. You should see an input field labeled ‘Label’. At the bottom you should also see the three questions for depression. Each of them should have the colored label saying 'Default question, only edit if necessary'.

			cy.contains(e2eTranslate(en, 'SYMPTOM_CHECKS.EDIT.HEADER_EDIT')).should('be.visible')                    // TODO: check for visibility

			cy.get('#rcc-e2e-symptom-check-question-default-category').should('have.text', ` ${e2eTranslate(en, 'CURATED.QUESTIONS.CATEGORIES.RCC-CATEGORY-DEFAULT-NOTE')} `).and('not.be.empty')

// Click the input field labeled ‘Label’, replace its content with “My first symptom check”

			cy.get('.native-input').clear({ force: true }).should('have.value', '').wait(100).scrollIntoView().type(`${ symptomCheckName }`,{ scrollBehavior:false }).wait(50)
			cy.get('.native-input').should('have.value', `${symptomCheckName}`)

// Click the button labeled ‘Select questions’ you should see a new screen/overlay listing questions and a green round button at the bottom right with a check mark on it.

			cy.contains(e2eTranslate(en, 'SYMPTOM_CHECKS.EDIT.SELECT_QUESTIONS')).should('be.visible').click()

			cy.get('rcc-item-tag')
			.should('have.length.greaterThan', 14)
			.first()
			.should('contain', '?')

			cy.get('.rcc-e2e-checkmark-button').should('be.visible')


// Click the first three questions in that list. All three questions should now have a green checkmark next to them (in contrast to all the other questions).

		cy.get('#rcc-curated-default-01-daily').children('ion-item').should('have.class', 'item-checkbox-checked')
		cy.get('#rcc-curated-default-02-daily').children('ion-item').should('have.class', 'item-checkbox-checked')
		cy.get('#rcc-curated-default-03-daily').children('ion-item').should('have.class', 'item-checkbox-checked')

// Click the round green button at the bottom right. The overlay/screen should disappear you should be back at the screen/overlay titled ‘Edit symptom check’. The three questions you selected should now be visible at the bottom of the scree2eTranslate(en, '

		cy.get('.rcc-e2e-checkmark-button').last().click()
		// cy.get('ion-list').eq(1).scrollTo('bottom', {ensureScrollable: false})
		cy.wait(500)
		cy.get('rcc-item-tag').eq(0).should('be.visible')
		cy.get('rcc-item-tag').eq(1).should('be.visible')
		// cy.get('rcc-item-tag').eq(2).scrollIntoView()

// Click the first question a menu should appear with various items: you should see one item labeled ‘Customize’.

		cy.get('rcc-item-tag').eq(0).click()
		cy.get('.action-sheet-button').should('have.length', 5)
		cy.contains('.action-sheet-button-inner', 'Customize').should('be.visible')

// Click the menu item labeled ‘Customize’ You should see a new screen/overlay, titled ‘Customize Question’, a cancel button, an apply button and various inputs. You should see an input field labeled ‘Wording’ and a select input labeled ‘Question type’.

		cy.contains('.action-sheet-button-inner', e2eTranslate(en, 'SYMPTOM_CHECKS.EDIT.CUSTOMIZE_QUESTION')).parent().click({ force: true })
		cy.contains('.rcc-e2e-question-edit-apply', e2eTranslate(en, 'APPLY')).should('be.visible')
		cy.contains('.rcc-e2e-question-edit-cancel', e2eTranslate(en, 'CANCEL')).should('be.visible')
		cy.contains(e2eTranslate(en, 'QUESTIONS.EDIT.WORDING')).should('exist')                      // TODO: check for visibility
		cy.get('.rcc-e2e-popover-select').shadow().find('button').eq(0).click({ force: true }).wait(500)
		cy.get('ion-select-popover>ion-list>ion-radio-group>ion-item').should('have.length', 7)
		cy.get('.rcc-e2e-basic-question-edit-widgets-yes-no-label').eq(0).should('have.text', ` ${e2eTranslate(en, 'BASIC_QUESTION_EDIT_WIDGETS.YES_NO.LABEL')} `)
		cy.get('.rcc-e2e-basic-question-edit-widgets-yes-no-label').eq(1).click()
		// cy.contains('Yes-No-Question , ` ${e2eTranslate(en, 'QUESTIONS.EDIT.QUESTION_TYPE} `).should('exist')                //TODO: check for visibility


		// !! CHANGE OF SEQUENCE due to change of code !!



// Click the input field labeled ‘Wording’. Replace its content with “My customized question'. Does it work?

		cy.contains(e2eTranslate(en, 'QUESTIONS.EDIT.WORDING')).next().children().clear({ force: true }).type(`${ questionName }`, { force: true }).wait(500).should('have.value', `${questionName}`)
		cy.contains(e2eTranslate(en, 'BASIC_QUESTION_EDIT_WIDGETS.YES_NO.USE_CUSTOM_OPTIONS')).next().click({ force: true })

// Click the select input labeled ‘Question type’. You should see a select menu with various options, among them ‘Yes-No-Question’.






// Click the option labeled ‘Yes-No-Question’. You should now see no input elements other than the cancel button, the apply button, the input field labeled ‘Wording’, the select input labeled ‘Question type’ and a red button labeled ‘GENERATE PREVIEW’.

		cy.contains('.rcc-e2e-question-edit-apply', e2eTranslate(en, 'QUESTIONS.EDIT.APPLY')).should('be.visible')      // TODO: check for visibility
		cy.contains('.rcc-e2e-question-edit-cancel', e2eTranslate(en, 'QUESTIONS.EDIT.CANCEL')).should('be.visible')    // TODO: check for visibility

// Click the red button labeled ‘GENERATE PREVIEW’. At the bottom of the screen/overlay you should see a preview of the question with a heading saying ‘My customized question. Did it work?’ and two radio buttons labeled ‘yes’ and ‘no’.

		cy.contains('#rcc-e2e-query-view-question', `${questionName}`).should('be.visible')
		// TODO: FIX THIS TEST cy.get('.rcc-e2e-answers').contains(e2eTranslate(en, 'YES')).should('exist')  // TODO: check for visibility
		// TODO: FIFX THIS TEST cy.get('.rcc-e2e-answers').contains(e2eTranslate(en, 'NO')).should('exist')   // TODO: check for visibility

// Click the apply button. You should be back at the screen/overlay titled ‘Edit symptom check’. The three questions may have changed in order. One of the questions should be labeled ‘My customized question. Did it work?’.

		cy.contains('.rcc-e2e-question-edit-apply', e2eTranslate(en, 'QUESTIONS.EDIT.APPLY')).click()
		cy.get('#rcc-e2e-symptom-checks-edit-header-edit').contains(e2eTranslate(en, 'SYMPTOM_CHECKS.EDIT.HEADER_EDIT')).should('be.visible')
		cy.get('rcc-item-tag').eq(0).should('be.visible')
		cy.get('rcc-item-tag').eq(1).should('be.visible')
		cy.get('rcc-item-tag').eq(2).should('be.visible')
		cy.get('rcc-item-tag').should('contain', `${questionName}`)

// Click on 'Add Custom Question'. You should see a new screen/overlay titled 'Add Custom Question'.


// Click on 'Question Type' and switch to 'Medication'. You should see a 'Wording' field and below the toggle titled 'Use Generic Wording' which should be switched on and the app should tell you to insert drug & dosage. Is that correct?

// Fill out the field titled 'Drug' with the help of the autocomplete function. Did it work? Also insert a dosage into the 'Dosage' field. You should now see in the preview at the bottom and in the 'Wording' field grayed out with the message 'Have you been taking your medication (X, Y) as planned? (not answered yet)'

// Click on 'Apply'. You should now be back in the previous menu with the medication question at the bottom of the list.

// Click on one of the other newly added weekly questions. A menu should appear with various items; you should see one item labeled ‘Add Individual Schedule’.

		cy.get('rcc-item-tag').eq(0).click().wait(500)
		cy.contains('.action-sheet-button', e2eTranslate(en, 'SYMPTOM_CHECKS.EDIT.SET_QUESTION_SCHEDULE')).should('be.visible')

// Click the menu item labeled Add individual schedule you should see a new screen/overlay titled ‘Schedule’, a cancel button, an apply button, a section labeled ‘Preview’ and a toggle/switch labeled ‘Restrict to specific days of the week’.

		.click()
		cy.contains('.rcc-e2e-schedule-apply', e2eTranslate(en, 'QUESTIONS.EDIT.APPLY')).should('be.visible')
		cy.contains('.rcc-e2e-schedule-cancel', e2eTranslate(en, 'QUESTIONS.EDIT.CANCEL')).should('be.visible')
		cy.contains('#rcc-e2e-custom-symptom-check-store-schedule-modal-header', e2eTranslate(en, 'SCHEDULE')).should('be.visible')
		cy.contains('h3', e2eTranslate(en, 'QUESTIONS.EDIT.PREVIEW')).should('be.visible')
		cy.get('.rcc-e2e-toggle').should('be.visible').wait(500)

		// Click the toggle/switch labeled ‘Restrict to specific days of the week’ a list of all weekdays should appear on the bottom of the screen/overlay.

		.click()
		cy.get('.rcc-e2e-schedule').should('be.visible')
		cy.get('.rcc-e2e-schedule').children().should('have.length', 7)


// Click on the list item labeled ‘Tuesday’ a green check mark should appear next to it the section labeled ‘Preview’ should now read ‘Every Tuesday’.

		cy.get('.rcc-e2e-schedule-tu').click()
		cy.get('#rcc-e2e-schedule-checkbox-tu').should('have.class', 'checkbox-checked')

// Click the apply button you should be back at the screen/overlay titled ‘Edit symptom check’. One of the questions should now be marked with red text reading ‘Every Tuesday’.

		cy.get('.rcc-e2e-schedule-apply').click()
		cy.get('#rcc-e2e-symptom-checks-edit-header-edit').contains(e2eTranslate(en, 'SYMPTOM_CHECKS.EDIT.HEADER_EDIT')).should('be.visible')
		cy.contains('rcc-item-tag', 'Every Tuesday').should('be.visible')

// Click the apply button you should now be at a screen titled ‘Open Sessions’. You should see a section labeled ‘Session – …’ and in it in a large font the time (ca.) you noted in 1.1 you should see another section labeled ‘Symptom check – My first symptom check’, and multiple buttons one of them labeled ‘Share active symptom check’.

		cy.get('#rcc-e2e-symptom-check-edit-apply').click()
		cy.contains('#rcc-e2e-sessions-home-header', e2eTranslate(en, 'SESSIONS.HOME.HEADER')).should('be.visible')
		// cy.get('.rcc-e2e-symptom-check-title').should('have.text', ` Symptom check – ${symptomCheckName} `)


		cy.get('body').find('#rcc-e2e-sessions-day').invoke('text')
		.then((day: string) => {

				const dayOfWeekName: string = new Date().toLocaleString(
					'default', { weekday: 'long' }
				)

				const days: string[] = day.trim().split(' ')

				expect(days[0]).to.eq('Session')
				expect(days[2]).to.eq(dayOfWeekName)
		})


		// Keep the app open and ready yourself to click the button labeled ‘Share active symptom check’.
		cy.contains('Share Active Symptom Check').click()
		cy.get('rcc-qr-code').wait(500).screenshot('QR', { overwrite: true })

	})

	}).timeout(1000)
})
