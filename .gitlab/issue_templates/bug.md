### 🐜 Bug report
### about: Short explanation  🔧

### Version Information
| Software                       | Version(s) |
| ------------------------| ---------- |
| Windows OS?     |                 |
| Linux OS?           |                 |
| MAC OS?           |


### I'm seeing this behaviour on
_Remove this hint: these checkboxes can be checked like this: [x]_

- [ ] iOS device
- [ ] Android device

- [ ] Chrome
- [ ] Firefox
- [ ] Safari
- [ ] Edge

- [ ] Other

_If you know, add the version of the browser_

### What is the expected behaviour?

### What is the actual behaviour?

### Steps to Reproduce
Example:

1. Open the app
2. Click 'Questions' on home screen
3. Turn device up side down
4. Alarm goes off



#### Screenshots
![Grogu Giffy](https://i.redd.it/ckgqh3bd85741.jpg)

#### Files
A list of relevant files for this issue. This will help people navigate the project and offer some clues of where to start.

***Related code:***

<!-- If you are able to illustrate the bug or feature request with an example, please provide a sample application via one of the following means:
A sample application via GitHub
StackBlitz (https://stackblitz.com)
Plunker (http://plnkr.co/edit/cpeRJs?p=preview)
-->

```
insert short code snippets here
```

### Tasks
Include specific tasks in the order they need to be done in. Include links to specific lines of code where the task should happen at.
- [ ] Task 1
- [ ] Task 2
- [ ] Task 3

### Any possible solutions?
If you have any ideas on how to solve this problem, add them here.
