import	{	NgModule                		}	from '@angular/core'

@NgModule()
export class RccFeaturesModule {

	public constructor(){

		console.error('RccFeaturesModule: no features loaded. Please use appropriate configuration to load a specific set of features. E.g. ng build @rcc/app --configuration=production,doc')

	}

}
