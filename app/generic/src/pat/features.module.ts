
import	{	NgModule                		}	from '@angular/core'

import	{
			RccServiceWorkerModule,
			QrCodeModule,
			ScheduledNotificationModule
		}										from '@rcc/common'

import	{
			BasePatModule,
			QrCodeScannerServiceModule,

			IndexedDbModule,

			SymptomCheckViewModule,

			ImportSymptomCheckStoreServiceModule,
			ImportQuestionStoreServiceModule,

			CombinedTransmissionModule,

			JournalServiceModule,

			BackupModule,

			BasicQueryWidgetsModule,

			DayQueryModule,
			DayQueryRemindersModule,

			// Entries
			EntryShareModule,

			// Basics:
			BasicReportRepresentationModule,
			BasicReportPreparatorModule,
			BasicDataViewWidgetsModule,
			QuadQuestionQueryModule,

			// Fallbacks:
			FallbackDataViewWidgetsModule,

		}										from '@rcc/features'

@NgModule({
	imports: [
		BasePatModule,
		IndexedDbModule,

		ScheduledNotificationModule,


		RccServiceWorkerModule,
		QrCodeModule,
		QrCodeScannerServiceModule,

		JournalServiceModule,

		BasicQueryWidgetsModule,
		QuadQuestionQueryModule,
		DayQueryModule,
		DayQueryRemindersModule,

		SymptomCheckViewModule,

		ImportSymptomCheckStoreServiceModule,
		ImportQuestionStoreServiceModule,

		// should use a different way to configure:
		CombinedTransmissionModule.forRoot(
			'wss://signal.recoverycat.de',
			[
				// 'stun:stun.services.mozilla.com:3478',
				'stun:stun.l.google.com:19302',
			]
		),

		// BackupModule,

		// Entries
		EntryShareModule,


		// Basics:
		BasicReportRepresentationModule,
		BasicReportPreparatorModule,
		BasicDataViewWidgetsModule,


		// Fallbacks:

		FallbackDataViewWidgetsModule,


	],
})
export class RccFeaturesModule{}
