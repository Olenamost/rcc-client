import	{	NgModule                		}	from '@angular/core'

import	{
			RccServiceWorkerModule,
			QrCodeModule,
		}										from '@rcc/common'

import	{
			BaseReposeModule,
			QrCodeScannerServiceModule,
			IndexedDbModule,

			ImportSymptomCheckStoreServiceModule,
			ImportQuestionStoreServiceModule,

			CombinedTransmissionModule,

			JournalServiceModule,

			BackupModule,

			BasicQueryWidgetsModule,
			DayQueryModule,

			// Entries
			EntryShareModule,

			// Basics:
			BasicReportRepresentationModule,
			BasicReportPreparatorModule,
			BasicDataViewWidgetsModule,

			// Fallbacks:
			FallbackDataViewWidgetsModule,
		}										from '@rcc/features'
import {
			ReposeModule,
			ReposeQuestionStoreServiceModule
		}										from '@rcc/repose'


@NgModule({
	imports: [
		BaseReposeModule,
		ReposeModule,
		IndexedDbModule,

		RccServiceWorkerModule,
		QrCodeModule,
		QrCodeScannerServiceModule,

		JournalServiceModule,

		BackupModule,

		BasicQueryWidgetsModule,
		DayQueryModule,

		ReposeQuestionStoreServiceModule,

		ImportSymptomCheckStoreServiceModule,
		ImportQuestionStoreServiceModule,

		// should use a different way to configure:
		CombinedTransmissionModule.forRoot(
			'wss://signal.recoverycat.de',
			[
				// 'stun:stun.services.mozilla.com:3478',
				'stun:stun.l.google.com:19302',
			]
		),

		BackupModule,

		// Entries
		EntryShareModule,

		// Basics:
		BasicReportRepresentationModule,
		BasicReportPreparatorModule,
		BasicDataViewWidgetsModule,

		// Fallbacks:
		FallbackDataViewWidgetsModule,

		// Examples/Mock:
		// ExampleDayQuestionsSetupModule,
		// ExampleIncomingSessionModule,
	],
})
export class RccFeaturesModule{}
