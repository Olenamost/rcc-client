import	{	NgModule                		}	from '@angular/core'


import	{

			RccServiceWorkerModule,
			QrCodeModule,
		}										from '@rcc/common'

import	{
			BaseDocModule,
			QrCodeScannerServiceModule,
			IndexedDbModule,
			PDFModule,

			BackupModule,

			// Sessions
			SessionHomeModule,

			CustomSymptomCheckStoreServiceModule,

			// Curated Questions
			CuratedSymptomCheckStoreServiceModule,

			CuratedQuestionStoreServiceModule,

			CombinedTransmissionModule,

			// Medication:
			MedicationModule,

			// Basics:
			BasicReportRepresentationModule,
			BasicReportPreparatorModule,
			BasicDataViewWidgetsModule,
			BasicQueryWidgetsModule,	// For previews
			BasicQuestionEditWidgetsModule,
			QuadQuestionEditModule,
			QuadQuestionQueryModule,

			// Fallbacks:
			FallbackDataViewWidgetsModule
		}										from '@rcc/features'

import	{
			ExampleIncomingSessionModule,
			// ExampleDayQuestionsSetupModule,
		}										from '@rcc/examples'

@NgModule({
	imports: [
		BaseDocModule,
		IndexedDbModule,

		RccServiceWorkerModule,
		QrCodeModule.provideEntries(),
		QrCodeScannerServiceModule,
		PDFModule,


		// Sessions
		SessionHomeModule.provideEntries(),

		CustomSymptomCheckStoreServiceModule,
		CuratedSymptomCheckStoreServiceModule,

		CuratedQuestionStoreServiceModule,
		// Medication:
		MedicationModule,

		// should use a different way to configure:
		CombinedTransmissionModule.forRoot(
			'wss://signal.recoverycat.de',
			[
				// 'stun:stun.services.mozilla.com:3478',
				'stun:stun.l.google.com:19302',
			]
		),

		BackupModule,

		// Basics:
		BasicReportRepresentationModule,
		BasicReportPreparatorModule,
		BasicDataViewWidgetsModule,
		BasicQueryWidgetsModule,	// For Previews
		BasicQuestionEditWidgetsModule,
		QuadQuestionEditModule,
		QuadQuestionQueryModule,


		// Fallbacks:

		FallbackDataViewWidgetsModule,

		// Examples/Mock:
		// ExampleDayQuestionsSetupModule,
		// ExampleIncomingSessionModule,
	],
})
export class RccFeaturesModule { }
