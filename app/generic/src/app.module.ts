import	{	NgModule                		}	from '@angular/core'
import	{
			IonicBaseAppComponent,
			IonicBaseAppModule
		}										from '@rcc/ionic'

import	{	RccRoutingModule				}	from '@rcc/common'

import	{	RccFeaturesModule				}	from './features/features.module'


@NgModule({
	imports: [
		IonicBaseAppModule,
		RccFeaturesModule,
		RccRoutingModule,
	],
	bootstrap: [IonicBaseAppComponent]
})
export class AppModule { }
