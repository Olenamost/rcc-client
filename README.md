Documentation slowly growing at https://docs.recoverycat.de

* [TranslationsModule](/modules/TranslationsModule.html)
* [WidgetsModule](/modules/WidgetsModule.html#readme)
* [DataVisualizationModule](/modules/DataVisualizationModule.html#readme)


Examples:

* [ExamplePageModule](/modules/ExamplePageModule.html#source)
* [ExampleQuestionStoreModule](/modules/ExampleQuestionStoreModule.html#source)
* [ExampleQueryWidgetsModule](/modules/ExampleQueryWidgetsModule.html#source)
* [ExampleReportPreparatorModule](/modules/ExampleReportPreparatorModule.html#source)


# Getting started

As of now there are no npm packages available, sorry.

## Setup


If you have not installed yarn you need to run ``npm install yarn -g``

1. Clone this repo: https://gitlab.com/recoverycat/rcc-client by executing ``git clone https://gitlab.com/recoverycat/rcc-client  rcc`` via terminal or use your favourite IDE
2. Change directory to the newly created directory ``cd rcc``
3. Run ``npm install`` or better ``yarn install`` to install the necessary dependencies (~~npm install~~ npm has problems with the workspace setup up)
4. You might also need to install angular CLI ``npm install -g @angular/cli``
5. Check if everything works by running either: ``ng serve @rcc/app --configuration=doc --ssl`` to serve the app version for doctors, or ``ng serve @rcc/app --configuration=pat --ssl`` to serve the app version for patients.

you should see sth. like:
```
** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **


✔ Compiled successfully.
```

1. You can copy and paste the displayed http adress to your browser. You can now use the app in your browser.
:tada:
6. with control+c in your terminal you can exit.

From here you can start working on additional or existing modules in e.g. @rcc/features (via: lib/features)
and test it by adding it to the app/src/app/app.module.ts. That's probably the best way if you want to contribute to the project.

But if you want to do something more standalone, there are three ways to do it.
All of them rather awkward because we have not yet published any packages:

1. Fork the repository and do your own thing.
2. Add a new application to the same Workspace/folder.
3. Create a new app in a separate worspace/folder.

Here's how this works:

## Add a new application to the same Workspace.

Add a new angular application to the workspace: ``ng generate application my-app-name`` (no extra routing needed)

Then change your app-module.ts to look like this:

```ts
//app-module.ts

import	{	NgModule				}	from '@angular/core'
import	{
			IonicBaseAppComponent,
			IonicBaseAppModule
		} 								from '@rcc/ionic'

import	{
			//...
			RccRoutingModule
		}								from '@rcc/common'

@NgModule({
	imports: [
		IonicBaseAppModule,
		//...
		RccRoutingModule		//should be last for now
	],
	bootstrap: [IonicBaseAppComponent]
})
export class AppModule { }
```

This should give you a running, but empty app. (try `` ng serve my-app-name``)

In order to see some content add the [HomePageModule](/modules/HomePageModule.html):

```ts
//app-module.ts

import	{	NgModule				}	from '@angular/core'
import	{
			IonicBaseAppComponent,
			IonicBaseAppModule
		} 								from '@rcc/ionic'

import	{
			RccRoutingModule,
			HomePageModule,		//<---
			//...
		}								from '@rcc/common'

@NgModule({
	imports: [
		HomePageModule, 		//<---
		IonicBaseAppModule,
		//...
		RccRoutingModule		//should be last for now
	],
	bootstrap: [IonicBaseAppComponent]
})
export class AppModule { }
```

This time there's a Heading and a menu button. But some styles and the icon on the menu button are missing.
Two steps are in order to fix this:

1. Add the following line to your styles.css (resp. .scss; in your project folder):

```scss
/* styles.css */
@import "~@rcc/ionic/assets/ionic-styles.scss";
@import "~@rcc/ionic/assets/theme.scss";
```

2. Add an entry for assets at your project config in angular.json:

```json
{
	//...
	"projects":{
		//...
		"my-app-name":{
			//...
			"architect":{
				//...
				"build": {
					//...
					"options":{
						//...
						"assets":[
							//...
							{
								"glob": "**/*.svg",
								"input": "node_modules/ionicons/dist/ionicons/svg",
								"output": "./svg"
							}
						]
					}
				}
			}
		}
	}
}
```

After this you should see the proper fonts and icons, and a very small main menu with a link to the home screen
you already know and a settings page that only consists of the language settings.

You can now use the packages of the /lib folder by importing from @rcc/common, @rcc/core, @rcc/examples or @rcc/ionic respectively.

Try adding some of the modules from lib/common oder lib/features. Maybe tale a look at @rcc/app for inspiration.
Sadly there is very little documentation for most of the modules for now, so you will have to figure things
out by yourself for the time being. The exmaples in lib/examples maybe a good starting point.


---

## Separate Workspace, new project, npm workspaces, paths.

TODO...


Add "compilerOptions": { "resolveJsonModule": true, ... } to your root tsconfig.json

```json
	{
		"compilerOptions": {
			"resolveJsonModule": true,
		}
	}

```




# Moving new workspace to /app/ folder and fixing relatives paths

If you move the new workspace to the app folder which contains the workspaces full & doc, you have to also fix the relative paths inside your angular.json. Please compare to other workspaces such as doc for reference.

Lastly also go to the tsconfig.spec.json of your new workspace. There add `../` to the "extends" file path so it looks like this:

```ts
"extends": "../../tsconfig.json"
```
