# Guide zur Anlegung neuer Vorlagefragen im Code

1. Sollte noch kein GitLab Account mit Zugang zum Repository erstellt worden sein, bitte folgenden [Guide](guide-zur-anlegung-eines-gitlab-accounts-und-zum-zugang-zum-rcc-repository.html) lesen.

2. Sollte nicht klar sein, wie man mit dem Terminal und Git Befehlen umgeht, bitte diesen [Guide](guide-zur-benutzung-von-terminal-und-git-befehlen.html) lesen.

Öffnen des Codes und Navigation zu folgendem Ordner. Hier werden Fragen im Code eingefügt:

>lib/features/src/redacted-questions/redacted-question-store.service.ts

Der Fragenkatalog ist **[hier](https://docs.google.com/spreadsheets/d/1-brvjWPvYnPQgQFvAYcc2UfD6QUSbPQtXVEyJA0jn7M/edit#gid=270111671)** zu finden.

Kommentare bei Unklarheiten bzw. To Dos können mit den Präfix  `//`  eingefügt werden.

## Aufbau des Codes

```js
const configs:QuestionConfig[] = [

   {
     id:       'rcc-redacted-a-daily',
     type:     'integer',
     meaning:  "Haben Sie sich heute deprimiert gefühlt?",  // todo
     translations: {
               'en': "Have you felt depressed today?",      // mit deepl übersetzt
               'de': "Haben Sie sich heute deprimiert gefühlt?"
             },
     options:    [
                                                            // engl. antworten benötigen noch professionelle übersetzung
               { value:0  , translations: { en: 'no', /*todo*/  de: 'nein' } },
               { value:1  , translations: { en: 'slightly', /*vorläufig übersetzt*/  de: 'gering' } },
               { value:2  , translations: { en: 'moderately', /*mit deepl übersetzt*/  de: 'mäßig' } },
               { value:3  , translations: { en: 'strongly', /*mit deepl übersetzt*/  de: 'stark' } },


             ],
     tags:     ['rcc-redacted-category-depression',"icd-10-f32","icd-10-f33", "daily"]
   },
```


### Vorgehensweise für jede Zeile

```js
     id:       'rcc-redacted-a-daily',
```
Bei id wird der Frage ein individueller Name gegeben.

Hier bitte wie alphabetisch vorgehen.

'rcc-redacted-a-daily'  -> 'rcc-redacted-b-daily' -> 'rcc-redacted-c-daily' etc.

Handelt es sich um wöchentliche Fragen, das Suffix daily gegen weekly austauschen und alphabetisch anknüpfen.
'rcc-redacted-a-weekly' -> 'rcc-redacted-b-weekly' etc.

```js
     type:     'integer',
```
'integer' sind Zahlenwerte und werden hier verwendet, sobald die Antwortmöglichkeit einen Grad ausdrückt.

'boolean' wird bei einfachen Ja/Nein-Fragen verwendet. (Antwort Ja = true,  Antwort Nein = false)
Einfach soll bedeuten, dass pro Frage nur ein Ja oder Nein erfragt wird.

Bei komplexen Ja/Nein-Fragen werden mehrere Antwortmöglichkeiten mit jeweils einer Ja/Nein-Antwort gestellt. Hier bitte 'integer' eintragen.

```js
     meaning:  "Haben Sie sich heute deprimiert gefühlt?",  // todo
```
Bei meaning bitte den Wert aus Spalte "prototype question with time daily" eintragen, wenn es sich um eine Tagesfrage handelt.

Bei wöchentlichen Fragen den Wert aus der Spalte "prototype question with time weekly" eintragen.

```js
     translations: {
               'en': "Have you felt depressed today?",      // mit deepl übersetzt
               'de': "Haben Sie sich heute deprimiert gefühlt?"
             },
```
Hier bitte die jeweilige Übersetzung eintragen. Sollte es sich nicht um die finale Übersetzung handeln, bitte mit einem Kommentar im Code mit den Präfix // vermerken. Bei 'de' kann derselbe Wert von meaning eingetragen werden.

```js
     options:    [
                                                            // englische antworten benötigen noch professionelle übersetzung
               { value:0  , translations: { en: 'no', /*todo*/  de: 'nein' } },
               { value:1  , translations: { en: 'slightly', /*vorläufig übersetzt*/  de: 'gering' } },
               { value:2  , translations: { en: 'moderately', /*mit deepl übersetzt*/  de: 'mäßig' } },
               { value:3  , translations: { en: 'strongly', /*mit deepl übersetzt*/  de: 'stark' } },
```
Hier jeweils die entsprechende Übersetzung der Antwortmöglichkeiten eintragen. Hier ebenso vorläufige Übersetzungen per Kommentar mit // kenntlich machen.

```js
     tags:     ['rcc-redacted-category-depression',"icd-10-f32","icd-10-f33", "daily"]
```
Bei den Tags mit Komma getrennt und in Anführungszeichen Stichwörter eintragen und gemeinsam in eckige Klammern fassen. Tags sind optional, dennoch sind sie wünschenswert.

Zum Beispiel aus Spalte "Syndrom":
'rcc-redacted-category-bipolar-disorder' bzw. 'rcc-redacted-category-depression'

Weitere Stichwörter können sein:

- Eintrag im ICD-10 (siehe entspr. Spalte):
'icd-10-f32'

- Intervalle:
'daily', 'weekly'

- Medikamenteneinnahme:
'meds'

Anschließend die Datei speichern mit **Strg + S** (Mac: **Cmd + S**). Wurde die Datei korrekt gespeichert, verschwindet oben im Tab der weiße Kreis.

Im Terminal danach eingeben:

```console
$ git add .
$ git commit -m “HIER KOMMENTAR EINFÜGEN”
$ git push origin BRANCH
```

In Zeile 2 innerhalb der Anführungszeichen einen deskriptiven Kommentar hinterlassen wie z. B.:

>"Tagesfragen von Zeile 15 bis 20 eingetragen. Finale Übersetzungen fehlen noch"

In Zeile 3 anstelle von BRANCH bitte den gewählten Namen von oben eingeben.

## Testung der Eingaben über lokalen Server

Um die Testung der Eingaben durchzuführen, gehe sicher, dass du auf dem korrekten Branch bist und gib im Terminal folgenden Befehl ein:

```console
$ ng serve @rcc/app/repose
```

Daraufhin startet der lokale Server und kann nach wenigen Minuten im Browser über folgende URL erreicht werden:

``http://localhost:4200/``

Über das Hauptmenü gelangt man zum ``Questionnaire``, wo sämtliche Fragen angezeigt werden.


### Best Practice Anmerkungen:

- Neue Daily bzw. Weekly Fragen bitte direkt nach der letzten jeweiligen Frage eintragen.
- Einheitliche Verwendung von einfachen Anführungszeichen
- Nach jeder neu eingetragenen Frage 3 Leerzeilen einfügen
- Bei Fragen bitte an Andreas Pittrich (andreas.pittrich@posteo.de) oder Hanhoan Truong (tqh@posteo.net) wenden
