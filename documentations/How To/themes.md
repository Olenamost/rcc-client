# Themes

Everything related to themes lies in `./lib/themes` or `@rcc/themes`. ( There is
one exception and that is the SharedModule, which lies in `./lib/common` and
imports and re-exports the common theme; in the future every module, that makes
use of the theme, should import it the active theme by itself and not through
SharedModule).


## General setup


Every theme consists of a set of components and an NgModule exporting all those
components. The set of components is determined by the files in
`./lib/themes/theming-mechanics/components`. Each component file in this
directory holds an abstract component, that defines an interface for a component
and a selector. For each of those abstract components a theme has to provide
an actual component that extends one of the abstract components and uses the
same selector as this abstract component.

In this sense every theme provides the same set of components; the actual
implementation beyond selector and component interface can vary between the
themes.

The active theme is determined by `./lib/themes/active.ts`. By default it exports
everything from `./lib/themes/default`: The default theme. Exchanging this
file for another by a fileReplacement configuration in angular.json allows to
export any other theme module as `RccThemeModule`.
```
//theme.ts replacing ./lib/themes/active.ts

export * from '@rcc/themes/bengal'

```




## Creating a new component from scratch

### Create a base component

The first step is to lay down the interface for the new component. The new component
file is meant to be extended by the real component later on. This way components
of every theme will have to conform to the same interface and will not cause
errors when one theme replaces another.

Go to `lib/themes/theme-mechanics/components` an create
`RccSomethingBaseComponent` in `something-base.component.ts`. Make it
an abstract class and define Inputs and Outputs and a selector. (Abstract
classes, properties and methods can be decorated just like in non-abstract
components.)

Add a Description on top of it telling what the actual component is supposed
to do. (The description will be the basis for the unit test, see below)

__Note:__ Whenever you import from `@rcc/common`, import from a deeper entry
point like `@rcc/common/action` to avoid circular dependencies (they might not
show up when serving the app, but only in the nit test)

### Create a default component

This is where the actual implementation starts. The default component should do
everything listed in the description of the base component. The default
component needs no fancy styling, it should just do its job with as little
expense as possible.

Go to `lib/themes/default/something` and create
`something.component.ts`. Add files for styles and templates as needed.

Make the default component standalone (see
https://angular.io/guide/standalone-components). This way it is a lot easier for
other themes to build on the new component.

Create `RccSomethingComponent` class extending  `RccSomethingBaseComponent`.

Add the new component to `lib/themes/default/theme.module.ts`

__Note__: Since the default theme is probably not the one th current variant use,
it is a bit cumbersome to develop. There are two ways to temporarily work with the
default components (will improve this over time), see below.

### ~Add  a unit test~

__Skip for now :(__

When the component works as intended add a unit test, testing everything
included in the description of the base class. First thing to test is if the
component extends the base class. Save the test into
`lib/themes/theming-mechanics/components/somethig.component.spec` next to the
base component.

This unit test will be used for this default component and for any component that
comes as replacement by means of another theme. Checking if it extends the base
class ensures that every component of every theme conforms to the same component
interfaces.

In order for the unit test to work with every theme, use
`@rcc/themes/active` as import source in the unit test file for the component to
be tested. If no theme is loaded this will point back to the default theme:

```
// Import the corresponding component from whatever theme is active.
// If no theme is loaded this will be the default theme.

import { RccSomethingComponent } from "@rcc/themes/active"

```
So the units will always test the active theme.

### Create the actual component for a specific theme

Goto `lib/themes/bengal` (or your theme folder). Create
`RccSomethingComponent` in `lib/themes/bengal/something.ts`. Add styles and
a template as needed, no restrictions just make sure the new Component passes
the unit test, when (e.g. bengal theme is loaded). This is where all the fancy
design templates ought to be applied :)

You can start by extending `RccSomethingBaseComponent` or reuse the code from
the default theme component by extending `RccSomethingComponent` from
`@rcc/themes/default`:


```
import { RccSomethingComponent as DefaultSomethingComponent } from '@rcc/themes/default'

@Component({
	//...
})
export class RccSomethingComponent extend DefaultSomethingComponent {
	//...
}

```

Make the actual component standalone too (see
https://angular.io/guide/standalone-components). This way it is a lot easier for
other themes to build on the new component.

Add the component to `lib/themes/bengal/theme.module`.

The app should now be able to pass all the unit tests with both the default
theme and with bengal theme loaded.



## Temporarily switching theme during development

I can think of three ways to do this (it is still
to be determined which one of them is the least annoying)

* Go to `app/generic/src` and then into the current variant sub folder. Edit
`theme.ts` so it points to the theme you want to work with. (Change it back when
you are done)
* Edit angular.json and comment out the fileReplacement configuration for the
variant you want to test. This will load the default theme. (Change it back when
you are done)
* While working on a specific component, have the current theme re-export the
corresponding default component as it's own. This way, as you can work on the
default theme component while another custom theme is loaded.



