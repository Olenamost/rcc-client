# Unit tests

## Frameworks

[Jasmine](https://jasmine.github.io/)

Jasmine is a behavior-driven development framework for testing JavaScript code. It does not depend on any other JavaScript frameworks. It does not require a DOM. And it has a clean, obvious syntax so that you can easily write tests.

[Karma](http://karma-runner.github.io/6.4/index.html)

A simple tool that allows you to execute JavaScript code in multiple real browsers.


## How to run tests

Be sure to have run `yarn install` beforehand.
For writing tests and testing them, you may use the following command in your terminal:

```
ng test
```

At this point, you will have output on your terminal. There are options available which you change in the `karma.conf.cjs` file. For more information visit [the official documentation](http://karma-runner.github.io/6.4/config/configuration-file.html).

There is also the possibility of getting output in a headless browser, but this is currently not properly working even though you may notice that a headless browser instance is initiated.

### Useful tips for writing tests:

You can disable the running of certain tests by writing `xdescribe` or `xit`. Alternatively you can write `fdescribe` or `fit` to run certain tests explicitly while not running all others which were not marked with `f`.

For more information about testing an Angular app via Jasmine, check out [the official documentation](https://angular.io/guide/testing).

# E2E tests

## Frameworks

[Cypress](https://www.cypress.io/)

Fast, easy and reliable testing for anything that runs in a browser with focus on E2E testing.

## How to write tests

Please refer to [the official documentation](https://docs.cypress.io/guides/end-to-end-testing/writing-your-first-end-to-end-test#What-you-ll-learn).

## How to run tests

Be sure to have run `yarn install` beforehand.
1. Next, you need to run the app on a local server. For that type in your terminal:

For the HPC version:
```
yarn run start:doc
```
This will open the localhost on *https://localhost:4201*.
FYI two JSON files are created automatically which contain all current translations which are then used in the E2E tests. When pushing your code, these files are will be ignored by git as well as created images such as the QR code and screen recordings of the tests that were run.

For the patient version:
```
yarn run start:pat
```
This will open the localhost on *https://localhost:4202*.



### With GUI

To run Cypress for the *HPC* version in the browser, please run the following command:

```
yarn run e2e:doc
```
This will open the Cypress browser interface. Select *E2E Testing*. Afterwards select a browser. This will open a headless browser. On the left hand side go to the tab called *Specs* and select the Cypress test file that you want to run.

This way is best for writing E2E tests as you will see in detail what is tested and what may not be working as intended.


### Without GUI

#### OPTION 1 without killing the server

To run Cypress without browser for the *HPC* version in the browser, please run the following command:

```
yarn run e2e:doc:run
```

For the *PAT* version please run the following command:

```
yarn run e2e:pat:run
```

Both commands will solely run with in comparison somewhat limited terminal output.
Please beware, that these commands will not kill the servers afterwards.


#### OPTION 2 with killing the server

To run Cypress without browser and with killing the server
- for the *HPC* version in the browser, please run the following command:

```
yarn run e2e:doc:cli
```

- for the *PAT* version please run the following command:

```
yarn run e2e:pat:cli
```

Both commands will solely run with terminal output.

These commands are best suited for integration into a CI pipeline. This is currently not working properly and will be implemented soon. (30.11.2022)
