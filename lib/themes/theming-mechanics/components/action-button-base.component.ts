import	{
			Component,
			Input,
		}									from '@angular/core'

import	{	Action						}	from '@rcc/common/actions'

import	{	HierarchicalColor			}	from '../interfaces'



/**
 * This class is meant to be a base theme component; meaning that every theme should
 * have a component extending this base class.
 *
 * Here's what any action button is supposed to do:
 * * If the action has a notification Observable<boolean|number>, always show the most recent value.
 * * Show label of the action
 * * Show description of the action if present
 * * When clicked: execute the provided action {@link #action}, for all types of actions
 *   (see {@link Action}, {@link ActionService.execute)} will help).
 * * Recognize `.failureMessage`, `.successMessage` and `.confirmMessage` properties of provided action.
 *   ({@link ActionService.execute)} will help)
 * * Use color scheme associated with hierarchical color if provided with color input ('primary', 'secondary'...)
 * * Use color scheme associated with the `.role` property of the action if no other color is provided ('share', 'details'...')
 * * Show `.childComponent` if present on action
 */
@Component({
	selector:		'rcc-action-button',
	template:		''
})
export abstract class RccActionButtonBaseComponent {

	@Input()
	public abstract action	: Action

	@Input()
	public abstract color	: HierarchicalColor

}
