import 	{
			NgModule,
			Type
		}											from '@angular/core'

import	{	RccActionButtonComponent			}	from './action-button/action-button.component'

const modules : Type<unknown>[] = [
	RccActionButtonComponent
]

@NgModule({
	imports: [
		...modules
	],
	exports:[
		...modules
	]
})
export class RccThemeModule {
}
