import	{
			Component,
			HostBinding
		}										from '@angular/core'

import	{	CommonModule					}	from '@angular/common'

import	{
			RccActionButtonComponent as DefaultActionButtonComponent
		}										from '@rcc/themes/default'

import	{	TranslationsModule				}	from '@rcc/common/translations'
import	{	UiComponentsModule				}	from '@rcc/common/ui-components'

@Component({
	selector:		'rcc-action-button',
	templateUrl:	'./action-button.component.html',
	styleUrls:		['./action-button.component.css'],
	standalone:		true,
	imports:[
		CommonModule,
		UiComponentsModule,
		TranslationsModule,
	]
})
export class RccActionButtonComponent extends DefaultActionButtonComponent{

	@HostBinding('class.disabled')
	public get disabled(): boolean { return this.action.disabled && this.action.disabled() }


	@HostBinding('style.--foreground-color')
	public get foregroundColor(): string { return this.color ? this.color : `var(--rcc-color-${this.action.role}-foreground)` }

	@HostBinding('style.--foreground-hover-color')
	public get foregroundHoverColor(): string { return this.color ? this.color : `var(--rcc-color-${this.action.role}-foreground-hover)` }

	@HostBinding('style.--background-color')
	public get backgroundColor(): string { return this.color ? this.color : `var(--rcc-color-${this.action.role}-background)` }

	@HostBinding('style.--background-hover-color')
	public get backgroundHoverColor(): string { return this.color ? this.color : `var(--rcc-color-${this.action.role}-background-hover)` }


}
