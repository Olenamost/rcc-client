import 	{
			NgModule,
			Type
		}								from '@angular/core'

import	{
			RccActionButtonComponent
		}								from './action-button/action-button.component'




 const components : Type<unknown>[] = [

	RccActionButtonComponent,

 ]

@NgModule({
	imports: [
		...components
	],
	exports:[
		...components
	]
})
export class RccThemeModule {}
