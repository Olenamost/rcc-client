import	{
			Component,
			Input,
			ViewEncapsulation,
			OnDestroy,
			ChangeDetectorRef
		}										from '@angular/core'

import	{
			Subscription
		}										from 'rxjs'

import	{
			Action,
			hasNotification,
			WithChildComponent,
			WithDescription,
			ActionService,
			RccAlertController,
			RccToastController
		}										from '@rcc/common'


import	{
			RccActionButtonBaseComponent,
			HierarchicalColor
		}										from '@rcc/themes/theming-mechanics'

import	{
			DefaultThemeCommonModule
		}										from '../default-theme-common.module'

@Component({
	selector:		'rcc-action-button',
	templateUrl:	'./action-button.component.html',
	styleUrls:		['./action-button.component.css'],
	encapsulation:	ViewEncapsulation.None,
	standalone:		true,
	imports:		[
						DefaultThemeCommonModule
					]
})
export class RccActionButtonComponent extends RccActionButtonBaseComponent implements OnDestroy {


	public notificationValue 	: boolean | number 		= false

	protected notificationSub	: Subscription | null	= null
	protected _action 			: Action | null			= null



	public constructor(
		private rccToastController	: RccToastController,
		private rccAlertController	: RccAlertController,
		private actionService		: ActionService,
		private changeDetectorRef	: ChangeDetectorRef
	){
		super()
	}

	@Input()
	public set action(action: Action | null)  {

		this.notificationSub?.unsubscribe()
		this.notificationValue 	= null
		this._action 			= null

		if(!action) return // This is weird: linting forces me not to put semicolons here, and not to put any other value!

		this._action 			= action

		if(!hasNotification(action)) return // This is weird: linting forces me not to put semicolons here, and not to put any other value!

		this.notificationSub 	= action.notification.subscribe( value => this.notificationValue = value)

	}

	public get action(): Action | null { return this._action }


	@Input()
	public color : HierarchicalColor = null




	public async execute(action: Action): Promise<void> {
		await this.actionService.execute(action)
		this.changeDetectorRef.markForCheck()
	}


	protected hasDescription(action: Action | null) 	: action is Action & WithDescription {
		if(!action) return false
		return 'description' in action
	}

	protected hasChildComponent(action: Action | null) : action is Action & WithChildComponent {
		if(!action) return false
		return 'WithChildComponent' in action
	}

	public ngOnDestroy() : void {
		this.notificationSub?.unsubscribe()
	}

}
