import 	{
			NgModule,
			Type
		}										from '@angular/core'

import	{
			CommonModule
		}										from '@angular/common'

import	{
			IonicModule
		}										from '@ionic/angular'

import	{	IconsModule 					}	from '@rcc/common/icons'
import	{	TranslationsModule 				}	from '@rcc/common/translations'
import	{	UiComponentsModule 				}	from '@rcc/common/ui-components'


/**
 * This file contains all the shared dependencies of the theme components.
 *
 */


const modules : Type<unknown>[]= 	[
							IonicModule,
							IconsModule,
							CommonModule,
							TranslationsModule,
							UiComponentsModule
						]

@NgModule({
	imports: [
		...modules
	],
	exports: [
		...modules
	]
})
export class DefaultThemeCommonModule {}
