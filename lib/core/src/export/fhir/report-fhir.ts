import	{	R4					} 	from '@ahryman40k/ts-fhir-types'
import	{
			Report,
			Question,
			Entry
		}							from '../../items'
import	{	uuidv4				}	from '../../utils'
import	{	hash				}	from '../../encryption'

export 	{	R4					} from '@ahryman40k/ts-fhir-types'

/**
 * Converts a report to FHIR format for export
 * @param report 		report that should be converted to FHIR (= a set of answers)
 * @param questions 	array of questions from the app
 * @param languageCode 	language of the report, e.g. 'en' or 'de'
 * @param respondentId	ID of respondent
 * @returns FHIR QuestionnaireReponse = answers to all questions in the report
 */
export function report2fhir(report: Report, questions: Question[], languageCode: string, respondentId: string) : R4.IQuestionnaireResponse {
	// Construct FHIR Questionnaire Response = answers
	const response		: R4.IQuestionnaireResponse
						=	{
								// defined by FHIR
								'resourceType': 	'QuestionnaireResponse' as const,
								// TODO?! FHIR says 'id for this set of answers',
								// npm package says 'business id assigned to completed questionnaire'
								// 'identifier':	{id: report.id},
								'id':				uuidv4(),
								// Reference to Patient who answered the questions (Probandencode)
								'source':			{
														'type':			'Patient',
														'identifier':	{ 'value': respondentId },
														'display':		'Respondent code'
													},
								// TODO in-progress | completed | amended | entered-in-error | stopped
								'status':			R4.QuestionnaireResponseStatusKind._completed,
								// TODO canonical URI of corresponding questionnaire
								// 'questionnaire':	questionnaire,
								// when report was generated
								'authored':			report.date.toISOString(),
								// answer(s), typeof IQuestionnaireResponse_Item[]
								'item':				report.entries.map(entry =>
														buildQuestionnaireResponseItem(entry, questions, languageCode)
													)
						}

	return response
}

/**
 * Converts a list of questions to FHIR Questionnaire resource for export
 * @param questions array of questions from the app
 * @param date Date object // TODO einfach heutiges Datum?
 * @param languageCode language of the report, e.g. 'en' or 'de'
 * @returns FHIR Questionnaire
 */
export async function questions2fhir(questions: Question[], date: Date, languageCode: string): Promise<R4.IQuestionnaire> {
	// generate Questionnaire ID as hash value of timestamp + language code + question IDs
	const timestamp		: string 		= date.getTime().toString()
	const joinedIds		: string		= questions.map(q => q.id).join('')
	const message		: string 		= timestamp + languageCode + joinedIds
	const id			: string		= await hash(message)

	const questionnaire	: R4.IQuestionnaire
						=	{
								// unique, immutable ID as used in the resource URI
								'id':				id,
								// defined by FHIR
								'resourceType': 	'Questionnaire' as const,
								// draft|active|retired|unknown - for lifecycle tracking
								'status':			R4.QuestionnaireStatusKind._unknown,
								// computer-friendly name
								'name':				'recovery-cat-adhoc-export',
								// human-friendly name
								'title':			'RecoveryCat adhoc Export',
								// markdown description from consumer's perspective
								'description':		'Exported data from the RecoveryCat App',
								// date last changed, time is optional
								'date':				date.toISOString(),
								// publisher contact details, typeof IContactDetail[]
								'contact':			[
														{
															name: 		'RecoveryCat',
															telecom:	[
																			{
																				system: R4.ContactPointSystemKind._email,
																				value:	'recoverycat@posteo.de'
																			},
																			{
																				system: R4.ContactPointSystemKind._other,
																				value:	'https://recoverycat.de'
																			}
																		]
														}
													],
								// questions, typeof IQuestionnaire_Item[]
								// map each question from rcc data structure to FHIR structure
								'item':				questions.map(question =>
														buildQuestionnaireItem(question, languageCode)
													)
							}

	return questionnaire
}

/**
 * Takes an rcc Entry and builds a QuestionnaireReponse_Item from it, i.e. one answer
 * @param {Entry} entry				a single entry = answer
 * @param {Question[]} questions	all questions in the Questionnaire
 * @param {string} languageCode 	TODO
 * @returns the response item
 */
 function buildQuestionnaireResponseItem(entry: Entry, questions: Question[], languageCode: string): R4.IQuestionnaireResponse_Item {
	const question: Question = questions.find(q => q.id === entry.questionId)

	if (!question) {
		console.error(`Could not find question for entry with questionId '${entry.questionId}'`)
		return {}
	}

	const responseItem: R4.IQuestionnaireResponse_Item = {
		'linkId':		entry.questionId,
		'text': 		question.translations[languageCode],
		// TODO valid date also needs seconds. brauchen wir das überhaupt?
		'extension': 	[
							{
								'url': 'http://CHARITESERVER.com/fhir/StructureDefinition/questionnaireresponse-item-date',
								'valueDateTime': entry.date
							}
						],
		// TODO oder hier schon Antworttext rückschließen als valueString ?
		// so ja nicht sehr aussagekräftig + schwer auszulesen wenn properties unterschiedlich sind
		'answer': 		((a=entry.answer) => {
			// other possible answer types:
			// valueDate, valueTime, valueUri, valueAttachment, valueCoding, valueQuantitiy, valueReference
			if(typeof a === 'boolean') 	return [{ 'valueBoolean': a }]
			if(typeof a === 'number' &&
				a % 1 !== 0)				return [{ 'valueDecimal': a }]
			if(typeof a === 'number')	return [{ 'valueInteger': a }]
			return [{ 'valueString': a }]
		})(),
	}

	return responseItem
}

/**
 * Takes an RCC Question and builds a Questionnaire_Item from it, i.e. one question with answer options
 * @param question 		a single question
 * @param languageCode 	TODO
 * @returns the Questionnaire_Item
 */
function buildQuestionnaireItem(question: Question, languageCode: string): R4.IQuestionnaire_Item {
	const questionnaireItem: R4.IQuestionnaire_Item = {
		// unique question id, typeof string
		// used to link to equivalent item in QuestionnaireResponse
		'linkId':			question.id,
		'text':				question.translations[languageCode] || question.meaning,
		'type':				(() => {
								if(question.type === 'boolean') 	return R4.Questionnaire_ItemTypeKind._boolean
								if(question.options)			return R4.Questionnaire_ItemTypeKind._choice
								if(question.type === 'string')	return R4.Questionnaire_ItemTypeKind._string
								if(question.unit)				return R4.Questionnaire_ItemTypeKind._quantity
								if(question.type === 'decimal')	return R4.Questionnaire_ItemTypeKind._decimal
								if(question.type === 'integer')	return R4.Questionnaire_ItemTypeKind._integer

								return R4.Questionnaire_ItemTypeKind._string
							})(),
		// whether this question may repeat
		'repeats':			true,
		// possible answers for choice question types, typeof IQuestionnaire_AnswerOption[]
		'answerOption':		question.options
							?	question.options.map( option => ({
									valueCoding:{
										code: 		String(option.value),
										display:	String(option?.translations?.[languageCode] || option.meaning || option.value)
									}
								}))
							:	undefined
	}

	return questionnaireItem
}
