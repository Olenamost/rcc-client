export abstract class EncryptionHandler {
	public abstract used 	:	{
							toEncrypt: number
							toDecrypt: number
						}

	public abstract encrypt(data: any)			: Promise<string>
	public abstract decrypt(str	: string)		: Promise<any>
}
