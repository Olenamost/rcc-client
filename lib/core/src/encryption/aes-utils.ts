import 	{
			EncryptionHandler
		}								from './encryption.commons'



export function randomString(length: number): string{

	if(!length || length < 0 ) throw new Error('randomString: length must be positive number')

	const a = new Uint8Array(length+2)

	return toBase64(window.crypto.getRandomValues(a)).substr(0,Math.ceil(length))

}


export function toBase64 (b: ArrayBuffer | Uint8Array): string {
	return btoa(String.fromCharCode(...new Uint8Array(b)))
}




export function fromBase64(base64: string) : Uint8Array{
	return Uint8Array.from(atob(base64), c => c.charCodeAt(0))
}

/**
 * Converts an ArrayBuffer or Uint8Array to a hex string
 * @param b Array to convert
 * @returns Hexadecimal string
 */
 export function toHex(b: ArrayBuffer | Uint8Array): string {
    return [...new Uint8Array (b)]
        .map (b => b.toString (16).padStart (2, "0"))
        .join ("");
}

/**
 * Uses Web Crypto's digest() function to generate a hash value of a string message.
 * @param algorithm Name of desired hash function, i.e. 'SHA-256', 'SHA-384' or 'SHA-512'.
 * Defaults to 'SHA-256'
 * @param message Data to encrypt
 * @returns Hash value in hexadecimal characters
 */
export async function hash(message: string, algorithm: string = 'SHA-256'): Promise<string> {
	const messageEncoded 	: ArrayBuffer	= new TextEncoder().encode(message)
	const hash				: ArrayBuffer	= await crypto.subtle.digest(algorithm, messageEncoded)
	return toHex(hash)
}


export async function GcmCreateKey(): Promise<string>{

	const key			= 	await	window.crypto.subtle.generateKey(
										{
											name: 'AES-GCM',
											length: 256
										},
										true,
										['encrypt']
									)

	const u8_key		=	await crypto.subtle.exportKey('raw', key)

	const b64_key		=	toBase64(u8_key)


	return b64_key
}



export async function GcmEncrypt(data:any, b64_key: string): Promise<string> {

	const textEncoder	=	new TextEncoder()
	const encoded_data	= 	textEncoder.encode(JSON.stringify(data))

	const u8_iv 		= 	crypto.getRandomValues(new Uint8Array(12))

	const u8_key		=	fromBase64(b64_key)

	const key			=	await	crypto.subtle.importKey(
										'raw',
										u8_key,
										{
											name: 'AES-GCM',
											length: 256,
										},
										false,
										['encrypt']
									)

	const cipher		= 	await 	crypto.subtle.encrypt(
										{
											name: 'AES-GCM',
											iv: u8_iv
										},
										key,
										encoded_data
									)

	const u8_combined	=	Uint8Array.from([...u8_iv, ...new Uint8Array(cipher)])

	const b64_combined	= 	toBase64(u8_combined)

	return b64_combined
}




export async function GcmDecrypt(b64_combined:string, b64_key:string) : Promise<any> {
	const textDecoder 	= 	new TextDecoder()

	const u8_combined	=	fromBase64(b64_combined)
	const u8_iv			=	u8_combined.slice(0,12)
	const u8_cipher		=	u8_combined.slice(12)
	const u8_key		=	fromBase64(b64_key)


	const key			=	await	crypto.subtle.importKey(
										'raw',
										u8_key,
										{
											name: 'AES-GCM',
											length: 256,
										},
										false,
										['decrypt']
									)

	const decrypted 	= 	await 	crypto.subtle.decrypt(
										{
											name: 'AES-GCM',
											iv: u8_iv
										},
										key,
										u8_cipher
									)


	const data			= 	JSON.parse(textDecoder.decode(decrypted))


	return data
}


export class GcmHandler extends EncryptionHandler {

	public static async generate() : Promise<[string, GcmHandler]>{
		const key  		= await GcmCreateKey()
		const handler 	= new GcmHandler(key)
		return [key,  handler]
	}


	// INSTANCE:

	public used = 	{
						toEncrypt: 0,
						toDecrypt: 0
					}

	public constructor(private key	: string){ super() }


	public async encrypt(data: any) : Promise<string> {

		this.used.toEncrypt ++

		return await GcmEncrypt(data, this.key)
	}

	public async decrypt(combined: string) : Promise<any> {

		this.used.toDecrypt ++

		return await GcmDecrypt(combined, this.key)
	}

}
