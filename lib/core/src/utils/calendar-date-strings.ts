import 	{
			daysAfter					as dateDaysAfter,
			daysBefore					as dateBefore,
			nextSunday					as dateNextSunday,
			previousMonday				as datePreviousMonday,
			range						as dateRange,
			toISO8601CalendarDate,
			today						as dateToday,
			fromISO8601CalendarDate,
		} 							from './date'
import	{
			assert,
			AssertionError,
			isErrorFree
		}							from './utils'

/**
 * Helper class to handle ISO8601 calendar dates.
 *
 * Any Date returned by this class will represent the start of a day in localtime.
 * Any string returned will be an ISO8601 calendar date - i.e. `YYYY-MM-DD`
 */
export class CalendarDateString {

	// #region parsing
	/**
	 * Converts a date into an ISO8601 calendar date string (YYYY-MM-DD).
	 * Note: This is timezone sensitive!
	 */
	public static fromDate(date: Date) : string {
		return toISO8601CalendarDate(date)
	}

	/**
	 * Converts dates into a ISO8601 calendar date strings (YYYY-MM-DD).
	 * Note: This is timezone sensitive!
	 */
	public static fromDates(dates: Date[]) : string[] {
		return dates.map(this.fromDate, this)
	}

	/**
	 * @param 	{string}	dateString 	The ISO8601 string representing a date.
	 * @returns	{Date}		The date represented by the date string.
	 */
	public static toDate(dateString: string): Date {
		return fromISO8601CalendarDate(dateString)
	}

	/**
	 * @param 	{string[]}		dateString 	The ISO8601 strings representing dates.
	 * @returns	{Date[]}		The dates represented by the date strings.
	 */
	public static toDates(dateStrings: string[]): Date[] {
		return dateStrings.map(this.toDate, this)
	}

	/**
	 * Clips any string starting with YYYY-MM-DD to this part. Mainly used to
	 * convert ISO date time strings like ```2022-01-03T05:23:16.000Z00``` to YYYY-MM-DD date
	 * string, ignoring the time.
	 */
	public static clip(dateTimeStr: string) : string {

		const matches = dateTimeStr.match(/^\d{4}-\d{2}-\d{2}/)

		assert(matches, 				'clipDate() unable to extract date string.', dateTimeStr)
		CalendarDateString.assert(matches[0],	'clipDate()', dateTimeStr)

		return matches[0]
	}

	// #endregion

	// #region validation
	/**
	 * Checks whether or not a string conforms to YYYY-DD-MM.
	 */
	public static isValid(x: unknown): x is string {
		return isErrorFree( () => CalendarDateString.parse(x) )
	}

	/**
	 * Throws an error, if provided string is not conforming to YYYY-MM-DD.
	 * Error message can be prefixed with an arbitrary string.
	 */
	public static assert(x: unknown, prefix?: string, context?: unknown) : asserts x is string {

		assert(typeof x === 'string', `${prefix}: DateString.assert() not a string.`, { context, assertion: x })

		try{
			CalendarDateString.parse(x)
		}
		catch(e){
			const cause = e as string | Error
			throw new AssertionError(`${prefix}: DateString.assert() must be YYYY-MM-DD`, { cause, context, assertion: x })
		}

	}

	/**
	 * Parses a date string into year, month and day values. Note: month starts counting at 1!
	 */
	public static parse(x: unknown) : {year:number, month:number, day:number, date: Date} {

		assert(typeof x === 'string', `parse() argument must be a string; got ${typeof x}`, x)

		const matches = x.match(/^(\d{4})-(\d{2})-(\d{2})$/)

		assert(matches, `parse() unable to parse string: "${x}"; expected YYYY-MM-DD.`)

		const date	= new Date(x)
		assert(!isNaN(date.getTime()), `parse() invalid date: "${x}"`) // date in a valid; excludes 2022-13-42


		const year 	= parseInt(matches[1], 10)
		const month = parseInt(matches[2], 10)
		const day	= parseInt(matches[3], 10)

		return { year, month, day, date }
	}
	// #endregion

	// #region date functions

	public static today(): string {
		return this.fromDate(dateToday())
	}

	public static range(start: string | Date, end: string | Date): string[] {
		return this.fromDates(dateRange(start, end))
	}

	public static daysBefore(date: string | Date, daysBefore: number): string {
		return this.fromDate(dateBefore(date, daysBefore))
	}

	public static daysAfter(date: string | Date, daysBefore: number): string {
		return this.fromDate(dateDaysAfter(date, daysBefore))
	}

	public static previousMonday(date: string | Date): string {
		return this.fromDate(datePreviousMonday(date))
	}

	public static nextSunday(date: string | Date): string {
		return this.fromDate(dateNextSunday(date))
	}
	// #endregion
}
