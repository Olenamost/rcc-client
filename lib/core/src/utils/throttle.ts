export class ThrottledCallbackQueue {

	protected callbackQueue : (() => Promise<void>) [] = []

	protected processing = false

	public queue<T>( callback : () => T ) : Promise<T> {

		let resolve : (...args:unknown[]) => unknown = undefined
		let reject	: (...args:unknown[]) => unknown = undefined

		const promise = new Promise<T>( (solve, ject) => { resolve = solve; reject = ject})

		this.callbackQueue.push( async () => {

			let result 	: T			= undefined
			let error	: unknown	= undefined

			try 		{ result 	= await callback() }
			catch(e)	{ error		= e }

			if (error) reject(error)
			else	resolve(result)

		})

		if(!this.processing) this.process()

		return promise

	}

	protected process() : boolean {

		if(this.callbackQueue.length === 0) 	return this.processing = false

		const callback = this.callbackQueue.shift()

		window.requestAnimationFrame(() => {
			callback()
			this.process()
		})

		return this.processing = true
	}

}

export const throttledCallbackQueue = new ThrottledCallbackQueue
