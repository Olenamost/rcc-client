/**
 * This file encapsulates `date-fns` so if we want to replace it we can replace it in one place only.
 *
 * This means apart from this file no other file should import `date-fns`!
 * Instead use a function here and if it does not exist add it.
 *
 */
import 	{
			addDays             as  fnsAddDays,
			eachDayOfInterval   as  fnsEachDayOfInterval,
            formatISO           as  fnsFormatISO,
            getDay              as  fnsGetDay,
			Interval            as  FnsInterval,
            isSameDay           as  fnsIsSameDay,
            nextSunday          as  fnsNextSunday,
            parseISO            as  fnsParseISO,
            previousMonday      as  fnsPreviousMonday,
			subDays             as  fnsSubDays,
            startOfToday        as  fnsStartOfToday,
        }                                   from 'date-fns'
import {    CalendarDateString          }   from './calendar-date-strings'



/**
 * Ensure that date is a Date or a string in valid ISO8601
 */
function ensureDate(date: string | Date): Date {
    return typeof date === 'string' ? CalendarDateString.toDate(date) : date
}

/**
 *
 * @returns	{Date[]}	An Array with dates representing the start of every day between
 *						and including the days startDate and endDate represent
 */
export function range(start: string | Date, end: string | Date): Date[] {
    const startDate : Date  = ensureDate(start)
    const endDate   : Date  = ensureDate(end)

    const interval: FnsInterval = { start: startDate, end: endDate }
    return fnsEachDayOfInterval(interval)
}

/**
 * Gets the number of the day of week of a given date string (YYYY-MM-DD). 0 for Sunday.
 */
export function getDayOfWeek(date: string | Date): number {
    return fnsGetDay(ensureDate(date))
}

/**
 * Add a specific amount of days to a date
 * @param   {string | Date}    date    Date object or string representing a date in ISO8601
 */
export function daysAfter(date: string | Date, amountOfDays: number): Date {
    return fnsAddDays(ensureDate(date), amountOfDays)
}

/**
 * Substracts a specific amount of days from a date
 * @param   {string | Date}    date    Date object or string representing a date in ISO8601
 */

export function daysBefore(date: string | Date, amountOfDay: number): Date {
    return fnsSubDays(ensureDate(date), amountOfDay)
}

/**
 * Transform a date into an ISO8601 calendar date string (YYYY-MM-DD)
 */
export function toISO8601CalendarDate(date: Date): string {
	return fnsFormatISO(date, { representation: 'date' })
}

/**
 * Transform an ISO8601 calendar date string (YYYY-MM-DD) into a Date
 */
 export function fromISO8601CalendarDate(iso8601CalendarDateString: string): Date {
	return fnsParseISO(iso8601CalendarDateString)
}

/**
 * return Date representing start of this day in localtime
 */
export function today(): Date {
    return fnsStartOfToday()
}

/**
 * Get the start of the first Monday previous to given date. (If the given date is a Monday go back one week)
 */
export function previousMonday(date: string | Date): Date {
    return fnsPreviousMonday(ensureDate(date))
}

/**
 * Get the start of the next Sunday after the given date. (If the given date is a Sunday advance one week)
 * @returns {Date} the next sunday after given date
 */
export function nextSunday(date: string | Date): Date {
    return fnsNextSunday(ensureDate(date))
}


export function isSameDay(first: string | Date, second: string | Date) : boolean {
    return fnsIsSameDay(ensureDate(first), ensureDate(second))
}
