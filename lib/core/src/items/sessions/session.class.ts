import	{	Item				}	from '../item.class'
import	{
			SymptomCheck,
		}							from '../symptom-checks'
import	{
			Report,
		}							from '../reports'

import	{
			Question,
		}							from '../questions'

import	{
			assertSessionConfig,
			isSessionConfig,
			SessionConfig,
			defaultSessionConfig
		}							from './sessions.commons'



/**
 * Class representing a single session at a doctor's appointment.
 */
export class Session extends Item<SessionConfig> {

	public symptomCheck		: SymptomCheck
	public report			: Report
	public questions		: Question[]

	public startDate		: Date

	public static acceptsAsConfig(config: unknown): config is SessionConfig {

		console.log(assertSessionConfig)
		console.log(config)

		return isSessionConfig(config)
	}

	public constructor(config: SessionConfig = defaultSessionConfig){
		super(config)

		this.startDate = new Date()
	}

	public set config(config: SessionConfig){

		assertSessionConfig(config)

		this.symptomCheck	= 	config.symptomCheck
								?	new SymptomCheck(config.symptomCheck)
								:	null

		this.report 		= 	config.report
								?	new Report(config.report)
								:	null

		this.questions		=	config.questions.map( questionConfig => new Question(questionConfig) )
	}

	public get config(): SessionConfig {

		return 	{

					symptomCheck: 	this.symptomCheck.config,
					report:			this.report	? this.report.config : null,
					questions:		this.questions.map( question => question.config )

				}

	}
}
