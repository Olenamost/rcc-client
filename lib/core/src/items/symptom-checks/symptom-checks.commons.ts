import	{
			ScheduleConfig,
			isScheduleConfig
		}								from '../schedules'

import	{
			isErrorFree,
			has,
			assert,
			assertProperty
		}								from '../../utils'

export interface SymptomCheckMetaConfig {
	label?				: string			// Some label
	defaultSchedule?	: ScheduleConfig	// default schedule, legacy: rrule
	creationDate?		: string			// Some date format
	paused?				: boolean
	reminder?			: string			// HH:MM
}


export interface QuestionScheduleConfig {
	id:				string 				// Question id
	schedule:		ScheduleConfig
}



export interface SymptomCheckConfig {
	id?			: string
	meta		: SymptomCheckMetaConfig
	questions	: (string | QuestionScheduleConfig)[]
}





// TypeGuards:

export function assertSymptomCheckMetaConfig(x: unknown): asserts x is SymptomCheckMetaConfig {
	assert(typeof x === 'object', 				'assertSymptomCheckMetaConfig() x must be an object.', x)

	if(has(x,'reminder') && x.reminder !== undefined && x.reminder !== null){
		assert(typeof x.reminder === 'string',	'assertSymptomCheckMetaConfig() .reminder must be a string if set.', x.reminder)
		assert(x.reminder.match(/\d\d:\d\d/),	'assertSymptomCheckMetaConfig() .reminder format not matching HH:MM.', x.reminder)
	}

	// TODO other properties
}

export function isQuestionScheduleConfig(x: unknown, throw_errors = false): x is QuestionScheduleConfig {

	if(!throw_errors) return isErrorFree( () => isQuestionScheduleConfig(x, true) )

	assert( has(x, 'id'), 				'isQuestionScheduleConfig(): missing .id on x', x)
	assert( has(x, 'schedule'), 		'isQuestionScheduleConfig(): missing .schedule on x.', x)
	assert( Object.keys(x).length <= 2, 'isQuestionScheduleConfig(): x as extra properties.', x)
	assert( typeof x.id === 'string',	'isQuestionScheduleConfig(): x.id must be a string.', x)
	assert( isScheduleConfig(x.schedule, throw_errors),
										'isQuestionScheduleConfig(): x.schedule must be a ScheduleConfig.', x)

	return true
}


export function assertSymptomCheckConfig(x: unknown): asserts x is SymptomCheckConfig {


	assertProperty(x, 'meta')
	assertProperty(x, 'questions')


	// TODO: that doesn't make much sense, property check meta:
	assertSymptomCheckMetaConfig( x.meta )

	assert( (x.questions instanceof Array),	'assertSymptomCheckConfig(): x.questions must be an Array.', x)

	assert( x.questions.every( (q:unknown) => typeof q === 'string' || isQuestionScheduleConfig(q, !!'throw_errors')),
											'assertSymptomCheckConfig(): every member of x.questions must be a string or a QuestionConfig.', x)

	if(has(x, 'id') && x.id !== undefined && x.id !== null) assert(typeof x.id === 'string', 'assertSymptomCheckConfig(): x.id must be a string.', x)

	// assert( Object.keys(x).length <= 3,		"assertSymptomCheckConfig(): x as extra properties.", x)

}


export function isSymptomCheckConfig(x: unknown) : x is SymptomCheckConfig {
	return isErrorFree( () => assertSymptomCheckConfig(x) )
}


export const defaultSymptomCheckConfig : SymptomCheckConfig = {
	meta:		{},
	questions:	new Array<string>()
}




// TODO:  is SymptomCheckMetaConfig
