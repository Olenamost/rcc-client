import	{	Item				}	from '../item.class'

import	{
			assert,
			isErrorFree
		}							from '../../utils'
import { Question, QuestionOptionConfig } from '../questions'

export type EntryConfig = [
	string,					// Question id
	string|number|boolean,	// Answer
	string,					// DateTimeString WITH Timezone ISO8601, RFC3339. Excluding Z - use +00:00 for timezone instead. Log date.
	string?,				// note
	string?,				// CalendarDate string. YYYY-MM-DD, TargetDay
]

export function assertEntryConfig(x:unknown) : asserts x is EntryConfig {



	assert(	Array.isArray(x), 												'isEntryConfig() config must be an array.')
	assert(	typeof x[0] === 'string',										'isEntryConfig() [0] must be a string.')
	assert( typeof x[2] === 'string',										'isEntryConfig() [2] must be a string.')
	assert( ['string', 'number', 'boolean'].includes(typeof x[1]),			'isEntryConfig() [1] must be a string|number|boolean.')
	assert( ['string', 'undefined'].includes(typeof x[3]) || x[3] === null,	'isEntryConfig() [3] must be a string|null|undefined.')
	assert( ['string', 'undefined'].includes(typeof x[4]) || x[4] === null,	'isEntryConfig() [4] must be a string|null|undefined.')

	const answerDate	= x[2]
	const targetDay 	= x[4] as string|undefined|null

	// TODO: make it DRY use method from CalendarDateString

	if(typeof targetDay === 'string'){
		assert( targetDay.match(/^\d\d\d\d-\d\d-\d\d$/), 	`isEntryConfig() [4] must conform to YYYY-MM-DD format; got '${targetDay}'`,)

		// Test if the string constitutes a valid date:
		const date	= new Date(targetDay)
		assert(!isNaN(date.getTime()), 						`isEntryConfig() [4] does not represent a date; got '${targetDay}'`)
	}

	if(typeof answerDate === 'string'){

		/**
		 * Check if the string roughly conforms to full ISO8601
		 * Do NOT allow 'Z' for UTC timezone
		 * We take Z to be an indicator that some point a date object
		 * stringified defaulting to UTC, distorting time and possibly date.
		 */
		const roughIso = /^\d\d\d\d-\d\d-\d\dT[0-9:.]+[0-9+-:]+$/

		assert( roughIso.test(answerDate),	`isEntryConfig() [2] must conform to ISO8601 WITH timezone. Use +00:00 instead of Z; got '${answerDate}'`)
	}

}

export function isEntryConfig(x:unknown): x is EntryConfig {
	return isErrorFree( () => assertEntryConfig(x) )
}



export class Entry extends Item<EntryConfig> {

	public 	questionId	: string
	public	answer		: string|number|boolean
	public	date		: string
	public	note		: string
	public 	targetDay	: string // YYYY-MM-DD

	public static acceptsAsConfig(config: unknown): config is EntryConfig {
		return isEntryConfig(config)
	}

	public set config(config: EntryConfig){

		assertEntryConfig(config)

		this.questionId = config[0]
		this.answer		= config[1]
		this.date		= config[2]
		this.note		= config[3]
		this.targetDay	= config[4]
	}

	public get config(): EntryConfig{
		return [this.questionId, this.answer, this.date, this.note, this.targetDay]
	}

	// TODO besserer name
	public evaluate(question: Question): QuestionOptionConfig|Entry["answer"] {
		if(question.options) return question.options.find(option => option.value == this.answer)

		return this.answer
	}
}
