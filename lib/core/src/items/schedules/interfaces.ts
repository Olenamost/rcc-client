import	{
			isErrorFree,
			throwError
		}							from '../../utils'

export type ScheduleConfig  = [number[], string[]] // [days of week, times of day], e.g. [[0,6], ['morning', 'afternoon']

export interface ParsedIsoString {
	timezoneOffset:	number, // in hours
	dayOfWeek:		number, // 0-6
	timeOfDay:		string,
	asLocalDate:	Date	// timezoneOffset in string ignored
}


export function isScheduleConfig(x: unknown, flatten_errors = true): x is ScheduleConfig {

	if(flatten_errors) return isErrorFree( () => isScheduleConfig(x) )

	if(!Array.isArray(x)) 		return throwError('isScheduleConfig(): x must be an array.', x)
	if(x.length !== 2)			return throwError('isScheduleConfig(): x must be an array of length 2', x)
	if(!Array.isArray(x[0]) )	return throwError('isScheduleConfig(): x[0] must be an array.', x)
	if(!Array.isArray(x[1]) )	return throwError('isScheduleConfig(): x[1] must be an array.', x)
	if(x[0].length > 6)			return throwError('isScheduleConfig(): x[0] can have at most 6 values.', x)


	const daysOfWeekOK 	=	x[0].every( (i:any) => (typeof i === 'number') && 0 <= i && i <= 6 )

	if(!daysOfWeekOK)			return throwError('isScheduleConfig(): values of x[0] must be numbers bewteen 0 and 6',x)

	const timesOfDayOK = 	x[1].every( (i:any) => (typeof i === 'string') )

	if(!timesOfDayOK)			return throwError('isScheduleConfig(): values of x[1] must be strings', x)


	return true
}
