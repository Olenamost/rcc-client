import	{
			assert
		}									from '../../utils'

import	{
			ScheduleConfig,
			isScheduleConfig,
			ParsedIsoString
		} 									from './interfaces'

import	{
			DayConfig,
			TimeOfDay
		}									from './time-of-day.class'

import	{	Item						}	from '../item.class'




export class Schedule extends Item<ScheduleConfig>{


	// STATIC:

	public 	static acceptsAsConfig 	= 	isScheduleConfig
	public 	static timeOfDay 		= 	new TimeOfDay([
											// Sunday:
											{
												'morning': 		10,
												'afternoon':	13,
												'evening':		18,
												'night':		23
											},
											// weekdays:
											...Array(5).fill({
												'morning': 		8,
												'afternoon':	12,
												'evening':		17,
												'night':		22
											}),
											// Saturday:
											{
												'morning': 		11,
												'afternoon':	14,
												'evening':		19,
												'night':		23
											},
										] as DayConfig[])

	public static assertData(data:unknown): asserts data is ScheduleConfig {
		assert(this.acceptsAsConfig(data), 'Schedule#assertData(): bad data')
	}

	public static parse(str: string) : ParsedIsoString | null {

		if(typeof str !== 'string') return null

		const match = str.match(/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}).*([\+\-Z])(\d{2})?:?(\d{2})?$/i)

		if(!match) return null

		const s		=	match.slice(1)
						.map( x =>	['+','-','Z'].includes(x)
								?	({ '+': 1, '-': -1, 'Z': 0 } as any)[x]
								:	parseInt( x || '0' )
						)

		const local_date = new Date(s[0], s[1]-1, s[2], s[3], s[4], 0, 0)

		if(isNaN(local_date.getTime())) return null

		return 	{
					timezoneOffset:		s[5]*(s[6]+s[7]/60),
					dayOfWeek:			local_date.getDay(),
					timeOfDay:			this.timeOfDay.at(local_date).key,
					asLocalDate: 		local_date
				}


	}

	public static hoursToTimestring(h:number) : string {
		const offsetSign 	= Math.sign(h)
		const offsetAbs		= Math.abs(h)
		const offsetHours 	= Math.floor(offsetAbs)
		const offsetMinutes = Math.floor( (offsetAbs-offsetHours) *60 )

		return 	(offsetSign < 0 ? '-' : '+')
				+	String(offsetHours).padStart(2, '0')
				+	':'
				+	String(offsetMinutes).padStart(2, '0')
	}


	public static getIsoStringWithTimezone(date: Date = new Date()): string {

		return 		String(date.getFullYear())
				+ 	'-'
				+	String(date.getMonth() + 1).padStart(2,'0')
				+	'-'
				+	String(date.getDate()).padStart(2,'0')
				+	'T'
				+	String(date.getHours()).padStart(2,'0')
				+ 	':'
				+	String(date.getMinutes()).padStart(2,'0')
				+	this.hoursToTimestring(-1* date.getTimezoneOffset() / 60)

	}



	// INSTANCE

	// TODO change into Sets

	public daysOfWeek	: number[]
	public timesOfDay	: string[]


	public constructor(config?: ScheduleConfig){

		super(config || [[],[]])

	}

	public set config([daysOfWeek, timesOfDay]: ScheduleConfig){

		if(!Schedule.acceptsAsConfig([daysOfWeek, timesOfDay])) throw new Error ('Schedule.set config(): Invalid Schedule config.')
		this.daysOfWeek = [...daysOfWeek]
		this.timesOfDay = [...timesOfDay]

	}

	public get config() : ScheduleConfig {

		return 	[
					[...this.daysOfWeek],
					[...this.timesOfDay]
				]

	}

	public get everyDay(): boolean {
		return	[0,1,2,3,4,5,6].every( day => this.daysOfWeek.includes(day) )
				||
				[0,1,2,3,4,5,6].every( day => !this.daysOfWeek.includes(day) )
	}

	public get allDay() : boolean {
		return	this.timesOfDay.length === 0
	}

	public matches(x: unknown) : boolean {
		if(x instanceof Date) 		return this.matchesDate(x)
		if(typeof x === 'string')	return this.matchesDate(Schedule.parse(x).asLocalDate)
		if(isScheduleConfig(x))		return this.matchesSchedule(x)
		if(x instanceof Schedule)	return this.matchesSchedule(x)

		return false
	}

	public matchesDate(date: Date = new Date() ): boolean {
		const time_slot 	= Schedule.timeOfDay.at(date)
		const day			= time_slot.startDate.getDay()
		const time_of_day	= time_slot.key

		return 	(this.daysOfWeek.length === 0 || this.daysOfWeek.includes(day))
				&&
				(this.timesOfDay.length === 0 || this.timesOfDay.includes(time_of_day))
	}

	public matchesSchedule(schedule: Schedule | ScheduleConfig) : boolean {

		function matchArrays(array_1 : unknown[], array_2: unknown[]): boolean {

			if(array_1.length !== array_2.length) return false

			const set = new Set([...array_1, ...array_2])

			return set.size === array_1.length
		}

		const daysOfWeek = isScheduleConfig(schedule) ? schedule[0] : schedule.daysOfWeek
		const timesOfDay = isScheduleConfig(schedule) ? schedule[1] : schedule.timesOfDay

		return 	matchArrays(this.daysOfWeek, daysOfWeek)
				&&
				matchArrays(this.timesOfDay, timesOfDay)

	}

}
