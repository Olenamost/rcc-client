import	{
			Item,
			ConfigOf
		}							from './item.class'

import	{
			Subject,
			Subscription,
			merge
		}							from 'rxjs'

import	{
			share
		}							from 'rxjs/operators'


import	{
			assert,
			assertProperty,
			throwError,
		}							from '../utils'

/**
 * Classes conforming to this interface represent means to persist the data of an ItemStore.
 */
export interface ItemStorage<I extends Item = Item, C = ConfigOf<I> > {
	getAll			: () 				=> Promise<C[]>
	store?			: (items: (I|C)[])	=> Promise<void>
}

/**
 * Checks if an input conforms to {@link ItemStorage} interface. Throws an error it doesn't.
 */

export function assertItemStorage(x:unknown ) : asserts x is ItemStorage {
	assertProperty(x, 'getAll')
	assert(typeof x.getAll === 'function', 'assertItemStorage: property .getAll is not a function')

	const y = x as Record<string, unknown>

	if (y.store) assert(typeof y.store === 'function', 'assertItemStorage: property .store is not a function')
}

/**
 * Configuration data for {@link ItemStore}
 */

export interface ItemStoreConfig<I extends Item = Item, C = ConfigOf<I> > {
	itemClass		: new (config: C) => I,
	storage?		: ItemStorage<I>,
	identifyItemBy?	: string | ( (item: I) => string),
}

/**
 * Checks if an input is an {@link ItemStoreConfig}. Throws an error if it isn't
 */
export function assertItemStoreConfig( x: unknown ): asserts x is ItemStoreConfig {

	assertProperty(x, 			'itemClass')
	assertProperty(x.itemClass, 'prototype')
	assert(x.itemClass.prototype instanceof Item, 	'assertItemStoreConfig: property .itemClass does not extend Item')

	const y = x as Record<string, unknown>

	if (y.storage) 			assertItemStorage(y.storage)
	if (y.identifyItemBy)	assert(
									typeof y.identifyItemBy === 'string'
								||	typeof y.identifyItemBy === 'function',
								'assertItemStoreConfig: property .identifyItemBy must be string or function'
							)
}


/**
 * This class represents the default storage.
 * Everything is thrown into the void; aka not stored at all.
 */
export class VoidStorage<I extends Item = Item> implements ItemStorage<I> {
	public async getAll(): Promise<ConfigOf<I>[]> { return await Promise.resolve([] as ConfigOf<I>[]) }
}




export abstract class ItemStore<I extends Item = Item>{

	protected	_map					= new Map<string,I>()

	// This getter/setter pair is here, so that sub classes, can
	// better manipulate the list of items.
	protected	get map() : Map<string,I> 	{ return this._map 	}
	protected	set map(m : Map<string,I>) 	{ this._map = m		}

	protected	updateSubscriptions	= new Map<string, Subscription>()


	public		name			:	string
	public 		itemClass!		: 	new (config:ConfigOf<I>) => I
	protected 	storage!		: 	ItemStorage<I>

	protected 	identifyItem!	:	(item: I) => string


	protected	additionSubject	= 	new Subject<I>()
	protected	removalSubject	= 	new Subject<I>()
	protected	updateSubject	= 	new Subject<I>()

	public		ready			:	Promise<void>

	public		addition$		= 	this.additionSubject.asObservable()
	public		removal$		= 	this.removalSubject.asObservable()
	public		update$			= 	this.updateSubject.asObservable()


	public		change$			= 	merge(this.addition$, this.removal$, this.update$)
									.pipe( share() )




	public constructor( config: ItemStoreConfig<I> ) {

		assertItemStoreConfig(config)

		this.ready = this.init(config)

	}

	protected async init(config: ItemStoreConfig<I>): Promise<void> {

		this.itemClass 			= config.itemClass
		this.storage			= config.storage || new VoidStorage<I>()

		this.identifyItem		= this.getIdentifyItemFn(config.identifyItemBy)


		// TODO: No unsubscribe needed, will run forever anyway, right?
		this.addition$.subscribe(	(item: I) => this.addUpdateSubscription(item) )
		this.removal$.subscribe(	(item: I) => this.removeUpdateSubscription(item) )

		await this.restoreFromStorage()

	}

	protected addUpdateSubscription(item: I) : void  {

		if(!item.update$) return

		const subscription = item.update$.subscribe( () => this.updateSubject.next(item) )

		this.updateSubscriptions.set(item.id,  subscription)
	}

	protected removeUpdateSubscription(item : I) : void {
		const subscription = this.updateSubscriptions.get(item.id)

		if(!subscription) return

		subscription.unsubscribe()

		this.updateSubscriptions.delete(item.id)
	}

	protected getIdentifyItemFn(identifyItemBy: undefined | string | ((item:I) => string) ): (item:I) => string {

		// default:
		if(!identifyItemBy) 						return 	(item:I) => item.id

		// use some property as identifier
		if(typeof identifyItemBy === 'string')		return 	(item:I) => {

																assertProperty(item, identifyItemBy, 'ItemStore.getIdentifyItemFn() property does not exist on I: '+identifyItemBy)

																const key	= identifyItemBy
																const value	= item[key]

																if(typeof value  === 'string') return value

																throwError(`ItemStore.getIdentifyItemFn() property ist not a string: ${key}`, item)

															}
		// use a custom function
		if(typeof identifyItemBy === 'function') 	return 	(item:I) => identifyItemBy(item)

		throw new Error('ItemSource.getIdentifyItemFn: invalid identifyItemConfig')
	}



	protected async restoreFromStorage(): Promise<any>{
		const configs = await this.storage.getAll()

		configs.forEach( (config:ConfigOf<I>) => this.addConfig(config) )
	}



	protected addConfig( config: ConfigOf<I>): I {

		const item  = new this.itemClass(config)

		this.addItem(item)

		return item
	}


	protected addItem(item: I): I {

		const id = this.identifyItem(item)

		const existing_item = this.map.get(id)

		this.map.set(id, item)

		if (existing_item) 	this.updateSubject.next(item)
		else 				this.additionSubject.next(item)

		return item
	}


	protected removeItem(item: I): I {

		const id 		= this.identifyItem(item)

		const storeType = this.constructor.name

		assert(this.map.has(id), `${storeType}.removeItem: item not found, id: ${id}`, { item, store:this })

		this.map.delete(id)

		this.removalSubject.next(item)

		return item
	}



	protected async storeAll(): Promise<void> {
		if(!this.storage.store) throw new Error('ItemStore.storage.store() not implemented: '+ this.constructor.name)

		return this.storage.store(Array.from(this.map.values()))
	}


	public async get(id: 		string)				: Promise<I | null>
	public async get(ids: 		string[])			: Promise< (I | null)[]>
	public async get(id_or_ids: string | string[])	: Promise<I|(I | null)[]> {

		await this.ready

		if(typeof id_or_ids === 'string') return await this.get([id_or_ids]).then( items => items[0] || null)

		const unique_ids	= Array.from(new Set(id_or_ids) )

		const ready_items 	= unique_ids.map(id => this.map.get(id) || null)

		return ready_items
	}


	public get items(): I[]{
		return Array.from(this.map.values())
	}

}
