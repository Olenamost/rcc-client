import	{
			Observable,
			Subject
		}						from 'rxjs'
import	{
			throwError,
			assert
		}						from '../utils'



export type ItemConfig = unknown

export type ConfigOf<I> = 	I extends Item<infer C>
							? 	C extends ItemConfig
								?	C
								:	never
							:	never


export abstract class Item<C extends ItemConfig = unknown> {



	// STATIC:

	public static acceptsAsConfig(x: unknown): x is unknown { // make this a type-guard when extending
		throwError('Item#acceptsAsConfig() please implement this static method on classes extending Item. ', x)
		return false
	}

	public static findConfigs<U>(data:unknown): U[] {		// sadly I cannot use C[] here: https://github.com/microsoft/TypeScript/issues/34665

		if(this.acceptsAsConfig(data))	return [data as U]
		if(Array.isArray(data)) 		return (data as unknown[]).map( (x:unknown) => this.findConfigs<U>(x) ).flat()

		return []
	}

	public static assertData(data:unknown): asserts data is never { // make this a proper assertion when extending
		const className = this.name
		assert(this.acceptsAsConfig(data), `${className}.assertData() failed.`, data)
	}




	// INSTANCE:

	public		id?				: string
	protected 	updateSubject?	: Subject<string>
	public 		update$?		: Observable<string>

	private 	_config?		: C



	public constructor(config: C) {
		this.config = config
	}

	public get config(): C {
		return this._config
	}

	public set config(config: C){

		const itemClass = this.constructor as unknown as { acceptsAsConfig: ((c:unknown) => boolean), name: string }

		itemClass.acceptsAsConfig(config) || throwError(`Invalid ${itemClass.name} config.`)

		this._config = config
	}

	public toJSON(): string { return JSON.stringify(this.config) }

	/**
	 * TODO: Is this useful or needed anywhere?
	 */
	public matches(query:unknown): boolean {
		const regex = new RegExp(String(query), 'gi')
		return !!JSON.stringify(this.config || {}).match(regex)
	}


}
