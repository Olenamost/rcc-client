import	{	QuestionConfig 	}		from './questions.commons'
import	{	assert			}		from '../../utils'


type QuestionValidatorFn = (test:unknown, config: QuestionConfig) => void





class QuestionValidatorClass{

	private	validators:QuestionValidatorFn[] = []

	public register(validator: QuestionValidatorFn):this
	public register(validators: QuestionValidatorFn[]):this
	public register(x: QuestionValidatorFn|QuestionValidatorFn[]):this {
		if (Array.isArray(x)) this.validators.push(...x)
		else this.validators.push(x)

		return this
	}

	// Resolves if answer is valid, rejects with a QuestionValidationError if not
	public async validateAnswer(value:unknown, config: QuestionConfig): Promise<void> {

		await Promise.all(this.validators.map( validator => validator(value, config) ))

		return

	}
}




class QuestionValidationError extends Error {

	public questionTypeConstraints : { message: string, value:unknown, config: QuestionConfig }

	public constructor(message: string, value:unknown, config: QuestionConfig){
		super(message)
		this.questionTypeConstraints = { message, value, config }
	}
}





const QuestionValidator = new QuestionValidatorClass()

export { QuestionValidator, QuestionValidatorFn, QuestionValidationError }







// validators for type = 'string':

function stringChecks(value:unknown, config:QuestionConfig):void{
	if(config.type !== 'string') 	return

	assert(typeof value === 'string',	new QuestionValidationError('NOT_A_STRING', value, config) )
	assert(value !== '',					new QuestionValidationError('EMPTY_STRING', value, config) )

	if(config.options === undefined)	return
	assert(config.options.map( option => option.value).includes(value) , new QuestionValidationError('NOT_AN_OPTION', value, config) )

	return
}


// validators for type = 'integer':

function integerChecks(value:unknown, config:QuestionConfig):void{
	if(config.type !== 'integer') 	return

	assert(typeof value === 'number',	new QuestionValidationError('NOT_AN_INTEGER', value, config))
	assert(Number.isInteger(value), 	new QuestionValidationError('NOT_AN_INTEGER', value, config) )

	assert(config.min === undefined || value >= config.min, new QuestionValidationError('TOO_SMALL', value, config) )
	assert(config.max === undefined || value <= config.max, new QuestionValidationError('TOO_LARGE', value, config) )

	if(config.options === undefined)	return
	assert(config.options.map( option => option.value).includes(value) , new QuestionValidationError('NOT_AN_OPTION', value, config) )

	return
}





// validators for type = 'decimal':

function decimalChecks(value:unknown, config:QuestionConfig):void{
	if(config.type !== 'decimal') 	return

	assert(typeof value === 'number',	new QuestionValidationError('NOT_A_DECIMAL', value, config) )

	assert(config.min === undefined || value >= config.min, new QuestionValidationError('TOO_SMALL', value, config) )
	assert(config.max === undefined || value <= config.max, new QuestionValidationError('TOO_LARGE', value, config) )

	if(config.options === undefined)	return
	assert(config.options.map( option => option.value).includes(value), new QuestionValidationError('NOT_AN_OPTION', value, config) )

	return
}


// validators for type = 'boolean':

function booleanChecks(value:unknown, config:QuestionConfig):void{
	if(config.type !== 'boolean') 	return
	if(typeof value !== 'boolean')	throw new QuestionValidationError('NOT_A_BOOLEAN', value, config)

	return
}




// validators for type = 'unknown':

function unknownChecks(value:unknown, config:QuestionConfig):void{
	if(config.type !== 'unknown') 		return

	throw new QuestionValidationError('TYPE_UNKNOWN', value, config)
}




// validators for type = undefined:

 function undefinedChecks(value:unknown, config:QuestionConfig):void{
	if(config.type !== undefined) 		return

	throw new QuestionValidationError('TYPE_UNDEFINED', value, config)
}



QuestionValidator.register([
	stringChecks,
	integerChecks,
	decimalChecks,
	booleanChecks,
	unknownChecks,
	undefinedChecks
])
