// TODO Plugin for Question Types!

import	{	Item				}		from '../item.class'

import	{
			QuestionConfig,
			QuestionOptionConfig,
			TranslationList,
			isQuestionConfig
		}								from './questions.commons'

import	{
			QuestionValidator
		}								from './question-validator'

import	{
			assert
		}								from '../../utils'

export const QuestionProperties = [
	'id',
	'type',
	'meaning',
	'translations',
	'options',
	'min',
	'max',
	'tags',
	'unit',
] as const

export function unknownConfig(id: string): QuestionConfig {
	return	{
				id: 			id,
				translations: 	{ en: 'Unknown question: #'+id }, // todo use translationService
				type:			'unknown',
				meaning:		'Missing config for question: #'+id
			}
}


export class Question extends Item<QuestionConfig> {



	// INSTANCE



	declare public id			: string

	public type					: string
	public meaning				: string
	public translations			: TranslationList
	public options?				: QuestionOptionConfig[]
	public min					: number
	public max					: number
	public tags					: string[]
	public unit					: string

	public static override acceptsAsConfig(x:unknown): x is QuestionConfig {
		return isQuestionConfig(x)
	}


	public constructor(id:string)
	public constructor(config:QuestionConfig)
	public constructor(idOrConfig: string | QuestionConfig)

	public constructor(idOrConfig: string | QuestionConfig){

		const config	=	typeof idOrConfig === 'string'
							?	unknownConfig(idOrConfig)
							:	idOrConfig

		super(config)

	}

	public override set config(config: QuestionConfig){
		assert(Question.acceptsAsConfig(config), 'Invalid Question config: '+JSON.stringify(config))
		QuestionProperties.forEach( (key) =>
			(this[key] as unknown) = config[key]
		)
	}

	public override get config(): QuestionConfig {
		const c: Record<string, any> = {}

		QuestionProperties.forEach( (key) => c[key] = this[key] )

		return (c as QuestionConfig)
	}

	public async validateAnswer(value:unknown):Promise<void>{
		return QuestionValidator.validateAnswer(value, this.config)
	}




}
