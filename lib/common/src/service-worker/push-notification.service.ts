import	{	Injectable					}	from '@angular/core'

import	{	RccAlertController			}	from '../modals-provider'
import	{	RccSwService				}	from './service-worker.service'



@Injectable()
export class RccPushNotificationService {


	public ready 				: Promise<void>

	public constructor(
		private rccSwService		: 	RccSwService,
		private rccAlertController	: 	RccAlertController,
	){
		this.ready = this.setup()
	}


	protected async setup() : Promise<void> {

		await this.rccSwService.ready

	}


	public async askPermission(): Promise<void> {

		await 	this.ready

		await	this.rccAlertController.present({
					header:		'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.HEADER',
					message: 	'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.MESSAGE',
					buttons:	[
									{ label: 'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.NO', 	rejectAs: 	'user_no'	},
									{ label: 'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.YES', resolveAs: 	'user_yes'	},
								]
				})

	}

	public async getSubscription(serverPublicKey: string): Promise<PushSubscription>{

		await this.ready

		const pushManager 			= this.rccSwService.serviceWorkerRegistration.pushManager

		let subscription			= await pushManager.getSubscription()

		if(subscription) return subscription

		const permission			= await pushManager.permissionState()
		console.log({ permission })

		if(permission === 'prompt')	await this.askPermission()
		if(permission === 'denied')	throw new Error('permission denied')

		const userVisibleOnly		= false
		const applicationServerKey	= serverPublicKey

		subscription 		= await pushManager.subscribe({ userVisibleOnly, applicationServerKey })

		return subscription
	}



}
