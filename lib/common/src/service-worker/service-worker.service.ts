import	{
			Injectable,
			OnDestroy
		}							from '@angular/core'


import	{
			assert,
			getTimeoutPromise
		}							from '@rcc/core'

import	{
			filter,
			from,
			fromEvent,
			map,
			merge,
			Observable,
			share,
			Subject,
		}							from 'rxjs'


@Injectable()
export class RccSwService implements OnDestroy {

	public ready 						: 	Promise<void>
	public serviceWorkerRegistration	: 	ServiceWorkerRegistration | null = null

	public message$						:	Observable<Record<string, unknown>>
	public incomingNotification$		:	Observable<{[index:string]: unknown, data?: Record<string,unknown> }>

	protected messageTimeout			= 	2000
	protected destroy$					=	new Subject<void>()

	// TODO, error handling falls service worker nicht angeht

	public constructor(){
		this.ready = this.setup()
	}

	public get isSupported(): boolean { return 'serviceWorker' in navigator }


	protected async setup(): Promise<void> {

		assert(this.isSupported, 'RccSwService.setup() service workers not supported.')

		const serviceWorkerContainer 	= 	navigator.serviceWorker

		const upcomingMessages			=	fromEvent<MessageEvent>(serviceWorkerContainer,'message')
											.pipe(
												map(
													event 	=>	(typeof event.data === 'object' && event.data !== null)
																?	event.data as Record<string, unknown> // could still be something else, but we do not mind
																:	{ 'Error: event.data was not a Record<string,unknown>, but this': (event.data as unknown) }
												)
											)

		const earlierMesssage			=	new Subject<Record<string, unknown>>()

		this.message$ 					= 	merge(
												earlierMesssage,
												upcomingMessages
											)

		this.incomingNotification$		=	this.message$
											.pipe(
												filter( ({ event }) 			=> event === 'notificationclick'),
												map(	({ notification }) 	=> (notification || {}) as Record<string, unknown>),
												share()
											)



		this.serviceWorkerRegistration 	= 	await serviceWorkerContainer.register('rcc-service-worker.js')

		await serviceWorkerContainer.ready

		from(this.postMessage({ action: 'popLastMessage' }))
		.pipe( filter (	(x) : x is Record<string, unknown> =>	(x !== null && typeof x === 'object') ) )
		.subscribe(earlierMesssage)


	}

	public async postMessage(data: Record<string, unknown>, timeout?: number): Promise<unknown> {

		await this.ready

		const activeServiceWorker 		= 	this.serviceWorkerRegistration.active

		assert(activeServiceWorker, 'RccSwService.postMessage() no active service worker.', this.serviceWorkerRegistration)

		const messageChannel			= 	new MessageChannel()

		let resolve	: (x: unknown) => void	= undefined

		const responsePromise			= 	new Promise<Record<string, unknown>>( solve => resolve = solve)
		const timeoutPromise			=	getTimeoutPromise(timeout || this.messageTimeout, 'RccSwService.postMessage(): timeout; ' + JSON.stringify(data))

		messageChannel.port1.onmessage 	= 	(event) => resolve(event.data)

		activeServiceWorker.postMessage(data, [messageChannel.port2])

		const responseData				= 	await Promise.race([responsePromise, timeoutPromise])
		const error						= 	responseData && responseData.error

		assert(!error, 'RccSwService.postMessage(): error from service worker: '+ String(error), data, error as string|Error)

		messageChannel.port1.close()
		messageChannel.port2.close()

		return responseData
	}


	public ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}

}
