import	{	NgModule					}	from '@angular/core'
import	{ 	ServiceWorkerModule 		}	from '@angular/service-worker'
import	{	provideTranslationMap		}	from '../translations'
import	{	RccSwService				}	from './service-worker.service'
import	{	RccPushNotificationService	}	from './push-notification.service'


import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports:[
		ServiceWorkerModule.register('rcc-service-worker.js', { enabled: true, registrationStrategy:'registerImmediately' })
	],
	providers:[
		provideTranslationMap('SERVICE_WORKER', { en,de }),
		RccSwService,
		RccPushNotificationService,
	]
})
export class RccServiceWorkerModule{}
