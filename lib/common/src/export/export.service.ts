import	{
			Injectable,
			Inject,
			Optional
		}								from '@angular/core'

import	{	RccModalController		}	from '../modals-provider'

import	{	assert					}	from '@rcc/core'

import	{
			ExportResult,
			Exporter,
			EXPORTERS,
			isExportResult
		}								from './export.commons'

import	{	ExportModalComponent	}	from './modal/export-modal.component'


export interface ExportConfig {
	data			: any,
	context?		: any,
	label?			: string,
	description?	: string,
	download?		: boolean
}

@Injectable()
export class ExportService {

	public constructor(
		@Optional()@Inject(EXPORTERS)
		public exporters: 				Exporter<any,any>[],
		public rccModalController:		RccModalController

	){
		this.exporters = exporters || []
	}

	getApplicableExporters(data: unknown, ...context: unknown[]): Exporter<unknown, unknown>[] {
		console.log('this.exporters', this.exporters)
		return this.exporters.filter( exporter => exporter.handles(data, context) )
	}

	isExportable<F=unknown>(data: F, ...context: unknown[]) : boolean {
		return this.getApplicableExporters(data, context).length > 0
	}

	async export(config: ExportConfig ): Promise<ExportResult<unknown>> {

		const exporters = this.getApplicableExporters(config.data)
		const result	= await this.rccModalController.present(ExportModalComponent, { exporters, ...config } )

		assert(isExportResult(result), 'ExportService.export() bad result type.', result)

		return result

	}

}
