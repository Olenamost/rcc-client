import	{	InjectionToken 	}		from '@angular/core'
import	{
			assert,
			assertProperty,
			isErrorFree
		}							from '@rcc/core'

export interface ExportResult<T = unknown> {

	toString: 	() => string
	raw:		() => T     // type of raw data, e.g. QuestionConfig

}

export function isExportResult<T>(x:unknown, throw_errors = false): x is ExportResult<T> {

	if(!throw_errors) return isErrorFree( () => isExportResult(x) )

	assertProperty(x,'toString', 				'isExportResult:')
	assertProperty(x,'raw', 					'isExportResult:')

	assert(typeof x.toString 	=== 'function',	'isExportResult: .toString is not a function.')
	assert(typeof x.raw 		=== 'function',	'isExportResult: .raw is not a function.')
}

export abstract class Exporter<F,T> {

	abstract label:			string
	abstract description:	string

	abstract handles(x:unknown, ...context: unknown[]): x is F
    // nimmt input für den handles => true
    // wrappt in ExportResult, d.h. bringt in anderes format
    // typ F => typ T, zB RecoveryCatQuestions => FHIRQuestions
	abstract export(x:F, ...context: unknown[]): Promise<ExportResult<T>>

}

export const EXPORTERS	= new InjectionToken<Exporter<any, any>>('Exporters')
