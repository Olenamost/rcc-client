import	{	Component			}	from '@angular/core'

import	{
			ExportResult,
			Exporter
		}							from '../export.commons'

import	{	RccModalController	}	from '../../modals-provider'
import 	{	Item				}	from '@rcc/core'



interface ExportData<T> {
	label		: string,
	description	: string,
	filename?	: string,
	dataUrl?	: string,
	get			: () => Promise<ExportResult<T>>
}

@Component({
	templateUrl:	'./export-modal.component.html',
	styleUrls:		['./export-modal.component.scss']
})
export class ExportModalComponent<FROM extends Item<unknown>, TO> {

	public data?			: FROM
	public label?			: string
	public description?		: string

	public context			: unknown[]				= []

	public exports			: ExportData<TO>[]		= []

	private _exporters		: Exporter<FROM,TO>[] 	= []
	private _download		: boolean				= false


	public set exporters(x: Exporter<FROM, TO>[]){
		this._exporters = x
		this.updateExports()
	}

	public set download(x: boolean){
		this._download = x
		this.updateExports()
	}

	public get download(): boolean {
		return this._download
	}

	public constructor(
		public rccModalController:	RccModalController
	){}

	private updateExports():void {

		this.exports 	= 	this._exporters.map( exporter => ({

								get: 			() => exporter.export(this.data, ...this.context),
								label: 			exporter.label,
								description: 	exporter.description

							}) )

		if(!this.download) return

		this._exporters.forEach( (exporter, index) => {

			exporter.export(this.data)
				.then( result => {
					this.exports[index].filename	= 	this.download
														?	'rcc_export.json'
														:	undefined

					this.exports[index].dataUrl		= 	this.download
														?	'data:application/json;base64,'+btoa(result.toString())
														:	undefined
				},
				() => {
					// TODO: handle rejection?
				})
		})
	}


	public async dismiss(exportData: ExportData<TO>): Promise<void> {
		this.rccModalController.dismiss(await exportData.get())
	}

	public cancel(): void {
		this.rccModalController.dismiss()
	}

}
