import	{
			NgModule,
			ModuleWithProviders,
			Type
		}									from '@angular/core'
import	{	provideTranslationMap		}	from '../translations'

import	{	SharedModule				}	from '../shared-module'

import	{	MetaStoreModule				}	from '../meta-store'

import	{
			Exporter,
			EXPORTERS,
		}									from './export.commons'

import	{	ExportService				}	from './export.service'

import	{	ExportModalComponent		}	from './modal/export-modal.component'

import en from './i18n/en.json'
import de from './i18n/de.json'



@NgModule({
	imports: [
		SharedModule,
		MetaStoreModule,
	],
	providers: [
		ExportService,
		provideTranslationMap('EXPORT', { en, de })
	],
	declarations: [
		ExportModalComponent
	]
})
export class ExportModule {


	public static forChild(exporters: Type<Exporter<any,any>>[]): ModuleWithProviders<ExportModule> {

		return	{
					ngModule:	ExportModule,
					providers:	[
									...exporters.map( exporter => ({
										provide: 		EXPORTERS,
										useClass:		exporter,
										multi:			true
									}))
								]
				}
	}
}
