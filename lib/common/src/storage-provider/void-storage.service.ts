import	{	Injectable				}	from '@angular/core'

import	{
			Item,
			ItemStorage,
			ConfigOf
		}								from '@rcc/core'


import	{	RccStorage				}	from './abstract-storage.class'


class VoidStoreService<I extends Item> implements ItemStorage<I> {

	public constructor(){
		console.warn('StorageProviderModule: using fallback voidStoreService; please provide alternative ItemStore.' )
	}

	async getAll() : Promise<ConfigOf<I>[]> { return [] }			// eslint-disable-line

	async storeAll(items: (I|ConfigOf<I>)[]) : Promise<void> {}		// eslint-disable-line
}



@Injectable()
export class RccVoidStorage extends RccStorage{

	public constructor(){
		super()
		throw new Error('StorageProviderModule: RccVoidStorage detected. Please provide alternative Storage service.')
	}

	createItemStorage<I extends Item>(name:string):ItemStorage<I> {
		return new VoidStoreService<I>()
	}

	async clearItemStorage(name: string):Promise<void>{
		return Promise.reject('StorageProviderModule: RccStorage.clearItemStorage(). Please provide alternative Storage service.')
	}

	async getStorageNames():Promise<string[]>{
		return Promise.reject('StorageProviderModule: RccStorage.getStorageNames(). Please provide alternative Storage service.')
	}

}
