import	{
			NgModule,
			Type
		}											from '@angular/core'
import	{	RccIconModule						}	from './icons'
import	{	RccFullPageModule					}	from './full-page'
import	{	RccSliderModule						}	from './slider'
import	{	RccPaginationModule					}	from './pagination'
import	{	RccCalendarModule					}	from './calendar'
import	{	RccClickOutsideModule				}	from './click-outside'
import	{	RccAutofocusModule					}	from './autofocus'
import	{	RccToIDPipeModule					}	from './toID'
import	{	RccInputListModule					}	from './input-list'
import	{	SortActionsPipeModule				}	from './actionSort'

const modules : Type<unknown> [] = [

	RccAutofocusModule,
	RccFullPageModule,
	RccCalendarModule,
	RccClickOutsideModule,
	RccIconModule,
	RccPaginationModule,
	RccSliderModule,
	RccToIDPipeModule,
	SortActionsPipeModule,
	RccInputListModule
]


@NgModule({
	imports: modules,
	exports: modules
})
export class UiComponentsModule{}
