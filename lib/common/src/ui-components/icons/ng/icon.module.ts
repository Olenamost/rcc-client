import	{	NgModule			}	from '@angular/core'
import	{	IonicModule			}	from '@ionic/angular'
import	{	RccIconComponent	}	from './icon.component'
import	{	IconsModule			}	from '../../../icons'


@NgModule({
	imports: [
		IonicModule,
		IconsModule	// legacy
	],
	declarations: [
		RccIconComponent
	],
	exports: [
		RccIconComponent
	]
})
export class RccIconModule{}

