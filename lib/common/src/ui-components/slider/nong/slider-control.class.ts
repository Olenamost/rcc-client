import	{
			BehaviorSubject,
			Subject
		}								from 'rxjs'


export interface SliderConfig {
	/**
	 * Sets whether or not you can keep sliding into one direction. If true when sliding to the next slide at the end
	 * (resp. the previous slide at the start), the slider will slide to the first (resp. last element). Default is false.
	 */
	loopAround?		: boolean,

	/**
	 * Selector string as used in [.querySelectorAll](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll).
	 * Determines which child elements of the container will be treated as slides. Default is all child elements.
	 */
	slidesSelector?	: string,

	snapStop?		: boolean
}


/**
 * This class is going to be the base class for {@link RccSliderComponent}.
 * It wraps a html element and turns it into a horizontal slider.
 * The element itself is considered the container of the slides,
 * and by default any direct child element is considered to be a slide.
 *
 * This class works without any reference to angular.
 *
 * _Important_: In order for the slider to actually work, you have to set some CSS rules yourself.
 * The rules may vary, depending on your use case; here's a working example for full width horizontal slides:
 *
 * Say #my-slider is the id of your HTML-element you want to use SliderControl on; and .slide is a class of your slide elements.
 *
 * ```css
 * 	#my-slider {
 * 		display:					flex;
 * 		flex-direction:				row;
 * 		overflow-x:					scroll;
 * 		scroll-behavior:			smooth;
 * 		scroll-snap-type:			x mandatory;
 * 	}
 *
 * 	.slide {
 * 		scroll-snap-stop:			always;
 * 		scroll-snap-align:			center;
 * 		flex:						0 0 100%;
 * 		height:						100%;
 * 	}
 *
 * ```
 *
 * Note: If your slides are smaller then their container and scroll-snap to one side,
 * it is possible that you won't be able to scroll all the way to the other side,
 * because the last/first slide cannot snap properly. To mitigate you can add
 * a pseudo-element with width 0, add padding or scroll-padding to the container.
 *
 */
export class SliderControl {

	/**
	 * Observable emitting the current slide, whenever it changes
	 * (due to e.g. scrolling).
	 */
	public slideChange$ 		=	new BehaviorSubject<HTMLElement|null>(null)


	/**
	 * Observable emitting after slide elements have changes (added, removed or moved).
	 */
	public setupChange$			=	new Subject<HTMLElement[]>()

	/**
	 * States whether or not the slider is at its start. The slider is considered to be at its start,
	 * if the first slide is fully visible.
	 */
	public atStart				=	true

	/**
	 * States whether or not the slider is at its end. The slider is considered to be at its end,
	 * if the last slide is fully visible.
	 */
	public atEnd				=	true


	/**
	 * Callback called when changes to the slides have been detected.
	 * Called with latest slide / position before the changes took effect.
	 * Expected to return some value, the current slide can be restored from (see. {@link #restoreState}, below)
	 */
	public storeState			: (slide?:HTMLElement, position?: number) => unknown

	/**
	 * Callback called after changes took effect. Called wit the previously stored value. (see, {@link #storeState}, above)
	 */
	public restoreState			: (x:unknown) => void

	public constructor(
		/**
		 * The element to be wrapped and turned into a slider
		 */
		public element	: HTMLElement,
		public config	: SliderConfig = {}
	){

		if(!window) console.error('SliderControl: missing global window; maybe needs different implementation for non-browser contexts.')


		const defaultConfig : SliderConfig = {

			loopAround: 	false,
			slidesSelector:	':scope > *',
			snapStop:		this.element.hasAttribute('snap-stop')

		}

		this.config = Object.assign(config || {}, defaultConfig)


		const scroll_snap_stop_supported = 'scroll-snap-stop' in (document?.body?.style ?? {})
		const scroll_snap_type_supported = 'scroll-snap-type' in (document?.body?.style ?? {})

		if(this.config.snapStop && !scroll_snap_stop_supported) this.polyfillScrollSnapStop()

		// Check if elements are added or removed:
		const mutation_observer = new MutationObserver(	mutationRecords 		=> this.handleMutations(mutationRecords) )
		// Check if elements are resized (e.g. at orientation change)
		const resize_observer	= new ResizeObserver( 	resizeObserverEntries 	=> this.handleResize(resizeObserverEntries) )

		mutation_observer.observe(this.element, { childList:true })

		/**
		 * At the moment only observes resizing of the container element.
		 * It might be useful to also check on slides, but that seems to happen rarely
		 * without the container resizing at the same time. Providing for such a case
		 * seems too be overly complex and not yet worth it.
		 */
		resize_observer.observe(this.element)

		// When scrolling occurs check if current slide changed:
		this.element.addEventListener('scroll', () => this.updateStatus(), { passive:true })

		// Initial Check:
		this.updateStatus()
	}

	protected handleMutations(mutationRecords: MutationRecord[]) : void {

		const addedNodes	=	mutationRecords
								.map( record => Array.from(record.addedNodes))
								.flat()

		const removedNodes	=	mutationRecords
								.map( record => Array.from(record.removedNodes))
								.flat()

		const changedNodes	=	[...addedNodes, ...removedNodes]

		const needsHandling	=	changedNodes.some( node => node.nodeType === 1) // Element

		if(needsHandling) this.handleChanges()
	}

	protected handleResize(resizeObserverEntries: ResizeObserverEntry[]) : void{
		this.handleChanges()
	}



	protected getScrollSnapAlign(slide: HTMLElement): string {
		return window.getComputedStyle(slide).getPropertyValue('scroll-snap-align')
	}


	protected scrollSnapCheck = false

	/**
	 * Workaround for Firefox bug: https://bugzilla.mozilla.org/show_bug.cgi?id=1312165
	 *
	 * scroll-snap-stop not working on Firefox. Remove this when Firefox fixed the bug. (Rather not support older versions)
	 */

	protected polyfillScrollSnapStop() : void {

		console.warn('SliderControl: SnapStop configured, but not supported. Using polyfill.')

		let timeout_id				: number 	= undefined
		let scroll_pos				: number 	= undefined

		let stops					: number[]	= []

		let previous_stop			: number	= undefined
		let current_stop			: number	= undefined
		let next_stop				: number	= undefined

		// let is_touched				= false
		let focus_scroll			= false

		this.element.addEventListener('focus-scroll-start', () => focus_scroll = true)


		const reset = () :void => {

			if(timeout_id) window.clearTimeout(timeout_id)

			timeout_id 			= 	undefined
			scroll_pos 			= 	undefined
			previous_stop		= 	undefined
			current_stop		=	undefined
			next_stop			= 	undefined
			focus_scroll		= 	false

			this.element.style.removeProperty('overflow-x')

		}

		const trackScrolling = ( ...args:unknown[] ) : void  => {


			scroll_pos 			= 	this.element.scrollLeft

			if(typeof timeout_id === 'number')

				window.clearTimeout(timeout_id)

			else {

				stops = this.slides.map( slide => {
							const align = this.getScrollSnapAlign(slide)

							if(align === 'start') 	return slide.offsetLeft
							if(align === 'center')	return slide.offsetLeft + slide.offsetWidth/2 	- this.element.clientWidth/2
							if(align === 'end')		return slide.offsetLeft + slide.offsetWidth 	- this.element.clientWidth

						})
						.filter( x => typeof x === 'number')
						.sort( (a,b) => Math.sign(a-b) )


				current_stop			= stops.find( stop => Math.abs( stop - scroll_pos) <= 10)

				const previous_stops	= stops.filter( stop => (scroll_pos - stop) >  10)
				const upcoming_stops	= stops.filter( stop => (scroll_pos - stop) < -10)

				previous_stop			= previous_stops.pop() 		|| stops[0]
				next_stop				= upcoming_stops.shift()	|| stops[stops.length-1]

			}

			timeout_id	= 	window.setTimeout(reset, 100)

			// Don't interfere if scroll was started programmatically (e.g. by this.focus())
			if(focus_scroll) 	return

			const past_previous	=	previous_stop 	> scroll_pos
			const past_next		=	next_stop		< scroll_pos

			const closest_stop 	= 	[previous_stop, current_stop, next_stop]
									.filter( x => typeof x === 'number')
									.sort( (a,b) => Math.sign(Math.abs(a-scroll_pos) - Math.abs(b-scroll_pos)) )[0]

			const in_vicinity	=	closest_stop === current_stop

			let target 			:	number = undefined



			if(in_vicinity)		return

			if(past_previous || past_next){

				if(past_previous)	target = previous_stop
				if(past_next)		target = next_stop

				this.element.style.setProperty('scroll-behavior', 'auto')
				this.element.style.setProperty('overflow-x', 'hidden')
				this.element.style.removeProperty('overflow-x')
				this.element.scrollTo(target, 0)
				this.element.style.removeProperty('scroll-behavior')

				return

			}

		}


		this.element.addEventListener('scroll', trackScrolling)

	}

	/**
	 * Gets all child elements that are considered to be slides.
	 */
	public get slides(): HTMLElement[] {
		return Array.from(this.element.querySelectorAll(this.config.slidesSelector))
	}

	/**
	 * The currently focused slide. Using this getter will not reevaluate the actual
	 * slide positions, but return the last known focused slide since the last change.
	 */
	public get currentSlide(): HTMLElement {
		return this.slideChange$.getValue()
	}

	/**
	 * The currently focused slide's position. Position _n_ means n/th/ place amoung
	 * the elements considered to be slides as displayed; starting at 0.
	 * (The display order of slides may be different to the order in the DOM tree, due to e.g. flex order)
	 * Using this getter will not reevaluate the actual slide positions,
	 * but return the position of the last known focused slide since the last change.
	 */
	public get currentSlidePosition(): number {

		const ordered_slides	=  this.slides.sort(this.visualOrderSortFn)
		const pos				=  ordered_slides.indexOf(this.currentSlide)

		return pos

	}

	/**
	 * This method is meant to be used by Array.sort() in order to sort slides by visual order,
	 * DOM order might be different (e.g. due to flex order).
	 * Uses .offsetLeft to determine the visual order.
	 */
	protected visualOrderSortFn(this: void, slide1 : HTMLElement, slide2 : HTMLElement): number {

		if(slide1.offsetLeft  > slide2.offsetLeft) return  1
		if(slide1.offsetLeft  < slide2.offsetLeft) return -1

		return  0

	}

	/**
	 * Gets the width of the visible horizontal portion of the slide.
	 * This method is used internally.
	 */
	protected getVisiblePortion(slide : HTMLElement): number {
		const scrollStart		=	this.element.scrollLeft
		const clientWidth		=	this.element.clientWidth
		const scrollEnd			=	scrollStart + clientWidth

		const visible_left		=	Math.max(slide.offsetLeft						, scrollStart)
		const visible_right		=	Math.min(slide.offsetLeft + slide.offsetWidth	, scrollEnd)

		return Math.max(0, visible_right - visible_left)
	}

	/**
	 * Unlike the getter .currentSlide above, this method will actually evaluate the positions
	 * of all the slides and return the current slide.
	 */
	public getCurrentSlide(): HTMLElement {

		const scrollStart		=	this.element.scrollLeft
		const clientWidth		=	this.element.clientWidth
		const scrollEnd			=	scrollStart + clientWidth

		const visible_slides 	= 	this.slides
									// remove slides that are out of view to left
									.filter( slide => slide.offsetLeft + slide.offsetWidth	>= scrollStart)
									// remove slides that are out of view to right
									.filter( slide => slide.offsetLeft 						<= scrollEnd)
									// sort by size of visible horizontal portion
									.sort( (s1, s2) => {

										const visibleH1 = this.getVisiblePortion(s1)
										const visibleH2 = this.getVisiblePortion(s2)

										if( visibleH1  < visibleH2) return  1
										if( visibleH1  > visibleH2) return -1

										return  0
									})

		return visible_slides[0] || null
	}

	/**
	 * Inverse of {@link getCurrentSlide}. Slides the provided slide into view, such that it will be the next current slide.
	 * This method is used internally, for all the sliding that is done programmatically.
	 */
	protected focus(slide: HTMLElement, behavior : string = 'smooth') : void {

		const slide_start 		= slide.offsetLeft
		const slide_width 		= slide.offsetWidth
		const slide_end 		= slide_start + slide_width
		const slide_center 		= slide_start + slide_width / 2

		const container_width	= this.element.clientWidth

		const align				= this.getScrollSnapAlign(slide)

		let destination			= slide_center - container_width / 2 // default case treated as if align was 'center'

		if(align === 'start')	destination = slide_start
		if(align === 'end')		destination = slide_end - container_width


		this.element.dispatchEvent(new Event('focus-scroll-start')) // Workaround for Firefox bug: https://bugzilla.mozilla.org/show_bug.cgi?id=1312165

		if(behavior === 'instant') this.element.style.setProperty('scroll-behavior', 'auto')

		this.element.scroll({ left:destination, behavior: behavior as ScrollBehavior })

		if(behavior === 'instant') this.element.style.removeProperty('scroll-behavior')

	}

	/**
	 * Method to advance slider by _amount_ slides (use a negative number to slide backwards)
	 * starting from either the current slide, a provided slide or slide at given position.
	 * This method is used internally.
	 */
	protected relativeSlide(amount: number, from? : HTMLElement | number, behavior : string = 'smooth'): void {

		const 	slides			=	this.slides

		if(slides.length === 0)	return

		const 	ordered_slides	= 	slides.sort(this.visualOrderSortFn)
		const 	count			=	ordered_slides.length

		let 	from_position	= 	ordered_slides.indexOf(this.currentSlide) || 0

		if(typeof from === 'number')		from_position = (count + from) % count
		if(from instanceof HTMLElement) from_position = ordered_slides.indexOf(from)

		let 	new_position	=	from_position + amount


		new_position 			=	this.config.loopAround
									?	(count + new_position) % count
									:	Math.min( Math.max( 0, new_position), count -1)

		this.focus(ordered_slides[new_position], behavior)

	}

	/**
	 * Advance the slider for _amount_ slides (use negative number to slide backwards).
	 */
	public slideBy(amount: number, behavior : string = 'smooth'): void {

		this.relativeSlide(amount, null, behavior)
	}

	/**
	 * Slide provided slide or slide at given position into view.
	 */
	public slideTo(target: number | HTMLElement, behavior : string = 'smooth') : void {

		if(target instanceof HTMLElement)	this.focus(target)
		if(typeof target === 'number')		this.relativeSlide(0, target, behavior)

	}

	/**
	 * Slide next slide into view.
	 */
	public next()		:void { this.slideBy( 1) }

	/**
	 * Slide previous slide into view.
	 */
	public previous()	:void { this.slideBy(-1) }


	/**
	 * This property is used to throttle reactions to changes in DOM structure or resizing, when they happen frequently.
	 */
	protected changing	: {requestId?: number, slideElement?: HTMLElement, slidePosition? :number, state?: unknown } = {}

	/**
	 * This method is called whenever changes to the setup of elements happen,
	 * that might change the current slide unintentionally or distort the current view.
	 * Ensures that after the change the current slide is still the same and that scroll snap
	 * will be triggered, so that all slides snap properly
	 * (e.g. some browsers will not re-snap on resize).
	 *
	 * This method is throttled, because it might be called very frequently
	 * (e.g. manual resize of the window by dragging the mouse).
	 */
	protected handleChanges(): void {

		if(!this.changing.requestId){

			// Record state when the changes happen
			this.changing.slideElement	= 	this.currentSlide
			this.changing.slidePosition	= 	this.currentSlidePosition
			this.changing.state			= 	typeof this.storeState === 'function'
											?	this.storeState(this.currentSlide,this.currentSlidePosition)
											:	null
		} else

			window.clearTimeout(this.changing.requestId)




		// Ensures that DOM changes (and everything it might trigger) happens at most once per animation frame:
		this.changing.requestId = window.setTimeout( () => {

			const hasCustomRestore 	= typeof this.restoreState === 'function'

			if( hasCustomRestore)	this.restoreState(this.changing.state)

			if(!hasCustomRestore)
									if		(this.slides.includes(this.changing.slideElement))
											this.slideTo(this.changing.slideElement, 'instant')
									else	this.slideTo(this.changing.slidePosition, 'instant')

			this.changing = {}

			if(this.config.snapStop) this.slides.forEach( slide => slide.style.setProperty('scroll-snap-stop', 'always'))

			this.setupChange$.next(this.slides)

			this.updateStatus()

		}, 64)
	}

	/**
	 * This property is used to throttle {@link updateStatus}, which will possibly be called frequently.
	 */
	protected updatingStatus	=	false


	/**
	 * Checks various states, such as currentSlide, atStart and atEnd.
	 *
	 * Checks if a new slide should be considered the current slide,
	 * if so emit that slide with .slideChange$.
	 *
	 * This method is throttled, because it might be called very frequently
	 * (most common case: scrolling)
	 */
	public updateStatus(): void {


		// Check if the component is currently handling DOM changes:
		if(this.changing.requestId) return

		if(this.updatingStatus) 	return

		this.updatingStatus = true

		// Ensures that sliding (and everything it might trigger) happens at most once per animation frame:
		window.requestAnimationFrame( () => {

			const ordered_slides	=  	this.slides.sort(this.visualOrderSortFn)
			const first_slide		=	ordered_slides[0]
			const last_slide		=	ordered_slides[ordered_slides.length-1]

			// Accounting for rounding deviations
			this.atStart 	= !first_slide	|| Math.abs(this.getVisiblePortion(first_slide)	- first_slide.offsetWidth)	< 2
			this.atEnd		= !last_slide	|| Math.abs(this.getVisiblePortion(last_slide)	- last_slide.offsetWidth)	< 2


			const past_slide 		= this.slideChange$.getValue()
			const current_slide		= this.getCurrentSlide()

			if(past_slide !== current_slide) this.slideChange$.next(current_slide)

			this.updatingStatus = false

		})


	}

	/**
	 * Completes all associated Observables.
	 */
	public complete(): void {
		this.slideChange$.complete()
		this.setupChange$.complete()
	}
}
