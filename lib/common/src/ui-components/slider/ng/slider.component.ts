import	{
			Component,
			ElementRef,
			Input,
			ViewEncapsulation,
			OnDestroy,
		} 								from '@angular/core'

import	{
			SliderControl,
		}								from '../nong'


/**
 * Child component of {@link RccSliderComponent}. It does nothing on its own.
 */
@Component({
	selector:		'rcc-slide',
	template: 		'<ng-content></ng-content>',
})
export class RccSlideComponent {

}

/**
 * This Component generates a  mini slide show. Heavy lifting is done by {@link SliderControl} of which this component is an extension of.
 * The component adds some CSS and basic setting.
 */
@Component({
	selector:		'rcc-slider',
	template: 		'<ng-content></ng-content>',
	styleUrls:		['./slider.component.css'],
	encapsulation:	ViewEncapsulation.None
})
export class RccSliderComponent extends SliderControl implements OnDestroy {

	/**
	 * If truthy allows sliding past the last (resp. first) slide programmatically.
	 * Will then slide to the other end of the slider for the next (resp. previous slide).
	 * If falsy, sliding attempts past the last (resp. first) slide will be ignored.
	 */
	@Input()
	public set loopAround(value: boolean){ this.config.loopAround = !!value }

	public constructor(
		protected elementRef:		ElementRef<HTMLElement>
	){

		super(
			elementRef.nativeElement,
			{
				slidesSelector: 	'rcc-slide',
			}
		)
	}

	public ngOnDestroy() : void {
		this.complete()
	}

}
