import	{	NgModule			} from '@angular/core'
import	{	SortActionsPipe		} from './action-sort.pipe'

@NgModule({
	declarations:[
        SortActionsPipe
	],
	exports: [
        SortActionsPipe
	]
})
export class SortActionsPipeModule {

}
