import 	{
			Pipe,
			PipeTransform,
		}								from '@angular/core'

import	{
			biEndSortFn,
			mapSort
		}								from '@rcc/core'

import	{
			Action,
			CustomAction
		}								from '../../actions'


@Pipe({
	name: 'sortActions',
	pure: false
})
export class SortActionsPipe implements PipeTransform {


	private positions 	= new Map<Action|CustomAction, number|null>()

	private lastValue	: (Action|CustomAction)[] = null

	public transform(actions: Action[]): Action[]
	public transform(actions: (Action|CustomAction)[]): (Action|CustomAction)[]
	public transform(actions: (Action|CustomAction)[]): (Action|CustomAction)[] {

		let change_detected = this.lastValue === null

		actions.forEach( action  => {

			const current_position 	= 	typeof action.position === 'function'
										?	action.position()
										:	action.position

			const previous_position	= 	this.positions.get(action)

			if(!this.positions.has(action))				change_detected = true
			if(previous_position !== current_position) 	change_detected = true

			this.positions.set(action, current_position )
		})

		if(change_detected) this.lastValue = 	[...actions]
												.filter( action => this.positions.get(action) !== null)
												.sort( mapSort( (action : Action) => this.positions.get(action), biEndSortFn) )

		return this.lastValue


	}

}
