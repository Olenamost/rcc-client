import	{	NgModule			}	from '@angular/core'
import	{	CommonModule		}	from '@angular/common'
import	{	RouterModule		}	from '@angular/router'
import	{	IonicModule			}	from '@ionic/angular'

import	{
			RccFullPageComponent
		}							from './full-page.component'
import	{	IconsModule			}	from '../../../icons'
import	{	RccIconModule		}	from '../../icons'


@NgModule({
	imports: [
		IonicModule,
		IconsModule, // meh needs renaming!
		RccIconModule,
		CommonModule,
		RouterModule
	],
	declarations: [
		RccFullPageComponent,
	],
	exports: [
		RccFullPageComponent,
	]
})
export class RccFullPageModule {}
