import	{
			Component,
			ViewEncapsulation,
			Output,
			EventEmitter
		}								from '@angular/core'

import	{
			Router
		}								from '@angular/router'

@Component({
	selector:		'rcc-full-page',
	templateUrl:	'./full-page.component.html',
	styleUrls:		['./full-page.component.css'],
	encapsulation:	ViewEncapsulation.None
})
export class RccFullPageComponent {

	@Output()
	public close = new EventEmitter()

	public constructor(
		protected router: Router
	){
		console.log('RccFullPage')
	}

	public get showHomeButton() : boolean { return this.router.url !== '/'}

}
