import	{
				Inject,
				Injectable,
				Optional,
		}							from '@angular/core'

import	{
		}							from '@rcc/core'


import	{
				WidgetControl,
				WidgetComponentType,
				WIDGETS,

		}							from './widgets.commons'



/**
 * This is the central widget handling service.
 */
@Injectable()
export class WidgetsService {

	public constructor(

		/**
		 * Array of all registerd widget components
		 */
		@Inject(WIDGETS)@Optional()
		public widgets: WidgetComponentType[]


	){
		this.widgets = widgets || []
	}


	/**
	 * Returns a sorted array of widget components ordered by matching score; highest comes first.
	 * (See {@link WidgetMatchFn} and {@link WidgetComponent}.)
	 * Includes only those widget components that match the type of control passed and have a matching score >= 0.
	 *
	 */
	public getWidgetMatches<CT extends WidgetControl = WidgetControl>(control: CT): WidgetComponentType<CT>[] {

		function isOfMatchingControlType(widget : WidgetComponentType<unknown>) : widget is WidgetComponentType<CT> {
			return  control instanceof widget.controlType
		}

		const widgetsOfMatchingControlType: WidgetComponentType<CT>[] 	= 	this.widgets.filter( isOfMatchingControlType )

		return	widgetsOfMatchingControlType
				.map(widget	=>	({
					widget,
					match:	widget.widgetMatch
							?	widget.widgetMatch(control)
							:	-1,
				}))
				.filter(({ match })		=> match >= 0)
				.sort(	(item1, item2) 	=> item2.match - item1.match)
				.map(	({ widget })	=> widget)

	}
}
