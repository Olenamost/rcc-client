import	{
			NgModule,
			Provider
		}											from '@angular/core'

import	{	provideRoutes						}	from '@angular/router'

import	{	RccServiceWorkerModule				}	from '../service-worker'
import	{	provideSettingsEntry				}	from '../settings'
import	{	provideTranslationMap				}	from '../translations'
import	{	provideMainMenuEntry				}	from '../main-menu'

import	{	ScheduledNotificationService		}	from './scheduled-notification.service'
import	{	ScheduledNotificationTestComponent	}	from './scheduled-notification-test.component'


import en from './i18n/en.json'
import de from './i18n/de.json'


// const routes		=	[{path: 'test-notifications', component:ScheduledNotificationTestComponent}]
// const menuEntry		=	{
// 							position: 		-1,
// 							icon:			'dev',
// 							label: 			'remove me: Test Notifications',
// 							description: 	'For developers: Test push notifications.',
// 							path: 			'test-notifications'
// 						}

const settingsEntry	=	{
							id:				'notifications-enabled',
							label:			'SCHEDULED_NOTIFICATIONS.SETTINGS.NOTIFICATIONS_ENABLED.LABEL',
							description:	'SCHEDULED_NOTIFICATIONS.SETTINGS.NOTIFICATIONS_ENABLED.DESCRIPTION',
							defaultValue:	true,
							type:			'boolean' as const,
							path:			['notifications'],
							icon:			'notfification'
						}

@NgModule({
	imports:[
		RccServiceWorkerModule
	],
	providers:[
		ScheduledNotificationService,
		// provideRoutes(routes) as Provider, //grr angular; return type was any -.-
		// provideMainMenuEntry(menuEntry),
		provideSettingsEntry(settingsEntry),
		provideTranslationMap('SCHEDULED_NOTIFICATIONS', { en,de })
	]
})
export class ScheduledNotificationModule {}
