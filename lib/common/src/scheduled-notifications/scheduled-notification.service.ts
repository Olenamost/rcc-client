import	{
			Injectable,
			OnDestroy,
			Inject
		}									from '@angular/core'

import	{
			DOCUMENT
		}									from '@angular/common'

import	{	assert						}	from '@rcc/core'

import	{
			concatWith,
			filter,
			fromEvent,
			map,
			Observable,
			Subject,
			takeUntil,
			firstValueFrom,
			from,
			startWith
		}									from 'rxjs'

import	{
			RccPushNotificationService,
			RccSwService
		}									from '../service-worker'

import	{	RccSettingsService			}	from '../settings'

import	{
			isScheduledNotification,
			ScheduledNotification
		}									from './scheduled-notification.commons'



export class TimingError extends Error {

	public name = 'TimingError'

}


@Injectable()
export class ScheduledNotificationService implements OnDestroy {

	protected	vapidPublicKeyUrl		=	'https://push.recoverycat.de/vapidPublicKey'
	protected	remotePushUrl			=	'https://push.recoverycat.de/register'

	protected	destroy$				=	new Subject<void>()

	public ready : Promise<void>



	public constructor(
		@Inject(DOCUMENT)
		private document					: Document,
		private	rccSwService				: RccSwService,
		private rccPushNotificationService	: RccPushNotificationService,
		private rccSettingsService			: RccSettingsService
	){


		this.ready = this.setup()


	}


	protected async setup(): Promise<void> {

		await this.rccSwService.ready

		fromEvent<Event>(this.document, 'visibilitychange')
		.pipe( takeUntil(this.destroy$) )
		.subscribe( () => void this.rccSwService.postMessage({ 'wakeUp': true }) )

		const vapidPublicKeyUrl 	= 	this.vapidPublicKeyUrl
		const remotePushUrl			= 	this.remotePushUrl

		await this.setSWConfig({ vapidPublicKeyUrl, remotePushUrl })

		// Whenever we get a new key use it to renew the push subscription,
		// stored wit hthe service worker.
		this.getVapidPublicKey()
		.pipe( takeUntil(this.destroy$) )
		.subscribe(  key => this.deferredPushSubscriptionUpdate(key) )

		const notificationControl = await this.rccSettingsService.get('notifications-enabled')

		notificationControl.valueChanges
		.pipe( startWith(notificationControl.value) )
		.subscribe(

			(enabled: boolean) =>{
				enabled
				?	void this.enableNotifications()
				:	void this.disableNotifications()

				if(!enabled) return

				void firstValueFrom(this.getVapidPublicKey()).then( key => this.deferredPushSubscriptionUpdate(key) )
		})
	}

	/**
	 * Send configuration object to the service worker and waits for a response.
	 */
	protected async setSWConfig(config: unknown) : Promise<void> {
		await this.rccSwService.postMessage({ action: 'configure', parameters: [config] })
	}




	/**
	 * Fetch vapid public key through service worker.
	 * The service worker will keep a copy for later use.
	 * If the key cannot be fetched from the remote server the cached
	 * value will be returned.
	 */
	public getVapidPublicKey(): Observable<string> {

		// Immediate service worker response:
		const storedKeyPromise	= 	this.rccSwService.postMessage({ action: 'getVapidPublicKey' })
									.catch( e => {
										console.groupCollapsed('ScheduledNotificationService.getVapidPublicKey() unable to get vapid public key on first try, will try again later.')
										console.error(e)
										console.groupEnd()
									})


		const vpkMessageObs		= 	this.rccSwService.message$
									.pipe(
										filter( ({ type }) 	=> type === 'vapid-public-key'),
										map(	({ newKey })	=> newKey as string),
									)

									vpkMessageObs.subscribe(console.info)

		const vpkObs			= 	from(storedKeyPromise)
									.pipe(
										concatWith(vpkMessageObs),
										filter( (key) : key is string => key && typeof key === 'string' )
									)

		return vpkObs
	}



	/**
	 * Get a push subscription and store it with the service worker.
	 * (The service worker needs it to register remote push notifications.)
	 * Will resolve, when the subscrption was sucessfuly stored with the
	 * service worker.
	 */
	public async renewPushSubscription(vapidPublicKey: string): Promise<void>{

		const rawPushSub		= await this.rccPushNotificationService.getSubscription(vapidPublicKey)

		assert(rawPushSub, 'ScheduledNotificationService.renewPushSubscription() unable to get push subscription', rawPushSub)

		const pushSubscription	= JSON.parse(JSON.stringify(rawPushSub)) as unknown

		await this.setSWConfig({ pushSubscription })

	}

	/**
	 * Works like {@link ScheduledNotificationService#renewPushSubscription},
	 * but will not throw an error or return anything.
	 *
	 * This method is meant to be called, in an event handler or observer,
	 * when a vapid public key becomes available.
	 *
	 * Calls of this method and any errors will be logged to the console.
	 */
	public deferredPushSubscriptionUpdate(vapidPublicKey: string) : void {

			console.log('deferredPushSubscriptionUpdate', vapidPublicKey)

			void 	this.renewPushSubscription(vapidPublicKey)
					.then(
						() 	=> {
							console.info('ScheduledNotificationService.deferredPushSubscriptionUpdate() successfully updated push subscription.')
						},
						e 	=> {
							console.groupCollapsed('ScheduledNotificationService.deferredPushSubscriptionUpdate() tried to renew push subscription after key arrival, but failed.')
							console.error(e)
							console.groupEnd()
						}
					)
	}


	/**
	 * Schedules a notification with the service worker. The service worker will
	 * try to trigger the notification at the time given by the .timestamp property
	 * of the notification. It will also try to register a push notification with
	 * the push server in order to wake up the service worker at that time.
	 *
	 * If .timestamp lies in the past, throws a {@link TimingError}.
	 */
	public async schedule(notification: ScheduledNotification) : Promise<unknown> {

		console.info('schedule', notification)

		void firstValueFrom(this.getVapidPublicKey()).then( key => this.deferredPushSubscriptionUpdate(key) )

		const { timestamp }	= notification

		if(timestamp <= Date.now() ) throw new TimingError('ScheduledNotificationService.schedule() cannot schedule for past dates.')

		const responseData 	= await this.rccSwService.postMessage({ action: 'notify', parameters: [notification] })

		return responseData
	}

	/**
	 * Cancels a previously scheduled notification,
	 * referring to it by its key (see {@link schedule}).
	 * (Does not cancel the _push_ notification that was intended to wake up the service worker;
	 * so the service worker will still wake up, but not trigger the notification)
	 */
	public async cancel(query: unknown): Promise<void> {
		await this.rccSwService.postMessage({ action: 'cancelNotification', parameters: [query] })
	}


	/**
	 * Retrieves all notifications currently scheduled with the service worker,
	 * that match the query parameter. A query notification counts as a match
	 * against the query, if all it's properties have exactly the same values as
	 * the corresponding properties on notification.data.
	 *
	 */
	public async getScheduledNotifications(query = {}): Promise<ScheduledNotification[]>{

		const result 	= await this.rccSwService.postMessage({ action:'getQueuedNotifications', parameters: [query] })

		assert(Array.isArray(result) , 'ScheduledNotificationService.getScheduledNotifications() response from service worker is not an Array.')

		assert(result.every( isScheduledNotification ),  'ScheduledNotificationService.getScheduledNotifications() some value is not a ScheduledNotification.', result)

		return result

	}

	/**
	 * Tells the service worker not to try and trigger any notifications.
	 * It can still queue scheduled notifications, in order to trigger
	 * them later, if by then notifications are enabled again.
	 *
	 * This gives the user the ability to stop notifications without revoking
	 * the permission at browser level. (When that happens, it is kind of hard to
	 * have the UI explain to the user how to reenable it, if needed.)
	 */
	public async disableNotifications() : Promise<void> {
		await this.rccSwService.postMessage({ action: 'configure', parameters: [{ pause_notifications: true }] })
	}

	/**
	 * Tells the service worker to try and trigger notifications, when applicable.
	 */
	public async enableNotifications() : Promise<void> {
		await this.rccSwService.postMessage({ action: 'configure', parameters: [{ pause_notifications: false }] })
	}



	ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}

}
