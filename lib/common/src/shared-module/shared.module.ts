import	{ 	NgModule, Type				}	from '@angular/core'
import 	{ 	CommonModule 				}	from '@angular/common'
import	{	ReactiveFormsModule			}	from '@angular/forms'
import 	{ 	IonicModule 				}	from '@ionic/angular'
import	{	RccThemeModule				}	from '@rcc/themes/active'
import	{	ErrorHandlingModule			}	from '../error-handling'
import	{	IconsModule					}	from '../icons'
import	{	ItemModule					}	from '../items'
import	{	MainHeaderModule			}	from '../main-header'
import	{	OverlaysModule				}	from '../overlays'
import	{
			TranslationsModule,
			TranslationSettingsModule
		}									from '../translations'
import	{	UiComponentsModule			}	from '../ui-components'

const 	shared_modules : Type<unknown>[] = [
				RccThemeModule,
				CommonModule,
				IonicModule,
				MainHeaderModule,
				ReactiveFormsModule,
				TranslationsModule,
				TranslationSettingsModule,
				IconsModule, // legacy
				OverlaysModule,
				UiComponentsModule,
				ItemModule,
				ErrorHandlingModule,
		]

@NgModule({
	imports: shared_modules,
	exports: shared_modules,
})
export class SharedModule {}
