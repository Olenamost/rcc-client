import	{	NgModule					}	from '@angular/core'
import	{	RccVersionControlService	}	from './version-control.service'
import	{	DevModule					}	from '../dev'


@NgModule({
	imports: [
		DevModule.note('ServiceWorkerModule, very raw')
	],
	providers :[
		RccVersionControlService,
	]
})
export class ServiceWorkerModule {

	public constructor(){


		(navigator.serviceWorker as any).onerror = function(errorevent:any) {
			  console.log(`received error message: ${errorevent.message}`)
			}

		;(navigator.serviceWorker as any).onerror = console.log
		navigator.serviceWorker.addEventListener('error', e => { console.log(e); throw e })

	}

}
