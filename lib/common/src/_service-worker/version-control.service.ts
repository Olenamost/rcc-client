import	{
			firstValueFrom,
			fromEvent,
			Observable,
			Subject,
			Subscriber,
		}							from 'rxjs'

import	{
			map,
			filter
		}							from 'rxjs/operators'

import	{
			timeoutPromise
		}							from '@rcc/core'

// TODO: check for updates periodically

export class RccVersionControlService {


	protected	updates				= new Subject<any>()

	public		update$				: Observable<ServiceWorker>


	public constructor(){

		this.update$ = 	new Observable<any>( (subscriber: Subscriber<ServiceWorker>) => {

							navigator.serviceWorker.ready
							.then( swReg => swReg.waiting && subscriber.next(swReg.waiting) )

							return this.updates.subscribe(subscriber)

						})

		navigator.serviceWorker.ready
		.then(
			swRegistration	=>	fromEvent(swRegistration, 'updatefound')
								.pipe(map( event => event.target ))
								.subscribe(this.updates)  // todo: when to unsubscribe?
		)

		// todo: is this of relevance here?
		navigator.serviceWorker.addEventListener('controllerchange', console.log)

		this.update$.subscribe( x => console.log('RccVersionControlService.update$', x))
	}

	public async checkForUpdates(): Promise<ServiceWorker|null>{

		const swRegistration = await navigator.serviceWorker.ready

		await swRegistration.update()


		return swRegistration.waiting || null
	}

	public async activateUpdate(): Promise<ServiceWorker|null> {

		const swRegistration 	= 	await navigator.serviceWorker.ready
		const waitingWorker		= 	swRegistration.waiting

		if(!waitingWorker) return null

		const activated 		= 	firstValueFrom(
										fromEvent(waitingWorker, 'statechange')
										.pipe(filter( event => (event.target as any).state === 'activated' ))
									)

		waitingWorker && waitingWorker.postMessage('skipWaiting')

		await Promise.race([activated, timeoutPromise(2000, 'timeout activating service worker') ])

		return waitingWorker
	}

}
