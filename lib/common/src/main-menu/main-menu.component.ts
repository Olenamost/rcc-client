import	{
			Component,
			Inject,
			Optional,
			Type
		}								from '@angular/core'

import	{
			Router,
		} 								from '@angular/router'
import	{	MenuController 			} 	from '@ionic/angular'
import	{	sortByKeyFn 			}	from '@rcc/core'

import	{
			Action,
			CustomAction,
			isPathAction,
			isHandlerAction,
			isCustomAction,
			hasDescription,
			hasNotification
		}								from '../actions'

import	{
			MainMenuEntry,
			MAIN_MENU_ENTRIES,
			MAIN_MENU_CONFIG
		}								from './main-menu.commons'


@Component({
	selector: 		'rcc-main-menu',
	templateUrl: 	'./main-menu.component.html',
	styleUrls: 		['./main-menu.component.scss'],
})
export class MainMenuComponent {

	public components	: Type<unknown>[]
	public actions		: (Action|CustomAction)[]

	public isPathAction 	= isPathAction
	public isHandlerAction 	= isHandlerAction
	public isCustomAction	= isCustomAction
	public hasDescription	= hasDescription
	public hasNotification	= hasNotification

	public constructor(
		@Optional() @Inject(MAIN_MENU_ENTRIES)
		public entries : MainMenuEntry[],

		@Optional() @Inject(MAIN_MENU_CONFIG)
		public		config					: any,

		protected	router					: Router,
		protected	menuController			: MenuController
	){

		this.entries = 	entries || []

		this.actions = 	this.entries
						.map(
							(entry: MainMenuEntry) 	=>	typeof entry === 'function'
														?	{ component: entry }
														:	entry
						)

	}


}
