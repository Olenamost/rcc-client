import	{
			NgModule,
			ModuleWithProviders,
		} 									from '@angular/core'
import	{	RouterModule				}	from '@angular/router'

import	{	CommonModule				}	from '@angular/common'
import 	{ 	IonicModule 				}	from '@ionic/angular'
import	{	TranslationsModule,
			provideTranslationMap
		}									from '../translations'
import	{	IconsModule					}	from '../icons'
import	{	MainHeaderModule			}	from '../main-header'
import	{	NotificationModule			}	from '../notifications'
import	{	UiComponentsModule			}	from '../ui-components'
import	{	MainMenuComponent 			}	from './main-menu.component'
import	{	MainMenuHeaderItemComponent	}	from './header-item/header-item.component'

import	{
			provideMainMenuEntry,
			MainMenuEntry,
			MAIN_MENU_COMPONENT,
			MAIN_MENU_CONFIG
		}									from './main-menu.commons'



import en from './i18n/en.json'
import de from './i18n/de.json'

const mainHeaderConfig = 	[
								{ component: MainMenuHeaderItemComponent, position: 1 }
							]


@NgModule({
	declarations: [
		MainMenuComponent,
		MainMenuHeaderItemComponent
	],

	imports: [
		RouterModule,
		CommonModule,
		IonicModule,
		TranslationsModule,
		IconsModule,
		NotificationModule.forChild(),
		MainHeaderModule.forChild(mainHeaderConfig),
		UiComponentsModule
	],

	exports: [
		MainMenuComponent,
		MainMenuHeaderItemComponent
	],

	providers: [
		provideTranslationMap('MAIN_MENU', { en, de }),
		{ provide: MAIN_MENU_CONFIG, 	useValue: {} },
		{ provide: MAIN_MENU_COMPONENT,	useValue: MainMenuComponent }
	]

})
export class MainMenuModule {
	public static forRoot(config: Record<string, unknown>, entries: MainMenuEntry[] = []): ModuleWithProviders<MainMenuModule> {

		return	{
			ngModule: 	MainMenuModule,
			providers:	[
							...entries.map( provideMainMenuEntry ),
							{ provide: MAIN_MENU_CONFIG, 	useValue:	config },
						]
		}

	}
}
