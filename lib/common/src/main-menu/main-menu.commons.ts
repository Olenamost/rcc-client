import	{
			InjectionToken,
			Type,
			Provider
		} 								from '@angular/core'

import	{
			Action,
			CustomAction
		}								from '../actions'

import	{
			ProductOrFactory,
			getProvider
		}								from '../interfaces'

export type MainMenuEntryComponent = Type<unknown>

export type MainMenuEntry = MainMenuEntryComponent | Action | CustomAction

export const MAIN_MENU_COMPONENT 	= new InjectionToken<Type<unknown>>('Component used as main menu')
export const MAIN_MENU_CONFIG 		= new InjectionToken<unknown>('Configuration object for main menu component')
export const MAIN_MENU_ENTRIES	 	= new InjectionToken<MainMenuEntry[]>('All main menu entries')


export function provideMainMenuEntry(entry: ProductOrFactory<MainMenuEntry>) : Provider{
	return 	getProvider(MAIN_MENU_ENTRIES, entry, true)
}
