import	{
			Component,
			Input,
			Optional,
			OnInit,
			OnDestroy,
			Injector
		} 									from '@angular/core'

import	{	FormControl					}	from '@angular/forms'
import	{	Router						}	from '@angular/router'
import	{	Location					}	from '@angular/common'
import	{	PopoverController 			}	from '@ionic/angular'
import	{	SubscriptionLike			}	from 'rxjs'
import	{	RccToastController			}	from '../../modals-provider'
import	{	MetaStore					}	from '../meta-store.class'
import	{	MetaAction					}	from '../meta-store.commons'





// TODO: abstract popover
@Component({
	templateUrl: './popover.html'
})
export class PopoverComponent {

	public actions?		: MetaAction<any,any,any>[] = []
	public getHandler?	: (action:MetaAction<any,any,any>) => (...args: any[]) => any

	public constructor(
		public popoverController: PopoverController
	){}

}







@Component({
	selector: 		'rcc-meta-store-header',
	templateUrl: 	'./header.component.html',
	styleUrls: 		['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy{

	@Input()
	public metaStore!		: MetaStore<any,any,any>

	@Optional() @Input()
	public filterControl!	: FormControl | null


	public showSearch 		: boolean = false
	public actions			: MetaAction<any,any,any>[] = []

	private subscriptions	: SubscriptionLike[] = []


	public constructor(
		public popoverController	: PopoverController,
		public rccToastController	: RccToastController,
		public router				: Router,
		public location				: Location,
		public injector				: Injector
	) {}


	public ngOnInit(): void{
		this.actions =	this.metaStore.metaActions

		if(this.filterControl) 	this.actions.unshift({
									label:  'META_STORE.ACTIONS.FILTER_ITEMS',
									icon: 	'filter',
									handler: () => this.toggleFilter()
								})

		this.subscriptions.push(
			this.location.subscribe( popstate =>{
				this.showSearch = !!popstate.state.showSearch
				if(!this.showSearch && this.filterControl) this.filterControl.setValue('')
			})
		)
	}

	public toggleFilter(): void{
		this.showSearch = !this.showSearch

		const path	= this.location.path()


		if (this.showSearch)	this.location.go(path, '', { showSearch: true })
		else					this.location.back()

	}

	public getHandler(itemAction: MetaAction<any,any,any>) : () => any {

		if( 'handler' in itemAction )

			return 	() =>	Promise.resolve(itemAction.handler(this.metaStore))
							.then(
								(result: unknown) => {
									itemAction.successMessage
									?	this.rccToastController.success(itemAction.successMessage)
									:	result
								},

								(reason: unknown) => {
									itemAction.failureMessage
									?	this.rccToastController.failure(itemAction.failureMessage)
									:	Promise.reject(reason)
								}

							)


		if( 'path' in itemAction )
			return () => this.router.navigateByUrl(itemAction.path)


		if( 'handlerFactory' in itemAction){
			const deps 			= (itemAction.deps||[]).map( dependency => this.injector.get(dependency) )
			const raw_handler 	= itemAction.handlerFactory(...deps)

			return () => raw_handler(this.metaStore)
		}

		return 	(function(): never {
					throw new Error('HeaderComponent.getHandler() ItemAction must have on of the following properties: handler, path and handlerFactory. '+JSON.stringify(itemAction) )
				})()


	}

	public async showActions(event: Event): Promise<void>{


		const popover = await 	this.popoverController.create({
									component: 		PopoverComponent,
									translucent: 	true,
									componentProps:	{
														actions: 	this.actions,
														getHandler:	(action:any) => this.getHandler(action)
													},
									event
								})


		return await popover.present()
	}

	public ngOnDestroy(): void{
		this.subscriptions.forEach( sub => sub.unsubscribe() )
	}
}
