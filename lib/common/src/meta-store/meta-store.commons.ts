import 	{
			Type,
			InjectionToken
		}							from '@angular/core'

import	{
			ItemConfig,
			Item,
			ItemStore
		}							from '@rcc/core'

import	{
			Action,
		}							from '../actions'

import	{	MetaStore			}	from './meta-store.class'

// TODO: remove
import	{	ItemActionRole		}	from '../items'



export interface ActionBase {
	label			: string,
	role?			: ItemActionRole,
	icon?			: string,
	position?		: number,
	successMessage?	: string,
	failureMessage?	: string,
	store?			: Type<ItemStore>,
}

export interface ActionWithPath extends ActionBase {
	path			: string,			// use ":id" for item id
}

export interface ActionWithHandler<H extends (...args: any[]) => any> extends ActionBase {
	handler			: H
}

export interface ActionWithHandlerFactory<H extends (...args: any[]) => any> extends ActionBase {
	deps			: any[],
	handlerFactory	: (...dependencies: any[]) => H,
}


// TODO update MetaActions, remove ActionX

type ActionX<H extends (...args: any[]) => any> = ActionWithPath | ActionWithHandler<H> | ActionWithHandlerFactory<H>


export type MetaAction<
	C extends ItemConfig,
	I extends Item<C>,
	S extends ItemStore<I>
>									= 	ActionX< (metaStore?: MetaStore<C,I,S>) => any >




export interface MetaStoreConfig<C extends ItemConfig, I extends Item<C> ,S extends ItemStore<I> > {
	itemClass			: Type<I>,
	itemLabelComponent?	: never,
	itemIcon?			: never,
	serviceClass		: Type<MetaStore<C,I,S>>,
}


export const META_STORE_CONFIGS = new InjectionToken<MetaStoreConfig<any,any,any> >('Meta Store Module Configs')
