import  {
			Component,
			Input,
			OnInit,
			OnDestroy,
			Optional,
			Type,
			ChangeDetectionStrategy,
			ChangeDetectorRef,
		}             				from '@angular/core'

import	{	FormControl			}	from '@angular/forms'
import	{
			Subject,
			takeUntil,
			startWith
		}							from 'rxjs'
import	{
			Item,
			ItemStore
		}							from '@rcc/core'


import	{	MetaStore			}	from '../meta-store.class'

@Component({
	selector: 'rcc-meta-store',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './meta-store.component.html',
	styleUrls: ['./meta-store.component.scss'],
})

export class MetaStoreComponent implements OnInit, OnDestroy {

	// @ContentChild(TemplateRef)
	// public itemLabelTemplate!	: TemplateRef<any>;

	@Input()
	public itemClass!			: Type<Item>

	@Input()
	set metaStore(metaStore : MetaStore) 	{ this.setMetaStore(metaStore) }
	get metaStore(): MetaStore				{ return this._metaStore }
	private _metaStore : MetaStore


	@Optional() @Input()
	public filterControl!		: FormControl | null

	@Optional() @Input()
	public set selectInto(items: Item[] | null)  {
		this.selected = items || null
	}

	@Optional() @Input()
	public set filterQuery(query: string){
		this.query = query
	}

	public activeStores			: ItemStore[] = []
	public selected				: Item[] | null  = null

	private query!				: string | null

	protected destroy$			= new Subject<void>()

	public constructor(
		protected changeDetectorRef : ChangeDetectorRef
	){

	}

	public setMetaStore(metaStore : MetaStore) : void {

		this._metaStore = metaStore

		if(!metaStore) return

		metaStore.change$
		.pipe(
			takeUntil(this.destroy$),
			startWith(null)
		)
		.subscribe( () => {
			this.activeStores = this.metaStore.stores?.filter( store => store.items?.length > 0 )
			this.changeDetectorRef.markForCheck()
		})

	}


	public filterItems(items: Item[]): Item[]{

		if(!this.query) return items

		return items.filter( (item:Item<unknown> ) => item.matches(this.query) )

	}


	public toggleSelect(item: Item): void {

		if(! Array.isArray(this.selected) ) return

		const pos = this.selected.indexOf(item)

		pos === -1
		?	this.selected.push(item)
		:	this.selected.splice(pos,1)

	}

	ngOnInit() : void {
		if(this.filterControl)
				this.filterControl.valueChanges
				.pipe( takeUntil(this.destroy$) )
				.subscribe( value => this.query = value )


	}

	ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}

}
