import 	{
			Injectable,
		}								from '@angular/core'

import	{
			MetaStoreService
		}								from './meta-store.service'

import	{
			RccModalController
		}								from '../modals-provider'

import	{
			ItemService
		}								from '../items'


// Workaround to prevent dpendency cycle

@Injectable()
export class ItemsService {

	public constructor(
		public metaStoreService: 	MetaStoreService,
		public rccModalController:	RccModalController,
		public itemService:			ItemService
	){
		console.warn('ItemsService is deprecated.')
	}

}
