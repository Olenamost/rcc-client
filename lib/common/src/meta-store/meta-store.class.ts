import	{	Item,
			ItemConfig,
			ItemStore,
		}							from '@rcc/core'

import	{
			Observable,
			merge
		}							from 'rxjs'

import	{
			share
		}							from 'rxjs/operators'

import	{
			MetaAction
		}							from './meta-store.commons'


export abstract	class MetaStore
				<
					C extends ItemConfig 	= unknown,
					I extends Item<C>		= Item<C>,
					S extends ItemStore<I>	= ItemStore<I>
				> {

	abstract	name 			: string

	public		change$			: Observable<I>


	public constructor(
		public stores			: S[]					= [],
		public metaActions		: MetaAction<C,I,S>[]	= []
	) {

		this.stores 		= stores 		|| []
		this.metaActions	= metaActions	|| []

		this.change$		= 	merge(...this.stores.map( store => store.change$ ))
								.pipe( share() )
	}

	get items() : I[] {

		const array_of_items 	= this.stores.map( store => store.items).flat()
		const unique_items		= new Array<I>()
		const ids				= new Array<string>()

		array_of_items.forEach( item => {
			if(!item.id) 				return unique_items.push(item)
			if(ids.includes(item.id))	return null

			ids.push(item.id)
			unique_items.push(item)
		})

		if(array_of_items.length !== unique_items.length) console.warn('MetaStore.items: duplicate item IDs!' )

		return unique_items
	}

	get ready() : Promise<unknown[]>{

		return 	Promise.all( this.stores.map( store => store.ready) )

	}

	public handleIdWithoutItem(id: string): I | null {
		return null
	}

	public async get (id: string): Promise<I | null>
	public async get (ids: string[]): Promise<( I | null)[]>
	public async get (id_or_ids: string | string []): Promise<(I|null)[]|(I|null)>
	public async get (id_or_ids: string | string []): Promise<(I|null)[]|(I|null)> {

		if(typeof id_or_ids === 'string') return await this.get([id_or_ids]).then( items => items[0])

		const 	getPromises	= 	this.stores.map( store => store.get(id_or_ids) ) // TODO: deal with doubles?
		const 	itemArrays 	= 	await Promise.all(getPromises)  // TODO: deal with undefined!

		const	result 		= 	id_or_ids.map( (id: string, index: number) => itemArrays.reduce( (result:I | null, itemArray: (I | null)[])  => result || itemArray[index], null )
											||
											this.handleIdWithoutItem(id))
								.filter( (item: I | null ) => item !== null )

		return result

	}

	public getStore(item: I) : S | null {
		return 	this.stores.find( (store: S) =>  store.items.includes(item) ) || null
	}


	async filter( callback: (item :I, index?:number, arr?: I[]) => boolean ) : Promise<I[]> {
		await this.ready

		return 	this.items
				.filter(callback)

	}

}
