import	{
			Type,
			ModuleWithProviders
		}							from '@angular/core'
import	{	ItemStore			}	from '@rcc/core'
import	{
			MetaAction,
		}							from './meta-store.commons'
import	{
			getProvider
		}							from '../interfaces'



export class BaseMetaStoreModule {

	static getModuleWithProviders<T extends BaseMetaStoreModule>(
		module		: Type<T>,
		stores		: Type<ItemStore>[],
		metaActions	: MetaAction<any,any,any>[],
		storeToken	: any,
		metaToken	: any
	) : ModuleWithProviders<T> {

		return 	{
					ngModule:	module,
					providers:	[
									...(stores||[]).map( storeClass	=> ({ provide: storeToken,	useExisting: 	storeClass, 	multi:true })),
									...(metaActions||[]).map( x => getProvider(metaToken, 	x, true ) )
								]
				}
	}

}
