import	{
			Optional,
			Inject,
			Injector,
			Type,
			Injectable,
			TemplateRef,
			ComponentFactoryResolver
		}								from '@angular/core'


import	{
			Item,
			ItemStore
		}								from '@rcc/core'

import	{	RccModalController		}	from '../modals-provider'

import	{
			MetaStoreConfig,
			META_STORE_CONFIGS
		}								from './meta-store.commons'

import	{	MetaStore				}	from './meta-store.class'


@Injectable()
export class MetaStoreService {

	public 	metaStoreConfigs	: MetaStoreConfig<any, any, any> []

	private	templateCache		: Map<Type<any>, TemplateRef<any>> = new Map()

	public constructor(
		@Optional() @Inject(META_STORE_CONFIGS)
		metaStoreConfigs				: MetaStoreConfig<any, any, any> [],
		public injector					: Injector,
		public componentFactoryResolver	: ComponentFactoryResolver,
		public rccModalController		: RccModalController,
	){
		this.metaStoreConfigs 			= metaStoreConfigs || []
	}

	getMetaStoreConfig<I extends Item>(item: 		I)			: MetaStoreConfig<any,I,any>
	getMetaStoreConfig<I extends Item>(itemClass:	Type<I>)	: MetaStoreConfig<any,I,any>
	getMetaStoreConfig<I extends Item>(value:		I|Type<I>)	: MetaStoreConfig<any,I,any>
	getMetaStoreConfig<I extends Item>(value:		I|Type<I>)	: MetaStoreConfig<any,I,any> {

		if(value instanceof Item) return this.getMetaStoreConfig(value.constructor as Type<I>)

		const config = this.metaStoreConfigs.find( (config: MetaStoreConfig<any,I,any>) => config.itemClass === value)

		if(!config) throw new Error('MetaStoreService.getMetaStoreConfig(): MetaStoreConfig missing for '+value.name)
		return config
	}

	getMetaStore<I extends Item>(item: 		I)					: MetaStore<any,I,any>
	getMetaStore<I extends Item>(itemClass:	Type<I>)			: MetaStore<any,I,any>
	getMetaStore<I extends Item>(value: 		I | Type<I>)	: MetaStore<any,I,any>
	getMetaStore<I extends Item>(value: 		I | Type<I>)	: MetaStore<any,I,any> {

		const config = this.getMetaStoreConfig(value)
		return this.injector.get(config.serviceClass)

	}

	getStore<I extends Item>(item: I): ItemStore<I> {
		return this.getMetaStore(item).getStore(item)
	}



	// getItemLabelComponent<I extends Item>(item: 		I)			: Type<any> | null
	// getItemLabelComponent<I extends Item>(itemClass: 	Type<I>)	: Type<any> | null
	// getItemLabelComponent<I extends Item>(value: 		I |Type<I>)	: Type<any> | null
	// getItemLabelComponent<I extends Item>(value: 		I |Type<I>)	: Type<any> | null {

	// 	const config = this.getMetaStoreConfig(value)
	// 	return config.itemLabelComponent || null

	// }


	// getItemLabelTemplate<I extends Item>(item: 		I)			: TemplateRef<any> | null
	// getItemLabelTemplate<I extends Item>(itemClass: 	Type<I>)	: TemplateRef<any> | null
	// getItemLabelTemplate<I extends Item>(value: 		I |Type<I>)	: TemplateRef<any> | null
	// getItemLabelTemplate<I extends Item>(value: 		I |Type<I>)	: TemplateRef<any> | null {

	// 	//make sure value is the item class
	// 	if(value instanceof Item) return this.getItemLabelTemplate(value.constructor as Type<I>)

	// 	if(this.templateCache.has(value)) return this.templateCache.get(value) || null

	// 	const itemLabelComponent 	= 	this.getItemLabelComponent(value)

	// 	if(!itemLabelComponent) return null

	// 	const instance				= 	this.componentFactoryResolver
	// 									.resolveComponentFactory(itemLabelComponent)
	// 									.create(this.injector)
	// 									.instance

	// 	this.templateCache.set(value, instance.itemLabelTemplate)

	// 	return this.getItemLabelTemplate(value)

	// }



	async get<I extends Item>(itemClass: Type<I>, id		: string			): Promise<I | null>
	async get<I extends Item>(itemClass: Type<I>, ids		: string[]			): Promise<(I | null)[]>
	async get<I extends Item>(itemClass: Type<I>, id_or_ids : string | string[]	): Promise< I | null | (I|null)[] > {

		return await this.getMetaStore(itemClass).get(id_or_ids)

	}

}
