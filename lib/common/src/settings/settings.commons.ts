import	{
			InjectionToken,
			Provider
		}								from '@angular/core'

import	{
			ProductOrFactory,
			getProvider
		}								from '../interfaces'

import	{
			Representation,
			WithDescription
		}								from '../actions'


export interface SettingsBase<T=unknown> {
	id				: string
	defaultValue	: T | null
	type			: 'select' | 'time' | 'string' | 'handler' | 'boolean'
	path?			: string[]
	options?		: {value: T, label:string}[]
	handler?		: () => void
}

export type SettingsEntry<T=unknown> = SettingsBase<T> & Representation & WithDescription

export const SETTING_CONFIGS = new InjectionToken<SettingsEntry<unknown>[]>('Settings')

export function provideSettingsEntry<T>(config: ProductOrFactory<SettingsEntry<T>>) : Provider {
	return getProvider(SETTING_CONFIGS, config, true)
}
