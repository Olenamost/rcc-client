import	{
			NgModule,
			Provider
		}									from '@angular/core'
import	{	CommonModule				}	from '@angular/common'
import 	{
			provideRoutes,
		}									from '@angular/router'
import	{	ReactiveFormsModule			}	from '@angular/forms'


import	{	provideMainMenuEntry		}	from '../main-menu/main-menu.commons'
import	{	MainHeaderModule			}	from '../main-header'
import	{	TranslationsModule			}	from '../translations/ng/translations.module'
import	{	provideTranslationMap		}	from '../translations/ng/translations.commons'
import	{	IconsModule					}	from '../icons'
import 	{ 	IonicModule 				}	from '@ionic/angular'

import	{	RccToIDPipeModule			} from './../ui-components/toID/toID.module'

import	{	OverviewPageComponent		}	from './overview-page/overview-page.component'
import	{	RccSettingsService			}	from './settings.service'

import en from './i18n/en.json'
import de from './i18n/de.json'



const menuEntry		=	{
							position: 	-1,
							path:		'/settings',
							label:		'SETTINGS.MENU_ENTRY',
							icon:		'settings'
						}

const routes		=	[
							{
								path: 		'settings',
								component:	OverviewPageComponent
							}
						]


@NgModule({
	providers: [

		RccSettingsService,
		provideTranslationMap('SETTINGS', { en, de }),
		provideMainMenuEntry(menuEntry),
		provideRoutes(routes) as Provider // grrr angular missing proper type -.-
	],
	declarations: [
		OverviewPageComponent
	],

	imports: [
		ReactiveFormsModule,
		CommonModule,
		IonicModule,
		TranslationsModule,
		IconsModule,
		MainHeaderModule,
        RccToIDPipeModule
	]
})
export class SettingsModule {}
