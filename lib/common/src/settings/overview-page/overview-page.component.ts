import	{
			Component,
		}								from '@angular/core'

import	{	FormControl				}	from '@angular/forms'
import	{	SettingsEntry			}	from '../settings.commons'
import	{	RccSettingsService		}	from '../settings.service'


@Component({
	selector: 		'rcc-overview-page',
	templateUrl: 	'./overview-page.component.html',
})
export class OverviewPageComponent {

	public settings : {config: SettingsEntry<unknown>, formControl: FormControl<unknown>}[]


	public constructor(
		private rccSettingsService : RccSettingsService
	){
		void this.setup()
	}

	async setup(): Promise<void>{
		await this.rccSettingsService.ready
		this.settings = this.rccSettingsService.settings
	}


}
