import	{
			Injectable,
			Injector,
			Optional,
			OnDestroy
		}								from '@angular/core'

import	{
			FormControl,
		}								from '@angular/forms'

import	{
			Subject,
			takeUntil
		}								from 'rxjs'

import	{
			assert,
			Item,
			ItemStorage,
		}								from '@rcc/core'

import	{
			SettingsEntry,
			SETTING_CONFIGS
		}								from './settings.commons'

import	{
			RccStorage
		}								from '../storage-provider'


export interface SettingValue {
	id: 		string
	value: 		unknown
}


@Injectable()
export class RccSettingsService implements OnDestroy {

	protected destroy$	=	new Subject<void>()
	protected storage	: 	ItemStorage<Item<SettingValue>>

	public settings		: 	{
								formControl	: FormControl<unknown>,
								config		: SettingsEntry<unknown>
							}[] = []

	public ready		:	Promise<void>

	public constructor(
		protected rccStorage	: RccStorage,
		protected injector		: Injector
	) {

		this.ready 		= 	this.setup()

	}

	async setup(): Promise<void> {

		this.storage 	= 	this.rccStorage.createItemStorage<Item<SettingValue>>('rcc-settings')

		const data		=	await this.storage.getAll()

		const entries	=	this.injector.get(SETTING_CONFIGS, [])



		this.settings	=	entries.map( (entry: SettingsEntry<unknown>) => {

								if(entry.type === 'handler') return { formControl: null, config:entry }

								const formControl 	= new FormControl<unknown>(null)

								formControl
								.valueChanges
								.pipe( takeUntil( this.destroy$ ) )
								.subscribe( () => void this.storeAll() )

								return { formControl, config: entry }

							})

		this.settings.forEach( setting => {

			const matchingData	= 	data.find( d =>  d.id === setting.config.id)

			const value 		= 		matchingData
									&& 	matchingData.value !== null
									&& 	matchingData.value !== undefined

									?	matchingData.value
									:	setting.config.defaultValue

			if(setting.formControl) setting.formControl.setValue(value)

		})




	}

	async storeAll() : Promise<void> {

		await this.ready

		const settingValues = this.settings.map( setting => ({ id: setting.config.id, value: setting.formControl?.value }))

		await this.storage.store(settingValues)
	}


	async get(id: string): Promise<FormControl<unknown>>{

		await this.ready

		const settings =	this.settings.find( s => s.config.id === id)

		assert(settings, `SettingsService.get() unknown setting ${id}`)

		return settings.formControl

	}

	async set(id: string, value: unknown) {
		const formControl = await this.get(id)
		formControl.setValue(value)
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}


}
