import	{
			Inject,
			Injectable,
			Optional,
		}							from '@angular/core'

import	{
			Translator,
			TranslationService
		}							from '../nong'

import	{
			TRANSLATORS
		}							from './translations.commons'



// TODO settings

/**
 * This service manages all translation related tasks. For the most part this is only an angular wrapper for {@link TranslationService}.
 *
 * In order to extend its capabilities use {@link provideTranslators} to register
 * custom extensions of {@link Translator}.
 *
 * The most prominent Translator is {@link StringTranslator} that translates message strings by the help of an extentable dictionary.
 * You can use {@link provideTranslationMap} to provide your own messages and corresponding translations.
 *
 */
@Injectable()
export class RccTranslationService extends TranslationService {

	public constructor(

		@Optional() @Inject(TRANSLATORS)
		translators		: Translator[] | null,

	){
		super(translators , 'en')
	}

}






