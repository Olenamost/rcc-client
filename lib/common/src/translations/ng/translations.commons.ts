import	{
			InjectionToken,
			Provider,
			Type
		}							from '@angular/core'

import	{
			Translator,
			ScopedTranslationMap,
			TranslationMap
		}							from '../nong'




/**
 * This injection token is used to store/provide Translators.
 * Try not to use this on it's own, but instead us {@link provideTranslator}.
 */
export const TRANSLATORS = new InjectionToken<Translator[]>('Translators')


/**
 * This function makes a {@link Translator} avaible to {@link RccTranslationService} and {@link RccTranslatePipe}.
 * The {@link Translator} will also be injectable.
 *
 * ```
 * @NgModule({
 * 	providers: [
 * 		provideTranslator(MyTranslator),
 * 		...
 * 	],
 * 	...
 * })
 * ```
 */
export function provideTranslator(translatorClass : Type<Translator>, ...dependencies: unknown[]): Provider {

	return 	{
				provide: 	TRANSLATORS,
				deps:		dependencies,
				useFactory:	(...deps: unknown[]) =>  new translatorClass(...deps),
				multi: 		true
			}

}




export const SCOPED_TRANSLATION_MAPS 	= new InjectionToken<ScopedTranslationMap>('Scoped translation map as needed for StringTranslator')


export function provideTranslationMap(scope: string | null, translationMap: TranslationMap): Provider {

	return 	{
				provide:	SCOPED_TRANSLATION_MAPS,
				useValue:	{ scope, translationMap },
				multi:		true
			}

}





