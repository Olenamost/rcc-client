import	{
			NgModule,
		}								from '@angular/core'

import	{
			filter,
			startWith
		}								from 'rxjs'

import	{
			SettingsModule,
			RccSettingsService,
			provideSettingsEntry
		}								from '../../settings'

import	{	TranslationsModule		}	from './translations.module'
import	{	RccTranslationService	}	from './translations.service'

const settingsEntry =	{
							deps: 		[RccTranslationService],
							factory:	(rccTranslationService: RccTranslationService) => ({
								id:				'activeLanguage',
								label: 			'TRANSLATIONS.SETTINGS.ACTIVE_LANGUAGE.LABEL',
								description:	'TRANSLATIONS.SETTINGS.ACTIVE_LANGUAGE.DESCRIPTION',
								icon:			'language',
								type:			'select' as const,
								defaultValue:	rccTranslationService.defaultLanguage,
								options:		rccTranslationService.availableLanguages
												?	rccTranslationService.availableLanguages
													.map( (lang: string) => ({ value: lang, label: 'TRANSLATIONS.LANGUAGES.'+lang.toUpperCase() }) )
												:	[{ value: 'universal', label: 'TRANSLATIONS.LANGUAGES.UNIVERSAL' }]

							})
						}


/**
 * This Module is neccessary to prevent cyclic dependencies between RccTranslationService and RccSettingsService
 */

@NgModule({
	imports : [
		SettingsModule,
		TranslationsModule
	],
	providers: [
		provideSettingsEntry(settingsEntry)
	]
})
export class TranslationSettingsModule{

	public constructor(
		private rccSettingsService		: RccSettingsService,
		private rccTranslationService	: RccTranslationService
	){
		void this.setup()
	}

	async setup() : Promise<void> {

		const activeLanguageControl = await this.rccSettingsService.get('activeLanguage')

		activeLanguageControl.valueChanges
		.pipe(
			// Make sure the settings restored from the storage take effect emmediatiately.
			startWith(activeLanguageControl.value),
			filter( (x) : x is string => typeof x === 'string')
		)
		.subscribe( activeLanguage => this.rccTranslationService.activeLanguage = activeLanguage)


	}


}
