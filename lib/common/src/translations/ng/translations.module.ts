import 	{
			NgModule,
		} 									from '@angular/core'

import	{
			BooleanTranslator,
			DateTranslator,
			DateStringTranslator,
			TimeStringTranslator,
			MapTranslator,
			NumberTranslator,
			StringTranslator,
			UndefinedTranslator,
			WithMeaningTranslator,
			WithValueTranslator,

		}									from '../nong'

import	{
			RccFillPipe,
			RccTranslatePipe,
		}									from './translations.pipes'

import	{
			provideTranslator,
			provideTranslationMap,
			SCOPED_TRANSLATION_MAPS
		}									from './translations.commons'

import	{
			RccTranslationService,
			// settingsEntry
		}									from './translations.service'

import  {   GlobalTranslationsMapModule }   from './global-translations-map.module'

import en from './i18n/en.json'
import de from './i18n/de.json'

/**
 * This module handles all translation related tasks. It is extendable, see {@link RccTranslationService}.
 * To provide your own string translationse use {@link provideTranlationMap}.
 */

@NgModule({
    imports: [
        GlobalTranslationsMapModule
    ],
	declarations: [
		RccFillPipe,
		RccTranslatePipe
	],
	exports: [
		RccFillPipe,
		RccTranslatePipe
	],
	providers: [
		RccTranslationService,
		provideTranslationMap('TRANSLATIONS', { en, de }),

		provideTranslator(BooleanTranslator),
		provideTranslator(DateTranslator),
		provideTranslator(DateStringTranslator),
		provideTranslator(TimeStringTranslator),
		provideTranslator(MapTranslator),
		provideTranslator(NumberTranslator),
		provideTranslator(StringTranslator, SCOPED_TRANSLATION_MAPS),
		provideTranslator(UndefinedTranslator),
		provideTranslator(WithMeaningTranslator),
		provideTranslator(WithValueTranslator),
	]
})
export class TranslationsModule { }
