export interface TranslationResultFinal {
	final			: string
	intermediate?	: never
}

export interface TranslationResultIntermediate {
	final?			: never
	intermediate	: any
}


export type TranslationResult = TranslationResultFinal | TranslationResultIntermediate | null


export function isFinal(x: TranslationResult): x is TranslationResultFinal {

	if(!x) 							return false
	if(typeof x.final === 'string')	return true

	return false
}

export function isIntermediate(x: TranslationResult): x is TranslationResultIntermediate {
	if(!x)							return false
	if('intermediate' in x)			return true

	return false
}
