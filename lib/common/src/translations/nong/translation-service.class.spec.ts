import { TranslationService, Translator, TranslationResult } from './'

describe('TranslationService', () => {
	it('TranslationService.constructor() should throw an error if translators are missing or empty.', () => {
		expect(() => new TranslationService(undefined)  ).toThrowError()
		expect(() => new TranslationService(null)       ).toThrowError()
		expect(() => new TranslationService([])         ).toThrowError()
	})

	it('TranslationService.constructor() should throw an error if translators don\'t enable any language.', () => {
		class TranslatorWithOnlyOneLanguageX extends Translator {
			compatibleLanguages = ['xx']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		class TranslatorWithOnlyOneLanguageY extends Translator {
			compatibleLanguages = ['yy']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		expect(
			() =>
				new TranslationService([
					new TranslatorWithOnlyOneLanguageX(),
					new TranslatorWithOnlyOneLanguageY(),
				])
		).toThrowError()
	})

	it('TranslationService.availableLanguages should be null if translators have no language restrictions.', () => {
		class TranslatorWithoutLanguageRestriction extends Translator {
			compatibleLanguages: null = null

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		const translationService = new TranslationService([
			new TranslatorWithoutLanguageRestriction(),
		])

		expect(translationService.availableLanguages).toBeNull()
	})

	it('TranslationService.availableLanguages should match the intersection of all translator\'s language restrictions.', () => {
		class TranslatorWithTwoLanguagesXZ extends Translator {
			compatibleLanguages = ['xx', 'zz']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		class TranslatorWithTwoLanguagesYZ extends Translator {
			compatibleLanguages = ['yy', 'zz']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		const translationService = new TranslationService([
			new TranslatorWithTwoLanguagesXZ(),
			new TranslatorWithTwoLanguagesYZ(),
		])

		expect(translationService.availableLanguages).toEqual(['zz'])
	})

	it('TranslationsService.activeLanguage (set) should trigger next on TranslationsService.activeLanguageChange$.', () => {
		class TranslatorWithTwoLanguagesXY extends Translator {
			compatibleLanguages = ['xx', 'yy']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		const translationService = new TranslationService(
			[new TranslatorWithTwoLanguagesXY()],
			'aa'
		)

		const languages: string[] = []

		translationService.activeLanguageChange$.subscribe((lang) =>
			languages.push(lang)
		)

		translationService.activeLanguage = 'abc'

		expect(languages).toEqual(['aa', 'abc'])
	})

	it('TranslationsService.activeLanguage (get) should return the current value of TranslationsService.activeLanguageChange$.', () => {
		class TranslatorWithOnlyLanguagesXY extends Translator {
			compatibleLanguages = ['xx', 'yy']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		const translationService = new TranslationService(
			[new TranslatorWithOnlyLanguagesXY()],
			'aa'
		)

		const languages: string[] = []

		expect(translationService.activeLanguage).toBe('aa')

		translationService.activeLanguageChange$.next('bb')

		expect(translationService.activeLanguage).toBe('bb')
	})

	it('TranslationsService.addTranslatator() should throw an error if translator has no proper compatibleLanguages property', () => {
		class TranslatorWithOnlyOneLanguageX extends Translator {
			compatibleLanguages = ['xx']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		class BadTranslator extends Translator {
			compatibleLanguages: string[] = []

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		const translationService = new TranslationService([
			new TranslatorWithOnlyOneLanguageX(),
		])

		expect(() => translationService.addTranslator(new BadTranslator())).toThrowError()
	})

	it('TranslationsService.addTranslatator() should throw an error if after adding a translator no language is available.', () => {
		class TranslatorWithOnlyOneLanguageX extends Translator {
			compatibleLanguages = ['xx']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		class TranslatorWithOnlyOneLanguageY extends Translator {
			compatibleLanguages = ['yy']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		const translationService = new TranslationService([
			new TranslatorWithOnlyOneLanguageX(),
		])

		expect(() =>
			translationService.addTranslator(
				new TranslatorWithOnlyOneLanguageY()
			)
		).toThrowError()
	})

	it('TranslationService.translate() should use translator with best match for translation', () => {
		class TranslatorWithTwoLanguagesXZ extends Translator {
			compatibleLanguages = ['xx', 'zz']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		class TranslatorWithTwoLanguagesYZ extends Translator {
			compatibleLanguages = ['yy', 'zz']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		class TranslatorWithHighPriority extends Translator {
			compatibleLanguages = ['xx', 'yy', 'zz']

			match(x: unknown) {
				return 10
			}

			translate(x: unknown) {
				return { final: 'high priority' }
			}
		}

		const translationService = new TranslationService([
			new TranslatorWithTwoLanguagesXZ(),
			new TranslatorWithHighPriority(),
			new TranslatorWithTwoLanguagesYZ(),
		])

		expect(translationService.translate('anything')).toBe('high priority')
	})

	it('TranslationService.translate() should try the next best translator when best matching translator return null', () => {
		class TranslatorWithTwoLanguagesXZ extends Translator {
			compatibleLanguages = ['xx', 'zz']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		class TranslatorWithTwoLanguagesYZ extends Translator {
			compatibleLanguages = ['yy', 'zz']

			match(x: unknown) {
				return 1
			}

			translate(x: unknown) {
				return { final: '' }
			}
		}

		class TranslatorWithHighPriority extends Translator {
			compatibleLanguages = ['xx', 'yy', 'zz']

			match(x: unknown) {
				return 10
			}

			translate(x: unknown) {
				return { final: 'high priority after higher priority failed' }
			}
		}

		class TranslatorWithHigherPriority extends Translator {
			compatibleLanguages = ['xx', 'yy', 'zz']

			match(x: unknown) {
				return 20
			}

			translate(x: unknown): TranslationResult | null {
				return null
			}
		}

		const translationService = new TranslationService([
			new TranslatorWithTwoLanguagesXZ(),
			new TranslatorWithHighPriority(),
			new TranslatorWithTwoLanguagesYZ(),
			new TranslatorWithHigherPriority(),
		])

		expect(
			translationService.translate('anything')).toBe(
			'high priority after higher priority failed'
		)
	})

	it('TranslationService.translate() should hand over intermediate values to the next translator', () => {
		let intermediate_called = 0

		class TranslatorWithFinalValue extends Translator {
			compatibleLanguages = ['xx', 'yy', 'zz']

			match(x: unknown) {
				return x === 'intermediate value' ? 20 : 0
			}

			translate(x: unknown) {
				return { final: 'final value' }
			}
		}

		class TranslatorWithIntermediateValue extends Translator {
			compatibleLanguages = ['xx', 'yy', 'zz']

			match(x: unknown) {
				return x === 'intermediate value' ? 0 : 20
			}

			translate(x: unknown) {
				intermediate_called++
				return { intermediate: 'intermediate value' }
			}
		}

		const translationService = new TranslationService([
			new TranslatorWithFinalValue(),
			new TranslatorWithIntermediateValue(),
		])

		expect(translationService.translate('anything')).toBe('final value')
		expect(intermediate_called).toBe(1)
	})
})
