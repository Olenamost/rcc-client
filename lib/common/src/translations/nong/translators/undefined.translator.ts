import	{ 	Translator 			} from '../translator.class'
import	{	TranslationResult	} from '../interfaces'

/**
 * This translator ensures that undefined values don't go unnoticed, when passing through translations.
 **/
export class UndefinedTranslator extends Translator {

	compatibleLanguages : null 	= null  // works with all languages

	match(x: any): number{
		return	x === undefined
				?	0
				:	-1
	}

	translate(value:any, language: string, param?: any): TranslationResult {
		if(this.match(value) === -1 ) return null

		return { intermediate: 'UNDEFINED' }
	}
}
