import { BooleanTranslator } from './boolean.translator'
import { genericTranslatorClassTests } from '../translator.class.spec'

describe('BooleanTranslator', () => {

    let booleanTranslator = new BooleanTranslator

    beforeEach(() => {
        booleanTranslator = new BooleanTranslator()
    })

    genericTranslatorClassTests(BooleanTranslator)

    it('should return -1 if value is not a boolean', () => {

        console.log('Running assertions for BooleanTranslator.match()')

        expect(booleanTranslator.match(true)          ).toBe(0)
        expect(booleanTranslator.match(false)         ).toBe(0)

        expect(booleanTranslator.match(5)             ).toBe(-1)
        expect(booleanTranslator.match('abc')         ).toBe(-1)
        expect(booleanTranslator.match([])            ).toBe(-1)
        expect(booleanTranslator.match({})            ).toBe(-1)
        expect(booleanTranslator.match(new Date())    ).toBe(-1)

    })

    it('should return null if tried on a non-boolean value', () => {

        console.log('Running assertions for BooleanTranslator.translate()')

        expect(booleanTranslator.translate(5)         ).toBe(null)
        expect(booleanTranslator.translate('abc')     ).toBe(null)
        expect(booleanTranslator.translate([])        ).toBe(null)
        expect(booleanTranslator.translate({})        ).toBe(null)
        expect(booleanTranslator.translate(new Date())).toBe(null)

    })

})
