import { DateTranslator } from './'
import { genericTranslatorClassTests } from '../translator.class.spec'


describe('DateTranslator', () => {

    let dateTranslator = new DateTranslator

    beforeEach(() => {
        dateTranslator = new DateTranslator()
    })
    genericTranslatorClassTests(DateTranslator)

	it('DateTranslator.match() should return -1 if argument is not a Date.', () => {

        console.log('Running assertions for DateTranslator.match()')

		expect(dateTranslator.match(new Date()) ).toBe(2)

		expect(dateTranslator.match(true)       ).toBe(-1)
		expect(dateTranslator.match(false)      ).toBe(-1)
		expect(dateTranslator.match(5)          ).toBe(-1)
		expect(dateTranslator.match('abc')      ).toBe(-1)
		expect(dateTranslator.match({})         ).toBe(-1)
		expect(dateTranslator.match([])         ).toBe(-1)
	})

	it('DateTranslator.translate() should return null if tried on a non-date value.', () => {

        console.log('Running 1st assertions for DateTranslator.translate()')

		expect(dateTranslator.translate(true as unknown as Date, 'xx')  ).toBeNull()
		expect(dateTranslator.translate(5 as unknown as Date, 'xx')     ).toBeNull()
		expect(dateTranslator.translate('abc' as unknown as Date, 'xx') ).toBeNull()
		expect(dateTranslator.translate({} as unknown as Date, 'xx')    ).toBeNull()
		expect(dateTranslator.translate([] as unknown as Date, 'xx')    ).toBeNull()
	})

	it('DateTranslator.translate() should return final string value for Date objects.', () => {

		const result = dateTranslator.translate(new Date(), 'xx')

        console.log('Running 2nd assertion for DateTranslator.translate()')

		expect('final' in result)
        expect((typeof (result.final))).toEqual('string')
	})
})
