import {
	isTranslationKey,
	isTranslationTemplate,
	isTranslationMap,
	normalizeTranslationKey,
	extendTranslationKey,
	StringTranslator,
} from './string.translator'

import { genericTranslatorClassTests } from '../translator.class.spec'


// utility functions:

describe('StringTranslator', () => {

    genericTranslatorClassTests(StringTranslator)

    it('isTranslationKey', () => {
		expect(isTranslationKey('abc')                                                 ).toBeFalse()
		expect(isTranslationKey('ABC D')                                               ).toBeFalse()
		expect(isTranslationKey('ABC{{D}}')                                            ).toBeFalse()
		expect(isTranslationKey('1,0524254')                                           ).toBeFalse()
		expect(isTranslationKey('%sABNV')                                              ).toBeFalse()

		expect(isTranslationKey('A.B.C')                                               ).toBeTrue()
		expect(isTranslationKey('AX.BX.CX.DX.EX')                                      ).toBeTrue()
		expect(isTranslationKey('AX.BX1.CX2.DX3.EX4')                                  ).toBeTrue()
		expect(isTranslationKey('AAA')                                                 ).toBeTrue()
		expect(isTranslationKey('AAA_BBB.C___')                                        ).toBeTrue()
		expect(isTranslationKey('AAA_BBB.C___')                                        ).toBeTrue()
		expect(isTranslationKey('A.AA_BBB.C___')                                       ).toBeTrue()
		expect(isTranslationKey('SCOPE.SUBSCOPE.KEY_1')                                ).toBeTrue()
	})

	it('isTranslationTemplate', () => {
		expect(isTranslationTemplate('abc')                                            ).toBeFalse()
		expect(isTranslationTemplate('ABC')                                            ).toBeFalse()
		expect(isTranslationTemplate('ABC.BB')                                         ).toBeFalse()
		expect(isTranslationTemplate('%sABC.BB')                                       ).toBeFalse()
		expect(isTranslationTemplate('1')                                              ).toBeFalse()
		expect(isTranslationTemplate('{')                                              ).toBeFalse()
		expect(isTranslationTemplate('{}}')                                            ).toBeFalse()
		expect(isTranslationTemplate('{{}}')                                           ).toBeFalse()

		expect(isTranslationTemplate('{{X}}')                                          ).toBeTrue()
		expect(isTranslationTemplate('{{ABC}}')                                        ).toBeTrue()
		expect(isTranslationTemplate('Some Text {{ABC}}')                              ).toBeTrue()
		expect(isTranslationTemplate('Some Text {{ABC}} after')                        ).toBeTrue()
		expect(isTranslationTemplate('Some Text {{SCOPE.ABC.SOME_KEY_1}} after')       ).toBeTrue()
	})

	it('normalizeTranslationKey should always return a translation key.', () => {
		expect(isTranslationKey(normalizeTranslationKey('xxx'))                        ).toBeTrue()
        expect(isTranslationKey(normalizeTranslationKey('xx.a'))                 ).toBeTrue()
		expect(isTranslationKey(normalizeTranslationKey('SCOPE.%s.xxx'))               ).toBeTrue()
		expect(isTranslationKey(normalizeTranslationKey('xxx{{abc}} 89098'))           ).toBeTrue()
		expect(isTranslationKey(normalizeTranslationKey('xxx{{abc}}_'))                ).toBeTrue()
		expect(isTranslationKey(normalizeTranslationKey('scope.subscope.some_key1'))   ).toBeTrue()
		expect(isTranslationKey(normalizeTranslationKey('{{}}'))                       ).toBeTrue()

		expect(normalizeTranslationKey('')                                             ).toBe('_EMPTY_KEY')
		expect(normalizeTranslationKey('{{}}')                                         ).toBe('_EMPTY_KEY')
		expect(normalizeTranslationKey('%  ')                                          ).toBe('_EMPTY_KEY')
		expect(normalizeTranslationKey('{{{{')                                         ).toBe('_EMPTY_KEY')
		expect(normalizeTranslationKey('/')                                            ).toBe('_EMPTY_KEY')
		expect(normalizeTranslationKey('*')                                            ).toBe('_EMPTY_KEY')
	})

	it('extendTranslationKey ', () => {
		expect(extendTranslationKey('%s', 'MY_KEY')                                    ).toBe('MY_KEY')
		expect(extendTranslationKey('%s', 'my_key')                                    ).toBe('MY_KEY')
		expect(extendTranslationKey('%s%s', 'my_key', 'my_second_key')                 ).toBe('MY_KEYMY_SECOND_KEY')
		expect(extendTranslationKey('%s%t', 'my_key', 'my_second_key')                 ).toBe('MY_KEYMY_SECOND_KEY')
		expect(extendTranslationKey('%t%s', 'my_key', 'my_second_key')                 ).toBe('MY_KEYMY_SECOND_KEY')
		expect(extendTranslationKey('%s.ABC.D_%t.K_%s', 'SCOPE', 1)                    ).toBe('SCOPE.ABC.D_1.K_')

		expect(extendTranslationKey('%1.%2.%3', 'one', 'two', 'three')                 ).toBe('ONE.TWO.THREE')
		expect(extendTranslationKey('%3.%2.%1', 'one', 'two', 'three')                 ).toBe('THREE.TWO.ONE')

		expect(extendTranslationKey('SCOPE_%1.SUBSCOPE_%1', 'one', 'two')              ).toBe('SCOPE_ONE.SUBSCOPE_ONE')
		expect(extendTranslationKey('SCOPE_%2.SUBSCOPE_%1', 'one', 'two')              ).toBe('SCOPE_TWO.SUBSCOPE_ONE')

		expect(extendTranslationKey('SCOPE_%2.SUBSCOPE_%1', 1, 2)                      ).toBe('SCOPE_2.SUBSCOPE_1')

		expect(extendTranslationKey('SCOPE_%s.sub_SCOPE_%1_%3.key_%2', 'ITEMS', 2, 'B')).toBe('SCOPE_ITEMS.SUB_SCOPE_ITEMS_B.KEY_2')})

	// tramslator class:

	it('StringTranslator.compatibleLanguages() should be null, if no translation maps were provided', () => {
		const stringTranslator = new StringTranslator([])

		expect(stringTranslator.compatibleLanguages).toBeNull()
	})

	it('StringTranslator.compatibleLanguages() should match given translation maps', () => {
		const stringTranslator = new StringTranslator([
			{
				scope: 'SCOPE',
				translationMap: {
					en: {},
					de: {},
					xx: {},
				},
			},

			{
				scope: 'SCOPE',
				translationMap: {
					yy: {},
				},
			},

			{
				scope: 'SCOPE_2',
				translationMap: {
					zz: {},
				},
			},
		])

		expect(stringTranslator.compatibleLanguages).toEqual([
			'en',
			'de',
			'xx',
			'yy',
			'zz',
		])
	})

	it('StringTranslator.constructor() should merge provided scoped translation maps, when in doubt the later map takes precedence.', () => {
		const stringTranslator = new StringTranslator([
			{
				scope: 'SCOPE',
				translationMap: {
					xx: {
						SUB: {
							A: 'value-a-old',
							B: 'value-b-old',
						},
						K: 'value-k',
					},
				},
			},
			{
				scope: 'SCOPE.SUB',
				translationMap: {
					xx: {
						A: 'value-a-new',
						C: 'value-c-new',
					},
				},
			},
		])

		if (!isTranslationMap(stringTranslator.translationMaps.xx))     return fail()

		const map = stringTranslator.translationMaps.xx

		if (!isTranslationMap(map.SCOPE))                               return fail()
		if (!isTranslationMap(map.SCOPE.SUB))                           return fail()

		expect(map.SCOPE.K                                             ).toBe('value-k')
		expect(map.SCOPE.SUB.A                                         ).toBe('value-a-new')
		expect(map.SCOPE.SUB.B                                         ).toBe('value-b-old')
		expect(map.SCOPE.SUB.C                                         ).toBe('value-c-new')
	})

	it('StringTranslator.match() should return 1 for tranlations keys and templates and 0 for any other non-empty string in every other case -1.', () => {
		const stringTranslator = new StringTranslator([
			{ scope: 'scope', translationMap: { xx: {} } },
		])

		expect(stringTranslator.match({ value: '' })                    ).toBe(-1)
		expect(stringTranslator.match({ value: 3 })                     ).toBe(-1)
		expect(stringTranslator.match({ value: null })                 ).toBe(-1)
		expect(stringTranslator.match({ value: {} })                   ).toBe(-1)
		expect(stringTranslator.match(undefined)                       ).toBe(-1)
		expect(stringTranslator.match(5)                               ).toBe(-1)
		expect(stringTranslator.match(9999)                            ).toBe(-1)
		expect(stringTranslator.match(0)                               ).toBe(-1)
		expect(stringTranslator.match(-10)                             ).toBe(-1)
		expect(stringTranslator.match(NaN)                             ).toBe(-1)
		expect(stringTranslator.match(null)                            ).toBe(-1)
		expect(stringTranslator.match({ translations: {} })            ).toBe(-1)
		expect(stringTranslator.match(new Date())                      ).toBe(-1)
		expect(stringTranslator.match(true)                            ).toBe(-1)
		expect(stringTranslator.match(false)                           ).toBe(-1)
		expect(stringTranslator.match({})                              ).toBe(-1)
		expect(stringTranslator.match([])                              ).toBe(-1)

		expect(stringTranslator.match('abc')                           ).toBe(0)
		expect(stringTranslator.match('This is just a normal sentence')).toBe(0)

		expect(stringTranslator.match('ABC')                           ).toBe(1)
		expect(stringTranslator.match('A.B.C')                         ).toBe(1)
		expect(stringTranslator.match('blub {{A.B.C}}')                ).toBe(1)
		expect(stringTranslator.match('blub {{A}} after')              ).toBe(1)
	})

	it('StringTranslator.translate() should return null for translation keys that are not listed in the central dictionary.', () => {
		const stringTranslator = new StringTranslator([
			{
				scope: 'SCOPE',
				translationMap: {
					xx: {
						TEST: 'xx works',
						SUB: {
							TEST: 'xx sub',
						},
					},
					yy: {
						TEST: 'yy works',
						SUB: {
							TEST: 'yy sub',
						},
					},
				},
			},
		])

		expect(stringTranslator.translate('ABC',                'xx')) .toBeNull()
		expect(stringTranslator.translate('SCOPE.TEST',         'en')) .toBeNull()
		expect(stringTranslator.translate('SCOPE.TEST',         'de')) .toBeNull()
		expect(stringTranslator.translate('SCOPE.TEST.BLUB',    'xx')) .toBeNull()
	})

	it('StringTranslator.translate() should return the matching dictionary entry for translation keys as final result.', () => {
		const stringTranslator = new StringTranslator([
			{
				scope: 'SCOPE',
				translationMap: {
					xx: {
						TEST: 'xx works',
						SUB: {
							TEST: 'xx sub',
						},
					},
					yy: {
						TEST: 'yy works',
						SUB: {
							TEST: 'yy sub',
						},
					},
				},
			},
		])

		expect(stringTranslator.translate('SCOPE.TEST',     'xx'))      .toBeDefined()
		expect(stringTranslator.translate('SCOPE.TEST',     'xx').final).toBe('xx works')

		expect(stringTranslator.translate('SCOPE.TEST',     'yy'))      .toBeDefined()
		expect(stringTranslator.translate('SCOPE.TEST',     'yy').final).toBe('yy works')

		expect(stringTranslator.translate('SCOPE.SUB.TEST', 'xx'))      .toBeDefined()
		expect(stringTranslator.translate('SCOPE.SUB.TEST', 'xx').final).toBe('xx sub')

		expect(stringTranslator.translate('SCOPE.SUB.TEST', 'xx'))      .toBeDefined()
		expect(stringTranslator.translate('SCOPE.SUB.TEST', 'yy').final).toBe('yy sub')
	})

	it('StringTranslator.translate() should replace bad translation keys in translation templates with the offending key and return them as final result.', () => {
		const stringTranslator = new StringTranslator([
			{
				scope: 'SCOPE',
				translationMap: {
					xx: {
						TEST: 'xx works',
						SUB: {
							TEST: 'xx sub',
						},
					},
					yy: {
						TEST: 'yy works',
						SUB: {
							TEST: 'yy sub',
						},
					},
				},
			},
		])

		expect(stringTranslator.translate('Something something {{ABC}}.',        'xx'))      .toBeDefined()
		expect(stringTranslator.translate('Something something {{ABC}}.',        'xx').final).toBe('Something something ABC.')

		expect(stringTranslator.translate('Something something {{AB.C}}.',       'xx'))      .toBeDefined()
		expect(stringTranslator.translate('Something something {{AB.C}}.',       'xx').final).toBe('Something something AB.C.')

		expect(stringTranslator.translate('Something something {{AB.C}} after.', 'xx'))      .toBeDefined()
		expect(stringTranslator.translate('Something something {{AB.C}} after.', 'xx').final).toBe('Something something AB.C after.')
	})

	it('StringTranslator.translate() should recursively replace params in translation templates and return them as final value.', () => {
		const stringTranslator = new StringTranslator([
			{
				scope: 'SCOPE',
				translationMap: {
					xx: {
						TEST: 'xx works',
						SUB: {
							TEST: 'xx sub',
						},
					},
					yy: {
						TEST: 'yy works',
						SUB: {
							TEST: 'yy sub',
						},
					},
				},
			},
		])

		const params = {
			ABC: 1,
			AB: {
				C: 'my value',
			},
			TEMPLATE: '{{AB.C}} all over again',
		}

		expect(stringTranslator.translate('Something something {{ABC}}.',            'xx',  params))        .toBeDefined()
		expect(stringTranslator.translate('Something something {{ABC}}.',	         'xx',	params).final)  .toBe('Something something 1.')

		expect(stringTranslator.translate('Something something {{AB.C}}.',	         'xx',	params))        .toBeDefined()
		expect(stringTranslator.translate('Something something {{AB.C}}.',	         'xx',	params).final)  .toBe('Something something my value.')

		expect(stringTranslator.translate('Something something {{AB.C}} after.',	 'xx',	params))        .toBeDefined()
		expect(stringTranslator.translate('Something something {{AB.C}} after.',	 'xx',	params).final)  .toBe('Something something my value after.')

		expect(stringTranslator.translate('Nested Template: {{TEMPLATE}} after.',	 'xx',	params))        .toBeDefined()
		expect(stringTranslator.translate('Nested Template: {{TEMPLATE}} after.',	 'xx',	params).final)  .toBe('Nested Template: my value all over again after.')
	})

	it('StringTranslator.translate() should recursively replace params and translation key in translation templates and return them as final value.', () => {
		const stringTranslator = new StringTranslator([
			{
				scope: 'SCOPE',
				translationMap: {
					xx: {
						TEST: 'xx works',
						KEY_WITH_PARAMS: 'param.test = "{{test}}"',
					},
				},
			},
		])

		const params = {
			template_with_key: 'before {{SCOPE.TEST}} after',
			test: 'my value',
		}

		expect(stringTranslator.translate('Params and templates: {{template_with_key}}.',	    'xx',	params))      .toBeDefined()
		expect(stringTranslator.translate('Params and templates: {{template_with_key}}.',	    'xx',	params).final).toBe('Params and templates: before xx works after.')

		expect(stringTranslator.translate('Params and templates: {{SCOPE.KEY_WITH_PARAMS}}.',	'xx',	params))      .toBeDefined()
		expect(stringTranslator.translate('Params and templates: {{SCOPE.KEY_WITH_PARAMS}}.',	'xx',	params).final).toBe('Params and templates: param.test = "my value".')
	})
})
