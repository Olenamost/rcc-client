import	{	TranslationResult	} from './interfaces'


/**
 * This is the base class for all translator services.
 * Extension of this class are used to extend translation capabilities of {@link TranslationService}.
 *
 */
export abstract class Translator {
	public constructor(...args: unknown[]) {}

	/**
	 * Indicates for which languages this Translator will work,
	 * Setting this value to an empty array `[]` means that this translator will not work for any language;
	 * {@link TranslationService} will ignore Translators like this and log a warning.
	 * Setting it to `null` though means, that it works for all languages or language independent (e.g. makes use of the browser's toLocaleString() ).
	 *
	 * Be careful not to add a translator that can only handle a few languages, because {@link TranslationService} determines the
	 * overall available languages as those that are supported by all translators. Having `translatorA.compatibleLanguages = ['de']` and
	 * `translatorB.compatibleLanguages = ['en']` will result in no language to be available overall.
	 *
	 * Can be implemented as a getter if need be.
	 */
	public abstract compatibleLanguages : string[] | null


	/**
	 * This method determines how well suited this translator is for a given input.
	 * The greater the return value the better it is suited:
	 *
	 * Return values are interpreted like this:
	 * ```
	 *   -1: can't handle this kind of input
	 *    0: can handle this kind of input - if need be
	 *    1: good at handling this kind of input
	 * >= 2: specifically made to handle this kind of input
	 * ```
	 * You should very rarely return a value greater than 2.
	 *
	 * This method will be called every time {@link TranslationService} is used to translate something.
	 * This will propaby happen in a pipe or filter or something that runs with each digest or change detection cycle (or similar).
	 * So please do not do anything expensive in here. If things have to be more complex, please put a proper caching mechanism in place.
	 *
	 * Also try to catch errors; any console output by this method can impact performance dramatically.
	 */
	public abstract match(x:unknown): number


	/**
	 * This value will translate the input for a given language and potentially extra parameters.
	 *
	 * In most cases this method will be called within {@link TranslationService};
	 * in these cases it will only be called if {@link match} returned a non-negative value.
	 * But since translators can be used separately, this method might be called from somewhere else without prior call of {@link match}.
	 * For those cases please make sure to restrict the interface for {@link input} paramter if applicable.
	 *
	 * This method will be called very frequently (e.g. when used in a pipe or filter on every digest or change detection cycle).
	 * So please do not do anything expensive in here. If things have to be more complex, please put a proper caching mechanism in place.
	 *
	 * Also try to catch errors; any console output by this method can impact performance dramatically.
	 */
	public abstract translate(input:unknown, language: string, param?: Record<string,unknown>): TranslationResult

	/**
	 * Creates a string value for any given input.
	 * Two inputs that have different translations are expected to have different hashes.
	 * This way we can check if a translation needs to be updated without repeating the whole
	 * translation process.
	 */
	public hash(input : unknown, param?	: Record<string,unknown>) : string | null {

		try {
			return JSON.stringify({ i:input, p:param })
		}catch(e){
			const translatorClassName = this.constructor.name
			console.warn(`${translatorClassName}.hash() unable to create hash. You probably have to overwrite .hash() method of the parent Translator class.`)
			return null
		}



	}



}
