// TODO besprechen mit Andreas
import { Translator } from './translator.class'
import { TranslationResult } from './interfaces'


// Generic test for newly created extensions of Translator class to see whether it was extended correctly.
class TranslatorExtension extends Translator {
	compatibleLanguages : string[] | null

	match(x:unknown): number {
        return 1
    }

	translate(input:unknown, language: string, param?: Record<string,unknown>): TranslationResult {
        return { final: '' }
    }

    public hash(input : unknown, param?	: Record<string,unknown>) : string | null {

		try {
			return JSON.stringify({ i:input, p:param })
		}catch(e){
			const translatorClassName = this.constructor.name
			console.warn(`${translatorClassName}.hash() unable to create hash. You probably have to overwrite .hash() method of the parent Translator class.`)
			return null
		}
	}
}

export function genericTranslatorClassTests(translatorClass: typeof Translator): void {

    describe('TranslatorClass', () => {

        it('should extend Translator.', () => {
            // console.log(typeof (TranslatorExtension.prototype))
            // console.log(expect(TranslatorExtension.prototype).toBeInstanceOf(Translator))
            expect(translatorClass.prototype).toBeInstanceOf(Translator)
        })

    })

}
