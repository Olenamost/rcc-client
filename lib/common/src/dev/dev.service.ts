import 	{
			Injectable,
		} 							from '@angular/core'

import	{	DevWarnings			}	from './dev.commons'


@Injectable()
export class DevService {

	public constructor(
		public warnings: DevWarnings
	){}

	addWarning(name: string, note: string){
		this.warnings.push({ name,note })
	}

}

