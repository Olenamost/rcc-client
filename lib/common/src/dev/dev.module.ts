import	{	TranslationsModule	}	from './../translations/ng/translations.module'
import 	{
			NgModule,
			ModuleWithProviders
		}							from '@angular/core'

import	{	RouterModule		}	from '@angular/router'

import	{
			MainMenuModule,
			provideMainMenuEntry
								}	from '../main-menu'
import	{	SharedModule		}	from '../shared-module'

import	{	DevWarnings			}	from './dev.commons'
import	{	DevService			}	from './dev.service'


import	{	DevPageComponent	}	from './dev.page/dev.page'

import	{	ConsolePipe			}	from './dev.pipes'

import	{	provideTranslationMap	}	from '../translations'

import en from './i18n/en.json'
import de from './i18n/de.json'

const routes 		=	[
							{ path: 'dev',	component: DevPageComponent	},
						]

export class MenuEntryDevComponent {

	public constructor(
		public devService: DevService
	){}
}

const menuEntry	=	{
	position: 8,
	label: 'DEV.MENU_ENTRY',
	icon: 'dev',
	path: '/dev',
}



@NgModule({
	declarations: [
		DevPageComponent,
		ConsolePipe,
	],
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		MainMenuModule,
		TranslationsModule

	],
	exports: [
		DevPageComponent,
		ConsolePipe,
	],
	providers: [
		DevService,
		{ provide: DevWarnings, useValue: { name: 'DevModule imported', note: 'Some other module imported DevModule' }, multi:true },
		provideTranslationMap('DEV', { en,de }),
		provideMainMenuEntry(menuEntry),
	]
})
export class DevModule {

	public constructor(){
		console.warn('DevModule in use!')
	}

	static note(note: string): ModuleWithProviders<DevModule>{
		return 	<ModuleWithProviders<DevModule>>{
					ngModule: 	DevModule,
					providers: 	[
									{ provide: DevWarnings, useValue: { name: 'DevModule imported', note }, multi:true }
								]
				}
	}

}
