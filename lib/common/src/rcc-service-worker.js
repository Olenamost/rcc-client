importScripts('./ngsw-worker.js')

self.addEventListener('install', 	event => self.skipWaiting() )
self.addEventListener('activate', 	event => event.waitUntil(self.clients.claim()))



class ScheduledNotificationController {

	dbInstance				=	undefined

	dbName 					= 	'rccSwScheduledNotifications'
	queueStoreName			= 	'actionQueue'
	configStoreName			= 	'config'
	counterStoreName		=	'counters'

	actions 				= 	{
									configure:				params => this.configure(...params),
									notify:					params => this.notify(...params),
									cancelNotification:		params => this.cancelNotification(...params),
									getQueuedNotifications:	params => this.getQueuedNotifications(...params),
									getVapidPublicKey:		params => this.getVapidPublicKey(...params),
									updateVapidPublicKey:	params => this.updateVapidPublicKey(...params),
									registerRemotePush:		params => this.registerRemotePush(...params),
									popLastMessage:			params => this.popLastMessage(...params),
								}

	defaultNotification 	= 	{
									title: 'RecoveryCat',
									vibrate: [100, 50, 200, 50, 200, 50, 100, 50, 200, 50, 100] //.-. -.-.
								}

	multiNotification 		= 	{
									title: 'RecoveryCat multiple notifications',
									vibrate: [100, 50, 200, 50, 200, 50, 100, 50, 200, 50, 100] //.-. -.-.
								}



	// IndexedDB realted methods:

	/**
	 * Retrieves an instance of IndexedDb. If we open a connection before,
	 * returns this open one; else opens a new connection.
	 */
	async getDB(){

		if(this.dbInstance) return this.dbInstance

		let resolve
		let reject

		const promise 					= 	new Promise( (solve, ject) => { resolve = solve; reject = ject })

		const DBOpenRequest 			= 	indexedDB.open(this.dbName, 1)

		DBOpenRequest.onupgradeneeded 	= 	() => {
												DBOpenRequest.result.createObjectStore(this.queueStoreName, {autoIncrement:true})
												DBOpenRequest.result.createObjectStore(this.configStoreName)
												DBOpenRequest.result.createObjectStore(this.counterStoreName)
											}

		DBOpenRequest.onsuccess 		= 	() => resolve(DBOpenRequest.result)
		DBOpenRequest.onerror 			= 	() => reject(DBOpenRequest.error)

		this.dbInstance 				= await promise


		return this.dbInstance

	}


	/**
	 * Runs a callback on a store.
	 * @param {string} 		storeName 	- Name of the targeted store
	 * @param {function]	callback	- (store: IDBObjectStore) => IDBRequest
	 */
	async withStore(storeName, callback){

		let resolve
		let reject

		const promise 			= new Promise( (solve, ject) => { resolve = solve; reject = ject })
		const db 				= await this.getDB()

		const transaction		= db.transaction(storeName, 'readwrite')


		const store 			= transaction.objectStore(storeName)
		const request			= callback(store)

		transaction.oncomplete	= () => resolve(request.result)
		transaction.onerror		= () => reject(transaction.error)

		const result 			= await promise

		return result
	}




	/**
	 * Runs a callback on every item of the store.
	 *
	 * @param {string} 		storeName	- Name of the targeted store
	 * @param {function}	callback	- (item:unknown, key: unknown, cursor: IDBCursorWithValue) => void
	 */
	async forEach(storeName, callback ) {

		let resolve
		let reject

		const promise 			= 	new Promise( (solve, ject) => { resolve = solve; reject = ject })
		const db				= 	await this.getDB()

		const transaction 		= 	db.transaction([storeName], 'readonly')
		const store 			= 	transaction.objectStore(storeName)
		const cursorRequest 	= 	store.openCursor()

		cursorRequest.onsuccess = 	() => {
										const cursor = cursorRequest.result
										if(!cursor) return resolve()
										callback(cursor.value, cursor.key, cursor)
										cursor.continue()
									}

		cursorRequest.onerror	=	() => reject(cursorRequest.error)

		transaction.oncomplete	= 	() => resolve()
		transaction.onerror		= 	() => reject(transaction.error)

		await promise
	}



	/**
	 * Retrieves all items of a store and returns them as Array of key value pairs.
	 */
	async getEntries(storeName){

		const result = []

		await this.forEach(storeName, (item, key) => result.push([key, item]) )

		return result
	}


	// Methods related to config store:

	/**
	 * Sets value for config key.
	 */
	async setConfig(key, value){
		try{
			await this.withStore(this.configStoreName, store => store.put(value, key) )
		} catch(e) {
			console.error('Tried to set config, but failed', key, value, e)
		}
	}

	/**
	 * Gets values for config key.
	 */
	async getConfig(key){

		try 		{ return await this.withStore(this.configStoreName, store => store.get(key) ) }
		catch(e)	{ throw `ScheduledNotificationController.getConfig() unable to get config for "${key}"; try configure({${key}: 'your value'})`}

	}


	/**
	 * (Counters are persisten and can be used by updated/later instances of the service worker)
	 *
	 * @returns current value of the counter; 0 if it does not yet exist.
	 */
	async getCounter(name){

		if(typeof name !== 'string') throw `ScheduledNotificationController.getCounter() name parameter must be a string; got: ${typeof name}`

		const counterExists = await this.withStore(this.counterStoreName, store => store.count(name) )

		return 	counterExists
				?	await this.withStore(this.counterStoreName, store => store.get(name) )
				:	0
	}

	/**
	 * Increases the value of the counter by one; if it does not yet exist, creates it and sets it to 1.
	 * (Counters are persistent and can be used by updated/later instances of the service worker)
	 *
	 * @returns the increased value of the counter
	 */
	async increaseCounter(name){

		if(typeof name !== 'string') throw `ScheduledNotificationController.increaseCounter() name parameter must be a string; got: ${typeof name}`

		const currentValue 	= await this.getCounter(name)
		const newValue		= currentValue + 1

		await this.withStore( this.counterStoreName, store => store.put(newValue, name) )

		return newValue
	}



	// Action processing related methods:


	/**
	 * Execute an action given by its name with the provided parameters.
	 */
	async executeAction(action, parameters){

		if(typeof action !== 'string') throw "ScheduledNotificationController.executeAction() action must be a string; got: ${action}`"

		parameters = parameters || []

		console.groupCollapsed(`Running ${action}`)
		console.info('Parameters: ', ...parameters)
		console.groupEnd()

		const fn = this.actions[action]

		if(typeof fn !== 'function') throw `ScheduledNotificationController.executeAction() unknown action: ${action}`

		try{
			const result = await fn(parameters)

			console.groupCollapsed("Action result,", action)
			console.info('Parameters: ', ...parameters)
			console.info("Result:", result)
			console.groupEnd()

			return result

		} catch(e) {

			console.groupCollapsed("Action error,", action)
			console.error(e)
			console.groupEnd()

			throw e
		}


	}

	/**
	 * Queues an action with provided parameters for deferred execution.
	 * @param {string} 		action 		- Name of the action
	 * @param {unknown[]}	parameters	- Any parameters you want to call the action with
	 * @param {boolean}		onlyOnce	- If true, the action will only be queued if it is not already on the queue.
	 */
	async queueAction(action, parameters, onlyOnce){

		if(typeof action !== 'string') throw "ScheduledNotificationController.queueAction() action must be a string; got: ${action}`"

		parameters = parameters || []

		const doNotQueAgain = onlyOnce && (await this.getQueuedActions(action)).length > 0

		if(doNotQueAgain){
			console.info(`Queueing ${action} (duplicate, not queueing again)`)
			return 'Ignored: duplicate'
		}

		console.groupCollapsed(`Queueing ${action} ${doNotQueAgain ? '(duplicate, not queueing again)':''}`)
		console.info('Parameters: ', ...parameters)
		console.groupEnd()

		await this.withStore(this.queueStoreName, store => store.add({action, parameters}))

		return `Queued: ${action}`
	}

	/**
	 * Runs all actions in the queue.
	 */
	async processQueue(){

		const queueEntries 	= await this.getEntries(this.queueStoreName)

		console.groupCollapsed(`Processing queue (${queueEntries.length})`)
		queueEntries.forEach( ([key,item]) => console.info(item.action, ...(item.parameters||[]) ))
		console.groupEnd()

		// Clear the queue; actions that require retries will readd themselves.
		await this.withStore(this.queueStoreName, store => store.clear() )

		const queuePromises	= queueEntries.map( ([key, item]) => this.executeAction(item.action, item.parameters || []) )

		await Promise.allSettled(queuePromises)

	}

	/**
	 * Returns all queued actions of a specific kind.
	 */
	async getQueuedActions(action){

		if(typeof action !== 'string') throw "ScheduledNotificationController.getQueuedAction() action must be a string; got: ${action}`"

		const queuedActions = 	await this.getEntries(this.queueStoreName)

		const actionItems 	= 	queuedActions
								.filter(	([key, value]) => value.action === action)

		return actionItems

	}



	// Action methods:

	/**
	 * Updates the config store, with provide key value pairs.
	 * @param 	{Map|Object}			config		-	Map/Object of new configuration values.
	 * @returns {[string, unknown][]}	updated 	key value pairs
	 */
	async configure(config){

		let updateConfig

		try {
			updateConfig	= 	config instanceof Map
							?	config
							:	new Map(Object.entries(config))
		} catch(e) {
			throw `ScheduledNotificationController.configure() unable to convert config into Map: ${config}; ${e}`
		}

		const entries			=	Array.from(updateConfig.entries())

		const updatePromises	=	entries.map( ([key,value]) => this.setConfig(key,value) )

		await Promise.all(updatePromises)

		return entries
	}


	/**
	 * Trigger Notification. If notification.timestamp lies 5 or more minutes
	 * in the future, try again later; if not display notification. If triggering
	 * notification fails for some other reason try again later.
	 */
	async notify(notification, id){

		const now		= new Date().getTime()
		const timestamp = notification.timestamp
		const diff		= timestamp - now

		id = id  || await this.increaseCounter('notifications')

		// More than 1 week into the future
		if(diff > 1000*60*60*24*7)	throw "ScheduledNotificationController.notify() .timestamp too far in the future (max is one week)"

		// Sometime in the future
		if(diff > 0) {

			const queued = await this.queueAction('notify', [notification, id])

			void this.executeAction('registerRemotePush', [notification, id])

			return queued
		}

		let error

		try 		{ return await this.triggerNotification(notification) }
		catch(e)	{ error = e }

		const queued = await this.queueAction('notify', [notification, id])

		throw e

		return queued
	}


	/**
	 * Checks if a notification matches a query. A query counts as matching if
	 * all properties of the query have the exact same values as the corresponding
	 * properties of notification.data. A matching query needs not to share all
	 * the properties; a subset of properties will also work.
	 */
	matchNotification(notification, query){

		const matchKeys	= 	Object.keys(query||{})
		const data		=	notification.data || {}

		return  matchKeys.every( key => key in data && (data[key] === query[key]) )
	}


	/**
	 * Returns all notification objects passed to .notify that are still queued
	 * to be triggered.
	 *
	 * @param {Object} [query] result will be filtered by query applying {@link matchNotification}.
	 */
	async getQueuedNotifications(query){

		const queuedNotifications 	= await this.getQueuedActions('notify')
		const notifications			= queuedNotifications.map( ([key, {action, parameters}]) => parameters[0])

		const matchingNotifications	= notifications.filter( notification =>  this.matchNotification(notification, query) )

		return matchingNotifications
	}

	/**
	 * Cancel a notify action currently in the queue. Looks for notify actions
	 * in the queue and removes every entry where the data property of the given
	 * parameter matches dataQuery. If all properties of dataQuery are present
	 * and have the same value as .data on the notification parameter, the
	 * notification counts as a match and will be canceled. Only exact matches
	 * will count; Arrays are not supported.
	 *
	 * Example:
	 * ```
	 * 	const scheduledNotificationController = new ScheduledNotificationController()
	 *
	 * 	// Trigger notification in one minute:
	 *	scheduledNotificationController.notify({
	 * 		title: 'My notification',
	 * 		body: 'Body of my notification',
	 * 		timestamp: new Date().getTime() + (1000*60)
	 *
	 * 		data: { 	// <-- This property can be used to cancel the notification
	 * 			id: '#my-notification-id',
	 * 			tags: [my-tag, my-other-tag],
	 * 			something: 'something'
	 * 		}
	 * 	)
	 *
	 *	// Cancel notification:
	 * 	scheduledNotificationController.cancelNotification({id: '#my-notification-id'})
	 *
	 * 	// This will also work:
	 * 	scheduledNotificationController.cancelNotification({id: '#my-notification-id', something; 'something'})
	 *
	 * 	// This will also work:
	 * 	scheduledNotificationController.cancelNotification({something; 'something'})
	 *
	 *  ```
	 *
	 * @param {Object} dataQuery object - Hashmap to be matched against .data property of queued notification.
	 */
	async cancelNotification(dataQuery){

		if(!dataQuery) 						throw "ScheduledNotificationController.cancelNotification() missing dataQuery."
		if(typeof dataQuery !== 'object')	throw "ScheduledNotificationController.cancelNotification() dataQuery must be an object."

		const entries			= 	await this.getEntries(this.queueStoreName)
		const notificationKeys	= 	entries.filter( ([storageKey, item]) => {

										const isNotification = item.action === 'notify'

										if(!isNotification)	return false

										const notification	= item.parameters[0]

										if(!notification) 	return false

										const data 			= notification.data

										if(!data) 			return false

										const matchesQuery	= this.matchNotification(dataQuery)

										if(!matchesQuery)	return false

										return true
									})
									.map( ([storageKey, item]) => storageKey)

		await Promise.all( notificationKeys.map( key => this.withStore(this.queueStoreName, store => store.delete(key) )) )

	}

	/**
	 * Returns stored vapid public key, if available. Queues remote fetch and will post message later on success.
	 * ```
	 * client.postMessage({type:'vapid-public-key', newKey}))
	 * ```
	 */
	async getVapidPublicKey(){

		let vapidPublicKeyUrl
		let configError
		let storedKey
		let newKey

		try {
			// If we cannot get vapidPublicKeyUrl, we cannot update the
			// key later on and probably never got it in the first place;
			// the user should know about this; throws an error if necessary:
			vapidPublicKeyUrl	= await this.getConfig('vapidPublicKeyUrl')

			// Actually retrieve stored key	:
			storedKey 			= await this.getConfig('vapidPublicKey')

		} catch(e) {	configError = e }

		void this.executeAction('updateVapidPublicKey')

		if(configError) 					throw `ScheduledNotificationController.getVapidPublicKey() failed to retrieve value form config: ${configError}`

		// If the key was retrievable but null or  undefined or empty for some reason:
		if(!storedKey)	 					throw `ScheduledNotificationController.getVapidPublicKey() stored key is empty; got: ${storedKey}`
		if(typeof storedKey !== 'string')	throw `ScheduledNotificationController.getVapidPublicKey() stored key is invalid; got: ${typeof storedKey}`

		return storedKey
	}

	/**
	 * Fetches vapid public key from remote Server. Post message later on success.
	 *
	 * ```
	 * client.postMessage({type:'vapid-public-key', newKey}))
	 * ```
	 */
	async updateVapidPublicKey(){

		const vapidPublicKeyUrl	= await this.getConfig('vapidPublicKeyUrl')

		let storedKey
		let newKey

		try{		storedKey			= await this.getConfig('vapidPublicKey') }
		catch(e){	storedKey			= null }

		// Try to fetch key from remote server
		try {
			const response 		= await fetch(vapidPublicKeyUrl)
			if(!response.ok) throw `request failed with status: ${response.status}`
			newKey 				= await response.text()

		} catch(e) {

			// Try again later:
			await this.queueAction('updateVapidPublicKey', [], true)

			throw `ScheduledNotificationController.updateVapidPublicKey() unable to fetch key: ${e}`
		}

		if(newKey === storedKey)	return newKey

		await this.setConfig('vapidPublicKey', newKey)

		const windowClients 	= await clients.matchAll({type: "window"})

		windowClients.forEach( client => client.postMessage({type:'vapid-public-key', newKey}))
	}

	async cancelRemotePush(internal_id){
		const rrpEntries			= await this.getQueuedActions('registerRemotePush')
		const matchingEntries		= rrpEntries.filter( ([storageKey, item]) => item.parameters[1] === internal_id)

		await Promise.all( matchingEntries.map( key => this.withStore(this.queueStoreName, store => store.delete(key) )) )

	}


	async registerRemotePush(notification, internal_id) {

		const timestamp = notification.timestamp
		const id		= notification.data?.id || undefined

		const now = new Date().getTime()

		if(internal_id) {

			await this.cancelRemotePush(internal_id)

			const notifyEntries			= await this.getQueuedActions('notify')
			const notificationPresent	= notifyEntries.some( ([storageKey, item])	=> item.parameters[1] === internal_id)

			if(!notificationPresent) throw "ScheduledNotificationController.registerRemotePush() associated notification is not scheduled anymore."
		}

		if(timestamp < now) throw `ScheduledNotificationController.registerRemotePush() timestamp has already passed.`

		try {
			const remotePushUrl		= 	await this.getConfig('remotePushUrl')
			const subscription		=	await this.getConfig('pushSubscription')

			const body				=	JSON.stringify({ subscription, reminders:[{id,timestamp}] })
			const fetchConfig		=	{
											method:'POST',
											headers: {
												'Accept': 		'application/json',
												'Content-Type':	'application/json'
											},
											credentials: 'omit',
											body
										}

			const response 			= 	await fetch(remotePushUrl, fetchConfig )

			if(!response.ok) throw `ScheduledNotificationController.registerRemotePush() network request failed with status ${response.status}.`

		} catch(e) {
			await this.queueAction('registerRemotePush', [notification, internal_id] )
			throw e
		}


	}


	// Misc:



	/**
	 * Merge multiple notifications config into one.
	 */

	mergeNotifications(notifications){

		const localeOptions =	{
									weekday: 	'short',
									hour:		'2-digit',
									minute:		'2-digit'
								}

		const body			= 	notifications.map( notification => {

									const lang 		= notification.lang
									const timestamp = notification.timestamp
									const body 		= notification.body
									const time 		= new Date(timestamp).toLocaleDateString(lang, localeOptions)

									return `(${time}) ${body||''}`.trim()

								})
								.join('\n')


		return	{
					...this.multiNotification,
					body,
					data: {
						notifications
					}

				}

	}



	notificationStack 	= []
	stackDuration		= 500
	stackPromise		= undefined



	/**
	 * Trigger a notification. If multiple notifications are trigger within a
	 * short period of time, they will be merged into one.
	 *
	 * If notifications are paused (config.pause_notifications), nothing will
	 * be trigger and no error will thrown.
	 */
	async triggerNotification(notification) {

		this.notificationStack.push(notification)

		if(this.stackPromise) return await this.stackPromise

		this.stackPromise = new Promise( (resolve, reject ) => {

			setTimeout( async () => {

				const config	=	this.notificationStack.length > 1
									?	this.mergeNotifications(this.notificationStack)
									:	{
											...this.defaultNotification,
											...this.notificationStack[0]
										}


				console.groupCollapsed('Trigger notification')
				console.info("Title:", config.title)
				console.info("Date/Time", config.timestamp ? new Date(config.timestamp) : 'multi' )
				console.info("Object:", config)
				console.groupEnd()


				const paused = await this.getConfig('pause_notifications')

				paused
				?	console.log('ScheduledNotificationController.triggerNotification() notifications are paused. Did not trigger notofication.', config)
				:	await registration.showNotification(config.title, config)

				this.notificationStack  = []
				this.stackPromise		= undefined

				resolve()

			}, this.stackDuration)

		})

		await this.stackPromise

	}

	/**
	 * Stores a message (as used in client.postMessage) for later retrieval.
	 *
	 * This is useful, if the postMessage method is called before the client was
	 * able to add an event listener.
	 */
	async getLastMessage(){
		return await this.getConfig('lastMessage')
	}

	/**
	 * Retrieves the message previously stored (as used in client.postMessage).
	 *
	 * This is useful, if the postMessage method is called before the client was
	 * able to add an event listener.
	 */
	 async setLastMessage(message){
		return await this.setConfig('lastMessage', message)
	 }

	 /**
	 * Retrieves and clears the message previously stored (as used in client.postMessage).
	 */
	 async popLastMessage(){
	 	const message = await this.getLastMessage()

		await this.setConfig('lastMessage', null)

		return message
	 }



	/**
	 * Scans a message event for an action, action parameters and a response port.
	 */
	async getActionFromMessageEvent(event){

		const data 			= event.data

 		if(!data) 							throw "missing .data."

 		const name			= data.action

 		if(typeof name !== 'string')			throw ".action is not a string."

 		const method 		= this.actions[name]

 		if(typeof method !== 'function')		throw `unable to find action with matching name: ${name}.`

 		const port 			= event.ports[0]

 		if(!port) 							return "missing port."

 		const parameters	= data.parameters || []

 		return { name, parameters, port }
	}






	// event handling methods:

	/**
	 * Callback for the message event.
	 */
	async handleMessage(event){

		const port = event.ports[0]

		let action

		try {
			action = await this.getActionFromMessageEvent(event)
		} catch(e) {
			return port && port.postMessage('Not an action: '+e)
		}


		try{
			const result = await this.executeAction(action.name, action.parameters)
			port && port.postMessage(result)

		}catch(e){
			const error = String(e)
			port && port.postMessage({error})
		}

	}


	processTimeout 			= undefined
	processDeferredPromise	= undefined
	processWaitingTime		= 3000

	/**
	 * Callback for any event. Whenever the service worker wakes up this is
	 * supposed to run. This can happen many times per second, so we throttle
	 * the execution to once every 3 seconds.
	 */
	async handleWakeUp(event){

		// clear previous timeout, in order to renew it below:
		clearTimeout(this.processTimeout)

		// schedule process and clear the queue for later:
		this.processTimeout = setTimeout(
			async () => {
				try{
					await this.processQueue()
					this.processDeferredPromise.resolve()
				} catch(e){
					this.processDeferredPromise.reject(e)
				}
				this.processDeferredPromise	= undefined
				this.processTimeout 		= undefined
			},
			this.processWaitingTime
		)

		const recentlyCalled = this.processDeferredPromise !== undefined

		if(recentlyCalled){
			console.info("Wake-Up (defer)")
			return await this.processDeferredPromise
		}

		console.groupCollapsed("Wake-Up (process)")
		console.info(`Has not been called in the last ${this.processWaitingTime} ms. Processing queue.`)
		console.groupEnd()

		let resolve
		let reject

		this.processDeferredPromise = new Promise( (solve, ject) => { resolve = solve; reject = ject })
		this.processDeferredPromise.resolve = resolve
		this.processDeferredPromise.reject	= reject

		await this.processQueue()

	}




}

const scheduledNotificationController = new ScheduledNotificationController()


addEventListener('notificationclick', event => {

	const location					= '/'

	const notification				= event.notification
	const {title, body, data, tag} 	= notification
	const notificationConfig 		= {title, body, data, tag}


	event.waitUntil( (async () => {

		const message 			= {event: 'notificationclick', notification: notificationConfig }

		const windowClients 	= await clients.matchAll({type: "window"})

		const existingClient	= windowClients[0]

		notification.close()

		if(existingClient){

			existingClient.focus()
			existingClient.postMessage(message)

		}else{

			scheduledNotificationController.setLastMessage(message)

			const newWindowClient	= await clients.openWindow(location)

			newWindowClient.focus()
		}

		console.log('WPORKEFD')

	})().catch(console.error))

})



addEventListener('message', event => {
	event.waitUntil( scheduledNotificationController.handleMessage(event).catch(console.error) )
})




function onWakeUp(event){
	if(event) return event.waitUntil( scheduledNotificationController.handleWakeUp(event).catch(console.error) )
	console.warn('onWakeUp() missing event')
}

addEventListener('activate', 		onWakeUp)
addEventListener('push', 			onWakeUp)
addEventListener('sync', 			onWakeUp)
addEventListener('fetch', 			onWakeUp)
addEventListener('message', 		onWakeUp)
addEventListener('periodicsync', 	onWakeUp)
