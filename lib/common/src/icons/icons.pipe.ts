import	{
			Pipe,
			PipeTransform
		}						from '@angular/core'

import	{
			IconsService
		}						from './icons.service'

@Pipe({ name: 'rccIcon' })
export class RccIconPipe implements PipeTransform {


	public constructor( public iconsService: IconsService ){}

	public transform(icon_name: string = ''): string {
		return this.iconsService.get(icon_name)
	}
}
