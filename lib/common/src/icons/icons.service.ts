import	{
			Injectable,
			Inject,
		}						from '@angular/core'

import	{
			ICON_MAP
		}						from './icons.commons'

@Injectable()
export class IconsService {

	public constructor(
		@Inject(ICON_MAP)
		public iconMap: Map<string, string>
	) {}

	public get(str: string): string {
		if(!this.iconMap.get(str)) console.warn('Icon missing: '+str)    // TODO :Console will say: Icon missing : null .  That is because the items sometimes are loaded async which is why the icons are missing, but are then loaded as soon as the item is available.
		return this.iconMap.get(str) || 'UNKNOWN_ICON_STRING'
	}

}
