import 	{	Injectable					} from '@angular/core'
import	{	RccModalController			} from '../modals-provider'
import	{	ShareDataModalComponent		} from './modal/share-data-modal.component'


@Injectable()
export class ShareDataService {

	public constructor(
		public rccModalController	: RccModalController
	){}

/**
 * Shares the provided data by opening a dedicated data sharing modal.
 *
 */
	public async share(data: unknown) : Promise<void> {
		await this.rccModalController.present(ShareDataModalComponent, { data })
	}
}
