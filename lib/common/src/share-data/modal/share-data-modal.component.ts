import 	{
			Component,
			OnInit,
			OnDestroy,
			Injectable
		}										from '@angular/core'

import	{
			RccTransmission,
			RccTransmissionService
		}										from '../../transmission'
import	{	RccToastController,
			RccModalController
		}										from '../../modals-provider'



@Component({
	templateUrl: 	'./share-data-modal.component.html',
	styleUrls: 		['./share-data-modal.component.scss'],
})
export class ShareDataModalComponent  implements OnDestroy {

	public set data(value:any){
		this._data = value
		this.setup()
	}
	public get data(){
		return this._data
	}

	public _data?			:	any
	public transmission?	:	RccTransmission
	public qr_data?			:	string
	public invalid			:	boolean				= false
	public failed			:	boolean				= false
	public complete			:	boolean				= false

	public constructor(
		private rccTransmissionService			: RccTransmissionService,
		private rccToastController				: RccToastController,
		private rccModalController				: RccModalController
	){}





	public async setup(){

		this.failed = false

		if(this.transmission) this.transmission.cancel()

		this.transmission 	= 	await this.rccTransmissionService.setup(this.data)
		this.qr_data 		= 	JSON.stringify(this.transmission.meta) // JSON.stringify(['rcc-wst',"scan-demo", "iWBMT2yPFYFqbbbiSpXCLPnRoBVzJb8G+npEFV4tWA0=","SUTBklneWT6akoHn"])

		try {
			await this.transmission.start()
			this.complete 		= 	true

		} catch(e) {
			// TODO: log
			console.log(e)
			this.failed = true
		}

	}


	public retry() {
		this.setup()
	}

	public done() {
		this.cancel()
	}

	public cancel(){
		if(this.transmission) this.transmission.cancel()
		delete this.transmission
		this.rccModalController.dismiss()
	}

	ngOnDestroy() {
		if(!this.transmission) return
		this.transmission.cancel()
		this.rccToastController.info('SHARE_DATA.TRANSMISSION.CANCELED')
	}



}
