import	{	NgModule				}	from '@angular/core'
import	{	SharedModule			}	from '../shared-module'
import	{	provideTranslationMap	}	from '../translations'
import	{	QrCodeModule			}	from '../qr-codes'
import	{	TransmissionModule		}	from '../transmission'
import	{	ShareDataService		}	from './share-data.service'
import	{	ShareDataModalComponent	}	from './modal/share-data-modal.component'


import en from './i18n/en.json'
import de from './i18n/de.json'



@NgModule({
	imports:[
		SharedModule,
		QrCodeModule,
		TransmissionModule,
	],
	providers:[
		ShareDataService,
		provideTranslationMap('SHARE_DATA', { en, de })
	],
	declarations:[
		ShareDataModalComponent
	]
})
export class ShareDataModule {}
