import { RccModalController      } from '../modals-provider'
import { ShareDataModalComponent } from './modal/share-data-modal.component'
import { ShareDataService        } from './share-data.service'

describe('ShareDataService', () => {

    it('should open an instance of ShareDataModalComponent to display the data', async() => {

        /**
         *
         * Create a spy on the method present() which is implemented instead of the actual RccModalController service.
         * Create a new instance of ShareDataService with the spy as the RccModalController service.
         * Create an object which is passed as data to the share() method.
         * Call the share() method and expect the spy to have been called with the appropriate arguments.
         *
         */

        const rccModalControllerSpy    = jasmine.createSpyObj('RccModalController', ['present'])

        const shareDataService = new ShareDataService(rccModalControllerSpy as RccModalController)

        const shareData        = { a : 1, b : 2 }
        const resultPromise    = shareDataService.share(shareData)
        const result           = await resultPromise
        const mostRecentArgs   = rccModalControllerSpy.present.calls.mostRecent().args


        expect(result).toBeUndefined()
        expect(resultPromise).toBeInstanceOf(Promise)
        expect(mostRecentArgs[0]).toBe(ShareDataModalComponent)
        expect(mostRecentArgs[1].data).toBe(shareData)

    })
})
