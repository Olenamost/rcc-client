import	{	Injectable					}	from '@angular/core'
import	{	Subject						}	from 'rxjs'


@Injectable()
export class IncomingDataService extends Subject<unknown> {
}
