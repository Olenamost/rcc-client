import 	{ 	Injectable			}	from '@angular/core'
import	{	UserCanceledError	}	from '@rcc/core'



export interface AlertConfig {
	cssClass?	: string
	header?		: unknown
	subHeader?	: unknown
	message?	: unknown
	buttons?	: ButtonConfig[]
}

export interface ResolveButtonConfig {
	label		: unknown
	icon?		: string

	resolveAs	: unknown
	rejectAs?	: never
}

export interface RejectButtonConfig  {
	label		: unknown
	icon?		: string

	resolveAs?	: never
	rejectAs	: unknown
}



export type ButtonConfig = ResolveButtonConfig|RejectButtonConfig


/**
 * This class is meant to be extended, but will still be used as injection token.
 */
@Injectable()
export class RccAlertController {

	/**
	 * Default message when the alert message gets closed by other means
	 * than clicking a button associated with it's own reject/resolve message).
	 */
	public readonly dismissedMessage = 'dismissed'

	/**
	 * Presents a message to the user with optional buttons.
	 * Should resolve or reject depending on which button was
	 * clicked (see {@link AlertConfig} / {@link ButtonConfig}).
	 *
	 * If the alert message is closed by other means than clicking
	 * a button reject with .dismissedMessage (see above); e.g. clicking
	 * outside the message box.
	 *
	 * Note: This method is meant to be overwritten by an extending class.
	 */
	public async present( config: AlertConfig): Promise<unknown> {
		const msg = 'ModalProviderModule: missing AlertProvider, please provide alternative AlertControllerClass extending RccAlertController.'
		console.warn(msg)
		throw msg
	}


	/**
	 * This method just calls .present() with fixed parameters. When the user
	 * picks the 'cancel'-option a {@link UserCancelledError} will be thrown.
	 *
	 * Note: No need to overwrite/implement this method when extending.
	 */
	public async confirm( message: string, confirmLabel: string = 'CONFIRM', cancelLabel: string = 'CANCEL'): Promise<unknown> {
		return await 	this.present({
							message,
							buttons: [
								{
									label: 		cancelLabel,
									rejectAs: 	this.dismissedMessage,
								},
								{
									label:		confirmLabel,
									resolveAs:	'confirm'
								}
							]
						})
						.catch( reason => {
							throw 	reason === this.dismissedMessage
									?	new UserCanceledError('User canceled at confirmation.')
									:	reason
						})
	}
}
