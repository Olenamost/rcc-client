import 	{
			Injectable,
			Type,
		}					from '@angular/core'


@Injectable()
export class RccModalController {

	// Should resolve with whatever data is passed into dismiss, unless an Error object is passed; in that case reject:
	public async present(component: Type<any>, data?: unknown): Promise<unknown> {
		return this.dismiss()
	}

	public dismiss(data? : unknown): void{
		console.warn('ModalProviderModule: missing ModalProvider, please provide alternative modalControllerClass extending RccModalController.' )
	}

}

