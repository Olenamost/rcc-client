import	{
			Component,
			Input,
			Output,
			EventEmitter
		}							from '@angular/core'

import	{
			UserCanceledError
		}							from '@rcc/core'

import	{
			RccModalController
		}							from './base-modal-controller.service'


export type ResultType<MWR> = 	MWR extends ModalWithResultComponent<infer R>
								?	R
								:	never


/**
 * This abstract class is meant to be used as base class for modal components
 * that have some kind of product they return. Think of something that would
 * popup and enable the user to create a comment or edit a question. The product
 * should be something complex enough, that warrens a `cancel` and an `apply`
 * button (a select menu with the selected value being the product is _NO_ such
 * case).
 *
 * A modal component is a component used with RccModalController.present()
 * or similar (like Ionic's ModalController). Components like this are commonly
 * used to display some interactive UI element, that is more complex then e.g.
 * an alert, on top of existing content most likely in some kind of overlay.
 * Modals will be dismissed after the interaction; that is the time when
 * extensions of this class are supposed to hand over their product.
 *
 * This base class handles everything related to cancelling, applying and
 * handing over the result in various ways. Extensions of this class are
 * supposed to be used with a ModalController, but can also be used as inline
 * components. That way extensions of this component can be used as e.g.
 * overlays or be incorporated into pages, such that e.g. your question-edit
 * overlay looks and works exactly like your question edit page; no need for two
 * separate components.
 *
 * This component class comes _without_ any template; the template has to be
 * provided by the extension. The template should make use of the `.cancel()`
 * and `.apply()` method.
 *
 * If used with RccModalControl.present(), the return value will be the product
 * if `.apply()` was used and a {@link UserCanceledError} if `.cancel()` was
 * used instead. In both cases the modal will be dismissed/closed.
 *
 * __Note:__ If the modal gets dismissed by some other means the outcome is
 * undetermined by this class.
 *
 */
@Component({
	template:''
})
export abstract class ModalWithResultComponent<Result=unknown> {

	/**
	 * Emits without any value, when `.cancel()` is used. Useful when this
	 * class is used for an inline component.
	 */
	@Output()
	public canceled: EventEmitter<void>		=	new EventEmitter<void>()

	/**
	 * Emits with product, when `.apply()` is used. Useful when this
	 * class is used for an inline component.
	 */
	@Output()
	public applied: EventEmitter<Result>	=	new EventEmitter<Result>()

	/**
	 * Callback function to be called, when `.cancel()` is used. Useful if you
	 * do not use RccModalController, but render an extension of this class
	 * programmatically (e.g. as a dynamic component)
	 */
	@Input()
	public onCancel?					:	() => void

	/**
	 * Callback function to be called, when `.apply()` is used. Useful if you
	 * do not use RccModalController, but render an extension of this class
	 * programmatically (e.g. as a dynamic component)
	 */
	@Input()
	public onApply?						:	(item: Result) => void

	/**
	 * The label of the apply button.
	 */
	@Input()
	public applyLabel					: string = 'APPLY'


	/**
	 * Label of the cancel button.
	 */
	@Input()
	public cancelLabel					: string = 'CANCEL'

	/**
	 * TODO: I cannot recall why this base class needs this input. Maybe it was
	 * just so common to use in on extension of this class, that it made sense
	 * to level it up?
	 */
	@Input()
	public heading						: string = null

	public constructor(
		/**
		 * If you use this class for an inline component you might not need
		 * a ModalController.
		 */
		protected rccModalController?	: RccModalController
	){}

	/**
	 * This method should calculate the result from the current state of the
	 * modal (most likely evaluate a form).
	 */
	public abstract getResult()			: Result


	/**
	 * Cancel the whole process. If used in conjunction with a ModalController,
	 * closes the modal. If used in conjunction with `onCancel` callback, will
	 * invoke that. Always emits `canceled` output.
	 */
	public cancel() : void {

		this.canceled.emit()

		if(this.onCancel)				return void this.onCancel()

		// If `this.canceled` is observed regard
		// the component as inline (not as modal) and prevent
		// this.rccModalController.dismiss() to be called for a modal that does not exists.
		if(this.canceled.observed)		return undefined

		if(this.rccModalController) 	return void this.rccModalController.dismiss(new UserCanceledError('ModalWithResultComponent'))

		throw new Error('ModalWithResultComponent.cancel() not configured; either provide onCancel callback or RccModalController (maybe you forgot to hand it over to super, when extending).')

	}

	/**
	 * Ends the process with a result, calling `.getResult()`.If used in
	 * conjunction with a ModalController, closes the modal. If used in
	 * conjunction with `onApply` callback, will invoke that. Always emits
	 * `applied` output.
	 */
	public apply() : void {

		const result : Result = this.getResult()

		if(result === null) return undefined

		this.applied.emit(result)

		if(this.onApply)				return void this.onApply(result)

		// If `this.applied` is observed regard
		// the component as inline (not as modal) and prevent
		// this.rccModalController.dismiss() to be called for a modal that does not exists.
		if(this.applied.observed)		return undefined

		if(this.rccModalController)		return void this.rccModalController.dismiss(result)

		throw new Error('ModalWithResultComponent.apply() not configured; either provide onApply callback or RccModalController (maybe you forgot to hand it over to super, when extending).')

	}


}
