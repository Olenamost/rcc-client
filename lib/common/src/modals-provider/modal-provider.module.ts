import 	{
			Type,
			NgModule,
			ModuleWithProviders

		}										from '@angular/core'

import	{	RccModalController				}	from './base-modal-controller.service'
import	{	RccAlertController				}	from './base-alert-controller.service'
import	{	RccToastController				}	from './base-toast-controller.service'
import	{	RccLoadingController			}	from './base-loading-controller.service'



export interface ModalConfig {
	modalController?: 		Type<RccModalController>
	alertController?: 		Type<RccAlertController>
	toastController?: 		Type<RccToastController>
	loadingController?: 	Type<RccLoadingController>
}


@NgModule({})
export class ModalProviderModule {

	static forRoot(modalConfig: ModalConfig): ModuleWithProviders<ModalProviderModule>{
		return 	{
					ngModule: 	ModalProviderModule,
					providers: 	Object.keys(modalConfig).map( (key:string) => {
									switch(key){
										case 'modalController': 	return { provide: RccModalController, 	useExisting: modalConfig.modalController }
										case 'alertController': 	return { provide: RccAlertController, 	useExisting: modalConfig.alertController }
										case 'toastController': 	return { provide: RccToastController, 	useExisting: modalConfig.toastController }
										case 'loadingController': 	return { provide: RccLoadingController, useExisting: modalConfig.loadingController }
										default: throw new Error('ModalProviderModule.forRoot(): invalid key in config: '+key)
									}
								})
				}
	}
}
