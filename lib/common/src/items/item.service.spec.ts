import	{ 	TestBed						}	from '@angular/core/testing'
import	{
			DefaultLabelComponent,
			ItemService
		}									from "./item.service"

import	{
			Item,
			ItemStore
		}									from '@rcc/core'
import	{
			ItemAction,
			ITEM_REPRESENTATIONS,
			ITEM_ACTIONS,
			provideItemAction,
			provideItemRepresentation
		}									from './item.commons'

import	{
			Action,
		}									from '../actions'



interface TestConfig{}
class TestItem extends Item<TestConfig> {
	static acceptsAsConfig(x: unknown): x is TestConfig { return true }
}

class TestItemLabelComponent{}

class TestItemStore extends ItemStore<TestItem> {
	constructor(){
		super({itemClass: TestItem})
	}
}


describe('ItemService without ItemActions and ItemRepresentations', () => {
	let itemService: ItemService

	beforeEach(() => {
		let itemRepresentationSpy = jasmine.createSpyObj(ITEM_REPRESENTATIONS)
		TestBed.configureTestingModule({

			providers: [

				ItemService,
				// { provide: ITEM_REPRESENTATIONS, useValue: [itemRepresentationSpy] },

			]

		})
		itemService = TestBed.inject(ItemService)
	});

		// [{provide: ItemService, useExisting: ItemService}] is the same as the code above.


	it('#constructor()', () => {
		expect(itemService).toBeTruthy()
		expect(itemService).toBeInstanceOf(ItemService)
	})

	it('#getItemRepresentation()', () => {
		const item = new TestItem({})
		const itemRepresentation = itemService.getItemRepresentation(item)

		expect(itemRepresentation).toBeDefined()
		expect(itemRepresentation.itemClass).toBe(TestItem)
		expect(itemRepresentation.name).toBe("ITEMS.MISSING_REPRESENTATION")
		expect(itemRepresentation.icon).toBe("item")
		expect(itemRepresentation.labelComponent).toBe(DefaultLabelComponent)

	})

	it('#getActions()', () => {

		const item = new TestItem({})
		const actions = itemService.getActions(item)

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions.length).toBe(0)

	})

})

describe('ItemService with ItemRepresentations', () => {
	let itemService: ItemService

	// let itemRepresentationSpy: jasmine.SpyObj<ItemRepresentation>

	beforeEach(() => {
		let itemRepresentation		= {
			itemClass: TestItem,
			name: "TestItemName",
			cssClass: "TestItemCssClass",
			icon: "TestItemIcon",
			labelComponent: TestItemLabelComponent,
		}

		TestBed.configureTestingModule({

			providers: [

				ItemService,
				provideItemRepresentation(itemRepresentation)

			]

		})
		itemService = TestBed.inject(ItemService)
	});

	it('#constructor()', () => {
		expect(itemService).toBeTruthy()
		expect(itemService).toBeInstanceOf(ItemService)
	})

	it('#getItemRepresentation()', () => {
		const item = new TestItem({})

		const itemRepresentation = itemService.getItemRepresentation(item)
		expect(itemRepresentation).toBeDefined()
		expect(itemRepresentation.itemClass).toBe(TestItem)
		expect(itemRepresentation.name).toBe("TestItemName")
		expect(itemRepresentation.icon).toBe("TestItemIcon")
		expect(itemRepresentation.labelComponent).toBe(TestItemLabelComponent)

	})

})

describe('ItemService with ItemActions', () => {
	let itemService	: ItemService
	let actionA		: Action = {
									icon: "TestItemIcon",
									label: "TestItemLabel",
									path: "/test/path"
								}
	let actionB		: Action = {
									icon: "TestItemIconB",
									label: "TestItemLabelB",
									path: "/test/path/b"
								}
	let actionLocal	: Action = {
									icon: "TestItemLocalIcon",
									label: "TestItemLocalLabel",
									path: "/test/local/path"
								}
	let itemActionA	: ItemAction<TestItem>	= {
													role: "details" as const ,
													itemClass: TestItem,
													storeClass: undefined,
													getAction: (item: TestItem) => actionA
												}
	let itemActionB	: ItemAction<TestItem>	= {
													role: "destructive" as const ,
													itemClass: TestItem,
													storeClass: undefined,
													getAction: (item: TestItem) => actionB
												}

	let itemActionLocal : ItemAction<TestItem>	= {
														role: "details" as const ,
														itemClass: TestItem,
														storeClass: undefined,
														getAction: (item: TestItem) => actionLocal
													}

	beforeEach(() => {
		TestBed.configureTestingModule({

			providers: [

				ItemService,
				TestItemStore,
				provideItemAction(itemActionA as ItemAction),  // TODO: Remove typecast
				provideItemAction(itemActionB as ItemAction)   // TODO: Remove typecast

			]

		})
		itemService = TestBed.inject(ItemService)
	})

	it('#constructor()', () => {
		expect(itemService).toBeTruthy()
		expect(itemService).toBeInstanceOf(ItemService)
	})


	it('#getActions() for global actions only', () => {

		const item = new TestItem({})
		const actions = itemService.getActions(item)

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(2)
		expect(actions).toContain(actionA)
		expect(actions).toContain(actionB)

	})

	it('#getActions() for global and local actions', () => {

		const item = new TestItem({})
		const actions = itemService.getActions(item, undefined, [itemActionLocal])

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(3)
		expect(actions).toContain(actionA)
		expect(actions).toContain(actionB)
		expect(actions).toContain(actionLocal)


	})

	it('#getActions() for local actions only', () => {

		const item = new TestItem({})
		const actions = itemService.getActions(item, undefined, [itemActionLocal], !!'skipGlobal')

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(1)
		expect(actions).toContain(actionLocal)


	})

	it('#getActions() for global & local actions and roles', () => {

		const item = new TestItem({})
		let actions = itemService.getActions(item, ['details'], [itemActionLocal])

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(2)
		expect(actions).toContain(actionA)
		expect(actions).toContain(actionLocal)
		expect(actions).not.toContain(actionB)

		actions = itemService.getActions(item, ['destructive'], [itemActionLocal])

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(1)
		expect(actions).toContain(actionB)
		expect(actions).not.toContain(actionA)
		expect(actions).not.toContain(actionLocal)


	})
})
