import	{
			Injectable,
			Optional,
			Inject
		}									from '@angular/core'

import	{
			assert,
			Item,
			ItemStore
		}									from '@rcc/core'

import	{	RccModalController			}	from '../../modals-provider'

import	{
			ITEM_SELECTION_FILTERS,
			ItemSelectionFilter
		}									from '../item.commons'

import	{	ItemSelectModalComponent	}	from './item-select-modal.component'


@Injectable()
export class ItemSelectService {

	public constructor(
		@Optional() @Inject(ITEM_SELECTION_FILTERS)
		private	itemSelectionFilters	: ItemSelectionFilter[],
		private rccModalController		: RccModalController,
	){
		if(!this.itemSelectionFilters) 	this.itemSelectionFilters	= []
	}


	/**
	 * Opens a modal to select items.
	 * TODO: Needs documentation.
	 */
	public async select<I extends Item>(
		config : {
			stores?			: ItemStore<I>[],
			items?			: I[],
			preselect?		: I[],
			filters?		: ItemSelectionFilter[],
			singleSelect?	: boolean,
			heading?		: string,
			message?		: string

		}
	)	: Promise<I[]> {

		config.filters = config.filters || this.itemSelectionFilters

		const result =	 await 	this.rccModalController.present(ItemSelectModalComponent, config )

		assert( Array.isArray(result), 							'ItemService.selectItems() result is not an array.', result)
		assert( result.every( item => item instanceof Item), 	'ItemService.selectItems() result is not an array of Item.', result)
		return result as I[]
	}


}
