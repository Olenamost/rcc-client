import  {
			Component,
			Input,
			OnInit,
			OnDestroy,
			ViewChild
		}             					from '@angular/core'

import	{
			FormControl
		}								from '@angular/forms'

import	{
			IonSearchbar,
			InfiniteScrollCustomEvent
		}								from '@ionic/angular'

import	{
			Subject,
			takeUntil,
			merge,
			map,
			filter
		}								from 'rxjs'

import	{
			Item,
			ItemStore,
		}								from '@rcc/core'

import	{
			ModalWithResultComponent,
		}								from '../../modals-provider'

import	{
			BaseRepresentation
		}								from '../../actions'

import	{
			ItemSelectionFilter
		}								from '../item.commons'


export interface ItemGroup{
	items			: Item[],
	representation	: BaseRepresentation
}

@Component({
	selector: 'rcc-item-select',
	templateUrl: './item-select-modal.component.html',
	styleUrls:	['./item-select-modal.component.css']
})
export class ItemSelectModalComponent extends ModalWithResultComponent implements OnInit, OnDestroy {


	@Input()
	public stores				: ItemStore[] = []

	@Input()
	public preselect			: Item[] = []

	@Input()
	public items				: Item[] = []

	@Input()
	public filters 				: ItemSelectionFilter[] = []

	@Input()
	public message				: string = null

	@Input()
	public heading				: string = 'ITEMS.SELECT.TITLE'

	@ViewChild(IonSearchbar)
	private set searchbar(searchbar: IonSearchbar) {
		if(searchbar)
			setTimeout( () => { void searchbar?.setFocus() } , 200)
	}

	public selected				: Set<Item>  = new Set()

	public itemGroups			: ItemGroup[] = []

	public allItems				: Item[] = []

	public showSearch			= false

	public groupControl			= new FormControl(null)

	public searchControl		= new FormControl('')

	public filteredItems		: Item[] = []

	public numberOfItems		: number

	protected destroy$			= new Subject<void>()



	public ngOnInit(): void {

		this.setupItems()
		this.setupFilterGroups()
		this.updateFilteredItems()

		// Update item list, whenever search or filter changes:
		merge(
			this.groupControl.valueChanges,
			this.searchControl.valueChanges
			.pipe(
					filter(	(x	: unknown) 	=> typeof x === 'string'),
					map(	(str: string)	=> str.trim() ),
			)
		)
		.pipe( takeUntil(this.destroy$) )
		.subscribe( ()=> this.updateFilteredItems() )

	}

	/**
	 * Updates the displayed item list. This method is called
	 * whenever the search/filter term changes or a filter is selected from the
	 * pulldown.
	 *
	 * Note: We could have used a getter here instead, or a method that returns
	 * the current array of filtered items at each time. However, such a method
	 * will be called dozens of times and may degrade performance. Therefore, we
	 * opt for an update function that is called only when we already know about
	 * a change.
	 */
	public updateFilteredItems() : void {

		this.resetNumberOfItems()

		const activeGroup 	= 	this.groupControl.value as {items: Item[]} | null

		const items			=	activeGroup
								?	activeGroup.items
								:	this.allItems

		const search_term 	= 	this.searchControl.value || ''
		const needle		= 	search_term.trim().toUpperCase()

		if(needle === '') return void (this.filteredItems = items)

		const filtered		=	items.filter( item => {

									const haystack 		= item.toJSON().toUpperCase()

									return haystack.includes(needle)
								})

		this.filteredItems = filtered

	}



	/**
	 * Toggles the search toolbar; also clears the search, when the toolbar is
	 * hidden.
	 */
	public toggleSearch(force? : boolean) : void {

		this.showSearch = 	force === undefined
							?	!this.showSearch
							:	!!force

		if(!this.showSearch) this.searchControl.setValue('')
	}


	/**
	 * Collects all relevant items from various sources, and marks preselected
	 * items as selected.
	 */
	public setupItems() : void {

		this.resetNumberOfItems()

		const preselectedItems 	= 	this.preselect || []

		// Collect all relevant items from various sources:
		this.allItems 			= 	Array.from(new Set([
										...this.stores.map( (store: ItemStore) => store.items).flat(),
										...this.items,
										...preselectedItems,
									]))

		// Make sure, that preselected items actually get selected:
		preselectedItems.forEach( item => this.selected.add(item) )

	}

	/**
	 * Configures filter groups. Each registered itemFilter {@link provideItemFilter}
	 * will have is own item Group (if applicable for the current item type).
	 * Each group consists of a {@link Representation} and an array of items that
	 * match the filter. If no filters were registered, groups are formed for each
	 * registered {@link ItemStore}, that holds items of the current type.
	 *
	 * Note: Since neither items nor registered filters will change while this
	 * modal is displayed, we can prearrange these groups, when the component is
	 * initialized; this also gives us the number of items per group to display
	 * in the filter menu.
	 */
	public setupFilterGroups(): void {

		// grouping
		this.itemGroups.length 	= 0

		// default Grouping if stores are present:
		const storeGroups 	=	this.stores.map( store => ({
									items: store.items,
									representation:{
										label:	store.name,
										icon:	store.itemClass.constructor.name
									}
								}))

		// grouping according to provided filters:
		const filterGroups	=	this.filters.map( itemSelectionFilter => ({
									items: 			this.allItems.filter(itemSelectionFilter.filter),
									representation: itemSelectionFilter.representation
								}))

		const groups 		= 	filterGroups.length > 0
								?	filterGroups
								:	storeGroups


		// items without a group:
		const grouped_items		= groups.map( group => group.items ).flat()
		const remaining_items 	= this.allItems.filter( item => ![...grouped_items, ...this.preselect].includes(item))

		groups.push({
			items:	remaining_items,
			representation: {
				label:	'ITEMS.SELECT.OTHER_ITEMS',
				icon:	'unknown'
			}
		})

		const non_empty_groups	= groups.filter( group => group.items.length > 0)

		// add all groups with at least one item:
		this.itemGroups.push(...non_empty_groups)

	}

	public selectAndApply(item: Item) : void {

		this.selected = new Set([item])
		this.apply()

	}

	public toggleSelect(item: Item, force? : boolean): void {

		if(force === true)	return void this.selected.add(item)
		if(force === false)	return void this.selected.delete(item)

		if (this.selected.has(item))
			this.selected.delete(item)
		else
			this.selected.add(item)

	}

	public resetNumberOfItems(): void {
		this.numberOfItems = 15
	}

	public async increaseNumberOfItems(ev: Event) : Promise<void> {

		const event = (ev as InfiniteScrollCustomEvent) // workaround for https://github.com/ionic-team/ionic-framework/issues/24952

		this.numberOfItems +=10

		await event.target.complete()
	}

	public getResult() : Item[] {
		return Array.from(this.selected)
	}


	public ngOnDestroy(): void {

		this.destroy$.next()
		this.destroy$.complete()

	}
}
