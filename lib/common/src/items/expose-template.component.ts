import 	{
			Component,
			ViewChild,
			TemplateRef,
			AfterViewInit
		} 									from '@angular/core'

@Component({
	template: ''
})
export class ExposeTemplateComponent implements AfterViewInit {
	@ViewChild(TemplateRef, { static: true })
	public itemLabelTemplate! : TemplateRef<unknown>

	public ngAfterViewInit(): void {
		if(!this.itemLabelTemplate) throw new Error('ItemLabel missing #itemLabelTemplate.')
	}
}
