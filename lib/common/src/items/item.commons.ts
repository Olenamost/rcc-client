import	{
			InjectionToken,
			Provider,
			Type
		}								from '@angular/core'
import	{
			ProductOrFactory,
			getProvider
		}								from '../interfaces'
import	{
			Item,
			ItemStore
		}								from '@rcc/core'

import	{
			Action,
			BaseRepresentation
		}								from '../actions'


// ACTIONS

/**
 * The role of the Action is meant to roughly describe its purpose; it determines under which circumstances
 * the action is available/presented to the user.
 * * "details": present details of an item without changing anything.
 * * "destructive": remove an item from a store or a list; e.g. remove a {@link Question} from a {@link SymptomCheck}.
 * * "productive": create an item or add it to a store or list.
 * * "edit": change an item.
 * * "share": export the item data and send it somewhere else.
 */
export type ItemActionRole = undefined | 'details' | 'destructive' | 'productive' | 'edit' | 'share'


/**
 * This interface represents an action the user can perform with a specific type
 * of item; e.g. view, edit, delete, activate...
 * This interface in contrast to {@link Action} makes use of information that is
 * only available at runtime.
 */
export interface ItemAction<
	I extends Item<unknown> 	= Item<unknown>,
	S extends ItemStore<I> 	= ItemStore<I>
> {
	/**
	 * The role of the Action is meant to roughly describe its purpose;
	 * it determines under which circumstances the action is
	 * available/presented to the user.
	 */
	role 		: ItemActionRole,

	/**
	 * Type of item this action can be applied to.
	 */
	itemClass	: Type<I>

	/**
	 * If the storeClass property is present, this action is only available to
	 * items in stores of that type. Also the respective ItemStore service will
	 * be used as argument to .getAction calls.
	 */
	storeClass?	: Type<S>,

	/**
	 * Creates an {@link Action} out of an Item instance and a respective
	 * ItemStore service.
	 * {@link Action}s can then be used in UI-Components like
	 * {@link RccActionButtonComponent}.
	 */
	getAction	: 	(
						item	: I,
						/**
						 * If the action is supposed to make use of an ItemStore
						 * (e.g. delete an item from the ItemStore) the
						 * ItemStore service represented buy the .storeClass
						 * property from above will be injected at this point.
						 * Some item actions though do not make use of an
						 * ItemStore at all, e.g. view actions, that only
						 * display information already available to the item
						 * itself.
						 */
						store? 	: S
					) => Action

	/**
	 * @deprecated
	 * The store property used to be what the storeClass property is now.
	 * This property only exists to throw errors if at some point the old
	 * notation is still in use.
	 */
	store?		: never
}


export const ITEM_ACTIONS = new InjectionToken<ItemAction[]>('ItemActions')

/**
 * Add an action to the item type of the provided {@link ItemAction}.
 * Any time the app makes item actions available to the user the given ItemAction will be included - if appropriate.
 */
export function provideItemAction( x : ProductOrFactory<ItemAction> ): Provider {
	return	getProvider(ITEM_ACTIONS, x, !!'multi')
}



// Representations


/**
 * This interface defines the visual representation of items of a given type.
 */
export interface ItemRepresentation {

	/**
	 * Type of item that is meant to be represented by the rest of the values.
	 * This property should not normally be omitted.
	 * The exception is, the fallback Representation if an actual representation for an item is missing.
	 */
	itemClass:		Type<Item<unknown>>

	/**
	 * The name of the item type (not to be confused with a possible name of a respective item instance).
	 * This should be a translation string; e.g. 'QUESTIONS.NAME'.
	 */
	name?:			string,

	/**
	 * A css class name to be added to the wrapping component.
	 */
	cssClass?:		string,

	/**
	 * The name of an icon representing the item type.
	 */
	icon:			string


	/**
	 * A component to be use in item tags (```rcc-item-tag```) when showing item previews as in lists of items.
	 * This component should include the title/name of an item instance and some details.
	 *
	 * If the component extends {@link ExposeTemplateComponent} then the ng-template within can be extracted
	 * and the outer component selector does not need to be rendered.
	 */
	labelComponent:	Type<unknown>

}


export const ITEM_REPRESENTATIONS 	= new InjectionToken<ItemRepresentation[]>('ItemRepresentations')


/**
 * Assign a {@ItemRepresentation} to an item type. Only one such representation can be assigned to each item type.
 * If you assign multiple representations to the same item type, there is no guarantee of which one will be used.
 */
export function provideItemRepresentation( x : ProductOrFactory<ItemRepresentation> ): Provider {
	return	getProvider(ITEM_REPRESENTATIONS, x, !!'multi')
}


/**
 * This interface represents a filter used to filter items when selecting.
 */
export interface ItemSelectionFilter {
	/**
	 * Function that determines wether an item should be counted as matching this filter.
	 * Note: you have to check yourself if the item is of appropriate type.
	 */
	filter			: (item:Item) => boolean

	/**
	 * Determines what the button in the filter menu looks like.
	 */
	representation	: BaseRepresentation
}


export const ITEM_SELECTION_FILTERS = new InjectionToken<ItemSelectionFilter[]>('ItemSelectionFilter')

/**
 * Register an {@link ItemSelectionFilter}. Whenever items are selected a filter
 * menu will provide the option to filter out all items that match this filter.
 */
export function provideItemSelectionFilter(x : ProductOrFactory<ItemSelectionFilter>): Provider {
	return getProvider(ITEM_SELECTION_FILTERS, x, true)
}
