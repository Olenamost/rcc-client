import	{
			Type
		}									from '@angular/core'

import	{
			assert,
			Item,
		}									from '@rcc/core'


import	{
			ModalWithResultComponent,
			RccModalController,
		}									from '../../modals-provider'

export interface ItemEditResult<I extends Item = Item, A = unknown> {
	item:		I,
	artifacts?: A
}



export class ItemEditService<I extends Item = Item, Artifacts = unknown> {

	public constructor(
		protected itemClass					: Type<I>,
		protected modalWithResultComponent	: Type<ModalWithResultComponent<ItemEditResult<I,Artifacts>>>,
		protected rccModalController		: RccModalController,

	){}

	/**
	 * Opens modal to edit item instance (of type I).
	 *
	 * Will Resolve with an
	 * (edited) copy of the original item, when the user saved or applied
	 * changes, even if the data/config is the same as the original.
	 *
	 * Will reject, if editing is canceled in some way.
	 *
	 */
	public async editCopy(item: I, heading?: string) : Promise<ItemEditResult<I,Artifacts>> {

		const result			=	await	this.rccModalController.present(
												this.modalWithResultComponent,
												{ item, heading }
											) as ItemEditResult<I,Artifacts>

		// this.rccModalController.present breaks the type inference,
		// but since we use a ModalWithResultComponent<ItemEditResult<I,Artifacts>>
		// the resulting type has to match ItemEditResult<I,Artifacts>.
		// this.rccModalController.present though just forwards whatever the modal
		// gets dismissed with, without checking the result. If the modal gets dismissed,
		// with a faulty value for a reason coming from somewhere other than the editing UI,
		// this method should fail. We can't check if ``result.artifacts`` is of
		// the right type and just assume that if ``result.item`` is, then everything
		// worked out nicely.


		const serviceName 	= this.constructor.name
		const itemClassName	= result?.item && result.item.constructor.name

		assert(result.item instanceof this.itemClass, 	`${serviceName}.editCopy() item in result is not an instance of ${itemClassName}`, result)
		return result
	}

	public async edit(item: I, heading?: string): Promise<ItemEditResult<I,Artifacts>> {

		const result			= 	await this.editCopy(item, heading)

		if ( item ) item.config = result.item.config
		if (!item ) item 		= new this.itemClass(result.item.config)

		return { item, artifacts: result.artifacts }
	}

	public async create(heading? : string): Promise<ItemEditResult<I,Artifacts>> {

		return await this.edit(new this.itemClass(), heading)
	}

}
