import  {
			Component,
			Input,
			OnInit
		}             					from '@angular/core'

import	{
			Item,
			ItemStore,
			assert
		}								from '@rcc/core'

import	{
			ModalWithResultComponent,
		}								from '../../modals-provider'

import	{
			Action,
			Representation
		}								from '../../actions'

import	{
			ItemSelectionFilter
		}								from '../item.commons'


export interface ItemGroup{
	items			: Item[],
	representation	: Representation
}

@Component({
	selector: 'rcc-item-select',
	templateUrl: './item-select-modal.component.html',
})
export class ItemSelectModalComponent extends ModalWithResultComponent implements OnInit {


	@Input()
	public stores				: ItemStore[] = []

	@Input()
	public preselect			: Item[] = []

	@Input()
	public items				: Item[] = []

	@Input()
	public filters 				: ItemSelectionFilter[] = []

	public selected				: Set<Item>  = new Set()

	public itemGroups			: ItemGroup[] = []

	public allItems				: Item [] = []

	public actions				: Action[] = []

	public defaultGroupFn(item: Item): string[] {

		const matching_stores = this.stores.filter( store => store.items.includes(item) )

		if(matching_stores.length === 0) return ['UNKNOWN']

		return matching_stores.map( store => store.name)

	}

	public ngOnInit(): void {
		this.update()
	}

	public update(): void {

		// preselect
		(this.preselect || []).forEach( item => this.selected.add(item) )

		this.allItems = 	Array.from(new Set([
								...this.stores.map( (store: ItemStore) => store.items).flat(),
								...this.items,
								...this.preselect,
							]))


		// grouping
		this.itemGroups.length 	= 0

		// default Grouping if stores are present:
		const storeGroups 	=	this.stores.map( store => ({
									items: store.items,
									representation:{
										label:	store.name,
										icon:	store.itemClass.constructor.name
									}
								}))

		// grouping according to provided filters:
		const filterGroups	=	this.filters.map( itemSelectionFilter => ({
									items: 			this.allItems.filter(itemSelectionFilter.filter),
									representation: itemSelectionFilter.representation
								}))

		const groups 		= 	filterGroups.length > 0
								?	filterGroups
								:	storeGroups


		// items without a group:
		const grouped_items		= groups.map( group => group.items ).flat()
		const remaining_items 	= this.allItems.filter( item => ![...grouped_items, ...this.preselect].includes(item))

		groups.push({
			items:	remaining_items,
			representation: {
				label:	'ITEMS.SELECT.REMAINING_ITEMS',
				icon:	'unknown'
			}
		})

		const non_empty_groups	= groups.filter( group => group.items.length > 0)

		// add all groups with at least one item:
		this.itemGroups.push(...non_empty_groups)

		this.actions = non_empty_groups.map( group => ({
			...group.representation,
			handler: () => this.subSelect(group)
		}))
	}

	public async subSelect(group: ItemGroup): Promise<void> {

		const preselect = Array.from(this.selected).filter( item => group.items.includes(item) )
		const items		= group.items

		const result 	= await this.rccModalController.present(
									ItemSelectModalComponent,
									{ items, preselect }
								)

		assert(Array.isArray(result), 'ItemSelectModalComponent.getGroupHandler() result must be an Array.')
		assert(result.every(item => item instanceof Item), 'ItemSelectModalComponent.getGroupHandler() result must be an Array of Item(s).')

		items.forEach(
			item 	=> 	result.includes(item)
						?	this.selected.add(item)
						:	this.selected.delete(item)
		)

		this.apply()

	}

	public get mode(): string {
		if(this.itemGroups.length === 1) return 'simple'
		if(this.itemGroups.length >  1)	return 'complex'
	}

	public toggleSelect(item: Item, force? : boolean): void {

		if (force === true)		return void this.selected.add(item)
		if (force === false)	return void this.selected.delete(item)

		if (this.selected.has(item))
			this.selected.delete(item)
		else
			this.selected.add(item)
	}


	public getResult() : Item[] {
		return Array.from(this.selected)
	}


}
