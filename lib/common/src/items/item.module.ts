import	{	NgModule					}	from '@angular/core'
import	{	CommonModule				}	from '@angular/common'
import	{	ReactiveFormsModule 		}	from '@angular/forms'
import	{	IonicModule					}	from '@ionic/angular'
import	{
			TranslationsModule,
			provideTranslationMap
		}									from '../translations'
import	{	IconsModule					}	from '../icons'
import	{
			UiComponentsModule
		}									from '../ui-components'
import	{	RccItemTagComponent			}	from './item-tag'
import	{
			ItemService,
			DefaultLabelComponent
		}									from './item.service'
import	{	ExposeTemplateComponent		}	from './expose-template.component'
import	{
			ItemSelectModalComponent,
			ItemSelectService
		}									from './select'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports:[
		IonicModule,
		CommonModule,
		TranslationsModule,
		IconsModule,
		ReactiveFormsModule,
		UiComponentsModule
	],
	providers:[
		ItemService,
		ItemSelectService,
		provideTranslationMap('ITEMS', { de, en })
	],
	declarations:[
		DefaultLabelComponent,
		RccItemTagComponent,
		ExposeTemplateComponent,
		ItemSelectModalComponent
	],
	exports:[
		RccItemTagComponent,
		ExposeTemplateComponent,
		ItemSelectModalComponent
	]
})
export class ItemModule {}
