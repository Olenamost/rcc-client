import	{
			Component,
			Injector,
			Input,
			OnChanges,
			TemplateRef,
			ViewChild,
			ViewContainerRef,
			ElementRef,
			ChangeDetectorRef
		}								from '@angular/core'

import	{
			Router
		}								from '@angular/router'


import 	{
			ActionSheetController,
			IonItemSliding,
		}								from '@ionic/angular'

import	{
			Item,
		}								from '@rcc/core'

import	{
			IconsService
		}								from '../../icons'
import	{	RccTranslationService 	}	from '../../translations'
import	{
			RccToastController,
			RccAlertController
		}								from '../../modals-provider'
import	{
			Action,
			PathAction,
			HandlerAction,
			DownloadAction,
			isPathAction,
			isDownloadAction,
			isHandlerAction
		}								from '../../actions'

import	{
			ItemRepresentation,
			ItemAction,
			ItemActionRole
		}								from '../item.commons'

import	{	ItemService				}	from '../item.service'
import	{	ExposeTemplateComponent	}	from '../expose-template.component'


/**
 * Render actions for given {@link item}
 * Which actions are rendered and in which order is resolved by using
 * {@link ItemService["getActions"]}.
 *
 */
@Component({
	selector: 		'rcc-item-tag',
	templateUrl: 	'./item-tag.component.html',
	styleUrls:		['./item-tag.component.css']
})
export class RccItemTagComponent implements OnChanges {

	// #region viewChilren
	@ViewChild(IonItemSliding)
	private slidingItem? 		: IonItemSliding

	@ViewChild('download')
	private downloadLink? 		: ElementRef<HTMLAnchorElement>
	// #endregion

	// #region getActions parameters
	/**
	 * used to decide which items to show actions for
	 */
	@Input()
	public item? 				: Item 						= null

	/**
	 * used to determine which roles to show actions for
	 */
	@Input()
	public itemActionRoles 		: ItemActionRole[] 			= []

	/**
	 * additional itemActions to consider in {@link ItemService["getActions"]}
	 */
	@Input()
	public extraItemActions		: ItemAction[] 				= []

	/**
	 *  tell {@link ItemService["getActions"]} to skip or consider globally provided actions
	 */
	@Input()
	public skipGlobalActions	: boolean					= false

	/**
	 * shortCut for {@link extraItemActions} = [] and {@link skipGlobalActions} true
	 */
	@Input()
	public noActions			: boolean					= false
	// #endregion

	/**
	 * this forces {@link Mode} 'select'
	 * note: this renders all action related parameters useless
	 */
	@Input()
	public selected?			: boolean 		 			= null

	/**
	 * use 'complex' {@link Mode} instead if 'basic'
	 * note: this only takes effect if EXACTLY 2 actions are shown
	 */
	@Input()
	public forceComplexMode		: boolean					= false

	@Input()
	public iconColor			: string					= undefined

	public actions				: Action[]					= []

	public itemRepresentation?	: ItemRepresentation 		= null
	public itemLabelInjector?	: Injector 					= null
	public itemLabelTemplate?	: TemplateRef<unknown>		= null
	public itemLabelCssClass?	: string					= null

	public constructor(
		public 	itemService				: ItemService,
		public 	actionSheetController	: ActionSheetController, // Should be wrapped in rccActionSheetController or something
		private	router					: Router,
		private rccToastController		: RccToastController,
		private rccAlertController		: RccAlertController,
		private	rccTranslationService	: RccTranslationService,
		private	iconsService			: IconsService,
		private	injector				: Injector,
		private viewContainerRef		: ViewContainerRef,
		private changeDetectorRef		: ChangeDetectorRef
	){}


	public update(): void {

		this.updateActions()
		this.updateItemRepresentation()

	}

	protected get mode(): Mode | null {

		if(!this.item)					return null

		if(this.selected !== null) 		return 'select'


		if(this.actions.length <= 1)	return 'simple'

		if(this.forceComplexMode)		return 'complex'
		if(this.actions.length === 2)	return 'basic'

		return 'complex'
	}



	public updateActions() : void {
		this.actions	=	[]

		if(!this.item)		return

		if(this.noActions)	return

		const actions 	= 	this.itemService.getActions(
								this.item,
								this.itemActionRoles,
								this.extraItemActions,
								this.skipGlobalActions,
							)

		this.actions 	= actions
	}


	public updateItemRepresentation(): void {

		if(!this.item) {
			this.itemLabelInjector 	= null
			this.itemRepresentation	= null
			this.itemLabelCssClass	= null
			return
		}

		this.itemRepresentation	= this.itemService.getItemRepresentation(this.item)

		const itemClass 		= this.itemRepresentation.itemClass
		const labelComponent	= this.itemRepresentation.labelComponent
		const provider			= { provide: itemClass, useValue: this.item }

		this.itemLabelInjector	= Injector.create({ providers: [provider], parent: this.injector })
		this.itemLabelTemplate 	= null

		if(labelComponent.prototype instanceof ExposeTemplateComponent){

			const injector			=	Injector.create( { providers: [provider], parent: this.injector })
			const instance			= 	this.viewContainerRef
										.createComponent(labelComponent, { injector })
										.instance as ExposeTemplateComponent

			this.itemLabelTemplate 	=	instance.itemLabelTemplate || null

		}


	}

	public executePathAction(action: PathAction) : void {
		void this.router.navigateByUrl(action.path)
	}

	public async executeHandlerAction(action: HandlerAction) : Promise<void> {

		if(action.confirmMessage) 		await this.rccAlertController.confirm(action.confirmMessage)

		try {
			action.handler()
			if (action.successMessage)	void this.rccToastController.success(action.successMessage)
		}catch(e){
			if (action.failureMessage)	void this.rccToastController.failure(action.failureMessage)
		}

	}

	public async executeDownloadAction(action: DownloadAction) : Promise<void> {

		const element		= this.downloadLink.nativeElement

		const data			= await action.data
		const mimeType		= action.mimeType || 'text/plain'
		const blob			= new Blob([data], { type: mimeType })

		element.href 		= URL.createObjectURL(blob)
		element.download 	= action.filename

		element.click()
	}

	public execute(action: Action): void {

		if(isPathAction(action))		void this.executePathAction(action)
		if(isHandlerAction(action))		void this.executeHandlerAction(action)
		if(isDownloadAction(action))	void this.executeDownloadAction(action)

		this.changeDetectorRef.markForCheck()
	}



	public async toggleSlidingItem(): Promise<void>{
		if(this.slidingItem === null) return

		const ratio = await this.slidingItem.getSlidingRatio()

		if (ratio === 0)	void this.slidingItem.open('end')
		else				void this.slidingItem.close()
	}


	public async showActions(): Promise<void>{

		const buttons : Record<string, unknown>[] = []

		this.actions
		.forEach( (action: Action) => {

			buttons.push({
				text: 		this.rccTranslationService.translate(action.label),
				icon: 		this.iconsService.get(action.icon || 'view'),
				handler:	(): void => void this.execute(action)
			})

		})

		buttons.push({
				text: this.rccTranslationService.translate('META_STORE.ACTIONS.CANCEL'),
				icon: 'close',
				role: 'cancel'
		})

		const actionSheet = await this.actionSheetController.create({
			// header: '{{}}'
			buttons
		})

		await actionSheet.present()
	}


	public fallbackMissingActions() : void {
		void this.rccToastController.failure('ITEMS.ACTIONS.MISSING')
	}

	public async onClick(): Promise<unknown> {

		this.updateActions()

		if(this.mode === 'select')		return null

		if(this.actions.length === 0)	return this.noActions || this.fallbackMissingActions()

		if(this.mode === 'simple')		return this.actions[0] && this.execute(this.actions[0])

		if(this.mode === 'basic')		return this.toggleSlidingItem()

		if(this.mode === 'complex')		return this.showActions()

		return null
	}

	public ngOnChanges(): void {
		this.update()
	}
}

type Mode = 'select' | 'simple' | 'basic' | 'complex'
