import	{
			Injectable,
			Inject,
			Injector,
			Optional,
			Component,
			Type,
		}									from '@angular/core'

import	{
			Router
		}									from '@angular/router'

import	{
			Item,
			sortByKeyFn,
			camel2Underscore,
			ItemStore
		}									from '@rcc/core'
import	{
			Action,
		}									from '../actions'

import	{
			ItemAction,
			ItemActionRole,
			ItemRepresentation,
			ITEM_REPRESENTATIONS,
			ITEM_ACTIONS,
		}									from './item.commons'


@Component({
	template: '{{"ITEMS.MISSING_REPRESENTATION" | translate}}'
})
export class DefaultLabelComponent{}


@Injectable()
export class ItemService{

	public constructor(
		@Optional() @Inject(ITEM_REPRESENTATIONS)
		public	itemRepresentations		: ItemRepresentation[],

		@Optional() @Inject(ITEM_ACTIONS)
		public	itemActions				: ItemAction[],

		protected injector				: Injector,
		protected router				: Router
	){

		if(!this.itemRepresentations) 	this.itemRepresentations	= []
		if(!this.itemActions)			this.itemActions			= []

	}


	public getItemRepresentation<I extends Item>(item: I) : ItemRepresentation {

		const itemRepresentation : ItemRepresentation	=	this.itemRepresentations
															.find( itemRep => item instanceof itemRep.itemClass )


		if(itemRepresentation){
			itemRepresentation.name = itemRepresentation.name || camel2Underscore(itemRepresentation.itemClass.name).toUpperCase()
			return itemRepresentation
		}

		console.warn(`ItemService.getRepresentation(): missing item representation for: ${item.constructor.name}`)

		// Default name is the capitalized class name. (will be run through translations)
		const itemRepresentationFallback : ItemRepresentation	= 	{
			itemClass :			item.constructor as Type<I>,
			name : 				'ITEMS.MISSING_REPRESENTATION',
			icon :				'item',
			labelComponent :	DefaultLabelComponent
		}

		return itemRepresentationFallback
	}

	/**
	 * Gets all {@link Action | Actions} associated with an item by type and store, that hold it,
	 * optionally filtering them by role or adding extra actions.
	 * @param item	 				The item to get actions for
	 * @param roles 				Only return actions that have one of the provided roles.
	 * 	 							If this array undefined return all action, if it is empty, none.
	 * @param extraItemActions 		Additional ItemActions to consider.
	 * @param skipGlobalActions		Only use {@param extraItemActions} not the globally defined ones.
	 */
	public getActions<I extends Item> (
		item:	 			I,
		roles?: 			ItemActionRole[],
		extraItemActions: 	ItemAction<I>[] = [],
		skipGlobalActions: 	boolean = false,
	) : Action[] {

		return 	(
					skipGlobalActions
					?	extraItemActions
					:	[...this.itemActions, ...extraItemActions]
				)
				.filter((itemAction : ItemAction<I>) => !roles || roles.length === 0 || roles.includes(itemAction.role) )
				.filter((itemAction : ItemAction<I>) => item instanceof itemAction.itemClass )
				.map( 	(itemAction : ItemAction<I>) => {

					const store 	: ItemStore<I> 	= 	itemAction.storeClass
														?	this.injector.get(itemAction.storeClass)
														:	null

					if(store && !store.items.includes(item)) return

					const action	: Action 		=	store
														?	itemAction.getAction(item, store)
														:	itemAction.getAction(item)

					if(itemAction.role && !action.role) action.role = itemAction.role

					return action

				})
				.filter( x => !!x)
				.filter(
					action => 	typeof action.position === 'function'
								?	action.position() 	!== null
								:	action.position		!== null
				)
				.sort(sortByKeyFn('position'))
	}
}
