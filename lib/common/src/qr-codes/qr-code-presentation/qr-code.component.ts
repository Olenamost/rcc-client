import	{
			Component,
			Input,
			ViewChild,
			ElementRef
		}							from '@angular/core'
import	{	throwError			}	from '@rcc/core'
import	QRCode	from 'qrcode'

/**
 * Component to present a QR-Code encoding provided data.
 */
@Component({
	selector: 		'rcc-qr-code',
	templateUrl: 	'./qr-code.component.html',
	styleUrls:		 ['./qr-code.component.scss'],
})
export class QrCodeComponent {
	/**
	 * Stores a stringified version of the provided data.
	 */
	protected string_data! 		: string

	/**
	 * The canvas element to render the QR-code in (part of the template).
	 */
	protected canvasElement!	: HTMLElement

	/**
	 * Data to be encoded by the QR-Code
	 */
	@Input()
	public set data(data: unknown) { this.setData(data) }

	/**
	 * Color for the frame around the actual QR-code.
	 */
	@Input()
	public color! : 'primary' | 'secondary' | 'dark' | 'medium' | 'light'

	/**
	 * Setter for the canvasElement. Will update QR-code when used.
	 */
	@ViewChild('qrcode')
	public set canvas(elementRef: ElementRef<HTMLElement>) { this.setCanvas(elementRef.nativeElement) }

	/**
	 * Trys to stringify provided data; will store the result in string_data and update the QR-code, if successful or if the data already was a string.
	 */
	public setData(data: unknown): void {
		if(typeof data === 'string') this.string_data = data

		if(typeof data !== 'string')
			try {
				this.string_data = JSON.stringify(data)
			} catch(e) {
				throwError('QrCodeComponent.setData() unable to stringify data.', data)
			}
		void this.update()
	}

	/**
	 * Sets the canvas element for the QR-code to be rendered in.
	 */
	public setCanvas(canvas: HTMLElement): void {
		this.canvasElement = canvas

		void this.update()
	}

	/**
	 * Checks if canvas element and string data are present, if so render the QR-code, else do nothing.
	 */
	public async update(): Promise<void> {
		if(!this.canvasElement || !this.string_data) return

		await 	QRCode.toCanvas(
					this.canvasElement,
					this.string_data, {
						'errorCorrectionLevel': 'M',
					}
		)
	}
}
