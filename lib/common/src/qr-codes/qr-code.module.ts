import	{
			NgModule,
			Component,
			ModuleWithProviders,
			Provider
		}								from '@angular/core'
import	{	MainMenuModule			}	from '../main-menu'
import	{
			HomePageModule,
			provideHomePageEntry
		}								from '../home'
import	{	SharedModule			}	from '../shared-module'
import	{
			IncomingDataServiceModule,
			IncomingDataService
		}								from '../incoming-data'
import	{	provideTranslationMap	}	from '../translations'
import	{	Factory					}	from '../interfaces'
import	{	Action					}	from '../actions'
import	{	QrCodeComponent			}	from './qr-code-presentation/qr-code.component'
import	{	RccQrCodeScanner		}	from './qr-code.commons'
import	{	QrCodeService			}	from './qr-code.service'

import	en	from './i18n/en.json'
import	de	from './i18n/de.json'

export interface QrCodeModuleConfig {
	homePageEntry?: 		boolean | number
}

@Component({
	template:	`
					<ion-item [button] = "true" (click) = "scan()">
						<ion-label
                            [id]	= "'QRCODE.SCAN' | toID: 'rcc-e2e'"
                        >
                            {{ "QRCODE.SCAN" | translate }}
                        </ion-label>

						<ion-icon [name] = "'qr-code' | rccIcon" slot = "end"></ion-icon>
					</ion-item>
				`
})
export class MenuEntryQrCodeComponent {

	public constructor(
		public qrCodeService			: QrCodeService,
		public incomingDataService		: IncomingDataService
	){}

	public scan(): void {
		this.qrCodeService.scanAndAnnounce()
		.catch(console.error)
	}

}


@NgModule({
	declarations: [
		QrCodeComponent,
		MenuEntryQrCodeComponent
	],
	imports:[
		IncomingDataServiceModule,
		SharedModule,
		MainMenuModule,
		HomePageModule,
	],
	providers: [
		provideTranslationMap('QRCODE', { en, de }),
		QrCodeService,
		RccQrCodeScanner
	],
	exports: [
		QrCodeComponent,
	]
})
export class QrCodeModule {

	public static provideEntries(config?: QrCodeModuleConfig): ModuleWithProviders<QrCodeModule> {

		let homePageEntryPosition		: number	= 1
		let showHomePageEntry			: boolean	= true

		if (typeof config?.homePageEntry === 'number') 	homePageEntryPosition	= config.homePageEntry
		if (config?.homePageEntry === false) 			showHomePageEntry		= false
		if (config?.homePageEntry === null) 			showHomePageEntry		= false

		const homePageEntry : Factory<Action> =	{
			deps:	[QrCodeService],
			factory: (qrCodeService: QrCodeService) => ({
					position: 		homePageEntryPosition,
					icon:			'scan',
					label:			'QRCODE.SCAN',
					description:	'QRCODE.DESCRIPTION',
					role:			'share',
					handler:		() =>  qrCodeService.scanAndAnnounce().catch(console.error)
			})
		}

		const providers : Provider[] = []

		if (showHomePageEntry) providers.push(provideHomePageEntry(homePageEntry))

		return {
			ngModule:	QrCodeModule,
			providers
		}
	}
}
