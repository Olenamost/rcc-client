import	{
			Component,
			Input,
		}								from '@angular/core'
import	{	UserCanceledError		}	from '@rcc/core'
import	{	RccModalController		}	from '../modals-provider'

@Component({
	template: 	`
						<ion-content>

							<div>
								<ion-spinner></ion-spinner>
							</div>


							<ion-text>{{message | translate}}</ion-text>

							<div>
								<ion-button (click) = "cancel()" >
									{{'cancel' | translate}}
								</ion-button>
							</div>

						</ion-content>
				`,

	styles:		[`
					ion-content::part(scroll){
						display:			flex;
						flex-direction:		column;
						align-items:		center;
						justify-content:	center;
					}

					ion-content * {
						text-align:			center;
						margin:				1rem 0;
					}
				`]

})
export class WaitOrCancelModalComponent {

	@Input()
	public message			: string

	@Input()
	public set promise(p: Promise<unknown>) { this.setPromise(p) }

	public constructor(
		protected rccModalController: RccModalController
	){}

	setPromise(promise: Promise<unknown>){
		if(promise) promise.then( (x: unknown) => this.done(x) )
	}

	done(x: unknown) : void {
		this.rccModalController.dismiss({ done: x })
	}

	cancel() : void {
		this.rccModalController.dismiss()
	}

}
