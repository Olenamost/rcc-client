import 	{	Injectable			}				from '@angular/core'
import	{
			RccTranslationService
		}										from '../translations'
import	{
			RccModalController,
			RccLoadingController,
			RccAlertController,
			ModalConfig
		}										from '../modals-provider'

import	{
			WaitOrCancelModalComponent
		}										from './wait-or-cancel-modal.component'

import	{
			has,
			UserCanceledError
		}										from '@rcc/core'


/**
 * This service is meant to manage all overlays, that are ready to use.
 * For custom Modals use the respective Controller
 * ( {@link RccModalController}, {@link RccAlertController}, {@link RccToastControll} )
 *
 * TODO: This Service needs to be expanded so that features modules can reliably
 * use it without falling back to  {@link RccModalController} and similar.
  *
 *
 */
@Injectable()
export class RccOverlayService {


	public constructor(
		protected rccModalController	: RccModalController,
		protected rccAlertController	: RccAlertController,
		protected rccLoadingController	: RccLoadingController,
		protected rccTranslationService	: RccTranslationService
	){}

	public async alert(message: string) : Promise<void> {
		return void await this.rccAlertController.present({ message })
	}


	// confirm
	// alert

	/**
	 * Open a modal with a text and a cancel button and some waiting indicator.
	 * The Modal will stay open until,the provided promises settles or the user
	 * clicks the cancel button.
	 *
	 * @param message Text to be displayed
	 * @param promise Promise to be awaited
	 *
	 * @returns 	A Promise that resolves when the provided promise resolves
	 * 				and rejects when the provided promise rejects. In both cases
	 * 				using the values from the original promise.
	 * 				When the user presses the cancel button or closes the modal
	 * 				some other way, rejects with a {@link UserCanceledError}
	 */
	public async waitOrCancel(awaitedPromise: Promise<unknown>, message: string) : Promise<unknown> {

		let resolve			: (...args: unknown[]) => unknown
		let reject 			: (...args: unknown[]) => unknown

		const resultPromise	= new Promise<unknown>( (solve, ject) => { resolve = solve, reject = ject } )

		const result 		= await	this.rccModalController.present(
										WaitOrCancelModalComponent,
										{ message, promise: awaitedPromise }
									)

		has(result, 'done')
		?	resolve(result.done)
		:	reject( new UserCanceledError() )

		return await resultPromise
	}

	/**
	 * Displays a waiting indicator and block UI until provided promise is resolved.
	 *
	 * Note: If the provided promise rejects, the UI will stay blocked, so make sure to catch errors yourself.
	 *
	 * @param awaitedPromise 	Promise to wait for.
	 * @param message 			Message to be shown alongside the waiting indicator.
	 *
	 * @returns Promise resolving to the same value as the provided promise.
	 */
	public async wait(awaitedPromise: Promise<unknown>, message? : string) : Promise<unknown> {

		const loadingInstance = await this.rccLoadingController.present({ message })

		const result = await awaitedPromise

		loadingInstance.close()

		return result
	}

}


