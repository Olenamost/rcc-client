import	{	NgModule						}	from '@angular/core'
import	{	IonicModule						}	from '@ionic/angular'
import	{	TranslationsModule				}	from '../translations'
import	{	RccOverlayService				}	from './overlay.service'
import	{	WaitOrCancelModalComponent		}	from './wait-or-cancel-modal.component'



@NgModule({
	imports:[
		IonicModule,
		TranslationsModule
	],
	providers:[
		RccOverlayService
	],
	declarations:[
		WaitOrCancelModalComponent
	]
})
export class OverlaysModule {}
