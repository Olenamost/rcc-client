import 	{ Type 				}	from '@angular/core'
import	{ Observable		}	from 'rxjs'

/**
 * Positive numbers mean: show me at the n-th position from the start - if possible. <br/>
 * Negative numbers mean: show me at the n-th position from the end - if possible. <br/>
 * ```undefined``` means: show me anywhere after the positive positions, but before the negative positions.<br/>
 * ```null``` means: don't show me at all<br/>
 * 0 counts as positive number for now.
 *
 */
export interface WithPosition {
	position?		: number | undefined | null | ( () => number| undefined | null )
}

/**
 * When used to render a button, the component will be rendered instead.
 */
export interface CustomAction extends WithPosition {
	component		: Type<unknown>

	path?			: never
	handler?		: never
	data?			: never
}

/**
 * Every Action (except a {@link CustomAction}) must at least have an icon and a label:
 */
export interface BaseRepresentation {
	icon			: string
	label			: string
	role?			: string
	disabled?		: () => boolean
}


/**
 * This kind of action comes with a description.
 */
export interface WithDescription extends BaseRepresentation {
	description		: string
	childComponent?	: never
}

/**
 * This kind of action replaces the description with its own component.
 */
export interface WithChildComponent extends BaseRepresentation {
	childComponent	: Type<unknown>
	description?	: never
}


export type Representation = BaseRepresentation | WithDescription | WithChildComponent


/**
 * Action that initiates route changes.
 */
export interface PathBased {
	path			: string

	handler?		: never
	component?		: never
	data?			: never
}


/**
 * Action that calls some function.
 */
export interface HandlerBased {
	handler			: () => unknown
	successMessage?	: string
	failureMessage?	: string
	confirmMessage?	: string

	path?			: never
	component?		: never
	data?			: never
}

/**
 * Action that initiates a download.
 */
export interface DownloadBased {

	data			: string | Promise<string>	// JSON string that is converted to base64
	filename		: string					// Suggested filename for download

	path?			: never
	handler?		: never
	component?		: never
	mimeType?		: string
}

export type PathAction		= PathBased 	& Representation & WithPosition
export type HandlerAction 	= HandlerBased 	& Representation & WithPosition
export type DownloadAction	= DownloadBased	& Representation & WithPosition

/**
 * Actions with notifications subscribe to an observable, in order to draw
 * attention to themselves with a badge or similar, when the observable emits a
 * number or true.
 */
export interface WithNotification {
	notification	: Observable<boolean|number>
}

// export type Action = (HandlerAction | PathAction | CustomAction) & Partial<WithNotification>

// custom actions not supported at the moment:
export type Action = (HandlerAction | PathAction | DownloadAction) & Partial<WithNotification>




// Type guards

export function isPathAction(action: Action | CustomAction):  action is PathAction {
	return typeof (action as PathAction).path === 'string'
}

export function isHandlerAction(action: Action | CustomAction) : action is HandlerAction {
	return typeof (action as HandlerAction).handler === 'function'
}

export function isDownloadAction(action: Action | CustomAction) : action is DownloadAction {

	const downloadAction	: DownloadAction 	= action as DownloadAction
	const typeOfData 		: string | unknown 	= typeof downloadAction.data
	// typeof 'object' represents here a promise, used for
	// async generated text, such as pdf
	const hasData			: boolean 			= typeOfData === 'string' || typeOfData === 'object'
	const hasFilename		: boolean 			= typeof downloadAction.filename === 'string'

	return hasData && hasFilename
}

export function isCustomAction(action: Action | CustomAction) : action is HandlerAction {
	return typeof (action as CustomAction).component === 'function'
}

export function hasDescription(action: Action| CustomAction) : action is Action & WithDescription{
	return typeof (action as Action & WithDescription).description === 'string'
}

export function hasNotification(action: Action | CustomAction) : action is Action & WithNotification {
	return 'notification' in action && action.notification instanceof Observable
}
