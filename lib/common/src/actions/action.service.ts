import	{
			Injectable,
			Inject
		}							from '@angular/core'
import	{	DOCUMENT			}	from '@angular/common'
import	{	Router				}	from '@angular/router'
import	{
			RccAlertController,
			RccToastController
		}							from '../modals-provider'

import	{
			Action,
			PathAction,
			HandlerAction,
			DownloadAction,
			isPathAction,
			isHandlerAction,
			isDownloadAction
		}							from './actions.commons'


@Injectable({
	providedIn: 'root'
})
export class ActionService {

	public constructor(
		private rccAlertController	: RccAlertController,
		private rccToastController	: RccToastController,
		private router				: Router,
		@Inject(DOCUMENT)
		private document			: Document
	){}


	public executePathAction(action: PathAction) : void {
		void this.router.navigateByUrl(action.path)
	}

	/**
	 * Executes the given {@link HandlerAction}. If `confirmMessage` property is present,
	 * ask for confirmation beforehand. Triggers toasts with `successMessage`
	 * resp. `failureMessage`.
	 */
	public async executeHandlerAction(action: HandlerAction) : Promise<void> {

		if(action.confirmMessage) 		await this.rccAlertController.confirm(action.confirmMessage)

		try {
			await action.handler()
			if (action.successMessage)	void this.rccToastController.success(action.successMessage)
		}catch(e){
			if (action.failureMessage)	void this.rccToastController.failure(action.failureMessage)
		}

	}

	public async executeDownloadAction(action: DownloadAction) : Promise<void> {

		const element	: HTMLAnchorElement	= this.document.createElement('a')

		const data		: BlobPart			= await action.data
		const mimeType	: string			= action.mimeType || 'text/plain'
		const blob		: Blob				= new Blob([data], { type: mimeType })

		element.href 		= URL.createObjectURL(blob)
		element.download 	= action.filename

		element.click()

	}

	public async execute(action: Action): Promise<void> {
		if(isPathAction(action))		void this.executePathAction(action)
		if(isHandlerAction(action))		void await this.executeHandlerAction(action)
		if(isDownloadAction(action))	void await this.executeDownloadAction(action)
	}



}
