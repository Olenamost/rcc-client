import 	{	Component,
			Optional,
		}								from '@angular/core'
import	{	sortByKeyFn 			}	from '@rcc/core'
import	{
			HomePageEntries,
		}								from './home.commons'
import	{	NotificationService 	}	from '../notifications'
import	{	Action					}	from '../actions'

@Component({
	selector: 		'rcc-home',
	templateUrl: 	'home.page.html',
})
export class HomePageComponent {

	public constructor(
		@Optional()
		public entries				: HomePageEntries = [],
		public notificationService	: NotificationService,
	) {
		this.entries = entries || []
	}

	public get activeEntries(): Action[] {
		return 	this.entries
				.filter(
					(action: Action) => typeof action.position === 'function'
										?	['number', 'undefined'].includes(typeof action.position() )
										:	['number', 'undefined'].includes(typeof action.position )
				)
				.sort( sortByKeyFn('position') )
	}

}


