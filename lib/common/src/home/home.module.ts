import	{
			NgModule,
		} 								from '@angular/core'
import 	{ 	RouterModule 			}	from '@angular/router'
import	{
            MainMenuModule,
            provideMainMenuEntry
                                    }	from '../main-menu'
import	{	provideTranslationMap	}	from '../translations'
import	{	SharedModule 			}	from '../shared-module'
import	{	NotificationModule		}	from '../notifications'

import	{	HomePageComponent 		}	from './home.page'

import	de from './i18n/de.json'
import	en from './i18n/en.json'


const routes 		=	[
							{ path: '',	component: HomePageComponent },
						]

export class MenuEntryHomeComponent {}

const menuEntry	=	{
    position: 1,
    label: 'HOMEPAGE.MENU_ENTRY',
    icon: 'home',
    path: '/',
}



@NgModule({
	imports: [
		RouterModule.forChild(routes),
		MainMenuModule,
		NotificationModule.forChild(),
		SharedModule,
	],
    providers: [
        provideMainMenuEntry(menuEntry),
		provideTranslationMap('HOMEPAGE', { en, de })
    ],
	declarations: [
		HomePageComponent,
	],

})
export class HomePageModule { }
