
import	{
			Provider
		} 								from '@angular/core'

import	{
			Factory,
			getProvider
		}								from '../interfaces'

import	{	Action					}	from '../actions'


export class HomePageEntries extends Array<Action> {}


export function provideHomePageEntry( config : (Action | Factory<Action>) ): Provider {

	return 	getProvider(HomePageEntries, config, true)


}
