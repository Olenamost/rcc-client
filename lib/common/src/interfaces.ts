import	{
			Provider
		}								from '@angular/core'


export interface Factory<T> {
	deps:		unknown[],
	factory:	(...args:unknown[]) => T
}

export type ProductOrFactory<T> = T | Factory<T>


export function isFactory<T>(x:ProductOrFactory<T>) : x is Factory<T> {

	const test = x as Record<string,unknown>

	return (test.deps instanceof Array) && (typeof test.factory === 'function')
}


export function getProvider<T>(token: unknown, x : ProductOrFactory<T>, multi: boolean = false): Provider {

	return	isFactory(x)
			?	{
					provide: 	token,
					deps:		x.deps,
					useFactory:	x.factory,
					multi
				}

			:	{
					provide:	token,
					useValue:	x,
					multi
				}

}
