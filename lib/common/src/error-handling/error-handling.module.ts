import	{
			NgModule,
			Injectable,
			ErrorHandler,
		}							from '@angular/core'
import	{
			UserCanceledError,
			AssertionError,
			has
		}							from '@rcc/core'
import	{
			RccToastController,
			ModalProviderModule
		}							from '../modals-provider'

@Injectable()
export class RccErrorHandler extends ErrorHandler {

	public constructor(
		protected rccToastController	: RccToastController,
	){
		super()
	}

	handleError(error: unknown ): void{

		if(error instanceof UserCanceledError) 	return void this.rccToastController.info('CANCELED')

		if(error instanceof AssertionError) 	console.warn('AssertionError context: ', error.context)

		if(has(error, 'rejection')) return this.handleError(error.rejection)

		console.error(error)

	}

}

@NgModule({
	imports: [
		ModalProviderModule
	],

	providers :[
		{ provide: ErrorHandler, useClass: RccErrorHandler }
	]
})
export class ErrorHandlingModule {


}

