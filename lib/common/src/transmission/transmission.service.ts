import	{
			Injectable,
			Inject,
			Optional,
			OnDestroy,
		}								from '@angular/core'

import	{	SubscriptionLike		}	from 'rxjs'
import	{
			filter,
		}								from 'rxjs/operators'


import	{
			log,
			assert
		}								from '@rcc/core'

import	{
			RccTransmission,
			AbstractTransmissionService,
			TRANSMISSION_SERVICE
		}								from './transmission.common'

import	{
			RccOverlayService
		}								from '../overlays'

import	{	IncomingDataService		}	from '../incoming-data'



@Injectable()
export class RccTransmissionService extends AbstractTransmissionService implements OnDestroy {

	protected subscriptions : SubscriptionLike [] = []

	public constructor(
		@Optional() @Inject(TRANSMISSION_SERVICE)
		public 		transmissionServices	: AbstractTransmissionService[],
		protected	incomingDataService		: IncomingDataService,
		protected	rccOverlayService		: RccOverlayService,
	){
		super()
		this.listenToIncomingDataService()

		if(!transmissionServices)
			console.error('RccTransmissionService: No transmission service registered; you probably forgot to load a module providing such a service.')

	}



	public getMatchingTransmissionService(data: unknown) : AbstractTransmissionService {
		return this.transmissionServices.find( ts => log(ts.validateMeta(data), ts.constructor.name, data) )
	}




	public validateMeta(data:unknown):boolean {
		return !!this.getMatchingTransmissionService(data)
	}


	public async setup(data:unknown): Promise<RccTransmission> {

		log('TODO: Pick transmission services via settings')
		console.log(this.transmissionServices)

		// TODO: will fail after minimization!!:
		let ts = 	this.transmissionServices.find( ts => ts.constructor.name === 'CombinedTransmissionService' ) // TODO: choose from settings

		if(!ts){
			console.warn('Missing transmission service: CombinedTransmissionService', this.transmissionServices)
			ts = this.transmissionServices[0]
		}

		log('Handling transmission with ', ts.constructor.name)

		console.log(data)

		return await ts.setup(data)
	}


	/**
	 * Should not be accessed from the outside because it lacks UI-interaction;
	 * use {@link #receiveAndAnnounce} instead. TODO: Add UI-interaction
	 */
	public async listen(meta:unknown): Promise<unknown> {
		// TODO: maybe multiple transmission services can claim the config?
		// use settings to turn off transmission services

		// TODO
		console.warn('RccTransmissionService.listen() listening cannot be stopped, only be ignored. This should be added in the future.')

		const matching_ts	= this.getMatchingTransmissionService(meta)

		assert(matching_ts, 'RccTransmissionService.receive(): no matching TransmissionService found.')

		const data			= await matching_ts.listen(meta)

		return data
	}




	public async receiveAndAnnounce(meta:any): Promise<void> {

		console.log('receive And Announce', meta)

		const result = await 	this.rccOverlayService.waitOrCancel(
									this.listen(meta),
									'TRANSMISSION.LISTENING_WAIT_OR_CANCEL'
								)

		this.incomingDataService.next(result)

	}



	protected listenToIncomingDataService(): void {

		this.subscriptions.push(
			this.incomingDataService
			.pipe( filter( 	(data:any) 		=> this.validateMeta(data) ) )
			.subscribe( 	(config:any) 	=> this.receiveAndAnnounce(config) )
		)

	}



	public ngOnDestroy(): void {
		this.subscriptions.forEach( sub => sub.unsubscribe() )
	}

}
