import	{	InjectionToken			}	from '@angular/core'
export interface RccTransmission {
	meta		: (string|number)[]
	// status:	: string		TODO

	start()		: Promise<any>
	cancel()	: Promise<any>
}

export abstract class AbstractTransmissionService {

	public abstract validateMeta(	data : any	)	: boolean
	public abstract setup(			data : any	)	: Promise<RccTransmission>
	public abstract listen(			meta : any	)	: Promise<any>

}

export const TRANSMISSION_SERVICE = new InjectionToken<AbstractTransmissionService>('Some transmission service')
