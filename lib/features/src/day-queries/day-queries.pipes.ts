import	{
			Pipe,
			PipeTransform
		}									from '@angular/core'
import	{	Question					}	from '@rcc/core'
import	{	DayQueryService				}	from './day-query.service'
import	{	QuestionnaireService		}	from '../questions/questionnaire'
@Pipe({
	name	:	'hasAnAnswerForToday'
})
export class HasAnAnswerForTodayPipe implements PipeTransform{
	public constructor(
		public dayQueryService		: DayQueryService,
		public questionnaireService	: QuestionnaireService

		){}

	public async transform(questionOrId: Question | string) : Promise<boolean> {
		const question			=	questionOrId instanceof Question
									?	questionOrId
									:	await this.questionnaireService.get(questionOrId)
		const query 			=	await this.dayQueryService.getQueryControl(question)
		const hasAnswer			=	query && !!query.entry

		return hasAnswer

	}
}
