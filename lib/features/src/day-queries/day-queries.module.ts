import	{	NgModule					}	from '@angular/core'
import	{	provideRoutes				}	from '@angular/router'
import	{
			SharedModule,
			TranslationsModule,
			provideTranslationMap,
			provideHomePageEntry,
			overrideRoute
		}									from '@rcc/common'
import	{	QueriesModule				}	from '../queries'
import	{	QuestionnaireServiceModule			}	from '../questions'
import	{	JournalServiceModule		}	from '../entries'
import	{	DayQueryService				}	from './day-query.service'
import	{	TodaysOpenQuestion$			}	from './todays-open-questions.observable'
import	{	DayQueryRunPageComponent	}	from './day-query-run-page'
import	{	DayQueryPageComponent		}	from './day-query-page'
import	{	dayQueryRunPath				}	from './day-queries.commons'
import	{	HasAnAnswerForTodayPipe		}	from './day-queries.pipes'

import en from './i18n/en.json'
import de from './i18n/de.json'



const routes 			=	[
								{
									path:		dayQueryRunPath,
									component:	DayQueryRunPageComponent
								}
							]

const queryPageRoute	= 	{
								path: 		'query/:id',
								component: 	DayQueryPageComponent
							}

const homePageEntry		=	{
								deps: [TodaysOpenQuestion$],
								factory: (todaysOpenQuestion$:TodaysOpenQuestion$) =>({
									position:		2,
									label:			'DAY_QUERIES.HOME.LABEL',
									description:	'DAY_QUERIES.HOME.DESCRIPTION',
									icon:			'notification',
									path:			'day-query-run',
									notification:	todaysOpenQuestion$
								})
							}



/**
 * This Module handles questions that are due on a target day. (No momentary assessment)
 */
@NgModule({
	imports:[
		QueriesModule,
		SharedModule,
		TranslationsModule,
		QuestionnaireServiceModule,
		JournalServiceModule,
	],
	declarations:[
		DayQueryRunPageComponent,
		DayQueryPageComponent,
		HasAnAnswerForTodayPipe
	],
	exports:[
		DayQueryRunPageComponent,
		DayQueryPageComponent,
		HasAnAnswerForTodayPipe
	],
	providers:[
		DayQueryService,
		TodaysOpenQuestion$,
		provideTranslationMap('DAY_QUERIES', { en, de }),
		provideHomePageEntry(homePageEntry),
		provideRoutes(routes),
		overrideRoute(queryPageRoute)
	]
})
export class DayQueryModule{}
