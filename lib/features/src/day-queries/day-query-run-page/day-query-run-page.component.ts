import 	{
			Component,
			ViewChild,
			OnDestroy
		}     								from '@angular/core'

import	{
			FormControl
		}									from '@angular/forms'

import	{
			Router
		}									from '@angular/router'

import	{
			merge,
			Subscription,
			Observable,
			share
		}									from 'rxjs'

import	{
			RccCalendarComponent,
			PageHandler,
			CalendarTickConfig,
		}									from '@rcc/common'

import	{
			CalendarDateString,
		}									from '@rcc/core'
import	{
			QueryControl,
			QueryRunComponent
		}									from '../../queries'


import	{	DayQueryService					}	from '../day-query.service'
import	{	SymptomCheckMetaStoreService	}	from '../../symptom-checks'


@Component({
	templateUrl:   './day-query-run-page.component.html',
	styleUrls:     ['./day-query-run-page.component.css']
})
export class DayQueryRunPageComponent implements OnDestroy{

	@ViewChild(QueryRunComponent)
	public 	queryRunComponent : QueryRunComponent

	@ViewChild(RccCalendarComponent)
	public 	calendarComponent : RccCalendarComponent


	public 	showCalendar			= false

	public	pageHandlers			= new Array<PageHandler>()
	public	queryControlRecords		: Record<string, QueryControl[]>	= {}
	public	calendarTickConfig		: CalendarTickConfig 				= {}
	public	currentQueryControls	: QueryControl[]					= []

	public	today					= CalendarDateString.today()
	public 	endDate					= this.today
	public 	startDate				= CalendarDateString.daysBefore(this.endDate, 28)
	public	dateControl				= new FormControl(CalendarDateString.today())

	public	answer$					: Observable<unknown>
	public 	answerSubscription		: Subscription
	public	dateSubscription		: Subscription


	// stats:
	public allDoneSelectedDate		= false
	public doneSelectedDate			: number
	public totalSelectedDate		: number

	public allDoneToday				= false
	public doneToday				: number
	public totalToday				: number

	public constructor(
		public		dayQueryService					: DayQueryService,
		public		symptomCheckMetaStoreService	: SymptomCheckMetaStoreService,
		private		router							: Router
	){
		void this.update() // TODO update on start/end date changes!

		this.dateSubscription	=	this.dateControl.valueChanges
								//	.pipe( delay(500) )
									.subscribe( () => this.onDateChanges() )

		// TODO: get number of answered questions!
		// TOOO: done page
		// TODO: Translations

	}

	public async update(): Promise<void> {

		await this.updateQueryControlRecord()
		this.updateCurrentQueryControls()
		this.updatePageHandlers()
		this.updateStats()

	}


	private onDateChanges() : void {

		this.updateCurrentQueryControls()
		this.updatePageHandlers()
		this.updateStats()


	}

	// Data for pagination/progress:

	/**
	 * Gets the currently focussed page index of the {@link QueryRun Component}
	 */
	public get activePage(): number {

		if(!this.currentQueryControls)	return undefined
		if(!this.queryRunComponent)		return undefined

		const activeQueryControl  	=	this.queryRunComponent.activeQueryControl

		return 	activeQueryControl
				?	this.currentQueryControls.indexOf(activeQueryControl)
				:	this.currentQueryControls.length
	}

	/**
	 * Updates page handlers for pagination/progress from selected date's query controls.
	 * Needs to be called when query controls change.
	 */
	public updatePageHandlers() : void {

		if(!this.queryRunComponent) return void (this.pageHandlers = [])

		const size					=	this.currentQueryControls.length +1
		const queryHandlers 		= 	this.currentQueryControls.map( (queryControl, index) => ({
											label: 		(index+1).toString(),
											handler:	() => this.queryRunComponent.slider.slideTo(index)
										}))

		const doneHandler			=	{
											icon:		'end',
											handler:	() => this.queryRunComponent.slider.slideTo(size-1)
										}

		this.pageHandlers			=	[ ...queryHandlers, doneHandler ]

	}


	/**
	 * Gets the currently selected date: e.g. '2022-01-10'
	 */
	public get selectedDate() : string { return this.dateControl?.value  || null}



	/**
	 * Updates queryControls for all dates
	 */
	public async updateQueryControlRecord() : Promise<void>  {
		await this.symptomCheckMetaStoreService.ready

		const symptomChecks 		= 	this.symptomCheckMetaStoreService.items

		this.queryControlRecords 	= 	await this.dayQueryService.getQueryControls(symptomChecks, this.startDate, this.endDate)

		if(this.answerSubscription) this.answerSubscription.unsubscribe()

		const allQueryControls		=	Object.values(this.queryControlRecords).flat()
		const allChanges			=	allQueryControls.map( queryControl => queryControl.change$)

		this.answer$				=	merge(...allChanges).pipe(share())

		this.answerSubscription		=	this.answer$.subscribe( () => this.updateStats() )
	}

	/**
	 * Updates to set of {@link QueryControl | QueryControls} currently used in the slider.
	 */
	public updateCurrentQueryControls() : void {
		this.currentQueryControls = this.queryControlRecords[this.selectedDate] || undefined
	}


	/**
	 * Counts answers for all relevant questions for a given date (e.g. '2022-02-23').
	 * Returns an array; ``[number_of_answered_questions, number_of_total_questions]``
	 */
	public getQuestionStats(dateString : string = this.today) : [number,number] {

		const queryControls	= this.queryControlRecords[dateString]

		if(!queryControls) return null

		const total			= queryControls.length
		const done 			= queryControls.filter( queryControl => !!queryControl.entry ).length
		const stats			= [done, total]

		return stats as [number, number]

	}



	/**
	 * Calculates various stats used in the template, so they don't have to be recalculated on every change detection.
	 */
	public updateStats() : void {

		const dateStrings			= Object.keys(this.queryControlRecords)
		const questionStatsEntries	= dateStrings.map( dateString => [dateString, this.getQuestionStats(dateString)] as [string, [number, number]])
		const questionStats			= Object.fromEntries(questionStatsEntries)

		const statsSelectedDate		= questionStats[this.selectedDate]

		this.doneSelectedDate		= statsSelectedDate ? statsSelectedDate[0] : undefined
		this.totalSelectedDate		= statsSelectedDate ? statsSelectedDate[1] : undefined

		this.allDoneSelectedDate	= statsSelectedDate && (this.doneSelectedDate === this.totalSelectedDate)

		const statsToday			= questionStats[this.today]

		this.doneToday				= statsToday ? statsToday[0] : undefined
		this.totalToday				= statsToday ? statsToday[1] : undefined

		this.allDoneToday			= statsToday && this.doneToday === this.totalToday

		this.calendarTickConfig		= questionStats

	}




	/**
	 * Toggles calendar component.
	 */
	public toggleCalendar(toggle?: boolean): void {

		void this.calendarComponent.focus()

		this.showCalendar =	typeof toggle === 'boolean'
							?	toggle
							:	!this.showCalendar
	}


	/**
	 * Restart the run with todays date.
	 */
	public completeToday() : void {

		if(this.selectedDate !== this.today) this.dateControl.setValue(this.today)

		this.queryRunComponent.gotoFirstUnansweredQuery()
	}


	public ngOnDestroy() : void {
		[
			this.answerSubscription,
			this.dateSubscription
		]
		.forEach( sub => sub.unsubscribe() )
	}

	/**
	 * Called when query run is cancelled.
	 */
	public onCancel(): void {
		void this.router.navigate(['/'])
	}


	/**
	 * Called when query run is done.
	 */
	public onDone(): void {
		void this.router.navigate(['/'])
	}

}
