import	{	Injectable			}	from '@angular/core'
import	{
			getDayOfWeek,
			Entry,
			Question,
			SymptomCheck,
			CalendarDateString

		}							from '@rcc/core'
import	{
			BehaviorSubject
		}							from 'rxjs'
import	{
			EntryMetaStoreService,
			JournalService
		}							from '../entries'
import	{	QueryControl		}	from '../queries'
import	{	QuestionnaireService }	from '../questions'


/**
 * This service will retrieve due questions, based on target days.
 * For details see main method below: {@link DayQueryService.html#getQueryControls|DayQueryService.getQueryControls}
 */
@Injectable()
export class DayQueryService {

	public ready			: Promise<unknown>

	protected 	openQuestions	: BehaviorSubject<number> = new BehaviorSubject(1)
	public 		openQuestion$	= this.openQuestions.asObservable()

	public constructor(
		protected entryMetaStoreService	: EntryMetaStoreService,
		protected journalService 		: JournalService,
		protected questionnaireService	: QuestionnaireService
	){
		this.ready = 	Promise.all([
							this.entryMetaStoreService,
							this.journalService.ready,
							this.questionnaireService.ready
						])
	}




	/**
	 * Gets all {@link Question}s due at the target day according to at
	 * least one of the provided {@link SymptomCheck}s. Questions will only
	 * appear once per day even if due for multiple symptom checks.
	 */
	public async getDueQuestions(symptomChecks: SymptomCheck[], targetDay: string): Promise<Question[]> {

		// Read daysOfWeek array as 'restricted to the following days'.
		// An empty array means no restrictions.
		const matchesDaysOfWeek	=	function(daysOfWeek: number[], targetDayStr: string): boolean {
										return 		daysOfWeek.length === 0										// empty means all days are fine, no limitations:
												|| 	daysOfWeek.includes(getDayOfWeek(targetDayStr) ) 	// restricted to one or more days, most likely to a single day for weekly questions:
									}

		const questionIds		=	symptomChecks
									.map( 		({ questionSchedules }) 	=> questionSchedules)
									.flat()
									.filter( 	({ schedule }) 			=> matchesDaysOfWeek(schedule.daysOfWeek, targetDay) )
									.map(		({ questionId }) 			=> questionId )


		await this.questionnaireService.ready

		const questions			=	this.questionnaireService.items.filter( question => questionIds.includes( question.id))

		return questions
	}



	/**
	 * Creates a {@link QueryControl} for the given {@link question} and target
	 * day. The query control is set up to write answers to the {@link JournalService}
	 * on submit. Also it will check if the question already has a stored answer
	 * for the given day and if so, assigns it to the returned query control
	 * {@link QueryControl.entry}.
	 */
	public async getQueryControl(question: Question, targetDay: string = CalendarDateString.today() ): Promise<QueryControl> {

		await this.entryMetaStoreService.ready

		const initialEntry		=	this.entryMetaStoreService.items.find( entry =>
											entry.questionId	=== question.id
										&&	entry.targetDay		=== targetDay
									) || null


		// backup the initial data, the entry itself might get changed
		// const initialEntryCopy	= 	initialEntry
		// 							?	new Entry(initialEntry.config)
		// 							:	null


		// Determines, what happens when the {@link QueryControl} submits:
		const submitFn			=	async (questionId:string, answer:string|number|boolean, note?:string): Promise<Entry> => await this.journalService.logForDay(targetDay, questionId, answer, note)

		// TODO: what to do if the entry come from a source that is undeletable, like example entries?


		// Determines, what happens when the {@link QueryControl},
		// wants to clean up an entry, that is no longer needed;
		// e.g. after multiple submissions or a reset.
		const cleanUpFn			=	async (entry: Entry) : Promise<Entry> => {

										// nothing to clean up:
										if(!entry) return null

										// There was an initial entry or some new entry
										// was created in the process,
										// that is no longer needed, so remove it:
										return await this.journalService.removeEntry(entry)

									}


		return 	new QueryControl(
					question,
					submitFn,
					cleanUpFn,
					initialEntry
				)
	}

	/**
	 * This method will retrieve a {@link QueryControl} for every day and for
	 * every {@link Question} that is due at that day according to the provided
	 * {@link Symptomcheck}s. Each question a day will give rise to at most one
	 * QueryControl – even if due for multiple symptom checks.
	 *
	 * Note: The other methods of this service are not async; they will just
	 * assume that all other relevant services have finished initializing.
	 * Calling them without waiting for {@link DayQueryService.html#ready} can cause problems.
	 * That's why none of them are public.
	 *
	 * Will return a dictionary of QueryControls, where the keys are date strings
	 * (YYYY-MM-DD, e.g, ```'2022-02-05'```). Its values are arrays of query
	 * controls.
	 */
	public async getQueryControls(

		symptomChecks	: SymptomCheck[],
		/**
		 * Defaults to today.
		 */
		startDateStr	: string = CalendarDateString.today(),
		/**
		 * Defaults to start date.
		 */
		endDateStr		: string = startDateStr

	): Promise<Record<string,QueryControl[]>> {

		CalendarDateString.assert(startDateStr,	'DayQueryService.getQueryControls()')
		CalendarDateString.assert(endDateStr,	'DayQueryService.getQueryControls()')

		await this.ready

		const dateStrings: string[]	= CalendarDateString.range(startDateStr, endDateStr)
		const dayQueryControls	:	Record<string, QueryControl[]> = {}

		await Promise.all( dateStrings.map( async (targetDay) =>  {

			const dayQuestions	:	Question[]	=	await this.getDueQuestions(symptomChecks, targetDay)
			dayQueryControls[targetDay]	=	await Promise.all( dayQuestions.map( question => this.getQueryControl(question, targetDay) ) )

		}))

		return dayQueryControls

	}

}
