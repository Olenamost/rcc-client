import	{
			Injectable,
			Inject
		}										from '@angular/core'

import	{
			DOCUMENT
		}										from '@angular/common'

import	{
			BehaviorSubject,
			Observable,
			merge,
			mergeMap,
			auditTime,
			fromEvent
		}										from 'rxjs'

import	{
			CalendarDateString,
			assert
		}										from '@rcc/core'

import	{	DayQueryService					}	from '../day-queries'
import	{	EntryMetaStoreService			}	from '../entries'
import	{	SymptomCheckMetaStoreService	}	from '../symptom-checks'

@Injectable()
export class TodaysOpenQuestion$ extends Observable<number> {


	public currentDate$ 			= new BehaviorSubject<string>(null)
	public numberOfOpenQuestion$ 	= new BehaviorSubject<number>(0)

	public constructor(
		@Inject(DOCUMENT)
		protected document							: Document,
		protected symptomCheckMetaStoreService		: SymptomCheckMetaStoreService,
		protected entryMetaStoreService				: EntryMetaStoreService,
		protected dayQueryService					: DayQueryService,
	){


		super( subscriber => this.numberOfOpenQuestion$.subscribe(subscriber) )

		const visibilityChange$ = fromEvent<Event>(this.document, 'visibilitychange')

		merge(
			this.symptomCheckMetaStoreService.change$,
			this.entryMetaStoreService.change$,
			this.currentDate$,
			visibilityChange$
		)
		.pipe(
			auditTime(1000),
			mergeMap( () => this.dayQueryService.getQueryControls(this.symptomCheckMetaStoreService.items) ),
		)
		.subscribe( queryControlRecord => {

			console.log('TodaysOpenQuestion$: got new Record')

			const dateString 			= Object.keys(queryControlRecord)[0]

			assert(dateString, 'TodaysOpenQuestion$: unable to get date from QueryControlRecord.', queryControlRecord)

			this.setDate(dateString)

			const queryControls			= queryControlRecord[dateString]

			const numberOfOpenQuestions = queryControls.filter( ({ entry }) => !entry ).length

			this.setNumberOfOpenQuestions(numberOfOpenQuestions)
		})

		this.trackDate()
	}

	protected setNumberOfOpenQuestions( n : number) : void {
		if(this.numberOfOpenQuestion$.value !== n) this.numberOfOpenQuestion$.next(n)
	}

	protected setDate(dateString : string) : void {
		if(this.currentDate$.value !== dateString) this.currentDate$.next(dateString)
	}

	protected trackDate() : void {

		this.setDate(CalendarDateString.today())

		setTimeout(
			() => this.trackDate(),
			1000*60
		)

	}


}
