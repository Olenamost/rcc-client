import	{	InjectionToken				}	from '@angular/core'

import	{
			isErrorFree,
			assertProperty,
			assert
		}									from '@rcc/core'



export type CmbTransmissionMeta = ['rcc-cmb', string, string] // ["cmb-wrt", channel, key]


export function assertCmbTransmissionMeta( x : unknown): asserts x is CmbTransmissionMeta {

	assert(x instanceof Array,		'assertCmbTransmissionMeta: must be an array.')
	assert(x[0] === 'rcc-cmb',		'isCmbTransmissionMeta: [0] must be \'rcc-rtc.')
	assert(typeof x[1] === 'string',	'isCmbTransmissionMeta: [1] must be string.')
	assert(typeof x[2] === 'string',	'isCmbTransmissionMeta: [2] must be string.')
}


export function isCmbTransmissionMeta( x : unknown): x is CmbTransmissionMeta {

	return isErrorFree( () => assertCmbTransmissionMeta(x))
}



export interface CmbTransmissionConfig {
	data			: unknown,
	signalServer	: string
	stunServers		: string[]
}


export function assertCmbTransmissionConfig(x : unknown): asserts x is CmbTransmissionConfig {


	assertProperty(x, 'data')
	assertProperty(x, 'signalServer')

	assert(typeof x.signalServer === 'string',	'assertCmbTransmissionConfig: url must be a string.')
	assert(x.signalServer.match(/^wss:/),		'assertCmbTransmissionConfig: url must start with \'wss:\'.')

}


export function isCmbTransmissionConfig(x : unknown): x is CmbTransmissionConfig {

	return isErrorFree(  () => assertCmbTransmissionConfig(x) )

}



export const CMB_SIGNAL_SERVER 	= new InjectionToken<string>('cmb: url of the websocket server for webrtc ice negotiation')
export const CMB_STUN_SERVERS 	= new InjectionToken<string[]>('cmb: url of the stun servers for webrtc ice negotiation')
