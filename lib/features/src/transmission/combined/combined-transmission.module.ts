import	{
			NgModule,
			ModuleWithProviders
		}										from '@angular/core'
import	{
			DevModule,
			TransmissionModule,
			IncomingDataServiceModule
		}										from '@rcc/common'

import	{	CombinedTransmissionService	}		from './combined-transmission.service'

import	{
			CMB_SIGNAL_SERVER,
			CMB_STUN_SERVERS
		}										from './combined-transmission.commons'





@NgModule({
	providers: [
		CombinedTransmissionService
	],
	imports: [
		DevModule.note('CombinedTransmissionServiceModule'),
		TransmissionModule.forChild(CombinedTransmissionService),
		IncomingDataServiceModule
	],
})
export class CombinedTransmissionModule {


	public static forRoot(url:string, stunServers: string[]): ModuleWithProviders<CombinedTransmissionModule> {

		return 	{
					ngModule: 	CombinedTransmissionModule,
					providers:	[
									{
										provide: 	CMB_SIGNAL_SERVER,
										useValue: 	url
									},
									{
										provide: 	CMB_STUN_SERVERS,
										useValue: 	stunServers
									},

								]
				}
	}
}
