import 	{
			Injectable,
			Inject,
		} 									from '@angular/core'

import	{
			firstValueFrom,
		}									from 'rxjs'

import	{
			cycleString,
			EncryptionHandler,
			GcmHandler,
			randomString,
			RccRtc,
			RccWebSocket,
			throwError,
			timeoutPromise,
			UserCancel,
			UserCanceledError
		}									from '@rcc/core'
import	{
			RccTransmission,
			AbstractTransmissionService
		}									from '@rcc/common'


import	{
			CmbTransmissionMeta,
			assertCmbTransmissionMeta,
			isCmbTransmissionMeta,
			CmbTransmissionConfig,
			assertCmbTransmissionConfig,
			CMB_SIGNAL_SERVER,
			CMB_STUN_SERVERS
		}									from './combined-transmission.commons'


export class CombinedTransmission implements RccTransmission{


	protected data				: unknown
	protected url				: string
	protected channel			: string



	protected signalServer		: string
	protected stunServers		: string[]

	protected key				: string
	protected encryptionHandler : EncryptionHandler

	protected rccRtc			: RccRtc
	protected rccWebSocket		: RccWebSocket


	public ready				: Promise<void>

	public userCancel			= new UserCancel('CombinedTransmission: user canceled.')


	public constructor(config: CmbTransmissionConfig){

		assertCmbTransmissionConfig(config)

		this.data			=	config.data

		this.data			=	config.data
		this.signalServer	=	config.signalServer
		this.stunServers	=	config.stunServers
		this.channel		=	randomString(20)

		this.ready	 		= 	this.setup()

	}


	public async setup() : Promise<void>{

		const encryption_setup	=	await GcmHandler.generate()

		this.key				=	encryption_setup[0]
		const encryptionHandler	= 	encryption_setup[1]

		const url				=	this.signalServer
		const stunServers		=	this.stunServers
		const channel			=	this.channel
		const fallbackChannel	=	cycleString(channel)
									// Nothing special happening here; I just want the fallback channel name
									// to be different but derivable from the channel name, in a way that you cannot easily
									// distinguish regular channel names from fallback channel names.

		this.rccRtc 			=	new RccRtc({
										url,
										channel,
										encryptionHandler,
										stunServers
									})

		this.rccWebSocket		=	new RccWebSocket({
										url,
										encryptionHandler,
										channel: 	fallbackChannel,
									})

	}

	public get meta() : CmbTransmissionMeta {
		return ['rcc-cmb', this.channel, this.key]
	}





	public async startRtcTransmission() : Promise<void> {

		await 	this.rccRtc.open({
					timeoutPeer: 		60*1000,
					timeoutConnection:	5 *1000
				})


		console.log('webrtc connection opened...')

		await this.rccRtc.send(this.data)

		console.log('webrtc: data sent')

		await this.rccRtc.done()

		this.rccRtc.close()
	}


	public async startWebsocketTransmission() : Promise<void> {

		await this.rccWebSocket.open()

		await this.rccWebSocket.send(this.data)

		await this.rccWebSocket.done()

		this.rccWebSocket.close()
	}


	public async start(): Promise<void>{

		try{ await this.startRtcTransmission() }

		catch(e) {

			if( e instanceof UserCanceledError ) throwError(e)

			this.rccRtc.close().catch(console.log)
			this.rccRtc.signalReliable || throwError('CombinedTransmission.start() failed due to WebSocket failure: '+ (e && e.message), e)

			console.log('Signaling worked but no RTC connection was established. Falling back to websocket transmission.')

			await 	Promise.race([
						this.startWebsocketTransmission(),
						timeoutPromise(10000)
					]) // TODO shorter timeout?

		}
	}

	public async cancel() : Promise<void>{
		// TODO catch errors
		this.userCancel.cancel()
		this.rccRtc.cancel()
		this.rccWebSocket.close()
	}

}




@Injectable()
export class CombinedTransmissionService extends AbstractTransmissionService {


	public constructor(
		@Inject(CMB_STUN_SERVERS)
		protected stunServers	: string[],
		@Inject(CMB_SIGNAL_SERVER)
		protected signalServer	: string
	){
		super()
	}



	public validateMeta(meta:any) {
		return isCmbTransmissionMeta(meta)
	}



	public async setup(data:any) : Promise<CombinedTransmission> {

		const stunServers	= this.stunServers
		const signalServer 	= this.signalServer

		const transmission 	= new CombinedTransmission({ data, signalServer, stunServers })

		await transmission.ready

		return transmission
	}



	public async listen(meta: CmbTransmissionMeta) : Promise<any> {

		assertCmbTransmissionMeta(meta)

		const [channel, key]		=	meta.slice(1)
		const fallbackChannel		=	cycleString(channel) // see above
		const url					=	this.signalServer
		const stunServers			=	this.stunServers
		const encryptionHandler		=	new GcmHandler(key)


		const rccRtc				= 	new RccRtc({ url, channel, encryptionHandler, stunServers })
		const rccWebSocket 			= 	new RccWebSocket({ url, channel:fallbackChannel, encryptionHandler })



		// TODO split into two methods:
		try {

			const data 		= 	firstValueFrom(rccRtc.data$)

			await 	rccRtc.open({
						timeoutPeer: 		60*1000,
						timeoutConnection:	5 *1000
					})

			const result	= 	await data

			rccRtc.done()
			.then( () => rccRtc.close() )
			.catch(console.log)

			console.log('WebRTC worked')

			return result

		} catch(e) {

			rccRtc.signalReliable || throwError('RtcTransmissionService.listen(): failed due to signaling failure.')

			console.log('Signaling worked, but no rtc connection was established. Falling back to websockets.')

			const data 		= 	firstValueFrom(rccWebSocket.data$)

			await rccWebSocket.open()

			const result	= 	await data

			rccWebSocket.done()
			.then( () => rccWebSocket.close() )
			.catch(console.log)


			return result

		}


	}

}
