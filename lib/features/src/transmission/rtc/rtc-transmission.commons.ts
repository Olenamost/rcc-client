import	{	InjectionToken		}	from '@angular/core'

import	{
			isErrorFree,
			assert,
			assertProperty
		}							from '@rcc/core'


export type RtcTransmissionMeta = ['rcc-wrt', string, string] // ["rcc-wrt", channel, key]

export function isRtcTransmissionMeta(x:unknown, throw_errors = false): x is RtcTransmissionMeta {

	if(!throw_errors) return isErrorFree( () => isRtcTransmissionMeta(x, true))

	assert(x instanceof Array,		'isRtcTransmissionMeta: must be an array.')
	assert(x[0] === 'rcc-rtc',		'isRtcTransmissionMeta: [0] must be \'rcc-rtc.')
	assert(typeof x[1] === 'string',	'isRtcTransmissionMeta: [1] must be string.')
	assert(typeof x[2] === 'string',	'isRtcTransmissionMeta: [2] must be string.')

}


export interface RtcTransmissionConfig {
	data			: any,
	signalServer	: string
	stunServers		: string[]
}



export function isRtcTransmissionConfig(x:unknown, throw_errors = false): x is RtcTransmissionConfig {

	if(!throw_errors) return isErrorFree( () => isRtcTransmissionConfig(x, true))

	assertProperty(x, 'data',					'isRtcTransmissionConfig: missing data.')
	assertProperty(x, 'signalServer',			'isRtcTransmissionConfig: missing url.')
	assert(typeof x.signalServer === 'string',	'isRtcTransmissionConfig: url must be a string.')
	assert(x.signalServer.match(/^wss:/),		'isRtcTransmissionConfig: url must start with \'wss:\'.')

}

export const RTC_SIGNAL_SERVER 	= new InjectionToken<string>('url of the websocket server for webrtc ice negotiation')
export const RTC_STUN_SERVERS 	= new InjectionToken<string[]>('url of the stun servers for webrtc ice negotiation')
