import	{	InjectionToken		}	from '@angular/core'
import	{
			isErrorFree,
			assertProperty,
			assert
		}							from '@rcc/core'


export type WsTransmissionMeta = ['rcc-wst', string, string] // ["rcc-wst", channel, key]

export function isWsTransmissionMeta(x:unknown, throw_errors = true): x is WsTransmissionMeta {

	if(!throw_errors) return isErrorFree( () => isWsTransmissionMeta(x, true))

	if(! (x instanceof Array)) 	throw new Error('isWstMeta: must be an array.')
	if(x[0] !== 'rcc-wst')		throw new Error('isWstMeta: [0] must be \'rcc-wst.')
	if(typeof x[1] !== 'string')	throw new Error('isWstMeta: [1] must be string.')
	if(typeof x[2] !== 'string') throw new Error('isWstMeta: [2] must be string.')

	return true
}



export interface WsTransmissionConfig {
	data	:	unknown,
	url		:	string
}

export function isWsTransmissionConfig(x:unknown, throw_errors = false): x is WsTransmissionConfig {

	if(!throw_errors) return isErrorFree( () => isWsTransmissionConfig(x, true))


	assertProperty(x, 'url', 			'isWsTransmissionConfig: missing url.')

	assert(typeof x.url === 'string',	'isWsTransmissionConfig: url must be a string.')
	assert(x.url.match(/^wss:/),		'isWsTransmissionConfig: url must start with \'wss:\'.')

	assertProperty(x, 'data',			'isWsTransmissionConfig: missing data.')
}




export const WEBSOCKET_DEFAULT_URL = new InjectionToken<string>('url of the websocket server')
