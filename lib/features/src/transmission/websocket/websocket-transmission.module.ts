import	{
			NgModule,
			ModuleWithProviders
		}										from '@angular/core'
import	{
			DevModule,
			TransmissionModule,
			IncomingDataServiceModule
		}										from '@rcc/common'
import	{	WebSocketTransmissionService	}	from './websocket-transmission.service'
import	{	WEBSOCKET_DEFAULT_URL			}	from './websocket-transmission.commons'




@NgModule({
	providers: [
		WebSocketTransmissionService
	],
	imports: [
		DevModule.note('WebsocketTransmissionModule'),
		TransmissionModule.forChild(WebSocketTransmissionService),
		IncomingDataServiceModule
	],
})
export class WebSocketTransmissionModule {


	public static forRoot(url:string): ModuleWithProviders<WebSocketTransmissionModule> {

		return 	{
					ngModule: 	WebSocketTransmissionModule,
					providers:	[
									{
										provide: 	WEBSOCKET_DEFAULT_URL,
										useValue: 	url
									}
								]
				}
	}
}
