import 	{
			Injectable,
			Inject,
		} 									from '@angular/core'
import	{
			Subscription
		}									from 'rxjs'

import	{
			firstValueFrom,
		}									from 'rxjs'

import	{
			EncryptionHandler,
			GcmHandler,
			RccWebSocket,
			randomString,
			throwError,
		}									from '@rcc/core'

import	{
			RccTransmission,
			AbstractTransmissionService,

		}									from '@rcc/common'

import	{
			WsTransmissionConfig,
			WsTransmissionMeta,
			isWsTransmissionConfig,
			isWsTransmissionMeta,
			WEBSOCKET_DEFAULT_URL
		}									from './websocket-transmission.commons'


// TODO needs rework or removal, see combined



export class WebSocketTransmission implements RccTransmission{

	public data					: any
	public channel				: string
	public url					: string
	public ready				: Promise<any>
	// public cancelled			: boolean = false

	private key					: string
	private rccWebSocket		: RccWebSocket
	private subscriptions		: Subscription[]
	private encryptionHandler	: EncryptionHandler

	public constructor(config: WsTransmissionConfig){

		isWsTransmissionConfig(config, false) || throwError(new Error('WebSocketTransmission.constructor() invalid config'))

		this.data		=	config.data
		this.url		=	config.url
		this.channel	=	randomString(20)

		this.ready	 	= 	this.setup()

	}


	public async setup(): Promise<void>{

		const encryption_setup	=	await GcmHandler.generate()

		this.key				=	encryption_setup[0]
		const encryptionHandler	= 	encryption_setup[1]

		const url				=	this.url
		const channel			=	this.channel

		this.rccWebSocket 		=  	new RccWebSocket({
										url,
										channel,
										encryptionHandler,
									})

	}



	public get meta(): WsTransmissionMeta {

		return 	['rcc-wst', this.channel, this.key ]
	}


	public async start(): Promise<any> {


		await this.rccWebSocket.open()
		// at least one other party present

		await this.rccWebSocket.send(this.data)
		// got receipt

		await this.rccWebSocket.close()
		// done disconnecting
	}


	public async cancel(): Promise<any> {

		this.rccWebSocket.close()

	}

}




@Injectable()
export class WebSocketTransmissionService extends AbstractTransmissionService {


	public constructor(
		@Inject(WEBSOCKET_DEFAULT_URL)
		public defaultUrl	: string,
	){
		super()
	}



	public validateMeta(data:any): boolean{
		return isWsTransmissionMeta(data)
	}


	public async setup(data:any) : Promise<WebSocketTransmission> {

		this.defaultUrl || throwError(new Error('WebsocketTransmissionService.setup(): missing defaultUrl. Please use WebsocketTransmissionModule.forRoot(url) to set defaultUrl.'))

		const url 			= this.defaultUrl
		const transmission 	= new WebSocketTransmission({ data, url })

		await transmission.ready

		return transmission
	}




	public async listen(meta: WsTransmissionMeta) : Promise<any> {

		isWsTransmissionMeta(meta) || throwError(new Error('WebSocketTransmissionService.listen(): invalid meta data'))

		const [channel, key]		=	meta.slice(1)
		const encryptionHandler		=	new GcmHandler(key)
		const url					=	this.defaultUrl

		const rccWebSocket 			= 	new RccWebSocket({ url, channel, encryptionHandler })

		const data 					= 	firstValueFrom(rccWebSocket.data$)

		await rccWebSocket.open()

		const result				= 	await data

		rccWebSocket.close()

		return result

	}

}
