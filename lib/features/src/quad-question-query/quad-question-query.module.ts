import { NgModule }							from '@angular/core'
import {
	provideWidget,
	SharedModule,
	WidgetsModule
}											from '@rcc/common'
// import { BasicQuestionEditWidgetsModule }	from '../basic-question-edit-widgets'
import { QuadQuestionQueryWidgetComponent } from '../quad-question-query/quad-question-query-widget/quad-question-query-widget.component'

const widgets = [
	QuadQuestionQueryWidgetComponent,
]

@NgModule({
	imports: [
		SharedModule,
		WidgetsModule,
	],
	providers: [
		...widgets.map(provideWidget),
	],
	declarations: [
		...widgets,
	],
	exports: [
		...widgets,
	]
})
export class QuadQuestionQueryModule {}
