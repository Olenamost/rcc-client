import 	{
			Component,
			OnDestroy
		}										from '@angular/core'

import	{
			FormControl,
			FormArray,
			ValidationErrors,
			AbstractControl
		}										from '@angular/forms'

import	{
			merge,
			takeUntil,
			startWith,
			debounceTime,
			Subject
		}										from 'rxjs'

import	{
			RccTranslationService,
			WidgetComponent,
			RccModalController
		}										from '@rcc/common'


import	{
			QuestionConfig,
			uuidv4
		}										from '@rcc/core'

import	{
			QuestionEditControl,
		}										from '../../questions/question-edit-widgets'
import 	{ 	DrugListService 				} 	from './drug-list.service'

@Component({
	templateUrl : './medication-question-edit-widget.component.html'
})
export class MedicationQuestionEditWidgetComponent extends WidgetComponent<QuestionEditControl> implements OnDestroy {

	public static label 		= 'MEDICATION.QUESTION_EDIT_WIDGET.LABEL'

	public static controlType 	= QuestionEditControl

	public static widgetMatch(questionEditControl: QuestionEditControl): number {

		const config			= questionEditControl.questionConfig

		// New question, can still be handled:
		if(!config)				return 0

		const hasMedicationTag	= config.tags?.includes('rcc-medication')

		// Can still handle the question:
		if(!hasMedicationTag)	return 0

		const hasFourOptions	= config.options?.length === 4

		// Can still handle the question:
		if(!hasFourOptions)		return 0

		const typeMatch 		= config.type === 'integer'

		// Can still handle the question:
		if(!typeMatch)			return 0

		const values			= config.options.map( option => option.value )
		const valuesMatch		= values.sort().every( (value, index) => value === index)

		// Can still handle the question:
		if(!valuesMatch)		return 0

		// Made to handle questions like this:
		return 2
	}


	// INSTANCE"

	public wordingControl			=	new FormControl<string>('', this.validateWording())
	public useGenericWordingControl	=	new FormControl<boolean>(true)
	public drugControl				= 	new FormControl<string>('', this.validateDrug())
	public dosageControl			= 	new FormControl<string>('')


	public allForms					= 	new FormArray([
											this.wordingControl,
											this.useGenericWordingControl,
											this.drugControl,
											this.dosageControl
										])

	public drugList					=	new Array<string>()
	protected destroy$				=	new Subject<void>()

	public constructor(
		protected questionEditControl	: QuestionEditControl,
		protected rccTranslationService	: RccTranslationService,
		protected rccModalController	: RccModalController,
		protected drugListService		: DrugListService
	){

		super(questionEditControl)

		drugListService.getListOfDrugNames().then(names => this.drugList = names)

		questionEditControl.editForms = this.allForms
		this.updateInputsFromConfig()

		this.useGenericWordingControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.useGenericWordingControl.value)
		)
		.subscribe( useGenericWording => {

			if (useGenericWording)	this.wordingControl.disable()
			else					this.wordingControl.enable()

			if (useGenericWording)	this.drugControl.enable()
			else					this.drugControl.disable()

			if (useGenericWording)	this.dosageControl.enable()
			else					this.dosageControl.disable()

			if (useGenericWording)	this.drugControl.markAsTouched()

		})

		merge(
			this.drugControl.valueChanges,
			this.dosageControl.valueChanges
		).pipe(
			takeUntil(this.destroy$),
			debounceTime(300)
		)
		.subscribe( () => this.updateWording() )


		// Update question:
		this.allForms.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(null),
			debounceTime(300)
		)
		.subscribe( ( ) => this.updateConfigFromInputs() )

	}


	public updateWording() : void {
		const drug			= this.drugControl.value
		const dosage		= this.dosageControl.value
		const medication	= drug + (dosage ? `, ${dosage}`: '')

		const wording 		= this.rccTranslationService.translate('MEDICATION.QUESTION_EDIT_WIDGET.GENERIC_WORDING', { medication })

		this.wordingControl.setValue(wording)
	}


	public validateDrug(): (ac: AbstractControl<unknown>) => ValidationErrors| null {

		return (ac: AbstractControl<unknown>) : ValidationErrors| null => {

			const useGenericWording = this.useGenericWordingControl.value

			if(!useGenericWording) 	return null

			const value				= ac.value

			if(!value) return { 'MEDICATION.QUESTION_EDIT_WIDGET.ERRORS.MISSING_DRUG': true }
		}

	}
	public validateWording(): (ac: AbstractControl<unknown>) => ValidationErrors| null {

		return (ac: AbstractControl<unknown>) : ValidationErrors| null => {

			const useGenericWording = this.useGenericWordingControl?.value

			if(useGenericWording) 	return null

			const value				= ac.value

			if(!value) return { 'MEDICATION.QUESTION_EDIT_WIDGET.ERRORS.MISSING_WORDING': true }
		}

	}

	public updateInputsFromConfig() : void {

		const editConfig		= 	this.questionEditControl.questionConfig

		// const hasMedicationTag	= 	editConfig.tags?.includes('rcc-medication')

		const activeLanguage	= 	this.rccTranslationService.activeLanguage

		const wording			= 	editConfig.translations[activeLanguage]
									||
									editConfig.meaning

		const wordingPattern	=	this.rccTranslationService.translate('MEDICATION.QUESTION_EDIT_WIDGET.GENERIC_WORDING', { medication: '###' })
									.replace(/[^a-z#]/gi, '.')
									.replace(/###/g, '(.+)')

		const medicationRegex	=	new RegExp(wordingPattern)

		const medicationMatch	=	medicationRegex.exec(wording)

		const medicationGuess	=	medicationMatch && medicationMatch[1]
		const drugGuess			=	medicationGuess && medicationGuess.split(', ')[0]
		const dosageGuess		=	medicationGuess && medicationGuess.split(', ')[1]

		const useGenericWording	=	!wording || !!medicationGuess

		this.useGenericWordingControl.setValue(useGenericWording)
		this.drugControl.setValue(drugGuess || '')
		this.dosageControl.setValue(dosageGuess || '')

		this.wordingControl.setValue(wording)

	}

	public getConfigFromInputs() : QuestionConfig {

		const editConfig		=	this.questionEditControl.questionConfig

		const id				=	editConfig?.id || uuidv4()
		const type				=	'integer'

		const activeLanguage	= 	this.rccTranslationService.activeLanguage

		const meaning			=	this.wordingControl.value

		const translations		=	{ [activeLanguage] : meaning }

		const options			=	[2,0,1,3].map( value => {

										const meaning 		= this.rccTranslationService.translate(`MEDICATION.QUESTION_EDIT_WIDGET.OPTIONS_${value}`)
										const translations 	= { [activeLanguage] : meaning }

										return { value, meaning, translations }

									})

		const tags				=	Array.from( new Set([...editConfig.tags, 'rcc-medication']) )

		const config : QuestionConfig	= 	{ id, type, meaning, translations, tags, options }

		return config

	}

	public updateConfigFromInputs() : void {

		const config = this.getConfigFromInputs()

		this.questionEditControl.questionConfig = config

	}

	public ngOnDestroy() : void {

		this.destroy$.next()
		this.destroy$.complete()

	}

}
