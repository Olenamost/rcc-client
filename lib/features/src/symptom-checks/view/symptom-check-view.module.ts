import 	{ 	NgModule 								}	from '@angular/core'
import 	{ 	RouterModule 							}	from '@angular/router'


import	{
			SharedModule,
			ItemAction,
			provideItemAction,
			provideTranslationMap
		}												from '@rcc/common'

import	{
			SymptomCheck
		}												from '@rcc/core'

import	{	QuestionnaireServiceModule				}	from '../../questions'

import	{	SymptomCheckMetaStoreServiceModule		}	from '../meta-store'

import	{	SymptomCheckViewPageComponent			}	from './view-page/view-page.component'

import	{	DayQueryModule							}	from '../../day-queries'



import en from './i18n/en.json'
import de from './i18n/de.json'


const routes =	[
					{ path: 'symptom-checks/:id/view',	component: SymptomCheckViewPageComponent	},
				]

const viewItemAction : ItemAction	=	{
											itemClass:		SymptomCheck,
											role:			'details' as const,
											getAction:		(symptomCheck: SymptomCheck) => ({

												label: 		'SYMPTOM_CHECKS_VIEW.ACTIONS.VIEW.LABEL',
												path:		`symptom-checks/${symptomCheck.id}/view`,
												icon:		'view',
												role:		'details' as const,
												position:	1

											})
										}

@NgModule({
	declarations: [
		SymptomCheckViewPageComponent
	],
	imports: [
		SharedModule,
		QuestionnaireServiceModule,
		RouterModule.forChild(routes),
		SymptomCheckMetaStoreServiceModule,
		DayQueryModule
	],
	providers: [
		provideItemAction(viewItemAction),
		provideTranslationMap('SYMPTOM_CHECKS_VIEW', { en, de })
	],
	exports: [
		SymptomCheckViewPageComponent
	]
})
export class SymptomCheckViewModule {

}
