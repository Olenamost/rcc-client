import 	{
			NgModule,
			ModuleWithProviders,
			Type,
		} 											from '@angular/core'

import 	{ 	RouterModule 			}				from '@angular/router'

import	{
			BaseMetaStoreModule,
			MetaAction,
			MetaStoreModule,
			provideItemRepresentation,
			provideMainMenuEntry,
			provideTranslationMap,
			SharedModule,
		}											from '@rcc/common'

import	{
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore,
		}											from '@rcc/core'


import	{	SymptomCheckMetaStoreService		}	from './symptom-check-meta-store.service'

import	{
			SYMPTOM_CHECK_STORES,
			SYMPTOM_CHECK_META_ACTIONS,
			SymptomCheckHomePath
		}											from './symptom-check-meta-store.commons'

import	{	SymptomCheckMetaStorePageComponent 	}	from './overview-page/overview-page.component'
import	{	SymptomCheckLabelComponent			}	from './item-label/item-label.component'


import en from './i18n/en.json'
import de from './i18n/de.json'


const routes 				=	[
									{ path: SymptomCheckHomePath,	component: SymptomCheckMetaStorePageComponent },
								]

const metaStoreConfig 		=	{
									itemClass:			SymptomCheck,
									serviceClass:		SymptomCheckMetaStoreService
								}

const itemRepresentation	=	{
									itemClass:			SymptomCheck,
									icon:				'symptom-check',
									labelComponent:		SymptomCheckLabelComponent,
								}


export class MenuEntrySymptomCheckMetaStoreServiceComponent {}

const menuEntry		=	{
							position: 6,
							label: 'SYMPTOM_CHECK_META_STORE.MENU_ENTRY',
							icon: 'symptom-check',
							path: '/symptom-checks',
						}

@NgModule({
	declarations: [
		SymptomCheckMetaStorePageComponent,
		SymptomCheckLabelComponent
	],
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		MetaStoreModule.forChild(metaStoreConfig),
	],

	exports: [
		MetaStoreModule // So other Module need not import this specifically
	],

	providers:[
		provideMainMenuEntry(menuEntry),
		provideItemRepresentation(itemRepresentation),
		SymptomCheckMetaStoreService,
		provideTranslationMap('SYMPTOM_CHECK_META_STORE', { en, de })
	]
})
export class SymptomCheckMetaStoreServiceModule extends BaseMetaStoreModule {

	public static 	forChild(
				stores?			: Type<SymptomCheckStore>[],
				metaActions?	: MetaAction<SymptomCheckConfig, SymptomCheck, SymptomCheckStore>[]
			): ModuleWithProviders<SymptomCheckMetaStoreServiceModule> {


		const mwp	=	BaseMetaStoreModule.getModuleWithProviders<SymptomCheckMetaStoreServiceModule>(
							this,
							stores,
							metaActions,
							SYMPTOM_CHECK_STORES,
							SYMPTOM_CHECK_META_ACTIONS
						)

		return mwp

	}

}
