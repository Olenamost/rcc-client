import	{	InjectionToken 	}		from '@angular/core'
import 	{
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore
		}							from '@rcc/core'

import	{
			MetaAction
		}							from '@rcc/common'



export const SYMPTOM_CHECK_STORES 		= new InjectionToken<SymptomCheckStore>('SymptomCheck Stores')
export const SYMPTOM_CHECK_META_ACTIONS = new InjectionToken<MetaAction<SymptomCheckConfig, SymptomCheck, SymptomCheckStore>>('Meta actions for symptom checks.')


export const SymptomCheckHomePath = 'symptom-checks'
