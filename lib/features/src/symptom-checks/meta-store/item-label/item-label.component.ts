import	{	Component 				}	from '@angular/core'
import	{	SymptomCheck			}	from '@rcc/core'
import	{	ExposeTemplateComponent	}	from '@rcc/common'

@Component({
	templateUrl: 	'./item-label.component.html',
})
export class SymptomCheckLabelComponent extends ExposeTemplateComponent {
	public constructor(public symptomCheck: SymptomCheck){ super() }
}
