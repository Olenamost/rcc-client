import 	{
			Injectable,
			Inject,
			Optional
		} 								from '@angular/core'


import	{
			MetaStore,
			MetaAction
		}								from '@rcc/common'

import	{
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore,
			Question,

		}								from '@rcc/core'

import	{
			SYMPTOM_CHECK_STORES,
			SYMPTOM_CHECK_META_ACTIONS
		}								from './symptom-check-meta-store.commons'



@Injectable()
export class SymptomCheckMetaStoreService extends MetaStore<SymptomCheckConfig, SymptomCheck, SymptomCheckStore> {

	public readonly name = 'SYMPTOM_CHECK_META_STORE.NAME'

	public dueQuestions		: Question[]

	public constructor(
		@Optional() @Inject(SYMPTOM_CHECK_STORES)
		stores		: SymptomCheckStore[],

		@Optional() @Inject(SYMPTOM_CHECK_META_ACTIONS)
		metaActions	: MetaAction<SymptomCheckConfig, SymptomCheck, SymptomCheckStore>[]
	) {
		super(stores, metaActions)
	}

	public async getSymptomChecks(questionIds: string[]): Promise<SymptomCheck[]>{

		await this.ready

		return this.items.filter( symptomCheck 	=> 	symptomCheck.coversQuestionIds(questionIds) )

	}


	public async getDueQuestionsIds(date: Date = new Date() ): Promise<string[]> {

		await this.ready

		const array_of_ids 	= 	this.stores
								.map( (store: SymptomCheckStore) => store.getDueQuestionIds(date))
								.flat()

		const unique_ids	=	Array.from( new Set(array_of_ids) )

		return unique_ids

	}


}
