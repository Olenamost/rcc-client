import 	{
			Component,
		} 								from '@angular/core'

import	{	FormControl 			}	from '@angular/forms'

import	{
			SymptomCheckMetaStoreService
		}								from '../symptom-check-meta-store.service'


@Component({
	selector: 		'rcc-meta-store-page',
	templateUrl: 	'./overview-page.component.html',
	styleUrls: 		['./overview-page.component.scss'],
})
export class SymptomCheckMetaStorePageComponent {

	public filterControl 	: FormControl	= new FormControl()

	public constructor(
		public symptomCheckMetaStoreService: SymptomCheckMetaStoreService
	) { }

}
