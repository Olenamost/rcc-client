import 	{ 	Injectable } 		from '@angular/core'

import	{
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore
		}						from '@rcc/core'


import	{
			RccStorage,
			RccAlertController
		}						from '@rcc/common'



@Injectable()
export class CustomSymptomCheckStoreService extends SymptomCheckStore {

	public readonly name = 'CUSTOM_SYMPTOM_CHECK_STORE.NAME'

	public constructor(
		private	rccStorage			: RccStorage,
		private	rccAlertController	: RccAlertController


	){
		super(
			rccStorage.createItemStorage('rcc-custom-symptom-checks'),
		)

		this.update$.subscribe( () => void this.storeAll() )

	}


	public async addSymptomCheckConfig(config: SymptomCheckConfig): Promise<SymptomCheck> {

		const symptom_check = this.addConfig(config)

		return 	this.storeAll()
				.then( () => symptom_check)

	}


	public async deleteSymptomCheck(symptom_check: SymptomCheck): Promise<SymptomCheck> {

		if(!this.removeItem(symptom_check)) throw new Error('CustomSymptomCheckStoreService.delete: Unable to delete symptom check with id: ' + symptom_check.id)

		return 	this.storeAll()
				.then( () => symptom_check)
	}
}
