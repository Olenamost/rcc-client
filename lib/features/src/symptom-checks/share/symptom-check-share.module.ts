import	{	NgModule						}	from '@angular/core'
import	{
			ShareDataService,
			provideItemAction,
			ItemAction,
			Factory,
			provideTranslationMap
		}										from '@rcc/common'

import	{
			SymptomCheck,
		}										from '@rcc/core'
import	{
			QuestionnaireService
		}										from '../../questions'
import	{
			SymptomCheckMetaStoreServiceModule
		}										from '../meta-store'


import en from './i18n/en.json'
import de from './i18n/de.json'



const	itemActionFactory: Factory<ItemAction<SymptomCheck>>	=	{
	deps:		[ ShareDataService, QuestionnaireService],
	factory: 	( shareDataService	: ShareDataService, questionnaireService : QuestionnaireService) => ({

		id:			'SYMPTOM_CHECK.SHARE.QR',
		role:		'share' as const,
		itemClass:	SymptomCheck,


		getAction:	(symptomCheck: SymptomCheck) => ({

			label: 			'SYMPTOM_CHECK_SHARE.ACTIONS.SHARE',
			icon: 			'qr-code',

			handler:		async () => {

				const question_ids	= symptomCheck.questionSchedules.map( qs => qs.questionId)
				const questions		= await questionnaireService.get(question_ids)

				await shareDataService.share([
					symptomCheck.config,
					questions.map( question => question.config)
				])
			}

		}),


	})
}

@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild(null),
	],
	providers:[
		provideItemAction(itemActionFactory),
		provideTranslationMap('SYMPTOM_CHECK_SHARE', { en, de })
	]
})
export class SymptomCheckShareModule {

}
