import 	{
			Injectable,
			OnDestroy
		} 								from '@angular/core'

import	{	Router					}	from '@angular/router'

import	{	SubscriptionLike		}	from 'rxjs'
import	{
			map,
			filter
		}								from 'rxjs/operators'

import	{
			RccAlertController,
		}								from '@rcc/common'

import	{
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore,
		}								from '@rcc/core'


import	{
			RccStorage,
			IncomingDataService
		}								from '@rcc/common'

import	{	IcsReminderService		}	from '../../ics-reminders'


@Injectable()
export class ImportSymptomCheckStoreService extends SymptomCheckStore implements OnDestroy {

	public readonly name = 'IMPORT_SYMPTOM_CHECK_STORE.NAME'

	private subscriptions : SubscriptionLike[] = []

	public constructor(
		private incomingDataService	: IncomingDataService,
		private rccStorage			: RccStorage,
		private rccAlertController	: RccAlertController,
		private router				: Router,
		private icsReminderService	: IcsReminderService,
	){
		super(
			rccStorage.createItemStorage('rcc-import-symptom-checks'),
		)

		this.listenToIncomingDataService()
	}


	protected listenToIncomingDataService() : void {
		this.subscriptions.push(
			this.incomingDataService
			.pipe(
				map( 	(data:unknown) 								=> SymptomCheck.findConfigs(data) ),
				filter(	(symptomCheckConfigs: SymptomCheckConfig[])	=> symptomCheckConfigs.length > 0)
			)
			.subscribe( (symptomCheckConfigs) => void this.handleIncomingSymptomCheckConfigs(symptomCheckConfigs) )
		)
	}


	public ngOnDestroy() : void {
		this.subscriptions.forEach( sub => sub.unsubscribe() )
	}

	public async handleIncomingSymptomCheckConfigs(configs: SymptomCheckConfig[]) : Promise<void> {

		const symptom_checks = configs.map( config => new SymptomCheck(config) )

		await 	this.rccAlertController.present({
					header: 	'IMPORT_SYMPTOM_CHECK_STORE.SUCCESS.HEADER',
					message: 	symptom_checks.map( symptom_check => symptom_check.meta.label).join('<br/>'),
					buttons:	[
									{
										label:		'IMPORT_SYMPTOM_CHECK_STORE.SUCCESS.DISMISS',
										rejectAs: 	'dismiss'
									},
									{
										label:		'IMPORT_SYMPTOM_CHECK_STORE.SUCCESS.ACCEPT',
										resolveAs: 	'accept'
									}
								]
				})

		await this.addSymptomCheckConfig(configs)

		await this.rccAlertController.present({
					header: 	'IMPORT_SYMPTOM_CHECK_STORE.ADD_ICS_REMINDER.HEADER',
					message: 	'IMPORT_SYMPTOM_CHECK_STORE.ADD_ICS_REMINDER.DESCRIPTION',
					buttons:	[
									{
										label:		'IMPORT_SYMPTOM_CHECK_STORE.ADD_ICS_REMINDER.DISMISS',
										rejectAs: 	'dismiss'
									},
									{
										label:		'IMPORT_SYMPTOM_CHECK_STORE.ADD_ICS_REMINDER.CONFIRM',
										resolveAs: 	'ics'
									}
								]
				})


		const timeGuess = 	configs
							.map( config => config.meta?.reminder)
							.filter( x => !!x)[0]

		await this.icsReminderService.promptDownload({ timeString: timeGuess }, 'RecoveryCat.ics')

	}


	public async addSymptomCheckConfig(configs: SymptomCheckConfig[])	: Promise<SymptomCheck[]>
	public async addSymptomCheckConfig(config: 	SymptomCheckConfig)		: Promise<SymptomCheck>

	public async addSymptomCheckConfig(x: 		SymptomCheckConfig | SymptomCheckConfig[] )	: Promise<SymptomCheck|SymptomCheck[]>
	{

		if(!Array.isArray(x)) return this.addSymptomCheckConfig([x]).then( arr => arr[0] )

		const symptom_checks = x.map( config => this.addConfig(config) )

		await this.storeAll()

		return symptom_checks

	}


	public async deleteSymptomCheck(symptom_check: SymptomCheck): Promise<any> {
		if(!this.removeItem(symptom_check)) throw new Error('ImportSymptomCheckStoreService.delete: Unable to delete symptom check with id: ' + symptom_check.id)

		return 	this.storeAll()
				.then( () => symptom_check)
	}


}
