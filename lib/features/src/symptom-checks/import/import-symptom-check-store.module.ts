import	{
			NgModule
		} 											from '@angular/core'

import	{
			SymptomCheck
		}											from '@rcc/core'

import	{
			IncomingDataServiceModule,
			provideItemAction,
			ItemAction,
			provideTranslationMap
		}									from '@rcc/common'

import	{	IcsRemindersServiceModule					}	from '../../ics-reminders' // temporary solution for bad push notification support
import	{	ImportSymptomCheckStoreService		}	from './import-symptom-check-store.service'
import	{	SymptomCheckMetaStoreServiceModule	}	from '../meta-store'

import en from './i18n/en.json'
import de from './i18n/de.json'


const itemAction: ItemAction<SymptomCheck> = 	{
							itemClass:		SymptomCheck,
							storeClass: 	ImportSymptomCheckStoreService,
							role:			'destructive' as const,
							getAction:		(symptomCheck: SymptomCheck, importSymptomCheckStoreService: ImportSymptomCheckStoreService) => ({

								label: 			'IMPORT_SYMPTOM_CHECK_STORE.ACTIONS.DELETE',
								icon:			'delete',
								successMessage:	'IMPORT_SYMPTOM_CHECK_STORE.ACTIONS.DELETE_SUCCESS',
								failureMessage:	'IMPORT_SYMPTOM_CHECK_STORE.ACTIONS.DELETE_FAILURE',
								confirmMessage: 'IMPORT_SYMPTOM_CHECK_STORE.ACTIONS.DELETE_CONFIRM',
								handler: 		() => importSymptomCheckStoreService.deleteSymptomCheck(symptomCheck)

							})

						}

@NgModule({
	providers: [
		ImportSymptomCheckStoreService,
		provideItemAction(itemAction),
		provideTranslationMap('IMPORT_SYMPTOM_CHECK_STORE', { en, de })
	],
	declarations: [

	],
	imports: [
		IcsRemindersServiceModule, // temporary
		SymptomCheckMetaStoreServiceModule.forChild([ImportSymptomCheckStoreService]),
		IncomingDataServiceModule
	]
})
export class ImportSymptomCheckStoreServiceModule { }
