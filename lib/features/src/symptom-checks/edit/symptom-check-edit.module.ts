import	{	NgModule					}	from '@angular/core'
import	{
			provideTranslationMap,
			SharedModule

		}									from '@rcc/common'

import	{	SymptomCheckEditComponent	}	from './symptom-check-edit.component'

import	{	SymptomCheckEditService		}	from './symptom-check-edit.service'

import	{	QuestionEditModule			}	from '../../questions'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	providers: [
		provideTranslationMap('SYMPTOM_CHECKS.EDIT', { en,de }),
		SymptomCheckEditService,
	],

	imports: [
		SharedModule,
		QuestionEditModule
	],
	declarations: [
		SymptomCheckEditComponent
	],

	exports: [
		SymptomCheckEditComponent
	]
})
export class SymptomCheckEditModule {



}
