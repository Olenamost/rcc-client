import 	{
			Component,
			Input,
			OnDestroy,
		}									from '@angular/core'


import	{
			FormControl,
		}									from '@angular/forms'

import	{
			takeUntil,
			Subject
		}									from 'rxjs'

import	{
			Question,
			Schedule,
			SymptomCheck,
		}									from '@rcc/core'

import	{
			ItemSelectService,
			RccAlertController,
			RccModalController,
			ModalWithResultComponent,
			ItemEditResult,
			ItemAction
		}									from '@rcc/common'

import	{
			ScheduleEditService
		}									from '../../schedules'


import	{
			QuestionnaireService,
			QuestionEditService
		}									from '../../questions'



/**
 * TODO
 */
@Component({
	selector:		'rcc-symptom-check-edit',
	templateUrl:	'./symptom-check-edit.component.html'

})
export class SymptomCheckEditComponent extends ModalWithResultComponent<ItemEditResult<SymptomCheck, Question[]>> implements OnDestroy{

	@Input()
	public set item(symptomCheck: SymptomCheck){ void this.updateFromSymptomCheck(symptomCheck) }

	@Input()
	public heading : string 		=	undefined


	@Input()
	public editLabel				=	true

	public id :string				=	undefined

	public creationDate : string	=	null

	public destroy$					=	new Subject<void>()

	public labelControl 			= 	new FormControl('')
	public reminderToggle			=	new FormControl<boolean>(false)
	public reminderControl			=	new FormControl<string>('12:00')

	public defaultSchedule			=	new Schedule()
	public questionSchedules		= 	new Map<Question, Schedule|null>()
	public customizedQuestions		=	new Map<Question, Question>()

	public questionActions : ItemAction<Question>[]	=	[
		{
			itemClass:	Question,
			role:		'destructive' as const,
			getAction:	(question: Question) => ({

				label 	: 	'SYMPTOM_CHECKS.EDIT.REMOVE_QUESTION',
				icon	: 	'remove',
				handler	: 	() => this.removeQuestion(question),
				position: 	1

			})
		},
		{
			itemClass:	Question,
			role:		'edit' as const,
			getAction:	(question: Question) => ({

				label: 		'SYMPTOM_CHECKS.EDIT.CUSTOMIZE_QUESTION',
				icon: 		'edit',
				handler: 	() => 	this.customizeQuestion(question),
				position: 	2

			})
		},
		{
			itemClass:	Question,
			role:		'edit' as const,
			getAction:	(question: Question) => ({

				label: 		'SYMPTOM_CHECKS.EDIT.RESTORE_QUESTION',
				icon: 		'restore',
				handler: 	() => 	this.restoreQuestion(question),
				position: 	() => 	this.customizedQuestions.has(question)
									?	2.5
									:	null

			})
		},
		{
			itemClass:	Question,
			role:		'edit' as const,
			getAction:	(question: Question) => ({
				label: 		'SYMPTOM_CHECKS.EDIT.SET_QUESTION_SCHEDULE',
				icon: 		'time',
				handler: 	() => this.editQuestionSchedule( question ),
				position: 	3,
				class:     	'rcc-e2e-schedule-action'

			})
		},
		{
			itemClass:	Question,
			role:		'destructive' as const,
			getAction:	(question: Question) => ({

				label: 		'SYMPTOM_CHECKS.EDIT.REMOVE_QUESTION_SCHEDULE',
				icon: 		'remove',
				handler: 	() => 	this.questionSchedules.set(question, null),
				position: 	() => 	this.questionSchedules.get(question)
									?	4
									:	null

			})
		},
	]

	public constructor(
		protected itemSelectService		: ItemSelectService,
		protected rccModalController	: RccModalController,
		protected questionnaireService	: QuestionnaireService,
		protected scheduleEditService	: ScheduleEditService,
		protected questionEditService	: QuestionEditService,
		protected rccAlertController	: RccAlertController,
	){
		super(rccModalController)


		this.reminderToggle.valueChanges
		.pipe( takeUntil(this.destroy$) )
		.subscribe(
			enable 	=> 	enable
						?	this.reminderControl.enable()
						:	this.reminderControl.disable()
		)

	}

	public async cancel(): Promise<void> {

		await this.rccAlertController.confirm(
				'CONFIRM_CANCEL',
				'YES',
				'NO'
				)

		super.cancel()
	}

	public get questions()	: Question[] { return Array.from(this.questionSchedules.keys()) }


	public async updateFromSymptomCheck(symptomCheck: SymptomCheck): Promise<void> {

		const symptomCheckCopy		=	new SymptomCheck(symptomCheck.config)

		this.labelControl.setValue(symptomCheckCopy.meta.label || null)
		this.reminderControl.setValue(symptomCheckCopy.meta.reminder || '12:00')
		this.reminderToggle.setValue(!!symptomCheckCopy.meta.reminder)
		this.defaultSchedule		=	symptomCheckCopy.meta.defaultSchedule
		this.id						=	symptomCheckCopy.id
		this.creationDate			=	symptomCheckCopy.config.meta.creationDate

		const questions				=	await this.questionnaireService.get(symptomCheckCopy.questionIds)

		symptomCheckCopy.questionSchedules.forEach( questionSchedule => {

			const question 	= 	questions.find( q => q.id === questionSchedule.questionId)
			const schedule	=	questionSchedule.schedule !== this.defaultSchedule
								?	questionSchedule.schedule
								:	null


			this.questionSchedules.set(question, schedule)
		})

		// remove questions that are not part of the new symptom check:
		this.updateQuestions(questions)
	}


	/**
	 * Updates list of questions, keeping the schedules of questions that are in
	 * the current and new list of questions.
	 */
	public updateQuestions(questions: Question[]): void {

		const entries				=	questions.map( question => {
											const schedule = this.questionSchedules.get(question) || null
											return [question, schedule]
										})

		this.questionSchedules 		=	new Map<Question, Schedule>(entries as [Question, Schedule|null][])
	}



	public async selectQuestions(): Promise<void> {

		const stores				=	this.questionnaireService.stores
		const preselect				=	this.questions
		const heading				=	'SYMPTOM_CHECKS.EDIT.SELECT_QUESTIONS'
		const questions 			=  	await 	this.itemSelectService
												.select({
													stores,
													preselect,
													heading
												})

		this.updateQuestions(questions)

	}



	public async editQuestionSchedule( question: Question ): Promise<void> {

		const schedule = this.questionSchedules.get(question) || new Schedule

		await this.scheduleEditService.edit(schedule)

		this.questionSchedules.set(question, schedule)

	}

	public async editDefaultSchedule():Promise<void>{

		const schedule			= this.defaultSchedule || new Schedule()

		await this.scheduleEditService.edit(schedule)

		this.defaultSchedule	= schedule

	}

	public removeQuestion(question: Question) : void {
		this.questionSchedules.delete(question)
		this.customizedQuestions.delete(question)
	}

	/**
	 * Opens a modal to create a new question. The result will be stored with the
	 * SymptomCheck and the default Schedule.
	 */
	public async addCustomQuestion(): Promise<void> {

		const result	=	await	this.questionEditService
									.create('SYMPTOM_CHECKS.EDIT.HEADER_ADD_CUSTOM_QUESTION')

		const question	=	result.item

		this.questionSchedules.set(question, this.defaultSchedule)
	}

	/**
	 * Opens a modal to edit the provided question. The provided question itself
	 * will not be changed; instead a new question with a different id using the
	 * edited config will be created. The original question is saved and can be
	 * restored at a later time ({@link #restoreQuestion}). Customizing an already
	 * customized question will only keep the original question; so you can only
	 * restore the original question, not any of the steps in between.
	 *
	 */
	public async customizeQuestion( question: Question): Promise<void> {

		const result 				= await	this.questionEditService
											.editCopy(
												question,
												'SYMPTOM_CHECKS.EDIT.HEADER_CUSTOMIZE_QUESTION'
											)

		const customized_question 	= result.item

		const original_question 	= this.customizedQuestions.get(question) || question

		this.customizedQuestions.delete(question)
		this.customizedQuestions.set(customized_question, original_question)

		const original_schedule		= this.questionSchedules.get(original_question)

		this.questionSchedules.delete(question)
		this.questionSchedules.set(customized_question, original_schedule)

	}



	public restoreQuestion( question: Question) : void {

		const original_question 	= this.customizedQuestions.get(question)

		if(!original_question) return null

		this.customizedQuestions.delete(question)

		const schedule				= this.questionSchedules.get(question)

		this.questionSchedules.delete(question)
		this.questionSchedules.set(original_question, schedule)


	}



	public getResult(): ItemEditResult<SymptomCheck, Question[]> {

		const id						=	this.id

		const meta						= 	{
												creationDate:		this.creationDate,
												defaultSchedule: 	this.defaultSchedule.config,
												label:				this.labelControl.value,
												reminder:			this.reminderToggle.value && this.reminderControl.value || null
											}

		const questions					=	Array.from( this.questionSchedules.keys() )

		const questionScheduleConfigs	=	questions.map( question => 	{

												const schedule = this.questionSchedules.get(question)

												return schedule
												?	{ id: question.id, schedule: schedule.config }
												:	question.id

											})

		const symptomCheck 				= new SymptomCheck({ id, meta, questions: questionScheduleConfigs })


		// question that have not yet been stored anywhere:
		const artifacts					= questions.filter( question => !this.questionnaireService.getStore(question) )

		return { item: symptomCheck, artifacts }
	}


	public ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}

}
