
import	{	Injectable					}	from '@angular/core'
import	{
			RccModalController,
			RccAlertController,
			ShareDataService
		}									from '@rcc/common'

import	{
			Report,
			SessionConfig,
			SymptomCheck,
			UserCanceledError
		}									from '@rcc/core'
import	{	ItemSelectService			}	from '@rcc/common'

import	{	QuestionnaireService		}	from '../../questions/questionnaire'
import	{	SymptomCheckMetaStoreService }	from '../../symptom-checks/meta-store'
import	{	EntryMetaStoreService		}	from '../meta-store'


/**
 * TODO: This whole service should be extendable, with sharing option that can be provided by other modules.
 */

@Injectable()
export class EntryShareService {


	public constructor(
		protected rccModalController				: RccModalController,
		protected rccAlertController				: RccAlertController,
		protected shareDataService					: ShareDataService,
		protected questionnaireService				: QuestionnaireService,
		protected entryMetaStoreService				: EntryMetaStoreService,
		protected itemSelectService					: ItemSelectService,
		protected symptomCheckMetaStoreService		: SymptomCheckMetaStoreService

	){}

	// public async share(): Promise<void>{

	// 	const data 		= await this.rccModalController.present(EntryShareModalComponent)

	// 	if(data === 'user canceled') return null //TODO: Maybe create special Object to represent cancellation by user.

	// 	assertProperty(data, 		'report', 		"EntryShareService.share() bad data:")
	// 	assertProperty(data.report, 'config', 		"EntryShareService.share() bad data.report:")
	// 	assertProperty(data, 		'questions', 	"EntryShareService.share() bad data:")
	// 	assert(Array.isArray(data.questions),		"EntryShareService.share() data.questions must be an array.")

	// 	await 	this.shareDataService.share([
	// 				data.report.config,
	// 				...data.questions.map( (question: Question) => question.config)
	// 			])
	// }

	public async getSessionConfigFromRawAnswers() : Promise<SessionConfig> {

		await this.entryMetaStoreService.ready

		const entries			=	this.entryMetaStoreService.items

		const questionIds 		= 	entries
									.map( entry => entry.questionId )
									.flat()

		const uniqueIds			=	Array.from( new Set<string>(questionIds))

		const questions 		=  	await this.questionnaireService.get(uniqueIds)
		const questionConfigs	=	questions.map(q => q.config)

		const report 			= 	Report.from(entries)

		return	{
					symptomCheck: 	null,
					report: 		report.config,
					questions:		questionConfigs
				}
	}

	public async getSessionConfigFromSymptomCheck(symptomCheck: SymptomCheck) : Promise<SessionConfig> {

		await this.entryMetaStoreService.ready

		const	questionIds 	= 	symptomCheck.questionIds
		const	questions		= 	await this.questionnaireService.get(questionIds)
		const	questionConfigs	=	questions.map( q => q.config )

		const	entries			=	this.entryMetaStoreService.items.filter( entry => questionIds.includes(entry.questionId) )

		const	report			= 	Report.from(entries)

		return 	{
					symptomCheck: 	symptomCheck.config,
					report:			report.config,
					questions:		questionConfigs
				}
	}

	public async shareRawAnswers() : Promise<void> {

		const sessionConfig = await this.getSessionConfigFromRawAnswers()

		await this.shareDataService.share(sessionConfig)

	}

	public async shareSymptomCheckAsSession() : Promise<void> {

		await this.symptomCheckMetaStoreService.ready
		await this.entryMetaStoreService.ready

		const noSymptomChecks	= this.symptomCheckMetaStoreService.items.length === 0
		const noAnswers			= this.entryMetaStoreService.items.length === 0


		// No Symptom Checks, no answers:

		if(noSymptomChecks && noAnswers){

			const sc_message	= 	'{{ENTRY_SHARE.NO_SYMPTOM_CHECKS}}'
			const an_message	= 	'{{ENTRY_SHARE.NO_ENTRIES}}'

			const message		= 	[sc_message, an_message].join(' ')

			this.rccAlertController.present({ message })
			return
		}


		// No Symptom Checks, some answers:
		if(noSymptomChecks && !noAnswers){

			const sc_message	= 	'{{ENTRY_SHARE.NO_SYMPTOM_CHECKS}}'
			const an_message	=	'{{ENTRY_SHARE.REQUEST_SHARE_RAW}}'

			const message		=	[sc_message, an_message].join(' ')

			const cancelButton	=	{
										label: 'CANCEL',
										rejectAs: 'cancel'
									}

			const answerButton	=	{
										label: 'ENTRY_SHARE.CONTINUE_WITHOUT_SYMPTOM_CHECK',
										resolveAs: 'raw'
									}

			const buttons		=	[cancelButton, answerButton]


			const choice		= 	await 	this.rccAlertController.present({ message, buttons })
											.catch( reason => {
												if(['dismiss', 'cancel'].includes(reason)) throw new UserCanceledError(reason)
											})


			await this.shareRawAnswers()

			return
		}


		const stores			= this.symptomCheckMetaStoreService.stores
		const heading			= 'ENTRY_SHARE.SELECT_SYMPTOM_CHECK.HEADING'
		const message			= 'ENTRY_SHARE.SELECT_SYMPTOM_CHECK.MESSAGE'
		const singleSelect		= true
		const symptomChecks		= await this.itemSelectService.select({ stores, singleSelect, heading, message })
		const sessionConfig 	= await this.getSessionConfigFromSymptomCheck(symptomChecks[0])

		await this.shareDataService.share(sessionConfig)

	}




}
