import	{	Component				}	from '@angular/core'
import	{
			Report,
			SymptomCheck,
			Question,
			UserCanceledError
		}								from '@rcc/core'

import	{
			RccModalController,
			ItemSelectService,
		}								from '@rcc/common'

import	{	QuestionnaireService			}	from '../../../questions'
import	{	SymptomCheckMetaStoreService	}	from '../../../symptom-checks/meta-store'
import	{	EntryMetaStoreService			}	from '../../meta-store'

@Component({
	templateUrl:	'./entry-share-modal.component.html',
	styleUrls:		['./entry-share-modal.component.scss']
})
export class EntryShareModalComponent {

	public report			: Report
	public questions		: Question[]		= []
	public symptom_checks	: SymptomCheck[]	= []

	public constructor(
		private rccModalController				: RccModalController,
		private entryMetaStoreService			: EntryMetaStoreService,
		private itemSelectService				: ItemSelectService,
		private questionnaireService			: QuestionnaireService,
		private symptomCheckMetaStoreService	: SymptomCheckMetaStoreService
	){}

	public async selectAll() : Promise<void> {
		this.report 		= 	Report.from(this.entryMetaStoreService.items)

		const question_ids 	= 	Array.from( new Set(
									this.report.entries
									.map( entry => entry.questionId )
									.flat()
								))

		this.questions 		=  await this.questionnaireService.get(question_ids)


		this.share()
	}

	public async selectSymptomCheck(): Promise<void>{

		try {
			const stores 		=	this.symptomCheckMetaStoreService.stores
			this.symptom_checks = 	await this.itemSelectService.select({ stores })

		} catch(reason){

			console.log('SELECTSYMPTOMCHECK 1', reason) // TODO

			this.symptom_checks	=	[]
		}

		const  question_ids 	= 	Array.from( new Set(
									this.symptom_checks
									.map( sc => sc.questionIds)
									.flat()
								))

		try {
			this.questions		= 	await this.questionnaireService.get(question_ids)
		} catch(reason) {

			console.log('SELECTSYMPTOMCHECK 2', reason) // TODO

			this.questions		=	[]
		}

		console.log('SELECTSYMPTOMCHECK 3', this.questions, this.symptom_checks)

		if(this.questions.length === 0) return

		this.report			= 	Report.from(
									this.entryMetaStoreService.items
									.filter( entry => question_ids.includes(entry.questionId) )
								)

		this.share()
	}



	public share(): void {

		this.rccModalController.dismiss({
			report: 		this.report,
			questions: 		this.questions,
			symptomChecks:	this.symptom_checks
		})

	}



	public cancel() : void {
		this.rccModalController.dismiss(new UserCanceledError() )
	}

}
