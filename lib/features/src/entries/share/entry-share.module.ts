import	{
			NgModule,
		}									from '@angular/core'
import	{
			HomePageModule,
			MainMenuModule,
			SharedModule,
			ShareDataModule,
			provideTranslationMap,
			provideHomePageEntry,
		}									from '@rcc/common'

import	{	EntryShareService			}	from './entry-share.service'
import	{	EntryShareModalComponent	}	from './modal/entry-share-modal.component'


import en from './i18n/en.json'
import de from './i18n/de.json'


export class EntryShareMenuEntry {
	public constructor(public entryShareService: EntryShareService){}
}


const homePageEntry		=	{
								deps:		[EntryShareService],
								factory:	(entryShareService: EntryShareService) => ({
												position:		3,
												label:			'ENTRY_SHARE.HOME_ENTRY.LABEL',
												icon:			'share',
												description:	'ENTRY_SHARE.HOME_ENTRY.DESCRIPTION',
												handler:		() => entryShareService.shareSymptomCheckAsSession()
											})
							}

@NgModule({
	imports: [
		SharedModule,
		ShareDataModule,
		MainMenuModule,
		HomePageModule,
	],
	providers: [
		EntryShareService,
		provideTranslationMap('ENTRY_SHARE', { en, de }),
		provideHomePageEntry(homePageEntry)
	],
	declarations: [
		EntryShareModalComponent
	]
})
export class EntryShareModule { }
