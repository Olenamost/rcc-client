import	{	Component 			}	from '@angular/core'
import	{	Entry				}	from '@rcc/core'

@Component({
	templateUrl: 	'./item-label.component.html',
})
export class EntryLabelComponent {
	public constructor(public entry: Entry){}
}
