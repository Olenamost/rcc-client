import	{	InjectionToken 	}		from '@angular/core'
import 	{
			Entry,
			EntryConfig,
			EntryStore,
		}							from '@rcc/core'

import	{	MetaAction		}		from '@rcc/common'



export const ENTRY_STORES 			= new InjectionToken<EntryStore>('Entry Stores')
export const ENTRY_META_ACTIONS 	= new InjectionToken<MetaAction<EntryConfig, Entry, EntryStore>>('Meta actions for entries.')
