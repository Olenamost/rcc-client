import 	{
			NgModule,
			ModuleWithProviders,
			Type
		} 									from '@angular/core'

import 	{ 	RouterModule, Routes 				}	from '@angular/router'

import	{
			BaseMetaStoreModule,
			ExportModule,
			ExportService,
			ItemRepresentation,
			MetaAction,
			MetaStoreConfig,
			MetaStoreModule,
			provideItemRepresentation,
			provideMainMenuEntry,
			provideTranslationMap,
			SharedModule,
		}									from '@rcc/common'

import	{
			Entry,
			EntryConfig,
			EntryStore,
			Report,
		}									from '@rcc/core'

import	{
			QuestionnaireServiceModule,
			QuestionnaireService
		}									from '../../questions/questionnaire'

import	{	EntryMetaStoreService		}	from './entry-meta-store.service'

import	{
			ENTRY_STORES,
			ENTRY_META_ACTIONS
		}									from './entry-meta-store.commons'

import	{	EntryMetaStorePageComponent 			}	from './overview-page/overview-page.component'
import	{	EntryLabelComponent			}	from './item-label/item-label.component'


import en from './i18n/en.json'
import de from './i18n/de.json'

const routes: Routes = [
		{ path: 'entries',	component: EntryMetaStorePageComponent	},
	]

const metaStoreConfig: MetaStoreConfig<EntryConfig, Entry, EntryStore> = {
		itemClass:		Entry,
		serviceClass:	EntryMetaStoreService
	}

const itemRepresentation: ItemRepresentation = {
		itemClass:		Entry,
		icon:			'entry',
		labelComponent:	EntryLabelComponent,
	}

const metaAction: MetaAction<EntryConfig, Entry, EntryStore> =
	{
		label:			'ENTRIES_META_STORE.ACTION.EXPORT',
		role:			'share' as const,
		icon:			'export',
		position:		-1,
		deps:			[ExportService, QuestionnaireService],
		handlerFactory:	(exportService:ExportService, questionnaireService: QuestionnaireService) =>
							async (entryMetaStoreService: EntryMetaStoreService) => {

								const 	report 	= Report.from(entryMetaStoreService.items)
								const 	questions	= await questionnaireService.get(report.questionIds)

								await 	exportService.export({
											data: 		report,
											context:	[questions],
											download: 	true
										})
							}
	}


const menuEntry	=	{
    position: 7,
    label: 'ENTRIES_META_STORE.MENU_ENTRY',
    icon: 'entry',
    path: '/entries',
}


@NgModule({
	imports: [
		SharedModule,
		ExportModule,
		RouterModule.forChild(routes),
		MetaStoreModule.forChild(metaStoreConfig),
		QuestionnaireServiceModule
	],
	providers:[
		provideMainMenuEntry(menuEntry),
		provideItemRepresentation(itemRepresentation),
		EntryMetaStoreService,
		{ provide: ENTRY_META_ACTIONS, multi:true, useValue: metaAction },
		provideTranslationMap('ENTRIES_META_STORE', { en, de })
	],
	declarations: [
		EntryMetaStorePageComponent,
		EntryLabelComponent
	],
	exports: [
		EntryMetaStorePageComponent,
		MetaStoreModule					// this is important so anything importing EntryMetaStoreModule can use the components of MetaStoreModule too
	],
})
export class EntryMetaStoreServiceModule extends BaseMetaStoreModule {

	public static forChild(
		stores?			: Type<EntryStore>[],
		metaActions?	: MetaAction<EntryConfig, Entry, EntryStore>[]
	): ModuleWithProviders<EntryMetaStoreServiceModule> {

		const mwp = BaseMetaStoreModule.getModuleWithProviders<EntryMetaStoreServiceModule>(this, stores, metaActions, ENTRY_STORES, ENTRY_META_ACTIONS)
		return mwp
	}

}
