import	{	NgModule				}		from '@angular/core'

import	{
			Entry,
			Report
		}									from '@rcc/core'

import	{
			StorageProviderModule,
			provideItemAction,
			provideTranslationMap,
			RccModalController,
			ItemAction,
			Action,
			Factory
		}								from '@rcc/common'

import	{
			ReportBasicViewModule,
			ReportViewComponent,
		}									from '../../reports/basic-view'
import	{	EntryMetaStoreServiceModule	}	from '../meta-store'
import	{	JournalService				}	from './journal.service'

import en from './i18n/en.json'
import de from './i18n/de.json'


const homePageEntry : Factory<Action> 	=	{
								deps: [JournalService, RccModalController],
								factory: (journalService: JournalService, rccModalController: RccModalController) => ({
									label: 		'JOURNAL.HOME.VIEW_DATA.LABEL',
									description:'JOURNAL.HOME.VIEW_DATA.DESCRIPTION',
									icon: 		'chart',
									position:	-1,
									handler:	async () => {
										const report : Report = Report.from(journalService.items)

										// TODO: This should be a method of a ReportService
										// We should not be able to use ReportViewComponent without
										// importing ReportModule first, because then among other issues
										// the translation strings are missing.
										await rccModalController.present(ReportViewComponent, { report  })
									}

								})
							}
const itemAction: ItemAction<Entry> = 	{
								role:		'destructive' as const,
								itemClass:	Entry,
								storeClass: JournalService,

								getAction:	(entry: Entry, journalService: JournalService) => ({
									label: 			'JOURNAL.ACTIONS.DELETE',
									icon:			'delete',
									successMessage:	'JOURNAL.ACTIONS.DELETE_SUCCESS',
									failureMessage:	'JOURNAL.ACTIONS.DELETE_FAILURE',
									confirmMessage:	'JOURNAL.ACTIONS.DELETE_CONFIRM',
									handler: 		() => journalService.removeEntry(entry)
								}),
							}





@NgModule({
	imports:[
		ReportBasicViewModule,
		EntryMetaStoreServiceModule.forChild([JournalService]),
		StorageProviderModule
	],
	providers: [
		JournalService,
		provideItemAction(itemAction),
		provideTranslationMap('JOURNAL', { en, de }),
		// provideHomePageEntry(homePageEntry)
	],
})
export class JournalServiceModule { }
