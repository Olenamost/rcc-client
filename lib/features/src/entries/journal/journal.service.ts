import 	{	Injectable 			} 	from '@angular/core'

import	{
			EntryStore,
			Entry,
			isEntryConfig,
			Schedule,
			assert
		}							from '@rcc/core'

import	{	RccStorage			}	from '@rcc/common'
import 	{ 	isSameDay 			} 	from '@rcc/core'


@Injectable()
export class JournalService extends EntryStore {

	public name = 'JOURNAL.NAME'

	public constructor(
		rccStorage: RccStorage
	) {
		super(rccStorage.createItemStorage('rcc-journal'))
	}

	public async logEntry(entry: Entry): Promise<Entry> {
		return this.log(entry.questionId, entry.answer, entry.note, entry.targetDay)
	}


	public async logForDay(targetDay : string, questionId : string, answer : string|number|boolean, note? : string): Promise<Entry> {

		return this.log(questionId, answer, note, null, targetDay)
	}

	public async log(questionId : string, answer : string|number|boolean, note? : string, date? : string, targetDay? : string): Promise<Entry> {

		await this.ready

		date = date || Schedule.getIsoStringWithTimezone(new Date())

		const existingEntry = targetDay && this.items.find( entry => entry.questionId === questionId && isSameDay(entry.targetDay, targetDay))
		const entryConfig	= [questionId, answer, date, note, targetDay]

		assert(isEntryConfig(entryConfig))

		if(existingEntry){
			existingEntry.config = entryConfig
			this.updateSubject.next(existingEntry)
		}

		const entry 		= existingEntry || this.addConfig(entryConfig)

		await this.storeAll()

		return entry

	}

	public async removeEntry(entry: Entry) : Promise<Entry>{
		await this.ready

		this.removeItem(entry)

		await this.storeAll()

		return entry
	}

}
