import 	{ 	Injectable 			} 	from '@angular/core'

import	{	RccStorage			}	from '@rcc/common'

import	{
			Item,
			ConfigOf,
			ItemStorage,
			assert
		}							from '@rcc/core'


export class IndexedDbItemStorage<I extends Item, C extends ConfigOf<I> = ConfigOf<I> > implements ItemStorage<I> {

	public static dbName		= 'rccDatabase'
	public static storeName		= 'rccStorage'

	public static async connect(): Promise<IDBDatabase> {

		return 	new Promise<IDBDatabase>( (resolve, reject) => {

					const request = indexedDB.open(IndexedDbItemStorage.dbName, 1)

					request.onsuccess 		= () 	=> resolve( request.result )
					request.onerror			= event => reject( event )

					request.onupgradeneeded	= event => {
						const db = request.result

						if(event.newVersion !== 1) return

						db.createObjectStore(IndexedDbItemStorage.storeName)
					}

				})
	}

	public static async request<T>(callback: (store: IDBObjectStore) => IDBRequest<T>, mode?: IDBTransactionMode): Promise<T> {

		const db 			= 	await this.connect()

		const transaction 	= 	db.transaction(IndexedDbItemStorage.storeName, mode)
		const completion	=	new Promise( resolve => transaction.oncomplete = resolve )
		const store			=	transaction.objectStore(IndexedDbItemStorage.storeName)

		const rPromise		=	new Promise<T>( (resolve, reject) => {

									transaction.onerror = () => reject(transaction.error)

									const request 		= callback(store)

									request.onerror		= () => reject(request.error)
									request.onsuccess	= () => resolve(request.result)

								})

		const data			= 	await rPromise

		await completion

		db.close()

		return data

	}


	public constructor(
		protected id: string
	){}




	public async getAll(): Promise<C[]> {

		const data = await IndexedDbItemStorage.request( (store:IDBObjectStore) =>  (store.get(this.id) as  IDBRequest<string>) )

		return (JSON.parse(data || '[]') as C[])
	}

	public async store(items: (C|I)[]): Promise<void> {

		const configs	= 	items.map( (i: C|I) =>	(i instanceof Item)
													?	i.config
													:	i
							)

		await IndexedDbItemStorage.request( store =>  store.put(JSON.stringify(configs), this.id), 'readwrite' )

	}

	public async clear(): Promise<void>{

		const result	= await IndexedDbItemStorage.request( store => store.delete(this.id) )

		return result
	}

}


@Injectable()
export class IndexedDbService extends RccStorage{

	public createItemStorage<I extends Item>(name:string): ItemStorage<I> {
		return new IndexedDbItemStorage<I>(name)
	}

	public async clearItemStorage(name:string): Promise<void>{
		await new IndexedDbItemStorage(name).clear()
	}

	public async getStorageNames(): Promise<string[]> {
		const keys 	= await IndexedDbItemStorage.request( store => store.getAllKeys() )

		assert( keys.every(key => typeof key === 'string') ,'IndexedDbService.getStorageNames() non-string keys detected.')

		return keys as string[]
	}
}
