import	{	NgModule							}	from '@angular/core'
import 	{ 	Title 								}	from '@angular/platform-browser'

import  { 	CommonModule						}	from '@angular/common'

import  { 	provideTranslationMap,
			RccTranslationService
		} 											from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	declarations: [],
	imports: [
	CommonModule
	],
	providers: [
	provideTranslationMap(null, { en,de })
	]
})



export class BasePatModule {
	public constructor(
		rccTranslationService: RccTranslationService,
		title: Title
	){
		rccTranslationService.activeLanguageChange$
		.subscribe(language => {
			const titleString = rccTranslationService.translate('APP_NAME', null, language)
			title.setTitle(titleString)

		})
	}
}
