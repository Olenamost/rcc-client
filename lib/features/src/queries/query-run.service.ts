// import	{	Injectable				}	from '@angular/core'
// import	{
// 			Question,
// 			Entry
// 		}								from '@rcc/core'
// import	{	QuestionnaireService			}	from '../questions'
// import	{	JournalService					}	from '../entries'
// import	{
// 			QueryControl,
// 			SubmitEntryFn,
// 			CleanUpFn
// 		}								from './query-control.class'

// /**
//  * @deprecated
//  */
// export class QueryRun {

// 	public queryControls	: QueryControl[]
// 	public timestamp		: number

// 	public constructor(
// 			questions		: Question[],
// 			createEntryFn	: SubmitEntryFn,
// 			cancelEntryFn	: CleanUpFn
// 	){
// 		this.timestamp		= 	Date.now()
// 		this.queryControls	= 	questions.map( (question: Question) => new QueryControl( question, createEntryFn, cancelEntryFn )	)
// 	}
// }

// /**
//  * @deprecated
//  */
// @Injectable()
// export class QueryRunService {


//   	public constructor(
//   		public questionnaireService	: QuestionnaireService,
//   		public journalService	: JournalService
//   	){}

//   	public async start(ids: string[]) : Promise<QueryRun> {

//   		const questions = 	await this.questionnaireService.get(ids)
//   		const queryRun	=	new	QueryRun(
// 		  						questions,
// 								(id:string, answer:string, note:string)	=> this.journalService.log(id,answer,note),
// 								(entry: Entry)							=> this.journalService.removeEntry(entry)
// 		  					)

// 		return queryRun
//   	}


// }
