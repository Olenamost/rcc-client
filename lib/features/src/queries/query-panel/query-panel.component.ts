import	{
			Component,
			Output,
			Input,
			EventEmitter
		}							from '@angular/core'

import	{
			RccSliderComponent,
			RccToastController,
			RccAlertController
		}							from '@rcc/common'

import	{
			QueryControl
		}							from '../query-control.class'



@Component({
	selector: 'rcc-query-panel',
	templateUrl: 'query-panel.component.html',
	styleUrls:	['./query-panel.component.css']
})
export class QueryPanelComponent {

	@Output()
	public revert			= new EventEmitter<void>()

	@Output()
	public remove			= new EventEmitter<void>()

	@Output()
	public store			= new EventEmitter<void>()

	@Input()
	public queryControl		: QueryControl

	@Input()
	public slider			: RccSliderComponent

	public constructor(
		protected rccToastController	: RccToastController,
		protected rccAlertController	: RccAlertController
	){}


	get entryExists(): boolean {
		return !!this.queryControl.entry
	}

	get validUserInputExists(): boolean {
		return this.queryControl.complete
	}

	get changesExist(): boolean {

		if(!this.validUserInputExists) 	return false
		if(!this.entryExists) 			return true


		return this.queryControl.answer !== this.queryControl.entry.answer
	}

	public async storeEntry(): Promise<void> {

		try {

			await this.queryControl.submit()

			void 	this.rccToastController.success('QUERIES.SAVE.SUCCESS')

			this.store.emit()

		} catch(reason) {

			void	this.rccToastController.failure('QUERIES.SAVE.FAILURE')

			throw reason
		}

	}





	public async removeEntry(): Promise<void> {

		await	this.rccAlertController.confirm('QUERIES.DELETE.CONFIRM', 'DELETE')

		try {


			await	this.queryControl.reset()

			void 	this.rccToastController.success('QUERIES.DELETE.SUCCESS')

			this.remove.emit()

		} catch(reason) {

			void 	this.rccToastController.failure('QUERIES.DELETE.FAILURE')

			throw(reason)
		}
	}





	public revertEntry(): void {

		try{

			this.queryControl.revert()

			void 	this.rccToastController.success('QUERIES.REVERT.SUCCESS')

			this.revert.emit()

		} catch(reason) {

			void 	this.rccToastController.failure('QUERIES.REVERT.SUCCESS')

			throw reason

		}
	}

}
