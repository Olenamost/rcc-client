// TODO, everything into its own component

import 	{
			OnDestroy,
			Component
		}									from '@angular/core'

import	{
			Subject,
			takeUntil,
		}									from 'rxjs'

import	{
			QueryWidgetComponent
		}									from '../query-widget-component.class'

import	{
			QueryControl,
		}									from '../query-control.class'

@Component({
	selector:    	'fallback-query-widget',
	templateUrl:  	'./fallback-query-widget.component.html',
	styles:			[':host{display:block}']
})
export class FallbackQueryWidgetComponent extends QueryWidgetComponent implements OnDestroy {

	static widgetMatch(queryControl:QueryControl) : number { return 0 }

	public		error		: string

	protected	destroy$	= new Subject<void>()



	public constructor(
		public 		queryControl	: QueryControl,
	){
		super(queryControl)

		queryControl.answerControl.valueChanges
		.pipe( takeUntil(this.destroy$) )
		.subscribe( (value: unknown) => {

			if(queryControl.question.type === 'string' 	&& typeof value !== 'string') 	this.queryControl.answerControl.setValue(String(value))
			if(queryControl.question.type === 'integer'	&& typeof value !== 'number') 	this.queryControl.answerControl.setValue(this.toInteger(value) )
			if(queryControl.question.type === 'decimal'	&& typeof value !== 'number') 	this.queryControl.answerControl.setValue(this.toDecimal(value) )

		})

		queryControl.answerControl.statusChanges
		.pipe( takeUntil(this.destroy$) )
		.subscribe( ()=> {


			const errors 			=  	this.queryControl.answerControl.errors
			const constraintError	=	errors?.questionTypeConstraints as string


			this.error = 	constraintError || null

		})

	}

	isNumber(x:unknown): boolean {
		return typeof x === 'number'
	}

	toInteger(x: unknown): number {

		const stringValue 	= 	String(x)
		const parsed		=	parseInt(stringValue)

		return	isNaN(parsed)
				?	0
				:	parsed

	}

	toDecimal(x: unknown): number {

		const stringValue 	= 	String(x).replace(/,/g,'.')
		const parsed		=	parseFloat(stringValue)

		return	isNaN(parsed)
				?	0
				:	parsed

	}

	ngOnDestroy() : void{
		this.destroy$.next()
		this.destroy$.complete()
	}
}

