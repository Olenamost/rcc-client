import 	{
			NgModule,
		}											from '@angular/core'

import 	{
			SharedModule,
			WidgetsModule,
			provideWidget,
			provideTranslationMap
		} 											from '@rcc/common'

import	{	FallbackQueryWidgetComponent 		}	from './fallback-query-widget.component'
import	{	FallbackQueryWidgetUnknownComponent }	from './fallback-query-widget-unknown.component'


const queryWidgets = 	[
							FallbackQueryWidgetComponent,
							FallbackQueryWidgetUnknownComponent
						]

import en from './i18n/en.json'
import de from './i18n/de.json'



@NgModule({
	imports: [
		SharedModule,
		WidgetsModule
	],
	providers: [
		...queryWidgets.map(provideWidget),
		provideTranslationMap('QUERIES.FALLBACK', { en, de }),
	],
	declarations:[
		...queryWidgets
	],
	exports: [
		...queryWidgets
	]
})
export class FallbackQueryWidgetsModule {}

