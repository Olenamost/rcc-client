import	{
			Component,
			Input,
		}							from '@angular/core'

import	{	QueryControl		}	from '../query-control.class'


/**
 * This Component renders the UI for answering of a single question.
 * This includes the presentation of the question's wording and input widgets.
 *
 * Note: Why have an extra component that only adds the question's wording
 * on top of the input widgets? Why not use the rcc-widget by itself and have
 * those widgets reder the question's wording? – Answer: The presentation
 * of the question's wording should have a consistent look, regardless
 * type of question or possible answer. The input widget though will vary
 * with question/answer type. So we should keep these to concern separated.
 */
@Component({
	selector:     'rcc-query-view',
	templateUrl:   './query-view.component.html',
	styleUrls:     ['./query-view.component.css'],
})
export class QueryViewComponent {

	/**
	 * The {@link QueryControl} holds all the necessary information to run
	 * the question answer process.
	 */
	@Input()
	queryControl: QueryControl


	get hasAnswer() : boolean {
		return !!this.queryControl?.entry
	}

}
