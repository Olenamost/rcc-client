import 	{
			Component,
			EventEmitter,
			Input,
			OnDestroy,
			Output,
			ViewChild,
		}     								from '@angular/core'
import	{
			BehaviorSubject,
			SubscriptionLike,
		}									from 'rxjs'
import	{
			RccToastController,
			RccAlertController,
			RccSliderComponent
		}									from '@rcc/common'
import	{	QueryControl				}	from '../query-control.class'


/**
 * This component deals with answering process of multiple questions at once.
 * The answering process for each question is represented by a {@link QueryControl}.
 *
 * You can provide extra slide (e.g. All-Done-Slide) by content projection. Make sure to use ```rcc-slide```.
 */
@Component({
	selector:     'rcc-query-run',
	templateUrl:   './query-run.component.html',
	styleUrls:     ['./query-run.component.css']
})
export class QueryRunComponent implements OnDestroy {

	/**
	 * One Query control for each question that is to be answered.
	 */
	@Input()
	queryControls: QueryControl[]

	/**
	 * Emits when users clicks 'done' button on the last slide.
	 */
	@Output()
	done 	= new EventEmitter<void>()

	/**
	 * Emits when the user clicks 'cancel' button.
	 */
	@Output()
	cancel	= new EventEmitter<void>()

	@Output()
	queryControlChange	= new EventEmitter<QueryControl>()


	/**
	 * The Slider component. We needs this to programattically trigger
	 * foward or backward sliding and to get the question, currently
	 * seen by the user.
	 */
	@ViewChild(RccSliderComponent)
	set sliderComponent(slider: RccSliderComponent){
		this.setupSlider(slider)
	}

	public slider						:	RccSliderComponent

	protected slideChangeSub			:	SubscriptionLike
	protected activeQueryControlSubject	= 	new BehaviorSubject<QueryControl>(null)

	public activeQueryControl$			= 	this.activeQueryControlSubject.asObservable()

	public constructor(
		public rccToastController	: RccToastController,
		public rccAlertController	: RccAlertController
	){
		this.activeQueryControl$.subscribe(this.queryControlChange)

	}

	/**
	 * {@link QueryControl}, representing the answering process of
	 * the currently displayed Question.
	 */
	get activeQueryControl(): QueryControl {
		return this.activeQueryControlSubject.value
	}


	/**
	 * Checks if all questions have been answered.
	 */
	get allDone() : boolean {

		if(!Array.isArray(this.queryControls)) return false

		return this.queryControls.every( queryControl => !!queryControl.entry)
	}


	/**
	 * Initiliazes the component to work with a slider component in the template, once it has been rendered.
	 */
	setupSlider(slider: RccSliderComponent): void {

		// nothing has changed, no need for setup:
		if(slider === this.slider) return

		this.slider = slider || undefined

		if(this.slideChangeSub) this.slideChangeSub.unsubscribe()

		if(!this.slider) return

		this.slideChangeSub 		= this.slider.slideChange$.subscribe( () => this.onSlideChange() )

		this.slider.storeState 		= () => this.storeState()
		this.slider.restoreState 	= (questionId:string) => this.restoreState(questionId)

	}

	/**
	 * When the slider gets new slides, it has to figure out, which of them is to
	 * be initially displayed when the slider rerendered. This method tells the slider,
	 * to remember the id of the currently displayed question, before rerendering starts.
	 *
	 */
	public storeState(): string{
		return this.activeQueryControl?.question.id
	}

	/**
	 * When the slider gets new slides, it has to figure out, which of them is to
	 * be initially displayed after the slider will have rerendered. This method
	 * scrolls to the slide of the question that matches the id stored before the
	 * slides rerendered (see .storeState()).
	 */
	public restoreState(questionId?: string): void {
		questionId
		?	this.gotoQuestion(questionId, 'instant')
		:	this.gotoStart()
	}



	/**
	 * Tracks slide changes and controls emissions of activeQueryControlSubject.
	 * Everytime the slide changes, activeQueryControlSubject will emit a new queryControl.
	 *
	 * Note: We don't create activeQueryControlSubject from this.slider.slideChange$
	 * with pipe and operators, because .activeQueryControlSubject exists earlier then the
	 * slider component and it's observables; also the slider component may change or
	 * get destroyed.
	 */
	public onSlideChange(): void {

		if(!this.slider) 		return
		if(!this.queryControls)	return

		const index 		= this.slider.currentSlidePosition
		const queryControl	= this.queryControls[index] || null

		this.activeQueryControlSubject.next(queryControl)

	}

	/**
	 * Scrolls to the earliest question that lacks an answer.
	 */
	public gotoFirstUnansweredQuery() : void {
		const position = this.queryControls.findIndex( queryControl => !queryControl.entry )

		this.slider.slideTo(position)
	}

	/**
	 * Scrolls to the question matchig the given id.
	 */
	public gotoQuestion(id: string, scrollBehavior = 'smooth') : void {
		const position = this.queryControls.findIndex( queryControl => queryControl.question.id === id)

		this.slider.slideTo(position, scrollBehavior) // if position === -1, will go to the last slide

	}

	/**
	 * Scroll to the wrap up page.
	 */
	public gotoWrapUp(): void {
		this.slider.slideTo(-1)
	}

	public gotoStart(): void {
		this.slider.slideTo(0)
	}


	ngOnDestroy(): void {
		this.slideChangeSub.unsubscribe()
		this.activeQueryControlSubject.complete()
	}

}
