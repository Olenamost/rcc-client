import	{	Component			}	from '@angular/core'
import	{	WidgetComponent		}	from '@rcc/common'
import	{	QueryControl		}	from './query-control.class'



/**
 * This class is the base class for all QueryWidgets.
 * You can still use `WidgetComponent<QueryControl>` instead,
 * but with {@Link QueryWidgetComponent} you don't have to touch
 * the generic widget infrastructure at all.
 *
 * Every QueryWidgetComponent is supposed to extend this class.
 * A query widget represented by QueryWidgetComponent is meant to manage
 * that part of the UI that lets the user input an answer to a given {@link Question}.
 *
 * A query widget is _not_ concerned with how the answer is stored,
 * where to put the question text or how to validate the answer.
 * Instead it brings up a multiple choice menu, or a text input or a scale of some sort;
 * whatever is suitable for the given question and answer type.
 *
 * Each query widget focusses on one type of input mechanism; the static widgetMatch method,
 * checks if that mechanism is suitable for a given question.
 *
 * Every extension of QueryWidgetComponent will receive an instance of {@link QueryControl}
 * as parameter to its constructor. This QueryControl instance has all the information about
 * the given question and comes with additional properties to interact with the rest of the UI.
 *
 * When implementing your own constructor, make sure to pass the QueryControl instance on to super().
 *
 * Here's how to use this class:
 *
 * ```ts
 *
 * import	{	Component			}		from '@angular/core'
 *
 * import	{
 * 				QueryControl,
 * 				QueryWidgetComponent
 * 			}								from '@rcc/features'
 *
 *
 * @Component({
 *	templateUrl: 	'./my-query-widget.component.html',
 *	styleUrls: 		['./my-query-widget.component.scss'],
 * })
 * export class MyQueryWidgetComponent extends QueryWidgetComponent {
 *
 *		static widgetMatch(queryControl: QueryControl): number{
 *
 *		return	queryControl.question?.tags?.includes('my-special-tag')
 *				?	 2
 *				:	-1
 *	}
 *
 * 	// You can skip the constructor, if you don't do anything with it.
 * 	public constructor(
 * 		public queryControl: QueryControl
 * 	){
 * 		super(queryControl)
 *
 * 		//...
 * 	}
 *
 * }
 *
 * ```
 *
 */
 @Component({
	template: 'Extension of QueryWidgetComponent is missing a template.'
 })
export class QueryWidgetComponent extends WidgetComponent<QueryControl> {

	public static controlType = QueryControl

	public static widgetMatch(queryControl: QueryControl): number {

		console.warn('static widgetMatch() not implemented for '+this.name )

		return 	-1
	}

	public constructor(
		public queryControl: QueryControl
	){
		super(queryControl)
	}
}
