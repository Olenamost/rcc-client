import	{
			Component,
			OnInit,
			OnDestroy,
			ChangeDetectorRef
		} 								from '@angular/core'

import	{
			Location
		}								from '@angular/common'

import	{
			ActivatedRoute
		}								from '@angular/router'

import	{
			Question,
			Entry
		}								from '@rcc/core'
import	{	JournalService			}	from '../../entries'

import	{	QuestionnaireService	}	from '../../questions'

import	{
			mergeMap,
			mergeAll,
			take,
			map,
			from,
			Subject,
			takeUntil,
		}								from 'rxjs'



import	{	QueryControl			}	from '../query-control.class'



@Component({
	selector: 	'rcc-query-page',
	templateUrl: 	'./query-page.component.html',
	styleUrls: 	['./query-page.component.css'],
})
export class QueryPageComponent implements OnInit,OnDestroy {

	public queryControl	: QueryControl

	public destroy$		=	new Subject<void>()


	public constructor(
		private	activatedRoute			: ActivatedRoute,
		private location				: Location,
		private questionnaireService	: QuestionnaireService,
		private	cd						: ChangeDetectorRef,
		private journalService			: JournalService
	){}

	ngOnInit() : void {

		this.activatedRoute.paramMap
		.pipe(
			takeUntil(this.destroy$),

			map( 		(params) 					=> 	params.get('id') ),
			mergeMap( 	(id			: string )		=> 	from(this.questionnaireService.get([id]) ) ), // TODO: multiple matches?
			mergeAll(),
			take(1),
			map(		(question	: Question)		=> 	new QueryControl(
																question,
																(id:string, answer:string, note:string)	=> this.journalService.log(id,answer,note),
																(entry: Entry)							=> this.journalService.removeEntry(entry)
														)
			)
		)
		.subscribe( queryControl => 	{

			this.queryControl = queryControl
			this.cd.detectChanges()

		})

	}



	public close(): void {
		this.location.back()
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
