import { Component } from '@angular/core'

@Component({
	templateUrl:	'./legal.page.html',
	styleUrls:		['./legal.page.scss'],
})
export class LegalPageComponent {}
