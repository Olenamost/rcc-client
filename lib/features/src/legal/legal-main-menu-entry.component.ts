import 	{	Component	}	from '@angular/core'


@Component({
	template:	`
					<ion-item routerLink = "legal">
						<ion-label [id]	= "'LEGAL.MENU_ENTRY' | toID:'rcc-e2e'">{{ "LEGAL.MENU_ENTRY" | translate }}</ion-label>
					</ion-item>
				`
})
export class LegalMainMenuEntryComponent {}
