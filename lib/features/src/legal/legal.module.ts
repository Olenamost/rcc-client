import 	{ 	NgModule						} 	from '@angular/core'

import	{	RouterModule					}	from '@angular/router'

import	{
			SharedModule,
			MainMenuModule,
			provideTranslationMap,
			provideMainMenuEntry
		}										from '@rcc/common'

import	{	LegalMainMenuEntryComponent		}	from './legal-main-menu-entry.component'
import  {	LegalPageComponent  			} 	from './legal.page'

import	en from './i18n/en.json'
import	de from './i18n/de.json'


const routes 			=	[
								{
									path: 		'legal',
									component: 	LegalPageComponent
								}
							]


const mainMenuEntry		=	{
								position: -1,
								component: LegalMainMenuEntryComponent
							}

@NgModule({
	imports: [
		SharedModule,
		MainMenuModule,
		RouterModule.forChild(routes),

	],
	declarations:[
		LegalMainMenuEntryComponent,
		LegalPageComponent
	],
	providers: [
		provideTranslationMap('LEGAL', { en, de }),
		provideMainMenuEntry(mainMenuEntry)
	]
})
export class LegalModule {


}
