import	{	WidgetControl		}		from '@rcc/common'
import	{	Dataset				}		from './data-sets'



/**
 * For the time being, the DataViewControl comes with only an array of {@link Dataset}s.
 * This will change soon and probably add parameters for start and end dates or other view options.
 *
 * The first element of the Datasets array is considered the primary Dataset. These second the secondary and so on.
 * Widgets don't have to visualize all of them. But if a Widget can only visualize one {@link Dataset} it should
 * not {@Link Widget#match} a DataViewControl with more than one Dataset.
 */
export class DataViewControl extends WidgetControl {

	public constructor(
		public datasets:	Dataset[]
	){
		super()
	}

	/**
	 * @deprecated legacy
	 */
	public get data(): Dataset[] {
		console.warn('DataViewControl.data is deprecated; use .datasets')
		return this.datasets
	}

}
