import 	{
			Pipe,
			PipeTransform,
		}								from '@angular/core'

import	{
			Question,
			QuestionOptionConfig
		}								from '@rcc/core'

import	{
			QuestionnaireService
		}								from './questionnaire.service'


@Pipe({
	name: 'id2Question'
})
export class Id2QuestionPipe implements PipeTransform {

	public constructor(private questionnaireService: QuestionnaireService){}

	public transform(id: string): Promise<Question> {
		return this.questionnaireService.get(id)
	}
}

@Pipe({
	name: 'labeledOptions'
})
export class LabeledOptionsPipe implements PipeTransform {

	public transform(question: Question): QuestionOptionConfig[] {
		if(!question.options) return []
		const options_with_labels = question.options.filter( option => option.meaning || option.translations)
		if(options_with_labels.length === 0) return question.options

		return 	options_with_labels
	}
}


@Pipe({
	name: 'asAnswerTo'
})
export class AsAnswerToPipe implements PipeTransform {


	public constructor(private questionnaireService: QuestionnaireService){}


	public async transform(answer: unknown, question: Question)	: Promise<QuestionOptionConfig| (typeof answer)>
	public async transform(answer: unknown, id: string)			: Promise<QuestionOptionConfig| (typeof answer)>
	public async transform(answer: unknown, q: Question|string)	: Promise<QuestionOptionConfig| (typeof answer)> {

		const question = 	q instanceof Question
							?	q
							:	await this.questionnaireService.get(q)


		if(question.options) return question.options.find( option => option.value === answer)

		return answer
	}
}
