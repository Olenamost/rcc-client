
import 	{
			Injectable,
			Inject,
			Optional
		} 								from '@angular/core'

import	{	Question,
			QuestionConfig,
			QuestionStore,
		}								from '@rcc/core'

import	{
			MetaStore,
			MetaAction
		}								from '@rcc/common'

import	{
			QUESTION_STORES,
			QUESTION_META_ACTIONS
		}								from './questionnaire.commons'


@Injectable()
export class QuestionnaireService extends MetaStore<QuestionConfig, Question, QuestionStore> {

	public name = 'QUESTIONNAIRE.NAME'

	public constructor(
		@Optional() @Inject(QUESTION_STORES)
		stores			: QuestionStore[],

		@Optional() @Inject(QUESTION_META_ACTIONS)
		metaActions		: MetaAction<QuestionConfig, Question, QuestionStore>[]
	) {
		super(stores, metaActions)
	}


	public handleIdWithoutItem(id: string): Question | null {
		return new Question(id)
	}
}
