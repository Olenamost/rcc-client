import 	{
			NgModule,
			ModuleWithProviders,
			Type
		} 								from '@angular/core'

import 	{ 	RouterModule 			}	from '@angular/router'

import	{
			BaseMetaStoreModule,
			MetaAction,
			MetaStoreModule,
			provideItemRepresentation,
			provideTranslationMap,
			provideMainMenuEntry,
			SharedModule,
			TranslationsModule,
		}								from '@rcc/common'

import	{
			Question,
			QuestionConfig,
			QuestionStore,
		}								from '@rcc/core'


import	{	QuestionnaireService			}	from './questionnaire.service'

import	{
			QUESTION_STORES,
			QUESTION_META_ACTIONS
		}								from './questionnaire.commons'

import	{	QuestionnairePageComponent 		}	from './questionnaire.page/questionnaire.page'
import	{
			Id2QuestionPipe,
			AsAnswerToPipe,
			LabeledOptionsPipe
		}								from './questionnaire.pipes'
import 	{	QuestionLabelComponent	} 	from './question-label/question-label.component'

import en from './i18n/en.json'
import de from './i18n/de.json'

const routes 				=	[
									{ path: 'questionnaire',	component: QuestionnairePageComponent	},
								]

const questionnaireConfig 	=  	{
									itemClass:			Question,
									serviceClass: 		QuestionnaireService,

								}

const itemRepresentation	=	{
									itemClass:			Question,
									icon:				'question',
									labelComponent:		QuestionLabelComponent,
								}

const pipes					=	[
									Id2QuestionPipe,
									AsAnswerToPipe,
									LabeledOptionsPipe
								]


export class MenuEntryQuestionnaireComponent {}

const menuEntry	=	{
    position: 5,
    label: 'QUESTIONNAIRE.MENU_ENTRY',
    icon: 'question',
    path: '/questionnaire',
}




@NgModule({
	declarations: [
		QuestionnairePageComponent,
		QuestionLabelComponent,
		...pipes
	],
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		MetaStoreModule.forChild(questionnaireConfig),
		TranslationsModule
	],
	exports: [
		MetaStoreModule, // So other Module need not import this specifically
		...pipes
	],
	providers:[
		QuestionnaireService,
		provideTranslationMap('QUESTIONNAIRE', { en, de } ),
		provideMainMenuEntry(menuEntry),
		provideItemRepresentation(itemRepresentation)
	]
})
export class QuestionnaireServiceModule extends BaseMetaStoreModule {

	public static 	forChild(
				stores			: Type<QuestionStore>[],
				metaActions?	: MetaAction<QuestionConfig, Question, QuestionStore>[]
			): ModuleWithProviders<QuestionnaireServiceModule> {


		const mwp = BaseMetaStoreModule.getModuleWithProviders<QuestionnaireServiceModule>(this, stores, metaActions, QUESTION_STORES, QUESTION_META_ACTIONS)
		return mwp

	}
}
