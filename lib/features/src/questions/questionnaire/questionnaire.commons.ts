import	{	InjectionToken 	}		from '@angular/core'
import 	{
			Question,
			QuestionConfig,
			QuestionStore,
		}							from '@rcc/core'

import	{
			MetaAction
		}							from '@rcc/common'



export const QUESTION_STORES 			= new InjectionToken<QuestionStore>('Question Stores')
export const QUESTION_META_ACTIONS		= new InjectionToken<MetaAction<QuestionConfig, Question, QuestionStore>>('metaAction, any action realted to an item type.')
