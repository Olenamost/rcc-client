import 	{
			Component,
			Input,
			OnDestroy,
			ChangeDetectorRef
		}									from '@angular/core'

import	{
			FormControl,
			FormArray,
			FormGroup,
			AbstractControl,
		}									from '@angular/forms'

import	{
			Subject,
			takeUntil,
			throttleTime,
			startWith
		}									from 'rxjs'

import	{
			Question,
			assertQuestionConfig,
		}									from '@rcc/core'

import	{
			ModalWithResultComponent,
			RccAlertController,
			RccModalController,
			ItemEditResult,
			WidgetsService,
			WidgetComponentType

		}									from '@rcc/common'

import	{
			QueryControl
		}									from '../../queries'

import	{
			QuestionEditControl,
		}									from '../question-edit-widgets'


@Component({
	selector:		'rcc-question-edit',
	templateUrl:	'./question-edit.component.html'

})
export class QuestionEditComponent extends ModalWithResultComponent<ItemEditResult<Question>> implements OnDestroy {

	@Input()
	public set item(question: Question){ this.updateFromQuestion(question) }


	@Input()
	public 	heading 			: string 			= 	undefined

	public 	questionConfigError : string			=	null

	public 	destroy$			: Subject<void>		= 	new Subject<void>()

	public	questionEditWidgets	: WidgetComponentType<QuestionEditControl>[]

	public	questionTypeControl	: FormControl<WidgetComponentType<QuestionEditControl>>
		= new FormControl<WidgetComponentType<QuestionEditControl>>(null)

	public 	questionEditControl	: 	QuestionEditControl

	public 	editForms			: 	AbstractControl =	new FormControl<null>(null)

	public 	queryControl		: 	QueryControl	=	undefined


	public constructor(
		private widgetsService			: WidgetsService,

		protected rccAlertController	: RccAlertController,
		protected rccModalController	: RccModalController,
		protected changeDetectorRef		: ChangeDetectorRef
	){
		super(rccModalController)
	}


	public updateFromQuestion(question: Question) : void {
		this.questionEditControl = new QuestionEditControl(question?.config)
		this.questionEditWidgets = this.widgetsService.getWidgetMatches(this.questionEditControl)// Type<QuestionEditWidgetComponent>[]
		this.questionTypeControl.setValue(this.questionEditWidgets[0])


		this.questionEditControl.editForm$
		.pipe( takeUntil(this.destroy$) )
		.subscribe( editForms => {
			this.editForms = editForms
			this.editForms.updateValueAndValidity()
			this.changeDetectorRef.detectChanges()
		})


		this.questionEditControl.questionConfig$
		.pipe(
			takeUntil(this.destroy$),
			throttleTime(300, undefined, { leading:true, trailing:true }),
			startWith(this.questionEditControl.questionConfig$.value)
		)
		.subscribe( config => {

			this.questionConfigError	= null
			this.queryControl			= null

			try { 		assertQuestionConfig(config) }
			catch(e){
				this.questionConfigError	=	e instanceof Error
												?	e.message
												:	String(e)
			}

			if(this.questionConfigError) return this.editForms.markAllAsTouched()

			this.queryControl = new QueryControl(
				new Question({ ...config }),
				() => {
					// do nothing if value changed
				},
				() => {
					// do not change value on cleanup
				}
			)
		})
	}


	public get error() : string | null {
		return this.getFirstError(this.editForms)
	}


	private hasError(control: AbstractControl): boolean {
		if(control.valid) 		return false
		if(control.disabled)	return false
		if(control.untouched)	return false
		if(control.errors && Object.keys(control.errors)[0] != null)
			return true
	}


	public getFirstError(control: AbstractControl = this.editForms): string | null {
		if (this.hasError(control))
			return Object.keys(control.errors)[0]

		if(	!(
			control instanceof FormGroup ||
			control instanceof FormArray
		))
			return null

		const subControls: AbstractControl[]
			= Array.isArray(control.controls) ? control.controls : Object.values(control.controls)

		const errorenousControl: AbstractControl = subControls.find(
			(it: AbstractControl) => { this.hasError(it) }
		)
		return (errorenousControl === undefined) ? null : Object.keys(errorenousControl.errors)[0]
	}

	public async cancel(): Promise<void> {
		await this.rccAlertController.confirm(
			'CONFIRM_CANCEL',
			'YES',
			'NO'
		)

		super.cancel()
	}


	// /**
	//  * Constructs Question from the form data.
	//  */
	public getResult(): ItemEditResult<Question> {
		return { item:  new Question(this.questionEditControl.questionConfig) }
	}


	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
