import	{
			Injectable,
			Inject
		}							from '@angular/core'

import	{
			DOCUMENT
		}							from '@angular/common'

import	{
			RccTranslationService,
			RccSettingsService
		}							from '@rcc/common'

import	{
			uuidv4,
			CalendarDateString,
			assert
		}							from '@rcc/core'

import	{
			settingsEntry
		}							from '../day-queries/day-query-reminders'

interface IcsReminderConfig {
	timeString			: string,
	frequency?			: 'daily' | 'weekly',
	startDateString?	: string
}

@Injectable()
export class IcsReminderService {



	public constructor(
		@Inject(DOCUMENT)
		private rccTranslationService	: RccTranslationService,
		private rccSettingsService		: RccSettingsService
	){}

	public async getIcsReminderData(config?: IcsReminderConfig): Promise<string> {

		await this.rccSettingsService.ready

		config					= config || {} as IcsReminderConfig

		const reminderControl	= await this.rccSettingsService.get(settingsEntry.id)
		const defaultReminder	= reminderControl.value as string

		config.timeString 		= config.timeString || defaultReminder

		console.log('defaultReminder', defaultReminder)

		assert(typeof config.timeString === 'string',  `IcsReminderService.getIcsReminderData(): bad timeString expected string, got: ${typeof config.timeString}`, config.timeString )
		assert(config.timeString.match(/^\d\d:\d\d$/), 'IcsReminderService.getIcsReminderData(): bad timeString Format, expected HH:MM, got: '+ config.timeString )

		config.frequency 		= config.frequency 			|| 'daily'
		config.startDateString	= config.startDateString 	|| CalendarDateString.today()

		const now			=	new Date().toISOString().match(/(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d)/)
		const date			=	now.slice(1,4).join('')
		const time			=	now.slice(4,6).join('')

		const eventName 	= 	this.rccTranslationService.translate('ICS_REMINDER.EVENT_NAME')
		const creationDate 	=	`${date}T${time}00Z`
		const uid			= 	uuidv4()+'@recovery.cat'
		const start			= 	`${date}T${config.timeString.replace(/:/g,'')}00`
		const end			= 	start
		const frequency		= 	config.frequency === 'daily'
								?	'DAILY'
								:	'WEEKLY'

		const reminderText	= 	eventName

		const icsTemplate 	=	`
									BEGIN:VCALENDAR

										VERSION:2.0
										PRODID:-RecoveryCat

										BEGIN:VEVENT

											DTSTAMP:${creationDate}
											UID:${uid}
											DTSTART:${start}
											RRULE:FREQ=${frequency}
											DTEND:${end}
											SUMMARY:${reminderText}

											BEGIN:VALARM
												ACTION:DISPLAY
												DESCRIPTION:${reminderText}
												TRIGGER:-PT0M
											END:VALARM

										END:VEVENT

									END:VCALENDAR
								`


		return icsTemplate.replace(/^\s*/gm,'')

	}

	public async promptDownload(config?: IcsReminderConfig, filename = 'recoverycat.ics') : Promise<void>{

		const data 				= await this.getIcsReminderData(config)
		const hiddenElement 	= document.createElement('a')

		hiddenElement.href 		= 'data:attachment/text,' + encodeURI(data)
		hiddenElement.target 	= '_blank'
		hiddenElement.download	= filename

		hiddenElement.click()
	}

}
