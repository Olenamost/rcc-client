import	{	NgModule				}	from '@angular/core'

import	{
			provideTranslationMap,
			provideSettingsEntry,
			SettingsEntry
		}								from '@rcc/common'

import	{
			settingsEntry,
			DayQueryRemindersModule
		}								from '../day-queries/day-query-reminders'

import	{	IcsReminderService		}	from './ics-reminder.service'

import en from './i18n/en.json'
import de from './i18n/de.json'


export const icsSettingsEntry = 	{
										deps: [IcsReminderService],
										factory: (icsReminderService: IcsReminderService) : SettingsEntry<null> => ({
											id: 			settingsEntry.id,
											label:			'ICS_REMINDER.SETTINGS.DOWNLOAD.LABEL',
											description:	'ICS_REMINDER.SETTINGS.DOWNLOAD.DESCRIPTION',
											path:			[...settingsEntry.path.slice(0,-1), 'ICS_REMINDER.SETTINGS.DOWNLOAD.LABEL'],
											icon:			'calendar',
											type:			'handler' as const,
											defaultValue:	null,
											handler:		() => icsReminderService.promptDownload()
										})
									}

@NgModule({
	imports:[
		DayQueryRemindersModule
	],
	providers:[
		IcsReminderService,
		provideTranslationMap('ICS_REMINDER', { en,de }),
		provideSettingsEntry(icsSettingsEntry)
	]

})
export class IcsRemindersServiceModule {

}
