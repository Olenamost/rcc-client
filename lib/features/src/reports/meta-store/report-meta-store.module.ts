import 	{
			NgModule,
			ModuleWithProviders,
			Type
		} 									from '@angular/core'

import 	{ 	RouterModule 			}		from '@angular/router'

import	{
			BaseMetaStoreModule,
			MetaAction,
			MetaStoreModule,
			provideMainMenuEntry,
			SharedModule,
            PathAction,
			provideTranslationMap
		}										from '@rcc/common'

import	{
			Report,
			ReportConfig,
			ReportStore,
		}										from '@rcc/core'


import	{	ReportMetaStoreService			}	from './report-meta-store.service'

import	{
			REPORT_STORES,
			REPORT_META_ACTIONS,
			ReportHomePath
		}										from './report-meta-store.commons'

import	{	ReportMetaStorePageComponent 	}	from './overview-page/overview-page.component'



import en from './i18n/en.json'
import de from './i18n/de.json'



const routes 				=	[
									{ path: ReportHomePath,	component: ReportMetaStorePageComponent	},
								]

const metaStoreConfig 		=	{
									itemClass:			Report,
									serviceClass:		ReportMetaStoreService
								}


export class MenuEntryReportMetaStoreService {}

const menuEntry: PathAction   =   {
    position: -3,
    path: ReportHomePath,
    label: 'REPORT_META_STORE.MENU_ENTRY',
    icon: 'report',
}


@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		MetaStoreModule.forChild(metaStoreConfig),
	],
	declarations: [
		ReportMetaStorePageComponent,
	],
	providers:[
		ReportMetaStoreService,
		provideMainMenuEntry(menuEntry),
		provideTranslationMap('REPORT_META_STORE', { en, de })
	],
	exports: [
		ReportMetaStorePageComponent,
		// this is important so anything importing ReportMetaStoreServiceModule
		// can use the components of MetaStoreModule too
		MetaStoreModule
	]
})
export class ReportMetaStoreServiceModule extends BaseMetaStoreModule {

	public static 	forChild(
				stores?			: Type<ReportStore>[],
				metaActions?	: MetaAction<ReportConfig, Report, ReportStore>[]
			): ModuleWithProviders<ReportMetaStoreServiceModule> {

		const mwp = BaseMetaStoreModule.getModuleWithProviders<ReportMetaStoreServiceModule>(this, stores, metaActions, REPORT_STORES, REPORT_META_ACTIONS)
		return mwp
	}

}
