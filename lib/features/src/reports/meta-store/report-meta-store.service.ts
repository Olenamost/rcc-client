import 	{
			Inject,
			Injectable,
			Optional
		} 								from '@angular/core'


import	{
			MetaStore,
		}								from '@rcc/common'

import	{
			Report,
			ReportConfig,
			ReportStore
		}								from '@rcc/core'

import	{
			REPORT_STORES
		}								from './report-meta-store.commons'

@Injectable()
export class ReportMetaStoreService extends MetaStore<ReportConfig, Report, ReportStore>{

	public name = 'REPORT_META_STORE.NAME'

	public constructor(
		@Optional() @Inject(REPORT_STORES)
		stores		: ReportStore[],

	) {
		super(stores)
	}
}
