import	{	InjectionToken 	}		from '@angular/core'
import 	{
			Report,
			ReportConfig,
			ReportStore,
		}							from '@rcc/core'

import	{
			MetaAction
		}							from '@rcc/common'



export const REPORT_STORES 			= new InjectionToken<ReportStore>('Report Stores')
export const REPORT_META_ACTIONS 	= new InjectionToken<MetaAction<ReportConfig, Report, ReportStore>>('Meta actions for reports')

export const ReportHomePath 		= 'reports'
