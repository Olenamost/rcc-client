import 	{ 	NgModule 						}	from '@angular/core'
import 	{ 	RouterModule, Routes 			}	from '@angular/router'

import 	{ 	Report 							}	from '@rcc/core'
import	{
			SharedModule,
			WidgetsModule,
			provideTranslationMap,
			ItemAction,
			Factory,
			RccModalController,
			provideItemAction,
		}										from '@rcc/common'


import	{	DataVisualizationModule			}	from '../../data-visualization'
import	{	ReportMetaStoreServiceModule			}	from '../meta-store'
import	{	ReportViewComponent				}	from './view'
import	{	ReportBasicViewPageComponent	}	from './view-page'


import en from './i18n/en.json'
import de from './i18n/de.json'


const routes: Routes = [
	{ path: 'reports/:id',	component: ReportBasicViewPageComponent	},
]

const reportDetailsItemActionFactory : Factory<ItemAction<Report>> = {

	deps: [RccModalController],

	factory(...args: unknown[]) {
		const rccModalController = args[0] as RccModalController
		return {
				itemClass:	 		Report,
				role: 				'details' as const,
				getAction: (report: Report) => ({
					label: 			'SESSIONS.HOME.INSPECT_DATA.LABEL' as const,
					description: 	'SESSIONS.HOME.INSPECT_DATA.DESCRIPTION' as const,
					icon: 			'view' as const,
					handler: 		() => rccModalController.present(ReportViewComponent, { report }),
					position: 0,
				})
			}
	}

}


@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		ReportMetaStoreServiceModule.forChild(null),
		DataVisualizationModule,
		WidgetsModule
	],
	providers:[
		provideItemAction(reportDetailsItemActionFactory),
		provideTranslationMap('REPORT_BASIC_VIEW', { en, de }),
	],
	declarations: [
		ReportBasicViewPageComponent,
		ReportViewComponent
	],
})
export class ReportBasicViewModule {}
