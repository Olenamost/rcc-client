import	{	Component 				}	from '@angular/core'
import	{
			Report,
		}								from '@rcc/core'

import	{	ExposeTemplateComponent	}	from '@rcc/common'


@Component({
	templateUrl: './basic-report-label.component.html',
})
export class BasicReportLabelComponent extends ExposeTemplateComponent{

	public numberOfQuestions 	: number
	public numberOfEntries		: number
	public timeSpan				: number

	public constructor(public report: Report){
		super()

		const questionIds 		= 	report.entries.map( entry => entry.questionId )
		const targetDays		= 	report.entries.map( entry => entry.targetDay )
									.sort()

		const firstDay			=	targetDays.shift()
		const lastDay			=	targetDays.pop() || firstDay

		const timestamp_first	=	new Date(firstDay).getTime()
		const timestamp_last	=	new Date(lastDay).getTime()

		this.numberOfQuestions 	=	new Set(questionIds).size
		this.numberOfEntries	=	report.entries.length
		this.timeSpan			=	1 + Math.round( Math.abs(timestamp_last - timestamp_first ) / (1000*60*60*24) )

	}


}
