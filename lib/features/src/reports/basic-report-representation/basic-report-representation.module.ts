import 	{
			NgModule,
		}									from '@angular/core'

import	{
			CommonModule
		}									from '@angular/common'

import	{
			provideItemRepresentation,
			provideTranslationMap,
			TranslationsModule,
			RccToIDPipeModule
		}									from '@rcc/common'
import	{	Report						}	from '@rcc/core'
import	{	BasicReportLabelComponent	}	from './basic-report-label'


import en from './i18n/en.json'
import de from './i18n/de.json'

const basicReportRepresentation = {
	itemClass:		Report,
	name:			'REPORT',
	icon:			'report',
	labelComponent:	BasicReportLabelComponent,
	cssClass:		'report-label'
}

@NgModule({
	imports:[
		CommonModule,
		TranslationsModule,
		RccToIDPipeModule
	],
	providers:[
		provideTranslationMap('REPORTS.BASIC_LABEL', { en,de }),
		provideItemRepresentation(basicReportRepresentation)
	],
	declarations:[
		BasicReportLabelComponent
	]
})
export class BasicReportRepresentationModule {}
