import	{
			Injectable,
		}								from '@angular/core'
import	{	Router					}	from '@angular/router'
import	{
			SubscriptionLike,
		}								from 'rxjs'
import	{
			map,
			filter,
		}								from 'rxjs/operators'
import	{
			IncomingDataService,
			RccAlertController
		}								from '@rcc/common'

import 	{
			ReportStore,
			Report,
			ReportConfig,
		}								from '@rcc/core'

import	{
			ReportHomePath
		}								from '../meta-store'



@Injectable()
export class ReportImportStoreService extends ReportStore {


	public name = 'REPORT_IMPORT_STORE.NAME'


	private subscriptions: SubscriptionLike[] = []

	public constructor(
		private incomingDataService		: IncomingDataService,
		private rccAlertController		: RccAlertController,
		private router					: Router
	){
		super()
		this.listenToIncomingDataService()
	}


	protected listenToIncomingDataService() : void {
		this.incomingDataService
		.pipe(
			map( 	(data:unknown) 					=> Report.findConfigs(data) ),
			filter(	(configs:ReportConfig[])		=> configs.length > 0 )
		)
		.subscribe({
			next: 	(reportConfigs: ReportConfig[]) => this.addReportConfigs(reportConfigs),
			error:	(e)								=> console.warn(e) // TODO: log, auch bei den anderen Importstores gucken
		})
	}

	public async addReportConfigs(configs: ReportConfig[]): Promise<void>{

		configs.forEach( config => this.addConfig(config) )

		await 	this.rccAlertController.present({
					header: 	'REPORT_IMPORT_STORE.SUCCESS.HEADER',
					message: 	'',
					buttons:	[
									{
										label:		'REPORT_IMPORT_STORE.SUCCESS.OKAY',
										rejectAs: 	'okay'
									},
									{
										label:		'REPORT_IMPORT_STORE.SUCCESS.GOTO',
										resolveAs: 	'goto'
									}
								]
				})
				.then(
					() 		=> this.router.navigate([ReportHomePath]),
					reason 	=> console.log('ReportImportStoreService.addReportConfigs()', reason)
				)

	}

	public async deleteReport(report: Report): Promise<Report> {
		if(!this.removeItem(report)) throw new Error('ReportImportStoreService.delete: Unable to delete report with id: ' + report.id)

		await this.storeAll()

		return report
	}

}
