import { NgModule }							from '@angular/core'
import {
	provideWidget,
	SharedModule,
	WidgetsModule
}											from '@rcc/common'
import { BasicQuestionEditWidgetsModule }	from '../basic-question-edit-widgets'
import { QuadQuestionEditWidgetComponent }	from './quad-question-edit-widget/quad-question-edit-widget.component'

const widgets = [
	QuadQuestionEditWidgetComponent,
]

@NgModule({
	imports: [
		SharedModule,
		WidgetsModule,
		BasicQuestionEditWidgetsModule,
	],
	providers: [
		...widgets.map(provideWidget),
	],
	declarations: [
		...widgets,
	],
	exports: [
		...widgets,
	]
})
export class QuadQuestionEditModule {}
