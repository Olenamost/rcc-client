import 	{
			NgModule,
		}										from '@angular/core'

import	{	RouterModule					}	from '@angular/router'

import	{	map								}	from 'rxjs/operators'

import	{
			SharedModule,
			MainMenuModule,
			HomePageModule,
			NotificationModule,
            provideMainMenuEntry,
			provideTranslationMap,
			provideHomePageEntry
		}										from '@rcc/common'

import	{	QueriesModule						}	from '../queries'

import	{	JournalServiceModule				}	from '../entries'
import	{	SymptomCheckMetaStoreServiceModule	}	from '../symptom-checks/meta-store'

import	{	DueQuestionsService					}	from './due-questions.service'
import	{	DueQuestionsOverviewPageComponent	}	from './overview-page/overview-page.component'
import	{	DueQuestionsHeaderItemComponent		}	from './due-questions-header-item.component'
import	{	DueQuestionsMainMenuEntryComponent	}	from './due-questions-main-menu-entry.component'
import	{
			DueQuestionsHomePageEntryComponent
		}										from './due-questions-home-page-entry.component'
import	{	DueQuestionsHomePath			}	from './due-questions-commons'

import	en from './i18n/en.json'
import	de from './i18n/de.json'




const routes 				=	[
									{
										path: 		DueQuestionsHomePath,
										component: 	DueQuestionsOverviewPageComponent
									}
								]

const notificationConfig 	=	{
									deps:		[DueQuestionsService],
									factory:	(dqs: DueQuestionsService) => dqs.pipe(map( x => x && x.questions.length || 0 ))
								}


const homePageEntry		=	{
									position:		2,
									label:			'DUE_QUESTIONS.MENU_ENTRY',
									icon:			'notification',
									path:			DueQuestionsHomePath,
									childComponent: DueQuestionsHomePageEntryComponent
								}

const mainMenuEntry			=	{
									position:   2,
									label:      'DUE_QUESTIONS.MENU_ENTRY',
									icon:       'notification',
									path:       DueQuestionsHomePath,
								}


@NgModule({
	imports: [
		JournalServiceModule,
		MainMenuModule,
		NotificationModule.forChild([notificationConfig]),
		QueriesModule,
		RouterModule.forChild(routes),
		SharedModule,
		SymptomCheckMetaStoreServiceModule,
		HomePageModule,
	],
	declarations:[
		DueQuestionsMainMenuEntryComponent,
		DueQuestionsHomePageEntryComponent,
		DueQuestionsOverviewPageComponent,
		DueQuestionsHeaderItemComponent
	],
	providers: [
		DueQuestionsService,
        provideMainMenuEntry(mainMenuEntry),
		provideHomePageEntry(homePageEntry),
		provideTranslationMap('DUE_QUESTIONS', { en, de })
	],
	exports:[
		DueQuestionsHomePageEntryComponent,
	]
})
export class DueQuestionsModule {}
