import 	{
			Component,
			OnDestroy
		}									from '@angular/core'
import	{	Subscription				}	from 'rxjs'
import	{	MainHeaderItemComponent		}	from '@rcc/common'
import	{	DueData						}	from './due-questions-commons'
import	{	DueQuestionsService			}	from './due-questions.service'

@Component({
	template:	`
					<ng-template>
						<ion-button
							*ngIf		= "questionCount > 0"
							routerLink  = "due-questions"
						>
							<ion-icon
								[name] 	= "'notification' | rccIcon"
								slot	= "icon-only"
								color	= "secondary"
							>
							</ion-icon>
						</ion-button>
					</ng-template>`
})

export class DueQuestionsHeaderItemComponent extends MainHeaderItemComponent implements OnDestroy {

	public questionCount : number

	private subscription : Subscription

	public constructor(
		public dueQuestionsService	: DueQuestionsService
	){
		super()

		this.dueQuestionsService
		.subscribe( (result: DueData) => this.questionCount = result.questions.length )

	}

	public ngOnDestroy(): void{
		this.subscription.unsubscribe()
	}
}
