import 	{	Injectable				}	from '@angular/core'
import	{
			CalendarDateString,
			sortByKeyFn,
			Question,
			assert
		}								from '@rcc/core'
import	{	RccTranslationService	}	from '@rcc/common'
import	{
			DataViewControl,
			Dataset,
			Datapoint,
		}								from '../data-visualization'
import	{
			CombinedData,
		}								from './basic-data-view-widgets.commons'
import 	{
			getDayOfWeek,
			isSameDay,
		}								from '@rcc/core'

@Injectable()
export class BasicDataViewService {

	public weekDays = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA']

	public constructor(
		protected rccTranslationService	: RccTranslationService
	){}


	public getDateStringRange(datasets: Dataset[]): string[] {

		if(datasets.length === 0) return []

		const datapoints 	: Datapoint[] 	= 	datasets.map(dataset => dataset.datapoints).flat()
		const dateStrings 	: string[] 		= 	datapoints.map(datapoint => datapoint.date)
		const uniqueDateStrings : string[] 	= 	Array.from(new Set(dateStrings))
		const sortedDateStrings : string[]	= 	uniqueDateStrings.sort()

		const firstDateStr: string 	= 	sortedDateStrings.shift()
		const lastDateStr : string	= 	sortedDateStrings.pop() || firstDateStr

		// Always start with a monday:
		const startDate : 	string	=	CalendarDateString.previousMonday(firstDateStr)

		// Always end on a sunday:
		const endDate :		string	=	CalendarDateString.nextSunday(lastDateStr)

		return 	CalendarDateString.range(startDate, endDate)
	}

	public getLabels(dateRange: string[]): string[] {


		const labels			=	dateRange.map( dateStr => {

										const day_number 	= getDayOfWeek(dateStr)
										const day_string	= this.weekDays[day_number]
										const day_label		= this.rccTranslationService.translate(`DAYS.SHORT.${day_string}`)

										return day_label
									})


		return labels
	}


	public getValue(dataset: Dataset, dateStr: string): unknown {

		const datapoints 	= 	dataset.datapoints
								.filter( datapoint => isSameDay(datapoint.date, dateStr))

		// If there are multiple datapoints at the same date,
		// take the most recent one:
		const datapoint		=	datapoints
								.sort( sortByKeyFn('date') )
								.reverse()[0]

		const is_boolean	=	dataset.question.type === 'boolean'

		if(!is_boolean) return 	datapoint
								?	datapoint.value
								:	null

		if(!datapoint)					return 0
		if(datapoint.value === true)	return 1
		if(datapoint.value === false)	return -1

		return null

	}

	public getValues(dataset: Dataset, dateRange: string[]) : unknown[] {
		return dateRange.map( date => this.getValue(dataset, date) )

	}

	public getNumericAverage( data: number[]): number | null{

		const non_null_values 	= data.filter(v => v !== null)
		const size 				= non_null_values.length

		if(size === 0) return null

		return non_null_values.reduce( (s,x) => s+x, 0 )/size
	}


	public getBooleanAverage( data: boolean[]): number | null {

		const non_null_values 	= data.filter(v => v !== null)
		const size 				= non_null_values.length

		if(size === 0) return null

		return non_null_values.filter( v => v ).length/size
	}

	public getStringAverage( data: string[]): Record<string,number> | null {

		const non_null_values 	= data.filter(v => v !== null)
		const size 				= non_null_values.length

		const result 			= {} as Record<string,number>

		if(size === 0) return null

		non_null_values.forEach( value => {
			result[value] =  result[value] || 0
			result[value] += 1/size
		})

		return result
	}

	public getAverages(dataset: Dataset, dateRange : string[], collect : (dateStr: string) => string|number) :	unknown[] {

		const allValues			=	this.getValues(dataset, dateRange)

		const collectedValues	:	Record<string|number, unknown[]> = {}

		dateRange.forEach( (dateString: string, index: number) => {

			const collectionName 	= collect(dateString)
			const value				= allValues[index]

			collectedValues[collectionName] = collectedValues[collectionName] || []

			if(value !== null) collectedValues[collectionName].push( value )

		})

		const collectionAverages	:	Record<string, unknown> = {}


		Object.entries(collectedValues).forEach( ([collectionName, values]) => {

			if( values.length === 0 ) return collectionAverages[collectionName] = null

			if( values.every(v => typeof v === 'number' ) )	collectionAverages[collectionName] = this.getNumericAverage(values as number[])

			if( values.every(v => typeof v === 'boolean') )	collectionAverages[collectionName] = this.getBooleanAverage(values as boolean[])

			if( values.every(v => typeof v === 'string' ) )	collectionAverages[collectionName] = this.getStringAverage(values as string[])

		})


		const averages 	= 	dateRange.map((date: string) => {
								const collectionName = collect(date)
								return collectionAverages[collectionName]
							})


		return averages
	}



	public getWeekAverages(dataset: Dataset, dateRange: string[]): unknown[] {

		// Average over all values that have the same preceding Monday, aka are in the same week:
		return this.getAverages(dataset, dateRange, (date: string) => CalendarDateString.previousMonday(date))
	}

	public getWeekDayAverages(dataset: Dataset, dateRange: string[]): unknown[] {

		// Average over all values at the same day of week:
		return this.getAverages(dataset, dateRange, date => getDayOfWeek(date) )
	}

	/**
	 * Calculates the smallest possible answer value. This is either ```.min````
	 * or the highest possible number option. Returns ```null``` if a minimum
	 * cannot be determined (e.g. question is a string type question or there is no limit)
	 */
	public getMin(question:Question): number | null {

		if(question.type === 'boolean')						return -1

		if(!['decimal', 'integer'].includes(question.type))	return null

		if(typeof question.min === 'number')					return question.min
		if(!question.options)								return null

		const values = question.options.map( o => o.value )

		if(values.some( v => typeof v !== 'number'))			return null

		return 	(values as number[]).reduce(
					(min, v) => Math.min(min, v),
					values[0] as number
				)

	}

	/**
	 * Calculates the largest possible answer value. This is either ```.min````
	 * or the highest possible number option. Returns ```null``` if a maximum
	 * cannot be determined (e.g. question is a string type question or there is no limit)
	 */
	public getMax(question:Question): number | null {

		if(question.type === 'boolean')							return 1

		if(!['decimal', 'integer'].includes(question.type) ) 	return null

		if(typeof question.max === 'number')						return question.max
		if(!question.options)									return null

		const values = question.options.map( o => o.value )

		if(values.some( v => typeof v !== 'number'))				return null

		return	(values as number[]).reduce(
					(max, v) => Math.max(max, v),
					values[0] as number
				)
	}

	/**
	 * Calculates the distance between answer options. Return null if it cannot
	 * be determined (e.g. question is a string type question or the distance
	 * varies incoherently between answer options)
	 */
	public getStepSize(question:Question): number | null {

		if(question.type === 'boolean')							return 1


		if(!['decimal', 'integer'].includes(question.type) ) 	return null

		if(!question.options && question.type === 'integer')		return 1
		if(!question.options && question.type !== 'integer')		return null

		if(question.options.length < 2)								return null

		assert(
			question.options.every( o => typeof o.value === 'number'),
			`BasicDataViewService.getStepSize an option has a non-number value, although the answer type is ${question.type}`,
			question.options
		)

		// Get all the values and sort them.
		const values 	= 	( question.options.map( o => o.value ) as number[] )
							.sort( (a,b) => a-b )

		// Since the values are sorted the minimum distance overall
		// will be the minimum distance of adjacent values.
		const min_step 	= 	values.reduce(
								(min, v, index) =>	index === 0
													?	min
													:	Math.min(min, values[index] - values[index-1])
								, Infinity
							)

		// This should never happen:
		if([0, Infinity].includes(min_step)){
			console.warn(`BasicDataViewService.getStepSize min_step is ${min_step}; that should never happen. Maybe .options is an empty error or two options have the same value.`)
			return null
		}

		const min 		=	values[0]
		const max		=	values[values.length-1]

		let on_track	=	0

		// If we can reach every value by taking minimal steps,
		// we found ourselves a proper step size.
		for (let i = min; i <= max; i+= min_step)
			if ( values.includes(i) ) on_track++

		return	on_track === values.length
				?	min_step
				:	null

	}


	public getCombinedDailyData(dataViewControl: DataViewControl): CombinedData {


		const datasets		= dataViewControl.datasets
		const questions		= datasets.map( ds => ds.question)

		const dateStrings: string[]	= this.getDateStringRange(datasets)
		const labels		= this.getLabels(dateStrings)

		const data 			= datasets.map( dataset => this.getValues(dataset, dateStrings))
		const weekAvg		= datasets.map( dataset => this.getWeekAverages(dataset, dateStrings))
		const weekDayAvg	= datasets.map( dataset => this.getWeekDayAverages(dataset, dateStrings))

		const min 			= datasets.map( ds => this.getMin(ds.question) )
		const max 			= datasets.map( ds => this.getMax(ds.question) )
		const stepSize 		= datasets.map( ds => this.getStepSize(ds.question) )


		const getComplexLabel = (index: number): string[] => {

			const dateStr		= dateStrings[index]
			const day_number 	= getDayOfWeek(dateStr)
			const day_string	= this.weekDays[day_number]
			const day_label		= this.rccTranslationService.translate(`DAYS.LONG.${day_string}`)

			const date_label	= this.rccTranslationService.translate(dateStr , { dateStyle:'short' })

			return [day_label, date_label]
		}

		const getMediumLabel = (index: number): string[] => {

			const dateStr		= dateStrings[index]
			const day_number 	= getDayOfWeek(dateStr)
			const day_string	= this.weekDays[day_number]
			const day_label		= this.rccTranslationService.translate(`DAYS.SHORT.${day_string}`)

			const date_label	= this.rccTranslationService.translate(dateStr , { day:'numeric', month: 'numeric' })

			return [day_label, date_label]
		}

		return { data, dateStrings, labels, min, max, stepSize, weekAvg, weekDayAvg, getComplexLabel, getMediumLabel, questions }
	}



}
