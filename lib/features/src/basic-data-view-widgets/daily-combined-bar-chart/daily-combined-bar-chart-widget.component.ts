import	{
			Component,
			AfterViewInit,
			ElementRef,
			HostBinding,
			Inject,
		}											from '@angular/core'
import	{	DOCUMENT							}	from '@angular/common'
import	{
			CategoryScale,
			Chart,
			LinearScale,
			LineController,
			BarController,
			LineElement,
			PointElement,
			BarElement,
			Filler,
			TooltipItem,
		}											from 'chart.js'
import  {
			RccTranslationService,
			WidgetComponent,
		} 											from '@rcc/common'
import	{
			getDayOfWeek,
			Question
		}											from '@rcc/core'
import	{	DataViewControl						}	from '../../data-visualization'
import	{	CombinedData						}	from '../basic-data-view-widgets.commons'
import	{	BasicDataViewService				}	from '../basic-data-view.service'

// Chart.js setup
Chart.register(
	BarController,
	BarElement,
	CategoryScale,
	LinearScale,
	LineController,
	LineElement,
	PointElement,
	Filler,
)


@Component({
	templateUrl: 	'./daily-combined-bar-chart-widget.component.html',
	styleUrls:		['./daily-combined-bar-chart-widget.component.css']
})
export class DailyCombinedBarChartWidgetComponent extends WidgetComponent<DataViewControl> implements AfterViewInit {

	// STATIC

	public static controlType = DataViewControl


	public static widgetMatch( dataViewControl	: DataViewControl ): number {
		const answer_types	= dataViewControl.datasets.map( ds => ds.question.type )
		const number_like 	= answer_types.every( answertype => ['integer', 'decimal', 'boolean'].includes(answertype))
		const some_boolean	= answer_types.every( answertype => answertype === 'boolean')

		if(!number_like) 	return -1
		if(some_boolean)	return 0

		return 1
	}



	// INSTANCE:


	// The number of data points will be used to adjust styles in order to set
	// the width of the wrapper div. This way we can make the chart scroll rather
	// than squeeze all the data points onto one screen.
	@HostBinding('style.--number-of-data-points')
	public get getNumberOfDataPoints(): number { return this.combinedData?.dateStrings.length }
	public numberOfDataPoints			: number

	public canvas 						: HTMLCanvasElement
	public combinedData					: CombinedData
	public legend						: Record<string, unknown> & {primaryQuestion: Question, secondaryQuestion: Question}


	public constructor(
		@Inject(DOCUMENT)
		private document				: Document,
		private elementRef				: ElementRef<HTMLCanvasElement>,
		private rccTranslationService	: RccTranslationService,

		public dataViewControl			: DataViewControl,
		public basicDataViewService		: BasicDataViewService,
	){
		super(dataViewControl)

		this.combinedData = this.basicDataViewService.getCombinedDailyData(dataViewControl)

		this.legend = {
			firstDate:			this.combinedData.dateStrings[0],
			lastDate:			this.combinedData.dateStrings[this.combinedData.dateStrings.length-1],
			primaryQuestion:	this.combinedData.questions[0],
			secondaryQuestion:	this.combinedData.questions[1],
		}

	}


	public ngAfterViewInit() : void {
		this.canvas	= this.elementRef.nativeElement.querySelector('canvas')
		this.setupCharts()
	}


	public setupCharts(): void {

		// TODO: Colors should be managed by a separate central service.

		const labels			=	this.combinedData.labels.map( (l, index) => this.combinedData.getMediumLabel(index))

		const primary_rgb 		= 	getComputedStyle(this.document.documentElement)
									.getPropertyValue('--ion-color-primary-rgb')		// TODO: maybe create utility function for this

		const secondary_rgb 	= 	getComputedStyle(this.document.documentElement)
									.getPropertyValue('--ion-color-secondary-rgb')		// TODO: maybe create utility function for this

		const primary_color		=	primary_rgb 	? 	`rgb(${primary_rgb})`			: 'rgba(0,0,0,0)'
		const secondary_color	=	secondary_rgb 	? 	`rgb(${secondary_rgb})`			: 'rgba(0,0,0,0)'

		const afterChartRendered: () => void = () => {
			const legend		= this.elementRef.nativeElement.querySelector('.legend')

			this.canvas.scrollIntoView({ inline: 'end' }) // effects only portrait orientation
			legend.scrollIntoView({ inline: 'end' }) // effects only landscape orientation
		}

		// TODO: Make chart config publicly accessible for extensions.

		// TODO: why is everything scrolling to the start, when datasets are changed?




		if(!this.combinedData.data[0]) return null


		const options0	= 	this.combinedData.questions[0]?.options ||[]
		const options1	= 	this.combinedData.questions[1]?.options ||[]


		const booleanDefaultLabels = 	{
											'1': 	this.rccTranslationService.translate('TRUE'),
											'-1': 	this.rccTranslationService.translate('FALSE')
										} as Record<string, string>

		const yAxis =	{
			beginAtZero:	true,
			suggestedMin: 	this.combinedData.min[0] !== null ? this.combinedData.min[0] 	: undefined,
			suggestedMax: 	this.combinedData.max[0] !== null ? this.combinedData.max[0] 	: undefined, // TODO
			ticks:{
				stepSize: 	this.combinedData.stepSize[0],
				callback:	(value:unknown) : string |  string[] => {

								const option0 		= 	options0.find( o => o.value === value)
								const option1 		= 	options1.find( o => o.value === value)

								const optionLabel0	= 	option0 && this.rccTranslationService.translate(option0)
								const optionLabel1	= 	option1 && this.rccTranslationService.translate(option1)

								const boolean0		= 	this.combinedData.questions[0]?.type === 'boolean'
								const boolean1		= 	this.combinedData.questions[1]?.type === 'boolean'

								const booleanLabel0	=	optionLabel0 || boolean0 && booleanDefaultLabels[String(value)]
								const booleanLabel1	=	optionLabel1 || boolean1 && booleanDefaultLabels[String(value)]

								const yLabels		= 	[
															String(value),
															optionLabel0 || booleanLabel0,
															optionLabel1 || booleanLabel1
														]
														.filter(x => !!x)

								const uniqueLabels	= Array.from( new Set(yLabels))

								return 	uniqueLabels.length
										?	uniqueLabels
										:	'?'

							}
			},
		}

		// dataset config:
		const mainDataSets = []


		if(this.combinedData.data[0])
			mainDataSets.push({
				data: 				this.combinedData.data[0],
				minBarLength:		10,
				backgroundColor: 	primary_color,
				borderColor:		primary_color,
			})


		if(this.combinedData.data[1])
			mainDataSets.push({
				data: 				this.combinedData.data[1],
				minBarLength:		10,
				backgroundColor: 	secondary_color,
				borderColor:		secondary_color,
				borderWidth:		1,
			})


		new Chart( this.canvas , {
			type:	'bar' as const,
			data:	{
						labels,
						datasets: [
							...mainDataSets,
							{
								type:				'bar',
								data:				this.combinedData.dateStrings.map(
														(dateStr) => [1,2,3,4,5].includes(getDayOfWeek(dateStr))
														?	0
														: 	1
													),
								backgroundColor:	'rgba(0,0,0,0.1)',
								categoryPercentage:	1,
								barPercentage:		1,
								xAxisID:			'xWeekend',
								yAxisID:			'yWeekend'
							},

						]
					},

			options: {
				scales: {
					yAxisLeft: 	{ ...yAxis, position:'left' },
					yAxisRight:	{ ...yAxis, position:'right' },
					yWeekend:	{ display: false },

					x:{
						ticks: {
							minRotation: 0,
							maxRotation: 0
						}
					},

					xSecondary:	{ display: false, offset: true },
					xWeekend:	{ display: false }
				},
				responsive: 			true,
				maintainAspectRatio:	false,
				animation: 				{
											duration: 0,
										},
				onResize: () => afterChartRendered(),
				plugins: {
						tooltip: {
							mode: 'index',
							filter: (item: TooltipItem<'bar'>):boolean => [0,1].includes(item.datasetIndex),

							callbacks:{

								title: (items: TooltipItem<'bar'>[]): string[] => {
									if(!items[0]) return ['error']

									const dataIndex 	= items[0].dataIndex
									const title_parts 	= this.combinedData.getComplexLabel(dataIndex)

									return title_parts
								},

								label: (item: TooltipItem<'bar'>): string => {

									const datasetIndex	=	item.datasetIndex
									const question 		= 	this.combinedData.questions[datasetIndex]
									const is_boolean	=	question?.type === 'boolean'
									const options		=	question?.options || []

									const getFormattedValue = () : string | null  => {
										if(typeof item.raw !== 'number')	return '?'
										if(!is_boolean)					return `${item.formattedValue || item.raw}`

										if(item.raw ===  1) 			return this.rccTranslationService.translate('TRUE')
										if(item.raw === -1) 			return this.rccTranslationService.translate('FALSE')
									}

									const value			=	getFormattedValue()

									if(!options) 			return value

									const option		=	options.find( op => op.value === item.raw)
									const annotation	=	item.raw
															?	`(${this.rccTranslationService.translate(option)})`
															:	''

									return `${value} ${annotation}`.trim()

								}
							}
						}
				}
			}

		})

	}

}
