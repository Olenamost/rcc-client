import 	{
			NgModule,
		}												from '@angular/core'

import	{
			SharedModule,
			WidgetsModule,
			provideWidget,
			provideTranslationMap
		} 												from '@rcc/common'

// import	{	DailyBarWidgetComponent					}	from './daily-bar-chart'
import	{	DailyCombinedBarChartWidgetComponent	}	from './daily-combined-bar-chart'
import	{	BasicDataViewService					}	from './basic-data-view.service'
// import	{	PieChartWidgetComponent				}	from './pie-chart/pie-chart.component'
// import	{	MultiLineChartWidgetComponent	t	}	from '../_basic-data-view-widgets/multi-line-chart/multi-line-chart.component'


import en from './i18n/en.json'
import de from './i18n/de.json'

const dataViewWidgets 	=	[
								DailyCombinedBarChartWidgetComponent,
								// DailyBarWidgetComponent
							]

@NgModule({
	imports: [
		SharedModule,
		WidgetsModule,
	],
	providers:[
		...dataViewWidgets.map(provideWidget),
		provideTranslationMap('BASIC_DATA_VIEW_WIDGETS', { en,de }),
		BasicDataViewService
	],
	declarations: [
		...dataViewWidgets,
	],
	exports: [
		...dataViewWidgets
	]
})
export class BasicDataViewWidgetsModule { }
