import 	{	NgModule									}	from '@angular/core'
import	{
			WidgetsModule,
			provideWidget,
			provideTranslationMap,
			SharedModule,
		}													from '@rcc/common'
import	{	YesNoQuestionEditWidgetComponent			}	from './yes-no/yes-no.component'
import	{	InputQuestionEditWidgetComponent			}	from './input/input.component'
import	{	ChoiceQuestionEditWidgetComponent			}	from './choice/choice.component'
import	{	ScaleQuestionEditWidgetComponent			}	from './scale/scale.component'
import  {	ChoiceOptionItemComponent					}	from './components/choice-option-item/choice-option-item.component'


const widgets =	[
					YesNoQuestionEditWidgetComponent,
					ChoiceQuestionEditWidgetComponent,
					InputQuestionEditWidgetComponent,
					ScaleQuestionEditWidgetComponent,
				]

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		SharedModule,
		WidgetsModule,
	],
	providers: [
		...widgets.map(provideWidget),
		provideTranslationMap('BASIC_QUESTION_EDIT_WIDGETS', { en,de }),
	],
	declarations: [
		...widgets,
		ChoiceOptionItemComponent,
	],
	exports: [
		...widgets,
		ChoiceOptionItemComponent,
	]
})
export class BasicQuestionEditWidgetsModule {

}
