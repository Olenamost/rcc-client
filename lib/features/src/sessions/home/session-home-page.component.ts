import	{
			Component,
			OnDestroy
		} 													from '@angular/core'
import	{	Router 										}	from '@angular/router'
import	{
			Report,
			Session,
			SymptomCheck,
			SymptomCheckConfig,
			assert,
		} 													from '@rcc/core'
import	{
			map,
			merge,
			concat,
			mergeMap,
			Observable,
			Subject,
			takeUntil,
		} 													from 'rxjs'
import	{
			Action,
			ItemAction,
			RccModalController,
			RccAlertController,
			RccToastController,
			ShareDataService,
			ItemSelectService,
		}													from '@rcc/common'
import	{	QuestionnaireService						} 	from '../../questions'
import	{	OpenSessionStoreService 					}	from '../open-sessions'

@Component({
	templateUrl: './session-home-page.component.html'
})
export class SessionHomePageComponent implements OnDestroy {

	public activeActions: Action[] = []
	public reportExtraActions: ItemAction<Report>[] = []
	public sessionActions: ItemAction<Session>[] = []
	public symptomCheckActions: ItemAction<SymptomCheck>[] = []

	public activeSymptomCheckUpdated: Observable<boolean>

	public destroy$ = new Subject<void>()

	public constructor(
		private itemSelectService		: ItemSelectService,
		private openSessionStoreService	: OpenSessionStoreService,
		private questionnaireService	: QuestionnaireService,
		private rccModalController		: RccModalController,
		private rccAlertController		: RccAlertController,
		private rccToastController		: RccToastController,
		private router					: Router,
		private shareDataService		: ShareDataService,
	) {
		void this.setup()
	}

	protected get activeSession(): Session | null { return this.openSessionStoreService.activeSession }
	protected get activeSymptomCheck(): SymptomCheck { return this.openSessionStoreService.activeSession?.symptomCheck }
	protected get activeReport(): Report | null { return this.openSessionStoreService.activeSession?.report }
	protected get startDate(): Date { return this.openSessionStoreService.activeSession?.startDate }
	protected get activeSymptomCheckBackup(): SymptomCheckConfig { return this.openSessionStoreService.symptomCheckBackups.get(this.activeSymptomCheck) }


	public async setup(): Promise<void> {

		this.activeSymptomCheckUpdated = merge(
			this.openSessionStoreService.activeSession$,
		)
			.pipe(
				mergeMap((session) => concat([this.activeSession?.symptomCheck], session?.symptomCheck?.update$ || [])),
				map(() => this.getActiveSymptomCheckUpdate()),
			)

		this.openSessionStoreService.removal$
			.pipe(takeUntil(this.destroy$))
			.subscribe(() => (this.openSessionStoreService.items.length === 0) && this.exit())



		const closeActiveSessionAction: Action = {

			label: 			'SESSIONS.HOME.CLOSE_SESSION.LABEL',
			description: 	'SESSIONS.HOME.CLOSE_SESSION.DESCRIPTION',
			icon: 			'close',
			handler:		 () => this.closeActiveSession(),

		}


		const switchSessionAction: Action = {

			label: 			'SESSIONS.HOME.SWITCH.LABEL',
			description: 	'SESSIONS.HOME.SWITCH.DESCRIPTION',
			icon: 			'select',
			handler: 		() => this.switch(),
			position: 		() => this.openSessionStoreService.items.length > (this.activeSession ? 1 : 0)
							?	undefined 	// no preference for position
							:	null		// do not show at all
		}

		const revertSymptomCheckAction: Action = {

			label: 			'SESSIONS.HOME.REVERT_ACTIVE_SYMPTOM_CHECK.LABEL',
			description: 	'SESSIONS.HOME.REVERT_ACTIVE_SYMPTOM_CHECK.DESCRIPTION',
			icon: 			'revert',
			handler: 		() => this.restoreOriginalSymptomCheck(),
			position: 		() => this.getActiveSymptomCheckUpdate() === true
							?	undefined
							:	null

		}

		const shareAction: Action = {
			label: 			'SESSIONS.HOME.SHARE.LABEL',
			description: 	'SESSIONS.HOME.SHARE.DESCRIPTION',
			icon: 			'share',
			handler: 		() => this.share()
		}



		this.sessionActions = [
			{
				itemClass: 	Session,
				role: 		'destructive',
				getAction: 	(session: Session) => ({

								...closeActiveSessionAction,

								position: () => this.activeSession === session
									?	-1
									:	null		// do not show at all
							})
			},

			{
				itemClass:			Session,
				role: 				undefined,
				getAction: 	() => ({
								...switchSessionAction,
							}),
			}
		]

		this.reportExtraActions = [
			{
				itemClass: 			Report,
				role: 				'share',
				getAction: (report: Report) => ({
					label: 			'SESSIONS.HOME.EXPORT_DATA.LABEL',
					description: 	'SESSIONS.HOME.EXPORT_DATA.DESCRIPTION',
					icon: 			'export',
					data:			JSON.stringify({
										report:		report.config,
										questions: 	this.activeSession.questions.map(q => q.config)
									}),
					filename:		`RCC-Report-${report.date.toISOString().split('T')[0]}__ID.json`,
					position: 1
				})
			},
		]

		this.symptomCheckActions = [
			{
				itemClass: 			SymptomCheck,
				role: 				'edit',
				getAction: 			(symptomCheck: SymptomCheck) => ({
					label: 			'SESSIONS.HOME.EDIT_ACTIVE_SYMPTOM_CHECK.LABEL',
					description: 	'SESSIONS.HOME.EDIT_ACTIVE_SYMPTOM_CHECK.DESCRIPTION',
					icon: 			'edit',
					handler: 		() => 	this.editActiveSymptomCheck(),
					position: 		() =>	this.activeSymptomCheck === symptomCheck
											?	undefined 	// no preference for position
											:	null		// do not show at all
				})
			},

			{
				itemClass: 			SymptomCheck,
				role: 				'edit',
				getAction: (symptomCheck: SymptomCheck) => ({
					...revertSymptomCheckAction,
					position: 	() =>	this.activeSymptomCheck === symptomCheck
										&& this.getActiveSymptomCheckUpdate() === true
										?	undefined 	// no preference for position
										:	null			// do not show at all
				})
			},

			{
				itemClass: 			SymptomCheck,
				role: 				'share',
				getAction: (symptomCheck: SymptomCheck) => ({
					...shareAction,
					position: 	() => 	this.activeSymptomCheck === symptomCheck
										?	undefined 	// no preference for position
										:	null			// do not show at all
				})
			}


		]

		this.activeActions = [

			{
				label: 			'SESSIONS.HOME.START_NEW_SESSION.LABEL',
				description: 	'SESSIONS.HOME.START_NEW_SESSION.DESCRIPTION',
				icon: 			'new',
				handler: 		() => this.startNewSession(),
				position: 		4
			},

			{
				...shareAction,
				position: 		() => this.activeSymptomCheck
								?	2
								:	null

			},

			{
				...switchSessionAction,
			},


			{
				...closeActiveSessionAction,
				position:		() => this.activeSession
								?	-1
								:	null
			},
		]

		if (!this.activeSession) await this.handleMissingSession()
	}




	public getActiveSymptomCheckUpdate(): boolean {

		if (!this.activeSymptomCheck) return false
		if (!this.activeSymptomCheckBackup) return false

		const activeConfigJson = JSON.stringify(this.activeSymptomCheck.config)
		const backupConfigJson = JSON.stringify(this.activeSymptomCheckBackup)

		if (activeConfigJson === backupConfigJson) return false

		return true
	}



	public async startNewSession(): Promise<void> {

		return await this.openSessionStoreService.startNewSession()
	}


	public async addNewSymptomCheck(): Promise<void> {

		assert(this.activeSession, 'SessionHomePageComponent.addNewSymptomCheck() no active session.')

		await this.openSessionStoreService.addNewSymptomCheck(this.activeSession)
	}



	public async closeActiveSession(): Promise<void> {

		assert(this.activeSession, 'SessionHomePageComponent.closeActiveSession() no active session.')


		await this.rccAlertController.confirm(
			this.activeReport
			?	'SESSIONS.HOME.CLOSE_SESSION.CONFIRM_WITH_REPORT'
			:	'SESSIONS.HOME.CLOSE_SESSION.CONFIRM'
		)

		this.openSessionStoreService.close(this.activeSession)
	}



	public editActiveSymptomCheck(): void {

		assert(this.activeSession, 'editActiveSymptomCheck.closeActiveSession() no active session.')

		void this.openSessionStoreService.editSessionSymptomCheck(this.activeSession)
	}



	public restoreOriginalSymptomCheck(): void {
		assert(this.activeSymptomCheck, 'editActiveSymptomCheck.closeActiveSession() no active symptom check.')

		this.openSessionStoreService.restoreOriginalSymptomCheck(this.activeSymptomCheck)
	}




	public async handleMissingSession(): Promise<void> {

		try {

			await this.rccAlertController.confirm(
				'SESSIONS.HOME.NO_OPEN_SESSION.CONFIRM_NEW_SESSION.MESSAGE',
				'SESSIONS.HOME.NO_OPEN_SESSION.CONFIRM_NEW_SESSION.CONFIRM_LABEL'
			)

			await this.startNewSession()

		} catch (e) {

			this.exit()
			throw e

		}

	}



	// public async addSymptomCheck() : Promise<void> {
	// 	this.openSessionStoreService.startNewSession()
	// }

	public async share(): Promise<void> {

		assert(
			this.openSessionStoreService.activeSession,
			'SessionHomePageComponent.share() no open session. Please open a session before trying to share.'
		)

		const symptomCheckConfig = this.activeSymptomCheck.config
		const questionIds = this.activeSymptomCheck.questionIds

		const questions = await this.questionnaireService.get(questionIds)
		const questionConfigs = questions.map(question => question.config)

		const data = [symptomCheckConfig, questionConfigs]

		return await this.shareDataService.share(data)
	}

	public async switch(): Promise<void> {

		const result = await this.itemSelectService.select<Session>({
			stores: [this.openSessionStoreService],
			singleSelect: true
		})

		const session = result[0]

		if (session) this.openSessionStoreService.activate(session)
	}

	public exit(): void {

		if (this.openSessionStoreService.items.length === 0) void this.rccToastController.present({ message: 'SESSIONS.HOME.ALL_SESSIONS_CLOSED' })

		void this.router.navigateByUrl('/')
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
