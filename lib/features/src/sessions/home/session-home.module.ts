import	{
			NgModule,
			Provider,
			ModuleWithProviders
		}											from '@angular/core'
import	{
			provideRoutes,
			Routes,
			Router
		}											from '@angular/router'

import	{
			map
		}											from 'rxjs'

import	{
			SharedModule,
			ShareDataModule,
			provideTranslationMap,
			provideHomePageEntry,
			provideMainMenuEntry,
			Factory,
			Action
		}											from '@rcc/common'

import	{
			ReportBasicViewModule
		}											from '../../reports/basic-view'

import	{
			SymptomCheckEditModule
		}											from '../../symptom-checks'

import	{
			OpenSessionModule,
			OpenSessionStoreService
		}											from '../open-sessions'

import	{	BasicSessionRepresentationModule	}	from '../basic-session-representation'

import	{	SessionHomePageComponent			}	from './session-home-page.component'


import	en	from './i18n/en.json'
import	de	from './i18n/de.json'

export interface SessionHomeModuleConfig {
	mainMenuEntry?: 		boolean | number,
	homePageEntry?: 		boolean | number
}

const openSessionStoreServiceHomePath : string = 'sessions/home'

const routes : Routes 	=	[
								{ path: openSessionStoreServiceHomePath,	component: SessionHomePageComponent	},
							]

@NgModule({
	providers:[
		// provideMainMenuEntry(mainMenuEntry),
		provideRoutes(routes) as Provider, // somehow eslint doesn't get this right and marks the whole array as problematic
		provideTranslationMap('SESSIONS.HOME', { en, de }),
		// ...homePageEntries.map(provideHomePageEntry)
	],
	imports: [
		SharedModule,
		ShareDataModule,
		OpenSessionModule,
		SymptomCheckEditModule,
		ReportBasicViewModule,
		BasicSessionRepresentationModule
	],
	declarations :[
		SessionHomePageComponent
	]
})

export class SessionHomeModule{

	public constructor(
		router				: Router,
		openSessionStoreService	: OpenSessionStoreService
	){
		// Whenever another session is opened, go to session home
		openSessionStoreService.activeSession$
		.subscribe( session => session && void router.navigateByUrl(openSessionStoreServiceHomePath) )
	}


	/**
	* This method can add entries to the main menu and/or the home page.
	*
	* Calling it without parameter, will add entries to the main menu and home
	* page automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | config     | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* |.mainMenu   | undefined, false, null | adds no menu entry                                        |
	* |	           | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	* |------------|------------------------|-----------------------------------------------------------|
	* |.homePage   | undefined, false, null | adds no home page entry                                   |
	* |			   | true                   | adds home page entry at a reasonably adequate position    |
	* |            | positive number        | adds home page entry at position counting from the top    |
	* |            | positive number        | adds home page entry at position counting from the bottom |
	* |------------|------------------------|-----------------------------------------------------------|
	*
	*/
	public static provideEntries(config?: SessionHomeModuleConfig): ModuleWithProviders<SessionHomeModule> {

		let mainMenuEntryPosition 	: number 	= 3
		let showMainMenuEntry 		: boolean 	= true

		if (typeof config?.mainMenuEntry === 'number')		mainMenuEntryPosition 	= config.mainMenuEntry
		if (config?.mainMenuEntry === false) 				showMainMenuEntry 		= false
		if (config?.mainMenuEntry === null) 				showMainMenuEntry 		= false
		if (config && config.mainMenuEntry === undefined) 	showMainMenuEntry 		= false

		const mainMenuEntry	: Factory<Action> =	{
			deps:		[OpenSessionStoreService],
			factory:	(openSessionStoreService: OpenSessionStoreService) => ({
				icon: 			'session',
				label: 			'SESSIONS.HOME.MAIN_MENU.LABEL',
				path:			openSessionStoreServiceHomePath,
				position:		mainMenuEntryPosition,
				notification:	openSessionStoreService.activeSession$.pipe(map(session => !!session))
			})
		}

		let homePageEntryPosition		: number	= 0.5
		let showHomePageEntry			: boolean	= true

		if (typeof config?.homePageEntry === 'number') 		homePageEntryPosition	= config.homePageEntry
		if (config?.homePageEntry === false) 				showHomePageEntry		= false
		if (config?.homePageEntry === null) 				showHomePageEntry		= false
		if (config && config.homePageEntry === undefined) 	showHomePageEntry		= false

		const homePageEntryActiveSession : Factory<Action>	= 	{

			deps:		[OpenSessionStoreService],
			factory:	(openSessionStoreService: OpenSessionStoreService) => ({
								position: 		() =>  openSessionStoreService.activeSession ? homePageEntryPosition : null,
								icon:			'session',
								label:			'SESSIONS.HOME.WITH_ACTIVE_SESSION.LABEL',
								description:	'SESSIONS.HOME.WITH_ACTIVE_SESSION.DESCRIPTION',
								role:			'details',
								path:			openSessionStoreServiceHomePath,
								notification:	openSessionStoreService.activeSession$.pipe(map(session => !!session))
						}),
		}

		const homePageEntryNoActiveSession : Factory<Action>	=	{

			deps:		[OpenSessionStoreService],
			factory:	(openSessionStoreService : OpenSessionStoreService ) => ({
								position: 		() =>  openSessionStoreService.activeSession ? null : homePageEntryPosition,
								icon:			'session',
								label:			'SESSIONS.OPEN_SESSIONS.ACTIONS.NEW_SESSION.LABEL',
								description:	'SESSIONS.OPEN_SESSIONS.ACTIONS.NEW_SESSION.DESCRIPTION',
								role:			'productive',
								handler:		() => openSessionStoreService.startNewSession(),
								notification:	openSessionStoreService.activeSession$.pipe(map(session => !!session))
					}	)
		}

		const providers : Provider[] = []

		if (showMainMenuEntry) providers.push(provideMainMenuEntry(mainMenuEntry))
		if (showHomePageEntry) providers.push(provideHomePageEntry(homePageEntryActiveSession))
		if (showHomePageEntry) providers.push(provideHomePageEntry(homePageEntryNoActiveSession))

		return {
			ngModule:	SessionHomeModule,
			providers
		}
	}

}
