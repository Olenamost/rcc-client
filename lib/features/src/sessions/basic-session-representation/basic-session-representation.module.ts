import 	{
			NgModule,
		}									from '@angular/core'

import	{
			CommonModule
		}									from '@angular/common'

import	{
			provideItemRepresentation,
			provideTranslationMap,
			TranslationsModule,
			RccToIDPipeModule
		}									from '@rcc/common'
import	{	Session						}	from '@rcc/core'
import	{	BasicSessionLabelComponent	}	from './basic-session-label/basic-session-label.component'


import en from './i18n/en.json'
import de from './i18n/de.json'

const basicSessionRepresentation = {
	itemClass:		Session,
	name:			'SESSION',
	icon:			'session',
	labelComponent:	BasicSessionLabelComponent,
	cssClass:		'session-label'
}

@NgModule({
	imports:[
		CommonModule,
		TranslationsModule,
		RccToIDPipeModule
	],
	providers:[
		provideTranslationMap('SESSIONS.BASIC_LABEL', { en,de }),
		provideItemRepresentation(basicSessionRepresentation)
	],
	declarations:[
		BasicSessionLabelComponent
	]
})
export class BasicSessionRepresentationModule {}
