import 	{
			Component,
			ViewEncapsulation
		}								from '@angular/core'

import	{	ExposeTemplateComponent		}	from '@rcc/common'

import	{	Session						}	from '@rcc/core'


@Component({
	selector: 		'rcc-session-label',
	templateUrl : 	'./basic-session-label.component.html',
	styleUrls:		['./basic-session-label.component.css'],
	encapsulation:	ViewEncapsulation.None
})
export class BasicSessionLabelComponent extends ExposeTemplateComponent {

	public constructor( public session : Session ){ super() }

}
