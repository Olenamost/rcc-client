import	{
			NgModule,
		} 										from '@angular/core'

import	{
			IncomingDataServiceModule,
			provideTranslationMap,

		}										from '@rcc/common'

import	{	OpenSessionStoreService			}	from './open-session-store.service'
import	{	OpenSessionQuestionStoreService	}	from './open-session-question-store.service'
import	{	OpenSessionReportStoreService			}	from './open-session-report-store.service'


import	{	ReportMetaStoreServiceModule	}	from '../../reports'
import	{	QuestionnaireServiceModule		}	from '../../questions'

import en from './i18n/en.json'
import de from './i18n/de.json'



@NgModule({
	imports:[
		IncomingDataServiceModule,
		ReportMetaStoreServiceModule.forChild([OpenSessionReportStoreService]),
		QuestionnaireServiceModule.forChild([OpenSessionQuestionStoreService])
	],
	providers:[
		provideTranslationMap('SESSIONS.OPEN_SESSIONS', { en, de } ),
		OpenSessionStoreService,
		OpenSessionReportStoreService,
		OpenSessionQuestionStoreService,
	]
})
export class OpenSessionModule {}
