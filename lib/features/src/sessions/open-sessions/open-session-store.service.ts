import	{
			Injectable,
		}									from '@angular/core'
import	{	Router					}		from '@angular/router'
import	{

			BehaviorSubject,
			map,
			filter,
		}									from 'rxjs'
import	{
			IncomingDataService,
			RccAlertController,
			RccTranslationService,
			ItemSelectService
		}									from '@rcc/common'
import 	{
			Session,
			SessionConfig,
			SessionStore,
			SymptomCheck,
			SymptomCheckConfig,
			assert,
			UserCanceledError
		}									from '@rcc/core'
import	{
			SymptomCheckEditService,
			SymptomCheckMetaStoreService
		}									from '../../symptom-checks'

import	{
			QuestionnaireService,
		}									from '../../questions'
import	{	CuratedDefaultQuestionStoreService	}	from '../../curated/curated-questions/curated-default-questions'

import	{	CuratedSymptomCheckStoreService	}	from '@rcc/features'



@Injectable()
export class OpenSessionStoreService extends SessionStore {


	public name = 'SESSIONS.OPEN_SESSIONS.STORE.NAME'


	protected 	activeSessions		= new BehaviorSubject<Session|null>(null)
	public 		activeSession$		= this.activeSessions.asObservable()

	public		symptomCheckBackups = new Map<SymptomCheck, SymptomCheckConfig>()


	public get activeSession() : Session|null { return this.activeSessions.value }

	public constructor(
		private incomingDataService					: IncomingDataService,
		private rccAlertController					: RccAlertController,
		private router								: Router,
		private symptomCheckEditService				: SymptomCheckEditService,
		private questionnaireService				: QuestionnaireService,
		private rccTranslationService				: RccTranslationService,
		private curatedDefaultQuestionStoreService 	: CuratedDefaultQuestionStoreService,
		private symptomCheckMetaStoreService		: SymptomCheckMetaStoreService,
		private itemSelectService					: ItemSelectService,
		private curatedSymptomCheckStoreService		: CuratedSymptomCheckStoreService
	){

		super()

		this.listenToIncomingDataService()

		this.removal$.subscribe(	session => this.cleanUp(session) )
		this.change$.subscribe(		()		=> this.keepQuestions() )


	}


	protected listenToIncomingDataService(): void {

		this.incomingDataService
		.pipe(
			map(	(data:unknown)						=> Session.findConfigs(data) ),
			filter(	(sessionConfigs: SessionConfig[])	=> sessionConfigs.length > 0 )
		)
		.subscribe(	(sessionConfigs: SessionConfig[]) 	=> void this.importSessionConfigs(sessionConfigs) )

	}

	public async importSessionConfigs(configs: SessionConfig[]): Promise<void>{

		assert(configs.length <= 1, 'activeSessionStore.importSessionConfigs() received multiple session. This should never happen; only one session at a time.', configs)
		assert(configs.length === 1, 'activeSessionStore.importSessionConfigs() missing session config.', configs)

		const session		=	new Session(configs[0])
		const header		=	'SESSIONS.OPEN_SESSIONS.IMPORT.HEADER'

		const sc_message	=	session.symptomCheck
								?	session.symptomCheck.meta.label
								:	''

		const rp_message	=	session.report
								?	`{{SESSIONS.OPEN_SESSIONS.IMPORT.ENTRIES}}: ${session.report.entries.length}`
								:	''

		const qs_message	=	session.questions.length > 0
								?	`{{SESSIONS.OPEN_SESSIONS.IMPORT.QUESTIONS}}: ${session.questions.length}`
								:	''

		const sc_rp_qa		=	[sc_message, rp_message, qs_message]
								.filter( x => !!x)
								.join(', ')

		const extra_message	=	sc_rp_qa
								?	`(${sc_rp_qa})`
								:	'({{SESSIONS.OPEN_SESSIONS.IMPORT.NO_DATA}})'

		const message		=	`{{SESSIONS.OPEN_SESSIONS.IMPORT.MESSAGE}} ${extra_message}`

		const result 		= 	await this.rccAlertController.present({
									header,
									message,
									buttons:	[
													{
														label:		'SESSIONS.OPEN_SESSIONS.IMPORT.IGNORE',
														rejectAs: 	'ignore'
													},
													{
														label:		'SESSIONS.OPEN_SESSIONS.IMPORT.ACCEPT_START',
														resolveAs: 	'accept_start'
													}
												]
								})

		this.addItem(session)

		if(session.symptomCheck) this.backupSymptomCheck(session.symptomCheck)

		if(result === 'accept_start') this.activate(session)

	}


	public backupSymptomCheck(symptomCheck: SymptomCheck) : void {
		this.symptomCheckBackups.set(symptomCheck, symptomCheck.config)
	}

	public restoreOriginalSymptomCheck(symptomCheck: SymptomCheck): void {

		const originalConfig	= this.symptomCheckBackups.get(symptomCheck)

		assert(originalConfig,	'OpenSessionStoreService.restoreOriginalSymptomCheck() unable to find symptom check backup for session.', symptomCheck)

		symptomCheck.config		= originalConfig
	}


	public getNewSymptomCheckDefaultLabel() : string {
		const date		= 	this.rccTranslationService.translate(
								new Date(),
								{
									year:		'numeric',
									month:		'2-digit',
									weekday:	'short',
									day: 		'numeric',
									hour:		'2-digit',
									minute:		'2-digit'
								}
							)

		const label		=	this.rccTranslationService.translate('SESSIONS.OPEN_SESSIONS.NEW_SYMPTOM_CHECK_LABEL', { date })

		return label

	}

	/**
	 * Adds a {@link SymptomCheck} to a {@link Session}, if it does not already
	 * have one. If it already has one, throws an Error.
	 */
	public async addNewSymptomCheck(session: Session) : Promise<void> {

		assert(!session.symptomCheck, 'openSessionStoreService.addNewSymptomCheck() session already has a symptom check.')

		session.symptomCheck 	= new SymptomCheck()

		const meta				= session.symptomCheck.meta

		meta.label				= this.getNewSymptomCheckDefaultLabel()

		try {
			await this.editSessionSymptomCheck(session)
		} catch(e){
			session.symptomCheck = null
			throw e
		}
	}

	/**
	 * Opens the symptom check editor and stores questions that may
	 * have been created on the fly with the session.
	 */
	public async editSessionSymptomCheck(session: Session) : Promise<void> {

		const result 		= 	await this.symptomCheckEditService.edit(session.symptomCheck)

		const questions		= 	result.artifacts

		questions.forEach( question => session.questions.includes(question) ||  session.questions.push(question) )

	}


	/**
	 * Creates a new session and opens symptom check editor. If editing
	 * the symptom check successfully ends, adds the session to the list and
	 * activates it.
	 */
	public async startNewSession() : Promise<void> {
		// const buttons: ButtonConfig[]	= [
		// 	{label: "SESSIONS.OPEN_SESSIONS.EDIT_WITH_TEMPLATE.NO", rejectAs: "empty"},
		// 	{label: "SESSIONS.OPEN_SESSIONS.EDIT_WITH_TEMPLATE.YES", 	resolveAs: "template"}

		// ]

		const session		= new Session()
		const symptomCheck	= session.symptomCheck
		const message		= 'SESSIONS.OPEN_SESSIONS.EDIT_WITH_TEMPLATE.MESSAGE'
		const heading		= 'SESSIONS.OPEN_SESSIONS.EDIT_WITH_TEMPLATE.HEADER'

		try {
				await this.symptomCheckMetaStoreService.ready

				const stores				= 	this.symptomCheckMetaStoreService.stores
				const singleSelect			=	true
				// console.log(stores[1])
				const result 				= 	await this.itemSelectService.select({ stores, singleSelect, heading, message })

				const selectedSymptomCheck 	= 	result[0]
				// console.log(result[0])

				const selectedConfig	=	selectedSymptomCheck
												?	selectedSymptomCheck.config
												:	symptomCheck.config			// This case should never happen (because of line 238 await)

				selectedConfig.meta.creationDate = 	new Date().toISOString()

				symptomCheck.config			= selectedConfig

			} catch(e: unknown) {
				const userCanceled = e instanceof UserCanceledError
				if(!userCanceled) throw e

		}

		symptomCheck.meta.label			= this.getNewSymptomCheckDefaultLabel()

		await this.editSessionSymptomCheck(session)

		this.addItem(session)

		this.activate(session)


	}



	public activate(session: Session) : void {

		assert(this.items.includes(session), 'openSessionStoreService.activate() session not yet present. Import sessions or start a a new one before activating them.', session)

		if(session === this.activeSession) return

		this.activeSessions.next(session)

	}


	public cleanUp(session: Session) : void  {

		if(this.activeSession === session) this.activeSessions.next(null)
	}


	public close(session : Session) : void {

		this.removeItem(session)
	}

	public closeActiveSession() : void {

		this.close(this.activeSession)

	}

	public keepQuestions() : void {
		console.log('KEEP QUESTIONS', this.items)
	}
}
