import { NgModule } from '@angular/core'
import { Action, Factory, ItemAction, provideItemAction, provideTranslationMap, RccTranslationService } from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'
import { DatasetsService } from '../data-visualization'
import { Report } from '@rcc/core'
import { RccPDFService } from './pdf.service'

const exportReportActionFactory : Factory<ItemAction<Report>> = {
	deps: [DatasetsService, RccTranslationService],

	factory: (
		datasetsService: DatasetsService,
		rccTranslationService: RccTranslationService
	) => {
		const rccPDFService = new RccPDFService(datasetsService, rccTranslationService)
		return {
			itemClass: 	Report,
			role: 		'share',
			getAction(report: Report) : Action {
				return {
					label:			'SESSIONS.HOME.EXPORT_PDF.LABEL',
					description:	'SESSIONS.HOME.EXPORT_DATA.DESCRIPTION',
					icon:			'export',
					filename:		`RCC-Report-${report.date.toISOString().split('T')[0]}.pdf`,
					data:			rccPDFService.createPDFOfReport(report),
					mimeType:		'application/pdf',
					position: 		2,
				}
			},
		}
	}
}

@NgModule({
	providers: [
		provideTranslationMap('PDF_EXPORT', { en,de }),
		provideItemAction(exportReportActionFactory)
	]
})
export class PDFModule {}
