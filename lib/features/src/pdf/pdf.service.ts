import {
	RccTranslationService
}				from '@rcc/common'
import {
	Question,
	Report,
}				from '@rcc/core'
import {
	DatasetsService
}				from '@rcc/features'
import {
	PDFDocument,
	PDFFont,
	StandardFonts,
}				from 'pdf-lib'

export type PDFData = {	questions: Question[]	}

type PageOptions = {
	pageMargin: number
}

type LineOptions = {
	lineMargin?: number
	fontSize: number
}

type LineWriter = {
	writeLine: (text: string, options: LineOptions) => void,
	newLine: (newLineHeight: number) => void,
}

export class RccPDFService {

	public constructor(
		private readonly datasetsService: DatasetsService,
		private readonly translationService: RccTranslationService
	) {}


	// TODO (#146): Refactor this function for clearer usage of creating a pdf
	// * account for line wrapping
	// * account for answer types that aren't simple multiple choice
	// * account for page wrapping
	public async createPDFOfReport(report: Report): Promise<string> {

		const pdf					= await PDFDocument.create()
		const margin				= 20
		const fontSizeH1			= 28
		const headingMargin			= 20
		const fontSize1				= 16
		const fontSize2				= 14
		const questionMargin		= 16
		const answerMargin			= 8
		const questionSectionMargin = 20

		const writer = this.createWriter(pdf, { pageMargin: margin })

		writer.writeLine(
			this.translationService.translate('PDF_EXPORT.REPORT_TITLE'),
			{ lineMargin: headingMargin, fontSize: fontSizeH1 }
		)

		const datasets = await this.datasetsService.getDatasets(report)

		datasets.forEach((dataset) => {
			const question		= dataset.question
			const questionText	= this.translationService.translate(question)

			writer.writeLine(questionText, { lineMargin: questionMargin, fontSize: fontSize1 })

			dataset.datapoints.forEach(datapoint => {
				const answerOption = question.options?.find(
					option => option.value === datapoint.value
				)

				const translated = this.translationService.translate(answerOption)
				const answerText = translated === '[undefined]' ? '' : `${translated} `

				const answerLineText = `${datapoint.value as string} - ${answerText}(${datapoint.date})`
				writer.writeLine(answerLineText, { lineMargin: answerMargin, fontSize: fontSize2 })
			})

			writer.newLine(questionSectionMargin)
		})

		return await pdf.save() as unknown as string
	}

	/**
	 * @returns A function to write a new line to the PDF, this handles the tracking
	 * of the current line position
	 */
	private createWriter(pdf: PDFDocument, options: PageOptions): LineWriter {
		let currentPage			= pdf.addPage()
		const { width, height }	= currentPage.getSize()
		const allowedWidth		= width - options.pageMargin * 2
		const font				= pdf.embedStandardFont(StandardFonts.Helvetica)

		// The y coordinates of the page go from 0 (bottom of the page), to [height]
		// (top of the page) - so to write top down, we set the y coordinates
		// to the height of the page, - minus the margin
		const initialYPosition 	= (): number => height - options.pageMargin
		let currentYPosition	= initialYPosition()

		const writeLine = (lineText: string, fontSize: number): void => {
			// To determine the y position that we actually write at, we
			// subtract the font size of the text we are writing divided
			// by 2, as the y position refers to the vertical center of
			// the text
			const y = (): number => currentYPosition - fontSize / 2

			const needToAddNewPage = currentYPosition - fontSize < options.pageMargin
			if (needToAddNewPage) {
				currentPage = pdf.addPage()
				currentYPosition = initialYPosition()
			}
			currentPage.drawText(lineText, { x: options.pageMargin, y: y(), size: fontSize, font })
			currentYPosition -= fontSize
		}

		return {
			writeLine: (text, lineOptions) => {
				const lineWidth				= font.widthOfTextAtSize(text, lineOptions.fontSize)
				const textTooWideForPage	= lineWidth > allowedWidth

				if (textTooWideForPage) {
					const lines = this.calculateLineBreaks(text, font, lineOptions.fontSize, allowedWidth)
					lines.forEach((line) => writeLine(line, lineOptions.fontSize))
				} else
					writeLine(text, lineOptions.fontSize)

				currentYPosition -= (lineOptions.lineMargin || 0)
			},
			newLine: (newLineHeight) => {
				currentYPosition -= newLineHeight
			}
		}
	}

	/**
	 * Uses bisection search to determine the maximum length of words that can fit on a line
	 *
	 * @returns a list of lines, each of which is short enough to fit on the allowed width of the page
	 */
	private calculateLineBreaks(text: string, font: PDFFont, fontSize: number, allowedWidth: number): string[] {
		const words = text.split(' ')
		const wordLength = words.length

		/** Used to track the start of the current line - starting with the first word */
		let start = 0
		/**
		 * Used to track the current lowest current guess of the bisection search, will
		 * be updated when the current guess is too short
		 */
		let lowest = start
		/**
		 * Used to track the current highest guess of the bisection search, will be updated
		 * when the current guess is too long
		 */
		let end = wordLength

		/** Used to track the middle point between the lower and upper markers of the bisection search */
		let middle = 0
		const determineMiddle = (): number => Math.floor((end - lowest) / 2) + lowest
		middle = determineMiddle()

		let finished = false
		const lines = []

		while (!finished) {
			const line = words.slice(start, middle).join(' ')
			const width = font.widthOfTextAtSize(line, fontSize)
			if (width > allowedWidth) {
				end = middle
				middle = determineMiddle()
			}
			if (width <= allowedWidth) {
				lowest = middle
				middle = determineMiddle()
			}
			if (middle === lowest) {
				lines.push(words.slice(start, lowest).join(' '))

				start = lowest
				lowest = start
				end = wordLength
				middle = determineMiddle()
				const remainingLine = words.slice(start, end).join(' ')

				if (font.widthOfTextAtSize(words.slice(start, end).join(' '), fontSize) < allowedWidth) {
					lines.push(remainingLine)
					finished = true
				}
			}

		}

		return lines
	}
}
