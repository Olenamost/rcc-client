import	{	NgModule				}	from '@angular/core'
import	{
			ExportModule,
			provideTranslationMap,
		}								from '@rcc/common'
import	{	FhirReportExporter		}	from './fhir-report-exporter.class'

import en from './i18n/en.json'
import de from './i18n/de.json'


const exporters = 	[
						FhirReportExporter
					]

@NgModule({

	imports: [
		ExportModule.forChild(exporters)
	],
	providers: [
		provideTranslationMap('FHIR_EXPORT', { en, de })
	]

})
export class FhirExportModule{}
