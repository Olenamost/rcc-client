import	{
			Exporter,
			ExportResult
		}						from '@rcc/common'

import	{
			Report,
			Question,
			report2fhir,
			R4
		}						from '@rcc/core'


export class FhirReportExporter extends Exporter<Report, R4.IQuestionnaireResponse> {

	public label			=	'FHIR_EXPORT.EXPORTERS.REPORT.LABEL'
	public description		=	'FHIR_EXPORT.EXPORTERS.REPORT.DESCRIPTION'



	public findQuestions(context: unknown[]): Question[] {
		return context.find( x => Array.isArray(x) && x.every( y => y instanceof Question )) as Question[]
	}

	public handles(data:unknown, ...context:unknown[]): data is Report {

		if(!data) 							return false
		if(!(data instanceof Report)) 		return false
		if(!this.findQuestions(context))	return false

		return true

	}


	public async export(report: Report, ...context: unknown[]): Promise<ExportResult<R4.IQuestionnaireResponse>> {

		const questions = this.findQuestions(context)

		const data 		= await Promise.resolve(report2fhir(report, questions, 'DE', 'abc')) // TODO language, id!!
		const data_str 	= JSON.stringify(data)

		// TODO doesn't work??
		// bei fhir.toString() kommt [object Promise], property raw does not exist
		return 	{
					toString: 	() => data_str,
					raw:		() => data
				}
	}

}
