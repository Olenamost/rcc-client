import	{	Injectable					} from '@angular/core'
import	{	RccModalController			} from '@rcc/common'
import	{	QrCodeScanModalComponent	} from './scan-modal/scan-modal.component'

@Injectable()
export class QrCodeScannerService {
	public constructor(
		public rccModalController: RccModalController
	) {}

	public async scan() : Promise<unknown> {
		return await this.rccModalController.present(QrCodeScanModalComponent, {})
	}
}
