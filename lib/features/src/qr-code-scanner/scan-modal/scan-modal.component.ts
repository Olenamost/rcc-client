import	{
			Component,
			AfterViewInit,
			OnDestroy,
			ViewChild,
			ElementRef,
		} 								from '@angular/core'
import	{	FormControl				}	from '@angular/forms'
import	{	SubscriptionLike		}	from 'rxjs'
import	{
			RccModalController,
			RccToastController
		}								from '@rcc/common'
import		QrScanner					from 'qr-scanner'

/**
 * This component opens a modal showing the video stream on the user's device
 * and scans a QR code
 */
@Component({
	selector: 		'rcc-qr-code-scanner',
	templateUrl: 	'./scan-modal.component.html',
	styleUrls: 		['./scan-modal.component.scss'],
})
export class QrCodeScanModalComponent implements AfterViewInit, OnDestroy {
	@ViewChild('video')
	public video			: ElementRef<HTMLVideoElement>
	public hasPermission	: boolean
	public availableDevices	: MediaDeviceInfo[]		= []
	public currentDevice	: FormControl<{ deviceId: string }>	= new FormControl<MediaDeviceInfo>(null)
	public codeReader		: QrScanner				= undefined
	public initializing		: boolean				= true
	public cameraNotFound	: boolean				= false
	public subscriptions	: SubscriptionLike[] 	= []
	private stream			: MediaStream

	public constructor(
		private rccModalController 	: RccModalController,
		private rccToastController 	: RccToastController,
	) {}

	private async setupStreamsAndDevices(): Promise<void> {
		try {
			this.stream	=	await navigator.mediaDevices.getUserMedia({ video: { facingMode: 'environment' } })
		} catch(e) {
			this.stream	= 	await navigator.mediaDevices.getUserMedia({ video: true })
		}

		// Get devices: Labels will only be present until tracks are closed -.-
		const devices : MediaDeviceInfo[]	= 	await navigator.mediaDevices.enumerateDevices()

		this.availableDevices 	= 	devices.filter(device => device.kind === 'videoinput')

		const defaultCameraId: string	= 	this.stream
											.getVideoTracks()[0]
											.getSettings().deviceId

		if(!defaultCameraId) throw new Error('Camera not found')

		const default_device : MediaDeviceInfo	=	this.availableDevices
													.find(device => device.deviceId === defaultCameraId)
		this.currentDevice.setValue(default_device)

		await this.scan()
	}

	private scanCallback(result: string, error?: Error): Promise<void> {
		if (error) throw error
		if (result) {
			this.codeReader.stop()
			return this.success(result)
		}
	}

	public async scan(): Promise<void> {
		this.codeReader	= new QrScanner(
			this.video.nativeElement,
			result => { void this.scanCallback(result.data) },
			{ highlightScanRegion: true }
		)

		await this.codeReader.start()
	}

	public async success(result: string): Promise<void> {
		let content: string | object = undefined

		try {
			content = JSON.parse(result) as object
		} catch(e) {
			content = result
		}

		await this.rccToastController.success('QRCODE.SCAN_SUCCESS')
		this.rccModalController.dismiss(content)
	}

	public cancel(): void {
		this.closeVideoTracks()
		void this.rccToastController.info('QRCODE.SCAN_CANCELLED')
		this.rccModalController.dismiss(null)
	}

	public async ngAfterViewInit(): Promise<void> {
		this.initializing = true

		await 	this.setupStreamsAndDevices()
				.catch((e) => {
					console.warn(e)
					this.cameraNotFound = true
				})
				.finally(
					()	=> this.initializing = false
				)

		this.subscriptions.push(
			this.currentDevice.valueChanges
				.subscribe((device) => {
					this.closeVideoTracks()
					void this.codeReader.setCamera(device.deviceId)
				})
		)

		this.initializing = false
	}

	private closeVideoTracks(): void {
		this.stream.getVideoTracks()
		.forEach((track) => track.stop())
	}

	public ngOnDestroy(): void {
		this.subscriptions.forEach( subscription => subscription.unsubscribe() )
		this.codeReader.destroy()
	}
}
