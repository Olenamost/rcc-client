import	{	NgModule					}	from '@angular/core'
import	{
			SharedModule,
			QrCodeModule,
            provideTranslationMap,
			provideScanService
		}									from '@rcc/common'
import	{	QrCodeScannerService		}	from './qr-code-scanner.service'
import	{	QrCodeScanModalComponent	}	from './scan-modal/scan-modal.component'

import	en	from './i18n/en.json'
import	de	from './i18n/de.json'

@NgModule({
	declarations: [
		QrCodeScanModalComponent
	],
	imports: [
		SharedModule,
		QrCodeModule
	],
	providers: [
		QrCodeScannerService,
		provideTranslationMap('QR_CODE_SCANNER', { en, de }),
		provideScanService(QrCodeScannerService)
	],
})
export class QrCodeScannerServiceModule { }
