import	{	NgModule 					}	from '@angular/core'
import	{
			SharedModule,
			WidgetsModule,
			provideWidget
		}									from '@rcc/common'
import	{	QuestionnaireServiceModule	}	from '../questions'
import	{	ScaleQueryWidgetComponent	}	from './scale/scale.component'
import	{	SelectQueryWidgetComponent	}	from './select/select.component'
import	{	BooleanQueryWidgetComponent	}	from './boolean/boolean.component'

const widgets 	=	[
						ScaleQueryWidgetComponent,
						SelectQueryWidgetComponent,
						BooleanQueryWidgetComponent
					]

@NgModule({
	imports: [
		SharedModule,
		QuestionnaireServiceModule,
		WidgetsModule,
	],
	providers: [
		...widgets.map(provideWidget)
	],
	declarations: [
		...widgets
	],
	exports: [
		...widgets
	]
})
export class BasicQueryWidgetsModule { }
