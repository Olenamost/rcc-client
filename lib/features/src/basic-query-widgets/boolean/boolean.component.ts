import	{
			Component
		}							from '@angular/core'

import	{
			QueryControl,
			QueryWidgetComponent
		}							from '../../queries'


@Component({
	templateUrl: 	'./boolean.component.html',
	styleUrls: 		['./boolean.component.scss'],
})
export class BooleanQueryWidgetComponent extends QueryWidgetComponent {

	public static widgetMatch(queryControl: QueryControl): number{

		return 	queryControl.question.type === 'boolean'
				?	 1
				:	-1
	}

}
