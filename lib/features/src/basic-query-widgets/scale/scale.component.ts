import	{
			Component,
			HostListener,
		}									from '@angular/core'

import	{
			QuestionOptionConfig,
			round,
			getDecimalPlaces
		}									from '@rcc/core'

import	{
			QueryControl,
			QueryWidgetComponent
		}									from '../../queries'


function getSteps( a : number[]): number[]{

	const decimalPlaces		=	getDecimalPlaces(a)
	const sortedArray 		= 	a.sort( (a,b) => Math.sign(b-a))
	const setOfSteps		= 	sortedArray
								.reduce(
									(acc, value, index, arr ) => {

										if(index === 0) return acc

										const diff = arr[index-1]-value

										return acc.add( round(diff, decimalPlaces) )
									},
									new Set<number>()
								)
	return Array.from(setOfSteps)

}


@Component({
	templateUrl: 	'./scale.component.html',
	styleUrls: 		['./scale.component.css'],
})
export class ScaleQueryWidgetComponent extends QueryWidgetComponent {


	public static widgetMatch(queryControl: QueryControl): number{

		const tags			= 	queryControl.question.tags
		const no_scale_tag	=	!tags?.includes('rcc-scale')
								&&
								!tags?.includes('scale')// legacy can be removed

		// Cannot handle questions like this:
		if(no_scale_tag )						return -1

		const type			= 	queryControl.question.type
		const isInteger		=	type === 'integer'
		const isDecimal		=	type === 'decimal'

		// Cannot handle questions like this:
		if(!isInteger && !isDecimal)			return -1

		const noOptions		= 	!queryControl.question.options?.length
		const minMax		=	Number.isFinite(queryControl.question.min) && Number.isFinite(queryControl.question.max)

		// Made to handle questions like this (min/max based):
		if(noOptions && minMax && isInteger)	return 2

		// Cannot handle questions like this, no options no min/max (decimal entails unknown step size):
		if(noOptions)							return -1


		const options		= 	queryControl.question.options
		const values 		= 	options.map(option => option.value)
		const non_numbers	=	values.some( value => typeof value !== 'number')

		// Cannot handle questions like this; this should never happen since the question config is invalid at this point:
		if(non_numbers) return -1

		const steps			=	getSteps(values as number[])
		const uniform		= 	steps.length === 1

		// Made to handle questions like this (option based):
		if(uniform)		return 2

		return -1
	}



	// prevent other slide effects when handling the scale
	@HostListener('mousedown', ['$event'])
	@HostListener('touchstart', ['$event'])
	@HostListener('pointerdown', ['$event'])
	public onTouch(event: Event): void {
		event.stopPropagation()
	}



	public min: 		number
	public max: 		number
	public step:		number
	public length:		number
	public label_count:	number

	public pinFormatter: (value:number) => string | number

	public options: QuestionOptionConfig[]

	public constructor(
		public queryControl	: QueryControl
	){
		super(queryControl)

		// assuming all option have number value, since widgetMatch requires it.

		const originalOptions = this.queryControl.question.options || []


		this.min 			= 	typeof this.queryControl.question.min === 'number'
								?	this.queryControl.question.min
								:	Math.min(...originalOptions.map( option => option.value as number ) )

		this.max 			= 	typeof this.queryControl.question.max === 'number'
								?	this.queryControl.question.max
								:	Math.max(...originalOptions.map( option => option.value as number ) )


		const values		=	originalOptions.map( option => option.value as number)

		this.step			=	getSteps(values)[0] || 1 // assuming there is only one stepsize, see widgetMatch



		const decimalPlaces	=	getDecimalPlaces(values)

		const size			=	1 + round( this.max - this.min, decimalPlaces ) / this.step

		this.options 		= 	[...Array(size).keys()]
								.map( (index:number) => {

									const value = round(this.min + index*this.step, decimalPlaces)

									return 	originalOptions.find( option => option.value === value )
											||
											{ value , meaning: '' }
								})

		this.pinFormatter	=	(value:number) => round(value, decimalPlaces)

		this.length 		= 	size
		this.label_count 	=	this.options.filter( option => this.hasLabel(option) ).length


	}

	public hasLabel(option: QuestionOptionConfig) : boolean {
		if(!option) 	return false

		const hasMeaning		= 	!!option.meaning
		const hasTranslations	= 	Object.values(option.translations || {})
									.filter( x => !!x )
									.length > 0

		return hasMeaning || hasTranslations

	}

	public getOptionStyle(index:number, option?:QuestionOptionConfig): unknown {

		const width				= 	1/this.label_count

		if(!this.hasLabel(option)) return { display: 'none' }

		let left 	= index/(this.length-1) - width/2
		let right 	= index/(this.length-1) + width/2

		if(index === 0){
			left 	= 0
			right 	= width
		}

		if(index === this.length-1){
			left	= 1-width
			right	= 0
		}

		return 	{
					left:			`${left*100}%`,
					right:			`${right*100}%`,
					width:			`${width*100}%`,
				}
	}


}
