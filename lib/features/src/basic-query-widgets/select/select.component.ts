import	{
			Component,
		}									from '@angular/core'

import	{
			QueryControl,
			QueryWidgetComponent
		}									from '../../queries'


@Component({
	templateUrl: 	'./select.component.html',
	styleUrls: 		['./select.component.scss'],
})
export class SelectQueryWidgetComponent extends QueryWidgetComponent {

	public static widgetMatch(queryControl: QueryControl): number {

		return 	queryControl.question.options?.length > 1
				?	1
				:	-1
	}

	public constructor(
		public queryControl: QueryControl
	){
		super(queryControl)
	}

}
