
import 	{ 	Component				}		from '@angular/core'

import	{	WidgetComponent			}		from '@rcc/common'

import	{
			CalendarDateString,
			isSameDay
		} 									from '@rcc/core'

import	{
			DataViewControl,
			Dataset,
		}									from '../data-visualization'


@Component({
	templateUrl:   	'./fallback-data-view-widget.component.html',
	styleUrls: 		['./fallback-data-view-widget.component.css']
})
export class FallbackDataViewWidgetComponent extends WidgetComponent<DataViewControl> {

	public static controlType = DataViewControl

	public static override widgetMatch(): number { return 0 }

	public combinedData 			: Record<string, Dataset[]>
	public allDateStrings			: string[]

	public additionalDataIgnored	: boolean


	public constructor(
		public 	dataViewControl	: DataViewControl,
	){

		super(dataViewControl)

		const combinedDataPoints 	=	dataViewControl.datasets
										.map( dataSet => dataSet.datapoints)
										.flat()

		this.allDateStrings			= 	combinedDataPoints
										.map( datapoint => datapoint.date)

		const startDate				=	this.allDateStrings.sort()[0]
		const endDate				=	this.allDateStrings.reverse()[0]

		this.combinedData 			=	Object.fromEntries(
											CalendarDateString.range(startDate, endDate)
											.map( dateStr => [
												dateStr,
												dataViewControl.datasets.map( dataset => ({
													question: 	dataset.question,
													datapoints:	dataset.datapoints.filter( datapoint => isSameDay(datapoint.date, dateStr))
												}))

											])
										)

	}





}
