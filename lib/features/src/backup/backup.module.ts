import 	{
			NgModule,
		}								from '@angular/core'

import	{
			RouterModule
		}								from '@angular/router'

import	{
			SharedModule,
			TranslationsModule,
			provideMainMenuEntry,
			provideTranslationMap
		}								from '@rcc/common'

import	{	BackupService			}	from './backup.service'
import	{	BackupPageComponent		}	from './backup-page/backup-page.component'


import de from './i18n/de.json'
import en from './i18n/en.json'

// settings for main menu entry, see provideMainMenuEntry()
const BackupMenuEntry	=	{
		position: -1,
		label: 'BACKUP.MENU_ENTRY',
		icon: 'backup',
		path: 'backup'	// ~ routerLink
}

const routes		=	[
							{
								path: 		'backup',
								component: 	BackupPageComponent
							}
						]

@NgModule({
	imports:[
		SharedModule,
		RouterModule.forChild(routes),
		TranslationsModule
	],
	providers:[
		BackupService,
		provideMainMenuEntry(BackupMenuEntry),
		provideTranslationMap('BACKUP', { en, de })
	],
	declarations:[
		BackupPageComponent
	]

})
export class BackupModule {

}
