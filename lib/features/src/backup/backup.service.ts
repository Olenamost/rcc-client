import	{	Injectable				} 	from '@angular/core'
import	{	RccStorage				}	from '@rcc/common'


@Injectable()
export class BackupService {

	public constructor(
		protected rccStorage: RccStorage
	){

	}

	// async backup(): Promise<void> {
	// 	await this.exportData()
	// }

	public async exportData(): Promise<Record<string, unknown[]>> {

		const storageNames 	=	await this.rccStorage.getStorageNames()

		const entries		=	await Promise.all(storageNames.map( name => this.rccStorage.createItemStorage(name).getAll()
											.then( data => [name, data] as [string, unknown[]] )))

		const result		=	Object.fromEntries(entries)

        return result

	}

	public async importData(data: Record<string, unknown[]>): Promise<void> {

		await 	Promise.all(
					Object.keys(data)
					.map( name => this.rccStorage.createItemStorage(name).store(data[name]) )
				)
	}

}
