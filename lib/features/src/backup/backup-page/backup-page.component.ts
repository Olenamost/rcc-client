import 	{
			Component
		}						from '@angular/core'

import {
			HandlerAction,
			DownloadAction
		}						from '@rcc/common'

import {	BackupService	}	from '../backup.service'

import {	CalendarDateString		}	from '@rcc/core'

@Component({
	templateUrl: 	'./backup-page.component.html',
	styleUrls:		['./backup-page.component.scss']
})

export class BackupPageComponent{
	protected data: Record<string, unknown[]> = undefined
	protected filenameSuggestion: string = undefined

	public backupAction: HandlerAction = {
		label:       'BACKUP.PAGE.CREATE_BACKUP.LABEL',
		description: 'BACKUP.PAGE.CREATE_BACKUP.DESCRIPTION',
		icon:		 'backup',
		handler:	 () => this.updateBackupData()
	}

	// action for <rcc-action-button>
    public downloadAction: DownloadAction = {
		label:       'BACKUP.PAGE.DOWNLOAD_BACKUP.LABEL',
		description: 'BACKUP.PAGE.DOWNLOAD_BACKUP.DESCRIPTION',
		icon:        'backup',
		data:		 '',
		filename:	 this.filenameSuggestion,
	}


	// 1. Backup erzeugen
	// 2. Download triggern (Action oben)

	public constructor(
		private backupService: BackupService
	){}

	public async updateBackupData(): Promise<void> {
		this.data = await this.backupService.exportData()
		this.downloadAction.data = JSON.stringify(this.data)
		this.downloadAction.filename = 'rcc-backup-' + CalendarDateString.today() + '-' + String(new Date().getTime())
		console.log('updateBackupData', this.downloadAction)
	}

	public async restore(): Promise<void>{
		return
	}

}
