import	{	Injectable			}	from '@angular/core'

import	{
			RccVersionControlService,
			RccAlertController
		}							from '@rcc/common'


@Injectable()
export class UpdateService {

	public constructor(
		protected rccVersionControlService		: RccVersionControlService,
		protected rccAlertController			: RccAlertController
	){

		this.rccVersionControlService.update$.subscribe( async event => {

			Promise.resolve()
			.then( () => this.rccAlertController.present({
					header: 	'UPDATES.UPDATE_AVAILABLE_ALERT.HEADER',
					message: 	'UPDATES.UPDATE_AVAILABLE_ALERT.MESSAGE',
					buttons: 	[
									{ label:'UPDATES.UPDATE_AVAILABLE_ALERT.NOT_NOW', rejectAs:	'cancel' },
									{ label:'UPDATES.UPDATE_AVAILABLE_ALERT.INSTALL', resolveAs:	'okay' }
								]
				})

			)
			.then( () => this.rccVersionControlService.activateUpdate() )
			.then(

				() => this.rccAlertController.present({

								header: 	'UPDATES.ACTIVATION_SUCCESS_ALERT.HEADER',
								message:	'UPDATES.ACTIVATION_SUCCESS_ALERT.MESSAGE',
								buttons: 	[
												{ label:'UPDATES.ACTIVATION_SUCCESS_ALERT.NOT_NOW', 	rejectAs: 	'cancel' },
												{ label:'UPDATES.ACTIVATION_SUCCESS_ALERT.RESTART', 	resolveAs: 	'okay' },
										]

							}),

				e => {

					if(e === 'cancel') return Promise.reject(e)

					return 	this.rccAlertController.present({

								header: 	'UPDATES.ACTIVATION_FAILURE_ALERT.HEADER',
								message: 	'UPDATES.ACTIVATION_FAILURE_ALERT.MESSAGE',
								buttons: 	[
												{ label:'UPDATES.ACTIVATION_FAILURE_ALERT.NOT_NOW', 	rejectAs: 	'cancel' },
												{ label:'UPDATES.ACTIVATION_FAILURE_ALERT.RESTART', 	resolveAs: 	'okay' },
										]

							})
				}
			)
			.then(
				()	=> location.reload(),
				e	=> console.log(e)
			)


		})


	}

	public async check(): Promise<void>{

		const waitingServiceWorker = await this.rccVersionControlService.checkForUpdates()

		if(waitingServiceWorker) return null

		this.rccAlertController.present({
				header: 	'UPDATES.NO_UPDATE_FOUND_ALERT.HEADER',
				message: 	'UPDATES.NO_UPDATE_FOUND_ALERT.MESSAGE',
				buttons: 	[
								{ label:'UPDATES.NO_UPDATE_FOUND_ALERT.NOT_NOW', rejectAs: 'cancel' },
								{ label:'UPDATES.NO_UPDATE_FOUND_ALERT.RESTART', resolveAs: 'okay' }
							]
			})
			.then(
				() => location.reload(),
				() => {}
			)

	}

}
