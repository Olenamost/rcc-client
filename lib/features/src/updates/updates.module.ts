import	{
			NgModule,
			Component
		}								from '@angular/core'

import {
			ServiceWorkerModule,
			MainMenuModule,
			SharedModule,
			DevModule,
			provideTranslationMap,
			provideMainMenuEntry
		}								from '@rcc/common'


import	{
			UpdateService
		}								from './updates.service'


import en from './i18n/en.json'
import de from './i18n/de.json'



// TODO: Maybe put component and module constructor code into a separate file:

@Component({
	template:	`
					<ion-item [button] = "true" (click) = "updateService.check()">
						<ion-label [id]	= "'UPDATES.MENU_ENTRY' | toID:'rcc-e2e'">
                            {{ "UPDATES.MENU_ENTRY" | translate }}
                        </ion-label>
						<ion-icon [name] = " 'update' | rccIcon" slot = "end"></ion-icon>
					</ion-item>
				`
})
export class UpdatesMainMenuEntryComponent {
	public constructor(public updateService : UpdateService){}
}

const mainMenuEntry =	{
							position: -2,
							component: UpdatesMainMenuEntryComponent
						}

@NgModule({
	imports:[
		ServiceWorkerModule,
		MainMenuModule,
		DevModule.note('UpdateModule, switch to custom swService'),
		SharedModule
	],
	providers: [
		UpdateService,
		provideTranslationMap('UPDATES', { en, de }),
		provideMainMenuEntry(mainMenuEntry)
	],
	declarations:[
		UpdatesMainMenuEntryComponent
	]
})
export class UpdateModule {

	public constructor(
		updateService	: UpdateService,
	){}

}
