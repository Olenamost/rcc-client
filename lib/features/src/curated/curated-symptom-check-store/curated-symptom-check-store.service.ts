import 	{ 	Injectable 							}	from '@angular/core'



import	{
			SymptomCheckStore,
			SymptomCheckConfig

		}											from '@rcc/core'

import	{
			RccTranslationService,
		}											from '@rcc/common'
import	{	CuratedDefaultQuestionStoreService	}	from '../curated-questions/curated-default-questions'


// TODO: Switch Translation Keys QUESTIONS.CURATED to CURATED.QUESTIONS  + move curated questions to features/src/curated

const categories = 	[
	'rcc-category-empty',						// there is no question with this tag, thus the resulting symptom check will have no questions
	'rcc-category-depression',
	'rcc-category-bipolar-disorder',
	'rcc-category-psychosis',
	'rcc-category-schizoaffective-disorders'
]

// const baseMeta:SymptomCheckMetaConfig = 	{
// 														meta: 	{
// 																	defaultSchedule: [ [], [] ],			// Any time of the day and every day
// 																},
// 														questions: []
// 													}

@Injectable()
export class CuratedSymptomCheckStoreService extends SymptomCheckStore {

	public readonly name = 'CURATED.SYMPTOM_CHECKS.STORE_NAME'


	public constructor(
		private curatedDefaultQuestionStoreService	: CuratedDefaultQuestionStoreService,
		private rccTranslationService				: RccTranslationService
	){
		super({
			getAll: async () =>		{
				await curatedDefaultQuestionStoreService.ready				// needed, because async function -> if not awaited, items might not be present yet

				return 	categories.map((tag):SymptomCheckConfig =>	({
							meta:	{
									label: rccTranslationService.translate('CURATED.SYMPTOM_CHECKS.TEMPLATE.LABEL', { category:rccTranslationService.translate(`CURATED.QUESTIONS.CATEGORIES.${tag.toUpperCase()}`)
								})
							},
							questions: 	curatedDefaultQuestionStoreService.items
										.filter( question => question.tags.includes(tag) )
										.map( q => q.id )
						}))
			}
		})
	}

}



// const curatedStorage = { getAll: async () => categories.map( tag => ({
// 	...baseSymptomCheckConfig,
// 	questions:
// }))  }
