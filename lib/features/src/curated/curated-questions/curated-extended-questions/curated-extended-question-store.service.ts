import	{	Injectable				}	from '@angular/core'
import	{
			QuestionStore,
			QuestionConfig
		}								from '@rcc/core'

// Question Config in: lib/core/src/items/questions/questions.commons.ts


@Injectable()
export class CuratedExtendedQuestionStoreService extends QuestionStore {

	public readonly name = 'CURATED.QUESTIONS.CURATED_EXTENDED_NAME'

	public constructor(){
		super(staticStorage)
	}
}


export const 	questionCategories = [
					'rcc-category-daily',
					'rcc-category-weekly',

					'rcc-category-depression',
					'rcc-category-bipolar-disorder',
					'rcc-category-psychosis',
					'rcc-category-anxiety',
					'rcc-category-vegetative-symptoms',
					'rcc-category-use',
					'rcc-category-dess',

					'rcc-symptom-area-mood',
					'rcc-symptom-area-drive',
					'rcc-symptom-area-joy-of-life',
					'rcc-symptom-area-social-interaction',
					'rcc-symptom-area-sleep-disorders',
					'rcc-symptom-area-self-worth',
					'rcc-symptom-area-guilt',
					'rcc-symptom-area-appetite',
					'rcc-symptom-area-libido',
					'rcc-symptom-area-concentration-attention',
					'rcc-symptom-area-future-prospects',
					'rcc-symptom-area-weariness-of-life',
					'rcc-symptom-area-sleep',
					'rcc-symptom-area-drive-activity',
					'rcc-symptom-area-risky-behavior',
					'rcc-symptom-area-impulsivity',
					'rcc-symptom-area-cognitive-abilities',
					'rcc-symptom-area-delusion',
					'rcc-symptom-area-ego-disorders',
					'rcc-symptom-area-thought-initiation-propagation',
					'rcc-symptom-area-paranoid-thoughts',
					'rcc-symptom-area-perceptual-changes',
					'rcc-symptom-area-irregular-heartbeat',
					'rcc-symptom-area-sedation',
					'rcc-symptom-area-slowing-of-movement-akinesia',
					'rcc-symptom-area-weight-gain',
					'rcc-symptom-area-dizziness',
					'rcc-symptom-area-heart-palpitations',
					'rcc-symptom-area-muscle-stiffness',
					'rcc-symptom-area-movement-disorder',
					'rcc-symptom-area-leg-restlessness',
					'rcc-symptom-area-salivation',
					'rcc-symptom-area-impaired-vision',
					'rcc-symptom-area-vegetative-symptoms',
					'rcc-symptom-area-hyperprolactinaemia',
					'rcc-symptom-area-sexual-dysfunction',
					'rcc-symptom-area-inner-restlessness',
					'rcc-symptom-area-depersonalisation',
					'rcc-symptom-area-concentration-disorders',
					'rcc-symptom-area-memory-disorders',
					'rcc-symptom-area-muscle-pain',
					'rcc-symptom-area-movement-restlessness',
					'rcc-symptom-area-gait-disorder',
					'rcc-symptom-area-impaired-vision',
					'rcc-symptom-area-speech',
					'rcc-symptom-area-headache',
					'rcc-symptom-area-perceptual-disturbance'

				]


const staticStorage = { getAll: () => Promise.resolve(configs) }


// depression  f32, f33  Stimmung  depressive Stimmung
// Haben Sie sich heute deprimiert gefühlt?  Haben Sie sich in den letzten sieben Tagen deprimiert gefühlt?  nein, gering, mäßig, stark  default Intensität  depression, negative-symptoms    Did you feel depressed, sad or down today?    Did you feel depressed, sad or down during the past seven days? none, somewhat, moderately, very  no, somewhat, moderately, a lot default Erleben Sie sich als häufiger deprimiert/ traurig/ niedergeschlagen?  nicht vorhanden, gering, mäßig, stark



const configs:QuestionConfig[] = [

		{
			id:       'rcc-curated-extended-0001-daily',
			type:     'integer',
			meaning:  'Haben Sie sich heute deprimiert gefühlt?',
			translations: {
								'en': 'Did you feel depressed today?',
								'de': 'Haben Sie sich heute deprimiert gefühlt?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all', de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',   de: 'gering' } },
								{ value:2  , translations: { en: 'moderately', de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',       de: 'stark' } },


							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'negative-symptoms', 'rcc-symptom-area-mood', 'rcc-category-daily']
		},


	{
				id:       'rcc-curated-extended-0001-weekly',
				type:     'integer',
				meaning:  'Haben Sie sich in den letzten sieben Tagen deprimiert gefühlt?',
				translations: {
									'en': 'Did you feel depressed during the past seven days?',
									'de': 'Haben Sie sich in den letzten sieben Tagen deprimiert gefühlt?'
								},
				options:    [

									{ value:0  , translations: { en: 'not at all',                                                  de: 'nein' } },
									{ value:1  , translations: { en: 'somewhat',                                                  de: 'gering' } },
									{ value:2  , translations: { en: 'moderately',                                                  de: 'mäßig' } },
									{ value:3  , translations: { en: 'very',   de: 'stark' } },


								],
				tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'negative-symptoms', 'rcc-symptom-area-mood', 'rcc-category-weekly']
			},


		{
			id:       'rcc-curated-extended-0002-daily',
			type:     'integer',
			meaning:    'Haben Sie sich heute traurig gefühlt?',
			translations: {
								en: 'Did you feel sad today?',
								de: 'Haben Sie sich heute traurig gefühlt?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',                                                   de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                                     de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                                   de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                                         de: 'stark' } },
							],
				tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'negative-symptoms', 'rcc-symptom-area-mood', 'rcc-category-daily']

		},
	{
				id:       'rcc-curated-extended-0002-weekly',
				type:     'integer',
				meaning:    'Haben Sie sich in den letzten sieben Tagen traurig gefühlt?',
				translations: {
									en: 'Did you feel sad during the past seven days?',
									de: 'Haben Sie sich in den letzten sieben Tagen traurig gefühlt?'
								},
				options:    [

									{ value:0  , translations: { en: 'not at all',                                              de: 'nein' } },
									{ value:1  , translations: { en: 'somewhat',                                              de: 'gering' } },
									{ value:2  , translations: { en: 'moderately',                                              de: 'mäßig' } },
									{ value:3  , translations: { en: 'very',                                                    de: 'stark' } },
								],
				tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'negative-symptoms', 'rcc-symptom-area-mood', 'rcc-category-weekly']

			},
		//        Haben Sie sich heute niedergeschlagen gefühlt?  Haben Sie sich in den letzten sieben Tagen niedergeschlagen gefühlt?  nein, gering, mäßig, stark

	/*
			THIS QUESTION WAS SELECTED TO BE ONE OF THE CURATED DEFAULT QUESTIONS, THAT'S WHY IT IS COMMENTED OUT IN THIS SELECTION.
	{
			id:       'rcc-curated-extended-0003-daily',
			type:     'integer',
			meaning:    "Haben Sie sich heute niedergeschlagen gefühlt?",
			translations: {
								en: "Did you feel down today?",
								de: "Haben Sie sich heute niedergeschlagen gefühlt?"
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',                                                   de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                                     de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                                   de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                                         de: 'stark' } },
							],
			tags:     ['rcc-category-depression',"icd-10-f32","icd-10-f33", "depression", "negative-symptoms", "rcc-symptom-area-mood", 'rcc-category-daily']

		}, */

	{
				id:       'rcc-curated-extended-0003-weekly',
				type:     'integer',
				meaning:    'Haben Sie sich in den letzten sieben Tagen niedergeschlagen gefühlt?',
				translations: {
									en: 'Did you feel down during the past seven days?',
									de: 'Haben Sie sich in den letzten sieben Tagen niedergeschlagen gefühlt?'
								},
				options:    [

									{ value:0  , translations: { en: 'not at all',                                                  de: 'nein' } },
									{ value:1  , translations: { en: 'somewhat',                                                  de: 'gering' } },
									{ value:2  , translations: { en: 'moderately',                                                  de: 'mäßig' } },
									{ value:3  , translations: { en: 'very',                                                        de: 'stark' } },
								],
				tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'negative-symptoms', 'rcc-symptom-area-mood', 'rcc-category-weekly']

			},

		//   Antrieb Antriebslosigkeit Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?   Konnten Sie Ihren Alltag in den letzten sieben Tagen so gut wie sonst bewältigen? ja, etwas schlechter, deutlich schlechter, sehr viel schlechter default Intensität, komparativ  depression          none, somewhat, moderately, very      Gelingt Ihnen Ihre Alltagsbewältigung so gut wie sonst auch?    nicht vorhanden, gering, mäßig, stark


/*

		THIS QUESTION WAS SELECTED TO BE ONE OF THE CURATED DEFAULT QUESTIONS, THAT'S WHY IT IS COMMENTED OUT IN THIS SELECTION.

		{
			id:       'rcc-curated-extended-0004-daily',
			type:     'integer',
			meaning:    "Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?",
			translations: {
								en: "How well were you able to manage your usual daily tasks today?",
								de: "Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?"
							},
			options:    [

								{ value:0  , translations: { en: 'yes',                                                   de: 'ja' } },
								{ value:1  , translations: { en: 'somewhat worse',                                        de: 'etwas schlechter' } },
								{ value:2  , translations: { en: 'moderately worse',                                      de: 'deutlich schlechter' } },
								{ value:3  , translations: { en: 'a lot worse',                                           de: 'sehr viel schlechter' } },
							],
			tags:     ['rcc-category-depression',"icd-10-f32","icd-10-f33", "depression", "rcc-symptom-area-drive", 'rcc-category-daily']

		},
 */

	{
				id:       'rcc-curated-extended-0004-weekly',
				type:     'integer',
				meaning:    'Konnten Sie Ihren Alltag in den letzten sieben Tagen so gut wie sonst bewältigen?',
				translations: {
									en: 'How well were you able to manage your usual weekly tasks?',
									de: 'Konnten Sie Ihren Alltag in den letzten sieben Tagen so gut wie sonst bewältigen?'
								},
				options:    [

									{ value:0  , translations: { en: 'yes',                                                 de: 'ja' } },
									{ value:1  , translations: { en: 'somewhat worse',                                      de: 'etwas schlechter' } },
									{ value:2  , translations: { en: 'moderately worse',                                 	de: 'deutlich schlechter' } },
									{ value:3  , translations: { en: 'a lot worse',                                         de: 'sehr viel schlechter' } },
								],
				tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-drive', 'rcc-category-weekly']

			},
		//        Sind Sie heute Antriebsloser als sonst?   Waren Sie in den letzten sieben Tagen Antriebsloser als sonst?  nein, etwas Antriebsloser, deutlich Antriebsloser, sehr viel Antriebsloser  default Intensität  depression  Caspar        none, somewhat, moderately, very      Sind Sie Antriebslos? nicht vorhanden, gering, mäßig, stark



	/*
		THIS QUESTION WAS SELECTED TO BE ONE OF THE CURATED DEFAULT QUESTIONS, THAT'S WHY IT IS COMMENTED OUT IN THIS SELECTION.
	{
			id:       'rcc-curated-extended-0005-daily',
			type:     'integer',
			meaning:    "Sind Sie heute antriebsloser als sonst?",
			translations: {
								en: "Was your energy low today?",
								de: "Sind Sie heute antriebsloser als sonst?"
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',                                 de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat low',                               de: 'etwas Antriebsloser' } },
								{ value:2  , translations: { en: 'moderately low',                             de: 'deutlich Antriebsloser' } },
								{ value:3  , translations: { en: 'very low',                                   de: 'sehr viel Antriebsloser' } },
							],
			tags:     ['rcc-category-depression',"icd-10-f32","icd-10-f33", "depression", "rcc-symptom-area-drive", 'rcc-category-daily']

		},
 */

	{
			id:       'rcc-curated-extended-0005-weekly',
			type:     'integer',
			meaning:    'Waren Sie in den letzten sieben Tagen antriebslos?',
			translations: {
								en: 'Was your energy low in the past seven days?',
								de: 'Waren Sie in den letzten sieben Tagen antriebslos?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',                                de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat low',                              de: 'etwas Antriebsloser' } },
								{ value:2  , translations: { en: 'moderately low',                            de: 'deutlich Antriebsloser' } },
								{ value:3  , translations: { en: 'very low',                                  de: 'sehr viel Antriebsloser' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-drive', 'rcc-category-weekly']

		},

		//        Haben Sie heute Ihre Alltagsroutinen schlechter als sonst geschafft  (Hygiene, Einkauf, Haushalt etc.)? Haben Sie in den letzten sieben Tagen Ihre Alltagsroutinen schlechter als sonst geschafft  (Hygiene, Einkauf, Haushalt etc.)? nein, etwas schlechter, deutlich schlechter, gar nicht geschafft  default Intensität, komparativ  depression                Schaffen Sie Ihre täglichen Routinen im Alltag schlechter als sonst (Hygiene, Einkauf, Haushalt etc.)?



		{
			id:       'rcc-curated-extended-0006-daily',
			type:     'integer',
			meaning:    'Haben Sie heute Ihre Alltagsroutinen schlechter als sonst geschafft (Hygiene, Einkauf, Haushalt etc.)?',
			translations: {
								en: 'Was it harder than usual to manage your daily routines (personal hygiene, chores) today?',
								de: 'Haben Sie heute Ihre Alltagsroutinen schlechter als sonst geschafft (Hygiene, Einkauf, Haushalt etc.)?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',                                                   de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat harder',                                              de: 'etwas schlechter' } },
								{ value:2  , translations: { en: 'moderately harder',                                            de: 'deutlich schlechter' } },
								{ value:3  , translations: { en: 'a lot harder',                                                 de: 'gar nicht geschafft' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-drive', 'rcc-category-daily']

		},

	{
				id:       'rcc-curated-extended-0006-weekly',
				type:     'integer',
				meaning:    'Haben Sie in den letzten sieben Tagen Ihre Alltagsroutinen schlechter als sonst geschafft (Hygiene, Einkauf, Haushalt etc.)?',
				translations: {
									en: 'Was it harder than usual to manage your daily routines (personal hygiene, chores) in the past seven days?',
									de: 'Haben Sie in den letzten sieben Tagen Ihre Alltagsroutinen schlechter als sonst geschafft  (Hygiene, Einkauf, Haushalt etc.)?'
								},
				options:    [

									{ value:0  , translations: { en: 'not at all',                                          de: 'nein' } },
									{ value:1  , translations: { en: 'somewhat worse',                                      de: 'etwas schlechter' } },
									{ value:2  , translations: { en: 'significantly worse',                                 de: 'deutlich schlechter' } },
									{ value:3  , translations: { en: 'not managed at all',                                  de: 'gar nicht geschafft' } },
								],
				tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-drive', 'rcc-category-weekly']
			},


		//    Lebensfreude  Anhedonie, Lustlosigkeit  Hat Ihnen heute Aktivitäten so viel Freude gemacht wie sonst auch?  Hat Ihnen in den letzten sieben Tagen Aktivitäten so viel Freude gemacht wie sonst auch?  ja, etwas weniger, deutlich weniger, sehr viel weniger  default Intensität, komparativ  depression  PAB       none, somewhat, moderately, very      Macht Ihnen alles so viel Freude wie sonst auch?  nicht vorhanden, gering, mäßig, stark



		{
			id:       'rcc-curated-extended-0007-daily',
			type:     'integer',
			meaning:    'Haben Ihnen heute Aktivitäten so viel Freude gemacht wie sonst auch?',
			translations: {
								en: 'Did you enjoy activities as much as usual today?',
								de: 'Haben Ihnen heute Aktivitäten so viel Freude gemacht wie sonst auch?'
							},
			options:    [

								{ value:0  , translations: { en: 'yes',                       de: 'ja' } },
								{ value:1  , translations: { en: 'somewhat less',             de: 'etwas weniger' } },
								{ value:2  , translations: { en: 'moderately less',           de: 'deutlich weniger' } },
								{ value:3  , translations: { en: 'a lot less',                de: 'sehr viel weniger' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-joy-of-life', 'rcc-category-daily']
		},

	{
				id:       'rcc-curated-extended-0007-weekly',
				type:     'integer',
				meaning:    'Haben Ihnen in den letzten sieben Tagen Aktivitäten so viel Freude gemacht wie sonst auch?',
				translations: {
									en: 'Did you enjoy activities as much as usual in the past seven days?',
									de: 'Haben Ihnen in den letzten sieben Tagen Aktivitäten so viel Freude gemacht wie sonst auch?'
								},
				options:    [

									{ value:0  , translations: { en: 'yes',                      de: 'ja' } },
									{ value:1  , translations: { en: 'somewhat less',            de: 'etwas weniger' } },
									{ value:2  , translations: { en: 'moderately less',          de: 'deutlich weniger' } },
									{ value:3  , translations: { en: 'a lot less',               de: 'sehr viel weniger' } },
								],
				tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-joy-of-life', 'rcc-category-weekly']
			},

		{
			id:       'rcc-curated-extended-0008-daily',
			type:     'integer',
			meaning:    'War das Zusammensein mit anderen Menschen heute anstrengend?',
			translations: {
								en: 'Was it exhausting being with other people today?',
								de: 'War das Zusammensein mit anderen Menschen heute anstrengend?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',       de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',         de: 'etwas anstrengend' } },
								{ value:2  , translations: { en: 'moderately',       de: 'deutlich anstrengend' } },
								{ value:3  , translations: { en: 'very',             de: 'sehr anstrengend' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-social-interaction', 'rcc-category-daily']

		},
		{
			id:       'rcc-curated-extended-0008-weekly',
			type:     'integer',
			meaning:    'War das Zusammensein mit anderen Menschen in den letzten sieben Tagen anstrengend?',
			translations: {
								en: 'Was being with other people exhausting in the past seven days? ',
								de: 'War das Zusammensein mit anderen Menschen in den letzten sieben Tagen anstrengend?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',       de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',         de: 'etwas anstrengend' } },
								{ value:2  , translations: { en: 'moderately',       de: 'deutlich anstrengend' } },
								{ value:3  , translations: { en: 'very',             de: 'sehr anstrengend' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-social-interaction', 'rcc-category-weekly']

		},
		{
			id:       'rcc-curated-extended-0009-daily',
			type:     'integer',
			meaning:    'Haben Sie sich heute mehr zurückgezogen?',
			translations: {
								en: 'Did you withdraw more from social interactions today?',
								de: 'Haben Sie sich heute mehr zurückgezogen?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all more',       de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',         de: 'etwas zurückgezogen' } },
								{ value:2  , translations: { en: 'moderately more',       de: 'deutlich zurückgezogen' } },
								{ value:3  , translations: { en: 'a lot more',            de: 'komplett zurückgezogen' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-social-interaction', 'rcc-category-daily']

		},
		{
			id:       'rcc-curated-extended-0009-weekly',
			type:     'integer',
			meaning:    'Haben Sie sich in den letzten sieben Tagen vermehrt zurück gezogen?',
			translations: {
								en: 'Were you withdrawing more from social interactions in the past seven days?',
								de: 'Haben Sie sich in den letzten sieben Tagen vermehrt zurück gezogen?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all more',       de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',         de: 'etwas zurückgezogen' } },
								{ value:2  , translations: { en: 'moderately more',       de: 'deutlich zurückgezogen' } },
								{ value:3  , translations: { en: 'a lot more',            de: 'komplett zurückgezogen' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-social-interaction', 'rcc-category-weekly']

		},
		{
			id:       'rcc-curated-extended-0010-daily',
			type:     'integer',
			meaning:    'Hatten Sie heute ein vermehrtes Schlafbedürfnis?',
			translations: {
								en: 'Were you more tired than usual today?',
								de: 'Hatten Sie heute ein vermehrtes Schlafbedürfnis?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all more',       de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',         de: 'etwas mehr' } },
								{ value:2  , translations: { en: 'moderately more',       de: 'deutlich mehr' } },
								{ value:3  , translations: { en: 'a lot more',            de: 'sehr viel mehr' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-sleep-disorders', 'rcc-category-daily']
		},
		{
			id:       'rcc-curated-extended-0010-weekly',
			type:     'integer',
			meaning:    'Hatten Sie in den letzten sieben Tagen ein vermehrtes Schlafbedürfnis?',
			translations: {
								en: 'Were you more tired than usual during the past seven days?',
								de: 'Hatten Sie in den letzten sieben Tagen ein vermehrtes Schlafbedürfnis?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all more',       de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',         de: 'etwas mehr' } },
								{ value:2  , translations: { en: 'moderately more',       de: 'deutlich mehr' } },
								{ value:3  , translations: { en: 'a lot more',            de: 'sehr viel mehr' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-sleep-disorders', 'rcc-category-weekly']
		},
		{
			id:       'rcc-curated-extended-0011-daily',
			type:     'integer',
			meaning:    'Haben Sie letzte Nacht schlecht einschlafen können?',
			translations: {
								en: 'Did you have trouble falling asleep last night?',
								de: 'Haben Sie letzte Nacht schlecht einschlafen können?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',                                                  de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',       												de: 'etwas Einschlafprobleme (weniger als 30 Minuten)' } },
								{ value:2  , translations: { en: 'moderately',     												de: 'deutlich Einschlafprobleme (30 Minuten bis 1h)' } },
								{ value:3  , translations: { en: 'a lot',             											de: 'starke Einschlafstörungen (über 1h)' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-sleep-disorders', 'rcc-category-daily']
		},
		{
			id:       'rcc-curated-extended-0012-daily',
			type:     'integer',
			meaning:    'Haben Sie letzte Nacht schlecht durchschlafen können?',
			translations: {
								en: 'Did you have trouble sleeping through the night? ',
								de: 'Haben Sie letzte Nacht schlecht durchschlafen können?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',    					de: 'etwas Durchschlafprobleme (ein bis zweimal aufgewacht, schnell wieder eingeschlafen)' } },
								{ value:2  , translations: { en: 'moderately',          			de: 'deutliche Durchschlafprobleme (mehr als zweimal aufgewacht, wachgelegen)' } },
								{ value:3  , translations: { en: 'a lot',                  			de: 'sehr starke Durchschlafprobleme (konnte nicht wieder einschlafen)' } },
							],
			tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-sleep-disorders', 'antidepressant reduction', 'rcc-category-daily']
		},

//	nein, etwas Durchschlafprobleme (ein bis zweimal aufgewacht, schnell wieder eingeschlafen), deutliche Durchschlafprobleme (mehr als zweimal aufgewacht, wachgelegen), sehr starke Durchschlafprobleme (konnte nicht wieder einschlafen)

{
	id:       'rcc-curated-extended-0013-daily',
	type:     'integer',
	meaning:    'Waren Sie heute tagsüber müde?',
	translations: {
				en: 'Were you tired during daytime today?',
				de: 'Waren Sie heute tagsüber müde?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'constantly',                de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-sleep-disorders', 'antidepressant reduction', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0013-weekly',
	type:     'integer',
	meaning:    'Waren Sie in den letzten sieben Tagen tagsüber müde?',
	translations: {
				en: 'Were you tired during daytime in the past seven days?',
				de: 'Waren Sie in den letzten sieben Tagen tagsüber müde?'
			},
			options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'constantly',                de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-self-worth', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0014-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute wertlos gefühlt?',
		translations: {
					en: 'Did you feel worthless today?',
					de: 'Haben Sie sich heute wertlos gefühlt?'
				},
		options:    [

					{ value:0  , translations: { en: 'not at all',   de: 'nein' } },
					{ value:1  , translations: { en: 'somewhat',     de: 'gering' } },
					{ value:2  , translations: { en: 'moderately',   de: 'mäßig' } },
					{ value:3  , translations: { en: 'very',         de: 'stark' } },
				],
						tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-self-worth', 'rcc-category-daily']
		},
	{
	id:       'rcc-curated-extended-0014-weekly',
	type:     'integer',
	meaning:    'Haben Sie sich in den letzten sieben Tagen wertlos gefühlt?',
	translations: {
				en: 'Did you feel worthless in the past seven days?',
				de: 'Haben Sie sich in den letzten sieben Tagen wertlos gefühlt?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',          de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',            de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',        	de: 'mäßig' } },
				{ value:3  , translations: { en: 'very',         		de: 'stark' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-self-worth', 'rcc-category-weekly']
	},


	{
	id:       'rcc-curated-extended-0015-daily',
	type:     'integer',
	meaning:    'Haben Sie sich heute schuldig gefühlt?',
	translations: {
				en: 'Did you feel guilty today?',
				de: 'Haben Sie sich heute schuldig gefühlt?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
				{ value:3  , translations: { en: 'very',                      de: 'stark' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-guilt', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0015-weekly',
	type:     'integer',
	meaning:    'Haben Sie sich in den letzten sieben Tagen beinahe täglich schuldig gefühlt?',
	translations: {
				en: 'Did you feel guilty almost every day for the past seven days?',
				de: 'Haben Sie sich in den letzten sieben Tagen beinahe täglich schuldig gefühlt?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'stark' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-guilt', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0016-weekly',
	type:     'integer',
	meaning:    'Haben Sie in den letzten sieben Tagen unter Appeititlosigkeit gelitten?',
	translations: {
				en: 'Did you have a loss of appetite in the past seven days?',
				de: 'Haben Sie in den letzten sieben Tagen unter Appeititlosigkeit gelitten?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat less',             de: 'gering' } },
				{ value:2  , translations: { en: 'moderately less',           de: 'mäßig' } },
				{ value:3  , translations: { en: 'a lot less',                de: 'stark' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-appetite', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0017-daily',
	type:     'integer',
	meaning:    'Hatten Sie heute weniger Appetit als gewöhnlich?',
	translations: {
				en: 'Was your appetite lower than usual today?',
				de: 'Hatten Sie heute weniger Appetit als gewöhnlich?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'slightly less',             de: 'gering' } },
				{ value:2  , translations: { en: 'moderately less',           de: 'mäßig' } },
				{ value:3  , translations: { en: 'a lot less',                de: 'stark' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-appetite', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0017-weekly',
	type:     'integer',
	meaning:    'Hatten Sie in den letzten sieben Tagen weniger Appetit als gewöhnlich?',
	translations: {
				en: 'Was your appetite lower than usual in the past seven days?',
				de: 'Hatten Sie in den letzten sieben Tagen weniger Appetit als gewöhnlich?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'slightly less',             de: 'gering' } },
				{ value:2  , translations: { en: 'moderately less',           de: 'mäßig' } },
				{ value:3  , translations: { en: 'a lot less',                de: 'stark' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-appetite', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0018-weekly',
	type:     'integer',
	meaning:    'Haben Sie in den letzten sieben Tagen unbeabsichtigt an Gewicht verloren?',
	translations: {
				en: 'Did you unintentionally lose weight in the past seven days?',
				de: 'Haben Sie in den letzten sieben Tagen unbeabsichtigt an Gewicht verloren?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'etwas' } },
				{ value:2  , translations: { en: 'moderately',                de: 'deutlich' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'sehr viel' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-appetite', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0019-weekly',
	type:     'integer',
	meaning:    'Haben Sie weniger sexuelles Verlangen als sonst verspürt?',
	translations: {
				en: 'Was your sex drive lower than usual in the past seven days?',
				de: 'Haben Sie weniger sexuelles Verlangen als sonst verspürt?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'etwas weniger' } },
				{ value:2  , translations: { en: 'moderately lower',          de: 'deutlich weniger' } },
				{ value:3  , translations: { en: 'a lot lower',               de: 'sehr viel weniger' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-libido', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0020-daily',
	type:     'integer',
	meaning:    'Hatten Sie heute Schwierigkeiten, sich zu konzentrieren?',
	translations: {
				en: 'Did you have trouble concentrating today?',
				de: 'Hatten Sie heute Schwierigkeiten, sich zu konzentrieren?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                   de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                     de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately lower',             de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot lower',                  de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-concentration-attention', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0020-weekly',
	type:     'integer',
	meaning:    'Hatten Sie in den letzten sieben Tagen Schwierigkeiten, sich zu konzentrieren?',
	translations: {
				en: 'Did you have trouble concentrating in the past seven days?',
				de: 'Hatten Sie in den letzten sieben Tagen Schwierigkeiten, sich zu konzentrieren?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-concentration-attention', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0021-daily',
	type:     'integer',
	meaning:    'Konnten Sie heute Gesprächen schlechter folgen als sonst?',
	translations: {
				en: 'Did you have more difficulty following conversations than usual?',
				de: 'Konnten Sie heute Gesprächen schlechter folgen als sonst?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-concentration-attention', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0021-weekly',
	type:     'integer',
	meaning:    'Konnten Sie in den letzten sieben Tagen Gesprächen schlechter folgen als sonst?',
	translations: {
				en: 'Did you have more difficulty following conversations in the past seven days?',
				de: 'Konnten Sie in den letzten sieben Tagen Gesprächen schlechter folgen als sonst?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-concentration-attention', 'rcc-category-weekly']
	},

	{
	id:       'rcc-curated-extended-0022-daily',
	type:     'integer',
	meaning:    'Haben Sie heute das Gefühl, dass Ihnen Worte nicht einfallen?',
	translations: {
				en: 'Did you have difficulty coming up with certain words today?',
				de: 'Haben Sie heute das Gefühl, dass Ihnen Worte nicht einfallen?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-concentration-attention', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0022-weekly',
	type:     'integer',
	meaning:    'Haben Sie in den letzten sieben Tagen das Gefühl, dass Ihnen Worte nicht einfallen?',
	translations: {
				en: 'In the past seven days, did you have difficulty coming up with certain words?',
				de: 'Haben Sie in den letzten sieben Tagen das Gefühl, dass Ihnen Worte nicht einfallen?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-concentration-attention', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0023-daily',
	type:     'integer',
	meaning:    'Hatten Sie heute Schwierigkeiten, Entscheidungen zu treffen?',
	translations: {
				en: 'Did you have trouble making decisions today?',
				de: 'Hatten Sie heute Schwierigkeiten, Entscheidungen zu treffen?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-concentration-attention', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0023-weekly',
	type:     'integer',
	meaning:    'Hatten Sie in den letzten sieben Tagen Schwierigkeiten, Entscheidungen zu treffen?',
	translations: {
				en: 'Did you have trouble making decisions in the past seven days?',
				de: 'Hatten Sie in den letzten sieben Tagen Schwierigkeiten, Entscheidungen zu treffen?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                   de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                     de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                   de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                        de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-concentration-attention', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0024-daily',
	type:     'integer',
	meaning:    'Fühlten Sie sich heute hoffnungslos?',
	translations: {
				en: 'Did you feel hopeless today?',
				de: 'Fühlten Sie sich heute hoffnungslos?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'very',                      de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-future-prospects', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0024-weekly',
	type:     'integer',
	meaning:    'Fühlten Sie sich in den letzten sieben Tagen beinahe täglich hoffnungslos?',
	translations: {
				en: 'Did you feel hopeless almost every day during the past seven days?',
				de: 'Fühlten Sie sich in den letzten sieben Tagen beinahe täglich hoffnungslos?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'very',                      de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-future-prospects', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0025-daily',
	type:     'integer',
	meaning:    'Haben Sie heute an den Tod gedacht?',
	translations: {
				en: 'Did you think about death today?',
				de: 'Haben Sie heute an den Tod gedacht?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'rarely',                    de: 'selten daran gedacht' } },
				{ value:2  , translations: { en: 'often',                     de: 'häufig daran gedacht' } },
				{ value:3  , translations: { en: 'constantly',                de: 'ständig daran gedacht' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-weariness-of-life', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0025-weekly',
	type:     'integer',
	meaning:    'Haben Sie in den letzten sieben Tagen an den Tod gedacht?',
	translations: {
				en: 'Did you think about death in the past seven days?',
				de: 'Haben Sie in den letzten sieben Tagen an den Tod gedacht?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'rarely',                    de: 'selten daran gedacht' } },
				{ value:2  , translations: { en: 'often',                     de: 'häufig daran gedacht' } },
				{ value:3  , translations: { en: 'constantly',                de: 'ständig daran gedacht' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-weariness-of-life', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0026-daily',
	type:     'integer',
	meaning:    'Haben Sie heute daran gedacht, sich selbst das Leben zu nehmen?',
	translations: {
				en: 'Did you think about taking your own life today?',
				de: 'Haben Sie heute daran gedacht, sich selbst das Leben zu nehmen?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                                   de: 'nein' } },
				{ value:1  , translations: { en: 'rarely',                                       de: 'selten daran gedacht' } },
				{ value:2  , translations: { en: 'often',                                        de: 'häufig daran gedacht' } },
				{ value:3  , translations: { en: 'constantly',                                   de: 'ständig daran gedacht' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-weariness-of-life', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0026-weekly',
	type:     'integer',
	meaning:    'Haben Sie in den letzten sieben Tagen daran gedacht, sich selbst das Leben zu nehmen?',
	translations: {
				en: 'Did you think about taking your own life in the past seven days?',
				de: 'Haben Sie in den letzten sieben Tagen daran gedacht, sich selbst das Leben zu nehmen?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                                   de: 'nein' } },
				{ value:1  , translations: { en: 'rarely',                                       de: 'selten daran gedacht' } },
				{ value:2  , translations: { en: 'often',                                        de: 'häufig daran gedacht' } },
				{ value:3  , translations: { en: 'constantly',                                   de: 'ständig daran gedacht' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-weariness-of-life', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0027-daily',
	type:     'integer',
	meaning:    'Haben Sie heute Ihr Leben vermehrt als sinnlos erlebt oder Lebensüberdruss verspürt?',
	translations: {
				en: 'Did you feel like life was meaningless or not worth living today?',
				de: 'Haben Sie heute Ihr Leben vermehrt als sinnlos erlebt oder Lebensüberdruss verspürt?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',        de: 'nein' } },
				{ value:1  , translations: { en: 'rarely',            de: 'selten' } },
				{ value:2  , translations: { en: 'often',             de: 'häufig' } },
				{ value:3  , translations: { en: 'constantly',        de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-weariness-of-life', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0027-weekly',
	type:     'integer',
	meaning:    'Haben Sie in den letzten sieben Tagen Ihr Leben vermehrt als sinnlos erlebt oder Lebensüberdruss verspürt?',
	translations: {
				en: 'In the past seven days, did you feel like life was meaningless or not worth living?',
				de: 'Haben Sie in den letzten sieben Tagen Ihr Leben vermehrt als sinnlos erlebt oder Lebensüberdruss verspürt?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',        de: 'nein' } },
				{ value:1  , translations: { en: 'rarely',            de: 'selten' } },
				{ value:2  , translations: { en: 'often',             de: 'häufig' } },
				{ value:3  , translations: { en: 'constantly',        de: 'ständig' } },
			],
					tags:     ['rcc-category-depression','icd-10-f32','icd-10-f33', 'depression', 'rcc-symptom-area-weariness-of-life', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0028-daily',
	type:     'integer',
	meaning:    'War Ihre Stimmung heute euphorisch?',
	translations: {
				en: 'Was your mood euphoric today?',
				de: 'War Ihre Stimmung heute euphorisch?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
				{ value:3  , translations: { en: 'very',                      de: 'stark' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-mood', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0028-weekly',
	type:     'integer',
	meaning:    'War Ihre Stimmung in den letzten sieben Tagen euphorisch?',
	translations: {
				en: 'Was your mood euphoric in the past seven days?',
				de: 'War Ihre Stimmung in den letzten sieben Tagen euphorisch?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
				{ value:3  , translations: { en: 'very',                      de: 'stark' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0029-daily',
	type:     'integer',
	meaning:    'Haben Sie sich heute aufgedreht gefühlt?',
	translations: {
				en: 'Did you feel wound up today?',
				de: 'Haben Sie sich heute aufgedreht gefühlt?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
				{ value:3  , translations: { en: 'very',                      de: 'stark' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-mood', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0029-weekly',
	type:     'integer',
	meaning:    'Haben Sie sich in den letzten sieben Tagen aufgedreht gefühlt?',
	translations: {
				en: 'Did you feel hyper / wound up in the past seven days?',
				de: 'Haben Sie sich in den letzten sieben Tagen aufgedreht gefühlt?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                   de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                     de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',                   de: 'mäßig' } },
				{ value:3  , translations: { en: 'very',                         de: 'stark' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},

	{
	id:       'rcc-curated-extended-0030-daily',
	type:     'integer',
	meaning:    'Haben Sie sich heute über Kleinigkeiten geärgert?',
	translations: {
				en: 'Were you annoyed by little things today?',
				de: 'Haben Sie sich heute über Kleinigkeiten geärgert?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-mood', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0030-weekly',
	type:     'integer',
	meaning:    'Haben Sie sich in den letzten sieben Tagen über Kleinigkeiten geärgert?',
	translations: {
				en: 'Were you annoyed by little things in the past seven days?',
				de: 'Haben Sie sich in den letzten sieben Tagen über Kleinigkeiten geärgert?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                   de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                     de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                   de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                        de: 'ständig' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0031-daily',
	type:     'integer',
	meaning:    'Haben Sie letzte Nacht weniger Schlaf als sonst gebraucht?',
	translations: {
				en: 'Did you need less sleep than usual past night?',
				de: 'Haben Sie letzte Nacht weniger Schlaf als sonst gebraucht?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat less',             de: 'etwas weniger' } },
				{ value:2  , translations: { en: 'moderately less',           de: 'deutlich weniger' } },
				{ value:3  , translations: { en: 'a lot less',                de: 'sehr viel weniger' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-sleep', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0031-weekly',
	type:     'integer',
	meaning:    'Haben Sie in den letzten sieben Tagen weniger Schlaf als sonst gebraucht?',
	translations: {
				en: 'Did you need less sleep than usual in the past seven days?',
				de: 'Haben Sie in den letzten sieben Tagen weniger Schlaf als sonst gebraucht?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                 de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat less',              de: 'etwas weniger' } },
				{ value:2  , translations: { en: 'moderately less',            de: 'deutlich weniger' } },
				{ value:3  , translations: { en: 'a lot less',                 de: 'sehr viel weniger' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-sleep', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0032-daily',
	type:     'boolean',
	meaning:    'Fühlten Sie sich heute trotz verminderter Schlafzeit voller Energie?',
	translations: {
				en: 'Did you feel full of energy today despite reduced sleep time?',
				de: 'Fühlten Sie sich heute trotz verminderter Schlafzeit voller Energie?'
			},
	tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-sleep', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0032-weekly',
	type:     'boolean',
	meaning:    'Fühlten Sie sich in den letzten sieben Tagen trotz verminderter Schlafzeit voller Energie?',
	translations: {
				en: 'In the past seven days, did you feel full of energy despite reduced sleep time?',
				de: 'Fühlten Sie sich in den letzten sieben Tagen trotz verminderter Schlafzeit voller Energie?'
			},
		tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-sleep', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0033-daily',
	type:     'integer',
	meaning:    'Hatten Sie letzte Nacht das Gefühl, Sie werden einfach nicht müde?',
	translations: {
				en: 'Did you have trouble getting tired last night?',
				de: 'Hatten Sie letzte Nacht das Gefühl, Sie werden einfach nicht müde?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'stark' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-sleep', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0033-weekly',
	type:     'integer',
	meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, dass Sie einfach nicht müde werden?',
	translations: {
				en: 'In the past seven days, did you have trouble getting tired during nighttime?',
				de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, dass Sie einfach nicht müde werden?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',              de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',            de: 'mäßig' } },
				{ value:3  , translations: { en: 'a lot',                 de: 'stark' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-sleep', 'rcc-category-weekly']
	},
	/*
		THIS QUESTION WAS SELECTED TO BE ONE OF THE CURATED DEFAULT QUESTIONS, THAT'S WHY IT IS COMMENTED OUT IN THIS SELECTION.
	{
	id:       'rcc-curated-extended-0034-daily',
	type:     'integer',
	meaning:    "Waren Sie voller Energie und motiviert, viele verschiedene Dinge zu tun?",
	translations: {
				en: "Were you full of energy and motivated to do lots of different things today?",
				de: "Waren Sie voller Energie und motiviert, viele verschiedene Dinge zu tun?"
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
				{ value:3  , translations: { en: 'very',                      de: 'stark' } },
			],
					tags:     ['rcc-category-bipolar-disorder',"icd-10-f31", "bipolar disorder", "rcc-symptom-area-drive-activity", 'rcc-category-daily']
	},*/
	{
	id:       'rcc-curated-extended-0034-weekly',
	type:     'integer',
	meaning:    'Waren Sie in den letzten sieben Tagen voller Energie und motiviert, viele verschiedene Dinge zu tun?',
	translations: {
				en: 'Did you feel full of energy and motivated to do many different things during the past seven days?',
				de: 'Waren Sie in den letzten sieben Tagen voller Energie und motiviert, viele verschiedene Dinge zu tun?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',              de: 'gering' } },
				{ value:2  , translations: { en: 'moderately',            de: 'mäßig' } },
				{ value:3  , translations: { en: 'very',                  de: 'stark' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0035-daily',
	type:     'integer',
	meaning:    'Haben Sie das Gefühl, Zusammenhänge schneller zu erfassen?',
	translations: {
				en: 'Did you feel like you were able to make connections faster than usual today?',
				de: 'Haben Sie das Gefühl, Zusammenhänge schneller zu erfassen?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'often',                     de: 'häufig' } },
				{ value:3  , translations: { en: 'constantly',                de: 'ständig' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0035-weekly',
	type:     'integer',
	meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, Zusammenhänge schneller zu erfassen?',
	translations: {
				en: 'Did you feel like you were able to make connections faster than usual in the past seven days?',
				de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, Zusammenhänge schneller zu erfassen?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'very',                      de: 'ständig' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0036-daily',
	type:     'integer',
	meaning:    'Konnten Sie heute viele Dinge in sehr kurzer Zeit und mit wenig Mühe tun?',
	translations: {
				en: 'Were you able to complete many tasks in a short time and with little effort today?',
				de: 'Konnten Sie heute viele Dinge in sehr kurzer Zeit und mit wenig Mühe tun?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0036-weekly',
	type:     'integer',
	meaning:    'Konnten Sie in den letzten sieben Tagen viele Dinge in sehr kurzer Zeit und mit wenig Mühe tun?',
	translations: {
				en: 'Were you able to complete many tasks in a short time and with little effort in the past seven days?',
				de: 'Konnten Sie in den letzten sieben Tagen viele Dinge in sehr kurzer Zeit und mit wenig Mühe tun?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
				{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
				{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0037-daily',
	type:     'integer',
	meaning:    'Waren Sie heute aktiver als gewöhnlich?',
	translations: {
				en: 'Were you more active than usual today?',
				de: 'Waren Sie heute aktiver als gewöhnlich?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all more',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat more',                  de: 'etwas aktiver' } },
				{ value:2  , translations: { en: 'moderately more',                de: 'deutlich aktiver' } },
				{ value:3  , translations: { en: 'a lot more',                     de: 'sehr viel aktiver' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0037-weekly',
	type:     'integer',
	meaning:    'Waren Sie in den letzten sieben Tagen aktiver als gewöhnlich?',
	translations: {
				en: 'Were you more active than usual in the past seven days?',
				de: 'Waren Sie in den letzten sieben Tagen aktiver als gewöhnlich?'
			},
	options:    [

				{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
				{ value:1  , translations: { en: 'somewhat more',             de: 'etwas aktiver' } },
				{ value:2  , translations: { en: 'significantly more',        de: 'deutlich aktiver' } },
				{ value:3  , translations: { en: 'a lot more',                de: 'sehr viel aktiver' } },
			],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0038-daily',
	type:     'boolean',
	meaning:    'Waren Sie heute ständig in Bewegung?',
	translations: {
				en: 'Were you moving constantly today?',
				de: 'Waren Sie heute ständig in Bewegung?'
			},
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0038-weekly',
	type:     'boolean',
	meaning:    'Waren Sie in den letzten sieben Tagen ständig in Bewegung?',
	translations: {
				en: 'Were you constantly on the move in the past seven days?',
				de: 'Waren Sie in den letzten sieben Tagen ständig in Bewegung?'
			},
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0039-daily',
	type:     'integer',
	meaning:    'Waren Sie heute gesprächiger als sonst?',
	translations: {
				en: 'Were you more talkative than usual today?',
				de: 'Waren Sie heute gesprächiger als sonst?'
			},
		options:    [

								{ value:0  , translations: { en: 'not at all more',            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',              de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately more',            de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot more',                 de: 'ständig' } },
								],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0039-weekly',
	type:     'integer',
	meaning:    'Waren Sie in den letzten sieben Tagen gesprächiger als sonst?',
	translations: {
				en: 'Were you more talkative than usual in the past seven days?',
				de: 'Waren Sie in den letzten sieben Tagen gesprächiger als sonst?'
			},
		options:    [

								{ value:0  , translations: { en: 'not at all more',            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',              de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately more',            de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot more',                 de: 'ständig' } },
						],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0040-daily',
	type:     'integer',
	meaning:    'Haben Sie heute so schnell gesprochen, dass andere Schwierigkeiten hatten, Sie zu verstehen?',
	translations: {
				en: 'Were you talking so fast that others had trouble understanding you today?',
				de: 'Haben Sie heute so schnell gesprochen, dass andere Schwierigkeiten hatten, Sie zu verstehen?'
			},
		options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                 de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',            de: 'ständig' } },
								],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0040-weekly',
	type:     'integer',
	meaning:    'Haben Sie in den letzten sieben Tagen so schnell gesprochen, dass andere Schwierigkeiten hatten, Sie zu verstehen?',
	translations: {
				en: 'In the past seven days, were you talking so fast that others had trouble understanding you?',
				de: 'Haben Sie in den letzten sieben Tagen so schnell gesprochen, dass andere Schwierigkeiten hatten, Sie zu verstehen?'
			},
		options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                 de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',            de: 'ständig' } },
						],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-drive-activity', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0041-daily',
	type:     'integer',
	meaning:    'Waren Sie heute risikofreudiger als gewöhnlich?',
	translations: {
				en: 'Were you more willing to take risks today than usual?',
				de: 'Waren Sie heute risikofreudiger als gewöhnlich?'
			},
		options:    [

								{ value:0  , translations: { en: 'not at all more',            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',              de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately more',            de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot more',                 de: 'ständig' } },
								],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-risky-behavior', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0041-weekly',
	type:     'integer',
	meaning:    'Waren Sie in den letzten sieben Tagen risikofreudiger als gewöhnlich?',
	translations: {
				en: 'Was your behavior riskier than usual in the past seven days?',
				de: 'Waren Sie in den letzten sieben Tagen risikofreudiger als gewöhnlich?'
			},
		options:    [

								{ value:0  , translations: { en: 'not at all more',            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',              de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately more',            de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot more',                 de: 'ständig' } },
						],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-risky-behavior', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0042-daily',
	type:     'integer',
	meaning:    'Haben Sie sich heute so verhalten, dass es Sie oder Ihre Familie in Schwierigkeit hätte bringen können?',
	translations: {
				en: 'Did you behave in such a way today that it could have gotten you or your family into trouble?',
				de: 'Haben Sie sich heute so verhalten, dass es Sie oder Ihre Familie in Schwierigkeit hätte bringen können?'
			},
		options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                 de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',            de: 'ständig' } },
								],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-risky-behavior', 'rcc-category-daily']
	},
	{
	id:       'rcc-curated-extended-0042-weekly',
	type:     'integer',
	meaning:    'Haben Sie sich in den letzten sieben Tagen so verhalten, dass es Sie oder Ihre Familie in Schwierigkeit hätte bringen können?',
	translations: {
				en: 'In the past seven days, did you behave in such a way today that it could have gotten you or your family into trouble?',
				de: 'Haben Sie sich in den letzten sieben Tagen so verhalten, dass es Sie oder Ihre Familie in Schwierigkeit hätte bringen können?'
			},
		options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                 de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',            de: 'ständig' } },
						],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-risky-behavior', 'rcc-category-weekly']
	},
	{
	id:       'rcc-curated-extended-0043-weekly',
	type:     'integer',
	meaning:    'Hatten Sie in den letzten sieben Tagen deutlich mehr Interesse an Sex?',
	translations: {
				en: 'Was your sex drive higher than usual in the past seven days?',
				de: 'Hatten Sie in den letzten sieben Tagen deutlich mehr Interesse an Sex?'
			},
		options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',              de: 'etwas mehr' } },
								{ value:2  , translations: { en: 'moderately',            de: 'deutlich mehr' } },
								{ value:3  , translations: { en: 'a lot',                 de: 'sehr viel mehr' } },
						],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-libido', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0044-daily',
		type:     'integer',
		meaning:    'Haben Sie heute mehr Geld als sonst ausgegeben?',
		translations: {
					en: 'Did you spend more money than usual today?',
					de: 'Haben Sie heute mehr Geld als sonst ausgegeben?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',              de: 'etwas mehr' } },
								{ value:2  , translations: { en: 'significantly',         de: 'deutlich mehr' } },
								{ value:3  , translations: { en: 'a lot',                 de: 'sehr viel mehr' } },
						],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-impulsivity', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0044-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen mehr Geld als sonst ausgegeben?',
		translations: {
					en: 'Did you spend more money than usual in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen mehr Geld als sonst ausgegeben?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',              de: 'etwas mehr' } },
								{ value:2  , translations: { en: 'significantly',         de: 'deutlich mehr' } },
								{ value:3  , translations: { en: 'a lot',                 de: 'sehr viel mehr' } },
						],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-impulsivity', 'rcc-category-weekly']
		},
	{
		id:       'rcc-curated-extended-0045-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute anderen Menschen überlegen gefühlt?',
		translations: {
					en: 'Did you feel superior to other people today?',
					de: 'Haben Sie sich heute anderen Menschen überlegen gefühlt?'
				},
	options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',              de: 'etwas überlegen' } },
								{ value:2  , translations: { en: 'moderately',            de: 'deutlich überlegen' } },
								{ value:3  , translations: { en: 'very',                  de: 'sehr stark überlegen' } },
								],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-self-worth', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0045-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen anderen Menschen überlegen gefühlt?',
		translations: {
					en: 'Did you feel superior to other people in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen anderen Menschen überlegen gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',              de: 'etwas überlegen' } },
								{ value:2  , translations: { en: 'moderately',            de: 'deutlich überlegen' } },
								{ value:3  , translations: { en: 'very',                  de: 'sehr stark überlegen' } },
						],
					tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-self-worth', 'rcc-category-weekly']
		},

	/*
		THIS QUESTION WAS SELECTED TO BE ONE OF THE CURATED DEFAULT QUESTIONS, THAT'S WHY IT IS COMMENTED OUT IN THIS SELECTION.

	{
		id:       'rcc-curated-extended-0046-daily',
		type:     'integer',
		meaning:    "Haben Sie heute den Eindruck, die Gedanken rasen Ihnen durch den Kopf?",
		translations: {
					en: "Did you have the impression that thoughts were racing through your head today?",
					de: "Haben Sie heute den Eindruck, die Gedanken rasen Ihnen durch den Kopf?"
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                 de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',            de: 'ständig' } },
								],
					tags:     ['rcc-category-bipolar-disorder',"icd-10-f31", "bipolar disorder", "rcc-symptom-area-cognitive-abilities", 'rcc-category-daily']
		}, */
		{
		id:       'rcc-curated-extended-0046-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen den Eindruck, die Gedanken rasen Ihnen durch den Kopf?',
		translations: {
					en: 'In the past seven days, did you have the impression that thoughts were racing through your head?',
					de: 'Hatten Sie in den letzten sieben Tagen den Eindruck, die Gedanken rasen Ihnen durch den Kopf?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                    de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                        de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                         de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                    de: 'ständig' } },
							],
				tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
		},

	{
		id:       'rcc-curated-extended-0047-daily',
		type:     'integer',
		meaning:    'Haben Sie heute viele Ideen?',
		translations: {
					en: 'Did you have a lot of ideas today?',
					de: 'Haben Sie heute viele Ideen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                    de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                      de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                    de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                         de: 'ständig' } },
							],
				tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0047-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen viele Ideen?',
		translations: {
					en: 'Did you have a lot of ideas in the past seven days?',
					de: 'Hatten Sie in den letzten sieben Tagen viele Ideen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                 de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                   de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                 de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                      de: 'ständig' } },
								],
		tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
		},

	{
		id:       'rcc-curated-extended-0048-daily',
		type:     'integer',
		meaning:    'Haben Sie heute Kleinigkeiten schnell abgelenkt?',
		translations: {
					en: 'Did you feel like any little interruption could distract you today?',
					de: 'Haben Sie heute Kleinigkeiten schnell abgelenkt?'
				},
		options:    [

									{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
									{ value:1  , translations: { en: 'rarely',                    de: 'manchmal' } },
									{ value:2  , translations: { en: 'often',                     de: 'häufig' } },
									{ value:3  , translations: { en: 'constantly',                de: 'ständig' } },
									],
		tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0048-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen Kleinigkeiten schnell abgelenkt?',
		translations: {
					en: 'In the past seven days, did you feel like any little interruption could distract you?',
					de: 'Haben Sie in den letzten sieben Tagen Kleinigkeiten schnell abgelenkt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                  de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                      de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                       de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                  de: 'ständig' } },
							],
		tags:     ['rcc-category-bipolar-disorder','icd-10-f31', 'bipolar disorder', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
		},

	{
		id:       'rcc-curated-extended-0049-daily',
		type:     'integer',
		meaning:    'Haben Sie heute den Eindruck gehabt, dass Dinge um Sie herum nur für Sie passieren?',
		translations: {
					en: 'Did you feel like things were happening just for you today?',
					de: 'Haben Sie heute den Eindruck gehabt, dass Dinge um Sie herum nur für Sie passieren?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                  de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                    de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                  de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                       de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0049-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen den Eindruck gehabt, dass Dinge um Sie herum nur für Sie passieren?',
		translations: {
					en: 'In the past seven days, did you feel like things around you were happening just for you?',
					de: 'Haben Sie in den letzten sieben Tagen den Eindruck gehabt, dass Dinge um Sie herum nur für Sie passieren?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                 de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                   de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                 de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                      de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},

	{
		id:       'rcc-curated-extended-0050-daily',
		type:     'integer',
		meaning:    'Haben Sie heute den Eindruck gehabt, dass es Probleme gibt, die nur Sie selbst lösen können?',
		translations: {
					en: 'Did you have the impression today that certain problems can only be solved by you?',
					de: 'Haben Sie heute den Eindruck gehabt, dass es Probleme gibt, die nur Sie selbst lösen können?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                 de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                   de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                 de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                      de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0050-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen den Eindruck gehabt, dass es Probleme gibt, die nur Sie selbst lösen können?',
		translations: {
					en: 'In the past seven days, did you have the impression that certain problems can only be solved by you?',
					de: 'Haben Sie in den letzten sieben Tagen den Eindruck gehabt, dass es Probleme gibt, die nur Sie selbst lösen können?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                  de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                     de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},

	/*
		THIS QUESTION WAS SELECTED TO BE ONE OF THE CURATED DEFAULT QUESTIONS, THAT'S WHY IT IS COMMENTED OUT IN THIS SELECTION.
	{
		id:       'rcc-curated-extended-0051-daily',
		type:     'integer',
		meaning:    "Haben Sie das Gefühl, dass etwas Seltsames vor sich geht?",
		translations: {
					en: "Did you feel like something strange was going on today?",
					de: "Haben Sie das Gefühl, dass etwas Seltsames vor sich geht?"
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                          de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis',"icd-10-f20", "psychosis", "rcc-symptom-area-delusion", 'rcc-category-weekly']
		},*/
		{
		id:       'rcc-curated-extended-0051-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, dass etwas Seltsames vor sich geht?',
		translations: {
					en: 'In the past seven days, did you feel that something strange was going on?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, dass etwas Seltsames vor sich geht?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},

	{
		id:       'rcc-curated-extended-0052-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, dass etwas verkehrt läuft, was Sie nicht erklären können?',
		translations: {
					en: 'Did you feel like something is going wrong that you can\'t explain today?',
					de: 'Hatten Sie heute das Gefühl, dass etwas verkehrt läuft, was Sie nicht erklären können?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                  de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                    de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                  de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                       de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0052-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, dass etwas verkehrt läuft, was Sie nicht erklären können?',
		translations: {
					en: 'In the past seven days, did you feel that something was going wrong that you cannot explain?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, dass etwas verkehrt läuft, was Sie nicht erklären können?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                 de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                   de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                 de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                      de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0053-daily',
		type:     'integer',
		meaning:    'Kam Ihnen Ihre Umgebung heute neu und stark verändert vor?',
		translations: {
					en: 'Did your surroundings appear new and different to you today?',
					de: 'Kam Ihnen Ihre Umgebung heute neu und stark verändert vor?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                          de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0053-weekly',
		type:     'integer',
		meaning:    'Kam Ihnen Ihre Umgebung in den letzten sieben Tagen neu und stark verändert vor?',
		translations: {
					en: 'Did your surroundings appear new and different to you in the past seven days?',
					de: 'Kam Ihnen Ihre Umgebung in den letzten sieben Tagen neu und stark verändert vor?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                 de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                   de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                 de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                      de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0054-daily',
		type:     'integer',
		meaning:    'Kam es Ihnen heute so vor, als ob andere Leute über Sie geredet hätten?',
		translations: {
					en: 'Did you feel like other people were talking about you today?',
					de: 'Kam es Ihnen heute so vor, als ob andere Leute über Sie geredet hätten?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                          de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0054-weekly',
		type:     'integer',
		meaning:    'Kam es Ihnen in den letzten sieben Tagen so vor, als ob andere Leute über Sie geredet hätten?',
		translations: {
					en: 'In the past seven days, did you feel like other people were talking about you today?',
					de: 'Kam es Ihnen in den letzten sieben Tagen so vor, als ob andere Leute über Sie geredet hätten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0055-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, dass Sie von anderen mit besonderer Aufmerksamkeit bedacht wurden?',
		translations: {
					en: 'Did you feel like other people were paying special attention to you today?',
					de: 'Hatten Sie heute das Gefühl, dass Sie von anderen mit besonderer Aufmerksamkeit bedacht wurden?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                          de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0055-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, dass Sie von anderen mit besonderer Aufmerksamkeit bedacht wurden?',
		translations: {
					en: 'In the past seven days, did you feel that other people were paying special attention to you?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, dass Sie von anderen mit besonderer Aufmerksamkeit bedacht wurden?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0056-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, dass Sie in irgendeiner Weise besonders wichtig waren?',
		translations: {
					en: 'Did you feel particularly important today?',
					de: 'Hatten Sie heute das Gefühl, dass Sie in irgendeiner Weise besonders wichtig waren?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
							{ value:3  , translations: { en: 'very',                           de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0056-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, dass Sie in irgendeiner Weise besonders wichtig waren?',
		translations: {
					en: 'In the past seven days, did you feel particularly important?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, dass Sie in irgendeiner Weise besonders wichtig waren?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'very',                            de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0057-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, dass Sie über besondere Fähigkeiten verfügten?',
		translations: {
					en: 'Did you feel like you have special abilities today?',
					de: 'Hatten Sie heute das Gefühl, dass Sie über besondere Fähigkeiten verfügten?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                         de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                          de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                     de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0057-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in das sieben Tagen das Gefühl, dass Sie über besondere Fähigkeiten verfügten?',
		translations: {
					en: 'In the past seven days, did you feel that you had special abilities?',
					de: 'Hatten Sie in das sieben Tagen das Gefühl, dass Sie über besondere Fähigkeiten verfügten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                      de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0058-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, eine besondere Schuld auf sich geladen zu haben?',
		translations: {
					en: 'Did you feel like you brought guilt upon yourself today?',
					de: 'Hatten Sie heute das Gefühl, eine besondere Schuld auf sich geladen zu haben?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
							{ value:3  , translations: { en: 'very',                           de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0058-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, eine besondere Schuld auf sich geladen zu haben?',
		translations: {
					en: 'In the past seven days, did you feel like you brought guilt upon yourself?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, eine besondere Schuld auf sich geladen zu haben?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'very',                            de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0059-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, dass jemand von außen Ihre Gedanken oder Handlungen kontrollierte?',
		translations: {
					en: 'Did you feel like someone else was controlling your thoughts or actions today?',
					de: 'Hatten Sie heute das Gefühl, dass jemand von außen Ihre Gedanken oder Handlungen kontrollierte?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                          de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0059-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, dass jemand von außen Ihre Gedanken oder Handlungen kontrollierte?',
		translations: {
					en: 'In the past seven days, did you feel like someone else was controlling your thoughts or actions?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, dass jemand von außen Ihre Gedanken oder Handlungen kontrollierte?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0060-daily',
		type:     'integer',
		meaning:    'Fühlten Sie sich heute in Ihrem Körper fremdgesteuert?',
		translations: {
					en: 'Did you feel like your body was being externally controlled today?',
					de: 'Fühlten Sie sich heute in Ihrem Körper fremdgesteuert?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                          de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0060-weekly',
		type:     'integer',
		meaning:    'Fühlten Sie sich in den letzten sieben Tagen in Ihrem Körper fremdgesteuert?',
		translations: {
					en: 'Did you feel like your body was being externally controlled in the past seven days?',
					de: 'Fühlten Sie sich in den letzten sieben Tagen in Ihrem Körper fremdgesteuert?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0061-daily',
		type:     'integer',
		meaning:    'Fühlten Sie sich heute in Ihrem Körper fremd?',
		translations: {
					en: 'Did you feel like a stranger in your own body today?',
					de: 'Fühlten Sie sich heute in Ihrem Körper fremd?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                       de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                         de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                       de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-ego-disorders', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0061-weekly',
		type:     'integer',
		meaning:    'Fühlen Sie sich in den letzten sieben Tagen in Ihrem Körper fremd?',
		translations: {
					en: 'Did you feel like a stranger in your own body in the past seven days?',
					de: 'Fühlen Sie sich in den letzten sieben Tagen in Ihrem Körper fremd?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-ego-disorders', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0062-daily',
		type:     'integer',
		meaning:    'Kam Ihre Umgebung Ihnen heute fremd und unwirklich vor?',
		translations: {
					en: 'Did your surroundings appear strange and unreal to you today?',
					de: 'Kam Ihre Umgebung Ihnen heute fremd und unwirklich vor?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',       de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',         de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',       de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-ego-disorders', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0062-weekly',
		type:     'integer',
		meaning:    'Kam Ihre Umgebung Ihnen in den sieben Tagen fremd und unwirklich vor?',
		translations: {
					en: 'Did your surroundings appear strange and unreal to you during the past seven days?',
					de: 'Kam Ihre Umgebung Ihnen in den sieben Tagen fremd und unwirklich vor?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',      de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-ego-disorders', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0063-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute von außen beeinflusst gefühlt?',
		translations: {
					en: 'Did you feel any outside influence today?',
					de: 'Haben Sie sich heute von außen beeinflusst gefühlt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',       de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',         de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',       de: 'häufig' } },
							{ value:3  , translations: { en: 'very',             de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0063-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen von außen beeinflusst gefühlt?',
		translations: {
					en: 'Did you feel any outside influence in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen von außen beeinflusst gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                               de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0064-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, keine Kontrolle über Ihre eigenen Ideen und Gedanken zu haben?',
		translations: {
					en: 'Did you feel like you had no control over your own ideas and thoughts today?',
					de: 'Hatten Sie heute das Gefühl, keine Kontrolle über Ihre eigenen Ideen und Gedanken zu haben?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'very',                                  de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0064-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, keine Kontrolle über Ihre eigenen Ideen und Gedanken zu haben?',
		translations: {
					en: 'In the past seven days, did you feel like you had no control over your own ideas and thoughts?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, keine Kontrolle über Ihre eigenen Ideen und Gedanken zu haben?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'very',                                 de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0065-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, als würden Gedanken in Ihren Kopf eingebracht?',
		translations: {
					en: 'Did you feel thoughts being put into your head today?',
					de: 'Hatten Sie heute das Gefühl, als würden Gedanken in Ihren Kopf eingebracht?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0065-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, als würden Gedanken in Ihren Kopf eingebracht?',
		translations: {
					en: 'In the past seven days, did you feel thoughts being put into your head?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, als würden Gedanken in Ihren Kopf eingebracht?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0066-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, dass Ihre Gedanken laut ausgesprochen werden, so dass andere Sie hören konnten?',
		translations: {
					en: 'Did you feel like your thoughts were being said out loud so that others could hear them today?',
					de: 'Hatten Sie heute das Gefühl, dass Ihre Gedanken laut ausgesprochen werden, so dass andere Sie hören konnten?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                                de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                 de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0066-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, dass Ihre Gedanken laut ausgesprochen werden, so dass andere Sie hören konnten?',
		translations: {
					en: 'In the past seven days, did you feel today that your thoughts were spoken out loud so that others could hear you?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, dass Ihre Gedanken laut ausgesprochen werden, so dass andere Sie hören konnten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                               de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0067-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, als würden Gedanken in Ihren Kopf entzogen?',
		translations: {
					en: 'Did you feel like thoughts were being brought into or withdrawn from your head today?',
					de: 'Hatten Sie heute das Gefühl, als würden Gedanken in Ihren Kopf entzogen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                                de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                 de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0067-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, als würden Gedanken in Ihren Kopf gebracht oder entzogen?',
		translations: {
					en: 'In the past seven days, did you feel like thoughts were being brought into or withdrawn from your head?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, als würden Gedanken in Ihren Kopf gebracht oder entzogen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                               de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-thought-initiation-propagation', 'rcc-category-weekly']
	},
	/*
		THIS QUESTION WAS SELECTED TO BE ONE OF THE CURATED DEFAULT QUESTIONS, THAT'S WHY IT IS COMMENTED OUT IN THIS SELECTION.
	{
		id:       'rcc-curated-extended-0068-daily',
		type:     'integer',
		meaning:    "Sind Sie heute misstrauisch?",
		translations: {
					en: "Did you feel suspicious today?",
					de: "Sind Sie heute misstrauisch?"
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'etwas' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'mäßig' } },
							{ value:3  , translations: { en: 'very',                                  de: 'stark' } },
							],
				tags:     ['rcc-category-psychosis',"icd-10-f20", "psychosis", "rcc-symptom-area-paranoid-thoughts", 'rcc-category-daily']
		},*/
		{
		id:       'rcc-curated-extended-0068-weekly',
		type:     'integer',
		meaning:    'Waren Sie in den letzten sieben Tagen misstrauisch?',
		translations: {
					en: 'Were you suspicious in the past seven days?',
					de: 'Waren Sie in den letzten sieben Tagen misstrauisch?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'etwas' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                 de: 'stark' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0069-daily',
		type:     'integer',
		meaning:    'Fühlen Sie sich heute verfolgt oder beobachtet?',
		translations: {
					en: 'Did you feel like you were being followed or watched today?',
					de: 'Fühlen Sie sich heute verfolgt oder beobachtet?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                                de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                 de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0069-weekly',
		type:     'integer',
		meaning:    'Fühlten Sie sich in den letzten sieben Tagen verfolgt oder beobachtet?',
		translations: {
					en: 'Did you feel like you were being followed or watched in the past seven days?',
					de: 'Fühlten Sie sich in den letzten sieben Tagen verfolgt oder beobachtet?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                               de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0070-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, es würde Ihnen jemand das Leben schwer machen?',
		translations: {
					en: 'Did you feel like someone was trying to make your life difficult today?',
					de: 'Hatten Sie heute das Gefühl, es würde Ihnen jemand das Leben schwer machen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0070-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, es würde Ihnen jemand das Leben schwer machen?',
		translations: {
					en: 'In the past seven days, did you feel like someone was trying to make your life difficult?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, es würde Ihnen jemand das Leben schwer machen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0071-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, dass es jemand auf Sie abgesehen hat?',
		translations: {
					en: 'Did you feel like someone was out to get you today?',
					de: 'Hatten Sie heute das Gefühl, dass es jemand auf Sie abgesehen hat?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0071-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, dass es jemand auf Sie abgesehen hat?',
		translations: {
					en: 'In the past seven days, did you feel that someone is out to get you?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, dass es jemand auf Sie abgesehen hat?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0072-daily',
		type:     'integer',
		meaning:    'Haben Sie heute Stimmen gehört, die andere nicht hören konnten?',
		translations: {
					en: 'Did you hear voices that other people couldn\'t hear today?',
					de: 'Haben Sie heute Stimmen gehört, die andere nicht hören konnten?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                                de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                 de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-perceptual-changes', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0072-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen Stimmen gehört, die andere nicht hören konnten?',
		translations: {
					en: 'In the past seven days, did you hear voices that others could not hear?',
					de: 'Haben Sie in den letzten sieben Tagen Stimmen gehört, die andere nicht hören konnten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                               de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-perceptual-changes', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0073-daily',
		type:     'integer',
		meaning:    'Haben Sie heute etwas gesehen, was andere nicht sehen konnten?',
		translations: {
					en: 'Did you see something that other people couldn\'t see today?',
					de: 'Haben Sie heute etwas gesehen, was andere nicht sehen konnten?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                                de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                 de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-perceptual-changes', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0073-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen etwas gesehen, was andere nicht sehen konnten?',
		translations: {
					en: 'Did you see something in the past seven days that others could not see?',
					de: 'Haben Sie in den letzten sieben Tagen etwas gesehen, was andere nicht sehen konnten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                               de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-perceptual-changes', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0074-daily',
		type:     'integer',
		meaning:    'Haben Sie heute Kribbeln oder Berührungen auf der Haut gespürt, was Sie sich nicht erklären können?',
		translations: {
					en: 'Did you feel tingling or something touching your skin that you can\'t explain today?',
					de: 'Haben Sie heute Kribbeln oder Berührungen auf der Haut gespürt, was Sie sich nicht erklären können?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                                de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                 de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-perceptual-changes', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0074-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen Kribbeln oder Berührungen auf der Haut gespürt, was Sie sich nicht erklären können?',
		translations: {
					en: 'In the past seven days, did you feel tingling or touching on your skin that you cannot explain?',
					de: 'Haben Sie in den letzten sieben Tagen Kribbeln oder Berührungen auf der Haut gespürt, was Sie sich nicht erklären können?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                               de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-perceptual-changes', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0075-daily',
		type:     'integer',
		meaning:    'Haben Sie heute das Gefühl, Ihre Gedanken laufen durcheinander?',
		translations: {
					en: 'Did you feel like your thoughts were muddled today?',
					de: 'Haben Sie heute das Gefühl, Ihre Gedanken laufen durcheinander?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0075-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, Ihre Gedanken laufen durcheinander?',
		translations: {
					en: 'In the past seven days, did you feel like your thoughts were muddled?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, Ihre Gedanken laufen durcheinander?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0076-daily',
		type:     'integer',
		meaning:    'Haben Sie das heute Gefühl, Ihre Gedanken nicht ordnen zu können?',
		translations: {
					en: 'Did you feel unable to organize your thoughts today?',
					de: 'Haben Sie das heute Gefühl, Ihre Gedanken nicht ordnen zu können?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                                de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                 de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0076-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen das Gefühl, Ihre Gedanken nicht ordnen zu können?',
		translations: {
					en: 'In the past seven days, did you feel like you cannot organize your thoughts?',
					de: 'Haben Sie in den letzten sieben Tagen das Gefühl, Ihre Gedanken nicht ordnen zu können?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                               de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0077-daily',
		type:     'integer',
		meaning:    'Haben Sie heute das Gefühl, zwischen Gedanken hin und her zu springen?',
		translations: {
					en: 'Did you feel like you were jumping back and forth between thoughts today?',
					de: 'Haben Sie heute das Gefühl, zwischen Gedanken hin und her zu springen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0077-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen das Gefühl, zwischen Gedanken hin und her zu springen?',
		translations: {
					en: 'In the past seven days, did you feel like you were jumping back and forth between thoughts?',
					de: 'Haben Sie in den letzten sieben Tagen das Gefühl, zwischen Gedanken hin und her zu springen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0078-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute Schwierigkeiten, sich zu konzentrieren?',
		translations: {
					en: 'Did you have trouble concentrating today?',
					de: 'Hatten Sie heute Schwierigkeiten, sich zu konzentrieren?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'mäßig' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'stark' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0078-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen Schwierigkeiten, sich zu konzentrieren?',
		translations: {
					en: 'Did you have trouble concentrating in the past seven days?',
					de: 'Hatten Sie in den letzten sieben Tagen Schwierigkeiten, sich zu konzentrieren?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'stark' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0079-daily',
		type:     'integer',
		meaning:    'Haben Sie das Gefühl, dass Sie Gedanken nicht zu Ende denken können?',
		translations: {
					en: 'Did you feel unable to finish your thoughts today?',
					de: 'Haben Sie das Gefühl, dass Sie Gedanken nicht zu Ende denken können?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'rarely',                                de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                 de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0079-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen das Gefühl, dass Sie Gedanken nicht zu Ende denken können?',
		translations: {
					en: 'In the past seven days, did you feel unable to finish your thoughts?',
					de: 'Haben Sie in den letzten sieben Tagen das Gefühl, dass Sie Gedanken nicht zu Ende denken können?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'rarely',                               de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0080-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute reizoffen gefühlt?',
		translations: {
					en: 'Did you feel like you were sensitive to stimuli today?',
					de: 'Haben Sie sich heute reizoffen gefühlt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0080-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen reizoffen gefühlt?',
		translations: {
					en: 'Did you feel like you were sensitive to stimuli in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen reizoffen gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0081-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute von Reizen schneller erschöpft gefühlt?',
		translations: {
					en: 'Did stimuli make you feel exhausted more quickly today?',
					de: 'Haben Sie sich heute von Reizen schneller erschöpft gefühlt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0081-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen von Reizen schneller erschöpft gefühlt?',
		translations: {
					en: 'Did stimuli make you feel exhausted more quickly in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen von Reizen schneller erschöpft gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0082-daily',
		type:     'integer',
		meaning:    'Haben Sie heute das Gefühl, Sie können sich anderen schwerer verständlich machen?',
		translations: {
					en: 'Did you have a harder time making your self understood today?',
					de: 'Haben Sie heute das Gefühl, Sie können sich anderen schwerer verständlich machen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'very',                                  de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0082-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen das Gefühl, Sie können sich anderen schwerer verständlich machen?',
		translations: {
					en: 'In the past seven days, did you have a harder time making your self understood?',
					de: 'Haben Sie in den letzten sieben Tagen das Gefühl, Sie können sich anderen schwerer verständlich machen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'very',                                 de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0083-daily',
		type:     'integer',
		meaning:    'Fühlen Sie sich heute unruhig?',
		translations: {
					en: 'Did you feel restless today?',
					de: 'Fühlen Sie sich heute unruhig?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'mäßig' } },
							{ value:3  , translations: { en: 'very',                                  de: 'stark' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0083-weekly',
		type:     'integer',
		meaning:    'Fühlten Sie sich in den letzten sieben Tagen unruhig?',
		translations: {
					en: 'Did you feel restless in the past seven days?',
					de: 'Fühlten Sie sich in den letzten sieben Tagen unruhig?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                 de: 'stark' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0084-daily',
		type:     'integer',
		meaning:    'Haben Sie heute vermehrt Handlungen begonnen und nicht zu Ende geführt?',
		translations: {
					en: 'Did you start more tasks without finishing them than usual today?',
					de: 'Haben Sie heute vermehrt Handlungen begonnen und nicht zu Ende geführt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat more',                         de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately more',                       de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot more',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0084-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen vermehrt Handlungen begonnen und nicht zu Ende geführt?',
		translations: {
					en: 'In the past seven days, did you start more tasks without finishing them than usual?',
					de: 'Haben Sie in den letzten sieben Tagen vermehrt Handlungen begonnen und nicht zu Ende geführt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately more',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot more',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0085-daily',
		type:     'integer',
		meaning:    'Haben Sie heute ungewöhnlich oft zwischen Handlungen gewechselt?',
		translations: {
					en: 'Did you switch between tasks more often than usual today?',
					de: 'Haben Sie heute ungewöhnlich oft zwischen Handlungen gewechselt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all more',                       de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat more',                         de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately more',                       de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot more',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0085-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen ungewöhnlich oft zwischen Handlungen gewechselt?',
		translations: {
					en: 'Did you switch between tasks more often than usual in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen ungewöhnlich oft zwischen Handlungen gewechselt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all more',                      de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',                        de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately more',                      de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot more',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0086-daily',
		type:     'integer',
		meaning:    'Sind Sie heute viel umher gelaufen und fühlen sich getrieben?',
		translations: {
					en: 'Did you walk around a lot and feel restless today?',
					de: 'Sind Sie heute viel umher gelaufen und fühlen sich getrieben?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'sometimes',                             de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                 de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                            de: 'ständig' } },
							],
				tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0086-weekly',
		type:     'integer',
		meaning:    'Sind Sie in den letzten sieben Tagen viel umher gelaufen und fühlen sich getrieben?',
		translations: {
					en: 'Did you walk around a lot and feel restless in the past seven days?',
					de: 'Sind Sie in den letzten sieben Tagen viel umher gelaufen und fühlen sich getrieben?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'sometimes',                            de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                           de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis','icd-10-f20', 'psychosis', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0087-daily',
		type:     'integer',
		meaning:    'Haben Sie letzte Nacht schlecht einschlafen können?',
		translations: {
					en: 'Did you have trouble falling asleep past night?',
					de: 'Haben Sie letzte Nacht schlecht einschlafen können?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'etwas Einschlafprobleme (weniger als 30 Minuten)' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'deutlich Einschlafprobleme (30 Minuten bis 1h)' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'starke Einschlafstörungen (über 1h)' } },
							],
				tags:     ['rcc-category-vegetative-symptoms', 'vegetative symptoms', 'rcc-symptom-area-sleep-disorders', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0087-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Nächten schlecht einschlafen können?',
		translations: {
					en: 'Did you have trouble falling asleep in the past seven nights?',
					de: 'Haben Sie in den letzten sieben Nächten schlecht einschlafen können?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'etwas Einschlafprobleme (weniger als 30 Minuten)' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'deutlich Einschlafprobleme (30 Minuten bis 1h)' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'starke Einschlafstörungen (über 1h)' } },
								],
					tags:     ['rcc-category-vegetative-symptoms', 'vegetative symptoms', 'rcc-symptom-area-sleep-disorders', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0088-daily',
		type:     'integer',
		meaning:    'Haben Sie letzte Nacht schlecht durchgeschlafen?',
		translations: {
					en: 'Did you have trouble staying asleep past night?',
					de: 'Haben Sie letzte Nacht schlecht durchgeschlafen?'
				},
		options:    [

							{ value:0  , translations: { en: 'no',                                                                                         de: 'nein' } },
							{ value:1  , translations: { en: 'some problems (woke up once or twice, quickly fell back asleep)',                            de: 'etwas Durchschlafprobleme (ein bis zweimal aufgewacht, schnell wieder eingeschlafen)' } },
							{ value:2  , translations: { en: 'significant problems (woke up more than twice, lay awake',                                   de: 'deutliche Durchschlafprobleme (mehr als zweimal aufgewacht, wachgelegen)' } },
							{ value:3  , translations: { en: 'very severe problems (could not fall back asleep)',                                          de: 'sehr starke Durchschlafprobleme (konnte nicht wieder einschlafen)' } },
							],
				tags:     ['rcc-category-vegetative-symptoms', 'vegetative symptoms', 'rcc-symptom-area-sleep-disorders', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0088-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Nächten schlecht durchgeschlafen?',
		translations: {
					en: 'Did you have trouble staying asleep in the past seven nights?',
					de: 'Haben Sie in den letzten sieben Nächten schlecht durchgeschlafen?'
				},
		options:    [

								{ value:0  , translations: { en: 'no',                                                                                        de: 'nein' } },
								{ value:1  , translations: { en: 'some problems (woke up once or twice, quickly fell back asleep)',                           de: 'etwas Durchschlafprobleme (ein bis zweimal aufgewacht, schnell wieder eingeschlafen)' } },
								{ value:2  , translations: { en: 'significant problems (woke up more than twice, lay awake',                                  de: 'deutliche Durchschlafprobleme (mehr als zweimal aufgewacht, wachgelegen)' } },
								{ value:3  , translations: { en: 'very severe problems (could not fall back asleep)',                                         de: 'sehr starke Durchschlafprobleme (konnte nicht wieder einschlafen)' } },
								],
					tags:     ['rcc-category-vegetative-symptoms', 'vegetative symptoms', 'rcc-symptom-area-sleep-disorders', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0089-daily',
		type:     'integer',
		meaning:    'Haben Sie heute an Tagesmüdigkeit gelitten?',
		translations: {
					en: 'Did you suffer from daytime sleepiness today?',
					de: 'Haben Sie heute an Tagesmüdigkeit gelitten?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                              de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                            de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                 de: 'ständig' } },
							],
				tags:     ['rcc-category-vegetative-symptoms', 'vegetative symptoms', 'rcc-symptom-area-sleep-disorders', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0089-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen an Tagesmüdigkeit gelitten?',
		translations: {
					en: 'Did you suffer from daytime sleepiness in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen an Tagesmüdigkeit gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                           de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                             de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                           de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                de: 'ständig' } },
								],
					tags:     ['rcc-category-vegetative-symptoms', 'vegetative symptoms', 'rcc-symptom-area-sleep-disorders', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0090-daily',
		type:     'integer',
		meaning:    'Haben Sie heute das Gefühl, dass Ihr Herz unregelmäßig schlägt oder stolpert?',
		translations: {
					en: 'Did you feel like your heart was beating irregularly or stumbling today?',
					de: 'Haben Sie heute das Gefühl, dass Ihr Herz unregelmäßig schlägt oder stolpert?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                              de: 'ständig' } },
							],
				tags:     ['rcc-category-anxiety', 'anxiety', 'rcc-symptom-area-irregular-heartbeat', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0090-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen das Gefühl, dass Ihr Herz unregelmäßig schlägt oder stolpert?',
		translations: {
					en: 'In the past seven days, did you feel like your heart was beating irregularly or stumbling?',
					de: 'Haben Sie in den letzten sieben Tagen das Gefühl, dass Ihr Herz unregelmäßig schlägt oder stolpert?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-anxiety', 'anxiety', 'rcc-symptom-area-irregular-heartbeat', 'rcc-category-weekly']
	},

		// TODO
	// 0091 - 0094

	{
		id:       'rcc-curated-extended-0095-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute vermehrt müde gefühlt?',
		translations: {
					en: 'Did you feel more tired than usual today?',
					de: 'Haben Sie sich heute vermehrt müde gefühlt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all more',                                    de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat more',                                      de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately more',                                    de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot more',                                         de: 'ständig' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-sedation', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0095-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen vermehrt müde gefühlt?',
		translations: {
					en: 'Did you feel more tired than usual in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen vermehrt müde gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all more',                                   de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',                                     de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately more',                                   de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot more',                                        de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-sedation', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0096-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute zugedröhnt oder wie ein Zombie gefühlt?',
		translations: {
					en: 'Did you feel sedated or like a zombie today?',
					de: 'Haben Sie sich heute zugedröhnt oder wie ein Zombie gefühlt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'sometimes',                                          de: 'manchmal' } },
							{ value:2  , translations: { en: 'often',                                              de: 'häufig' } },
							{ value:3  , translations: { en: 'constantly',                                         de: 'ständig' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-sedation', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0096-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen zugedröhnt oder wie ein Zombie gefühlt?',
		translations: {
					en: 'Did you feel drugged or like a zombie in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen zugedröhnt oder wie ein Zombie gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'sometimes',                                         de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                             de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                                        de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-sedation', 'rcc-category-weekly']
	},

		{
		id:       'rcc-curated-extended-0097-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen vermehrt Appetit?',
		translations: {
					en: 'Did you notice an increased appetite in the past seven days?',
					de: 'Hatten Sie in den letzten sieben Tagen vermehrt Appetit?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',                                     de: 'etwas mehr' } },
								{ value:2  , translations: { en: 'moderately more',                                   de: 'deutlich mehr' } },
								{ value:3  , translations: { en: 'a lot more',                                        de: 'sehr viel mehr' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-slowing-of-movement-akinesia', 'rcc-category-weekly']
	},

		{
		id:       'rcc-curated-extended-0098-weekly',
		type:     'integer',
		meaning:    'Haben Sie an in der letzten Woche an Gewicht zugenommen?',
		translations: {
					en: 'Have you gained any weight in the past week?',
					de: 'Haben Sie an in der letzten Woche an Gewicht zugenommen?'
				},
		options:    [

								{ value:0  , translations: { en: 'no',                                                de: 'nein' } },
								{ value:1  , translations: { en: '<0.5kg',                                            de: 'etwas zugenommen (<0,5kg)' } },
								{ value:2  , translations: { en: '0.5-1kg',                                           de: 'deutlich zugenommen (0,5-1kg)' } },
								{ value:3  , translations: { en: '>1kg',                                              de: 'sehr viel zugenommen (>1kg)' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-weight-gain', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0099-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute schwindelig oder unwohl beim Aufstehen gefühlt?',
		translations: {
					en: 'Did you feel dizzy or unwell after standing up today?',
					de: 'Haben Sie sich heute schwindelig oder unwohl beim Aufstehen gefühlt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig' } },
							{ value:3  , translations: { en: 'very',                                               de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-dizziness', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0099-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen schwindelig oder unwohl beim Aufstehen gefühlt?',
		translations: {
					en: 'Did you feel dizzy or unwell after standing up in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen schwindelig oder unwohl beim Aufstehen gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-dizziness', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0100-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute Herzklopfen?',
		translations: {
					en: 'Did you have palpitations today?',
					de: 'Hatten Sie heute Herzklopfen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig' } },
							{ value:3  , translations: { en: 'very',                                               de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-heart-palpitations', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0100-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen Herzklopfen?',
		translations: {
					en: 'Did you have palpitations in the past seven days?',
					de: 'Hatten Sie in den letzten sieben Tagen Herzklopfen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-heart-palpitations', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0101-daily',
		type:     'integer',
		meaning:    'Haben sich Ihre Muskeln heute steif angefühlt?',
		translations: {
					en: 'Did your muscles feel stiff today?',
					de: 'Haben sich Ihre Muskeln heute steif angefühlt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig' } },
							{ value:3  , translations: { en: 'very',                                               de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-muscle-stiffness', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0101-weekly',
		type:     'integer',
		meaning:    'Haben sich Ihre Muskeln in den letzten sieben Tagen steif angefühlt?',
		translations: {
					en: 'Did your muscles feel stiff in the past seven days?',
					de: 'Haben sich Ihre Muskeln in den letzten sieben Tagen steif angefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-muscle-stiffness', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0102-daily',
		type:     'integer',
		meaning:    'Haben Sie heute unwillkürliche Bewegungen Ihrer Muskeln bemerkt?',
		translations: {
					en: 'Did you notice involuntary movements of your muscles today?',
					de: 'Haben Sie heute unwillkürliche Bewegungen Ihrer Muskeln bemerkt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                              de: 'ständig' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-movement-disorder', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0102-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unwillkürliche Bewegungen Ihrer Muskeln bemerkt?',
		translations: {
					en: 'Did you notice involuntary movements of your muscles in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unwillkürliche Bewegungen Ihrer Muskeln bemerkt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-movement-disorder', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0103-daily',
		type:     'integer',
		meaning:    'Haben Ihre Hände heute gezittert?',
		translations: {
					en: 'Did you feel tremors in your hands today?',
					de: 'Haben Ihre Hände heute gezittert?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig' } },
							{ value:3  , translations: { en: 'a lot',                                              de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-movement-disorder', 'antidepressant reduction', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0103-weekly',
		type:     'integer',
		meaning:    'Haben Ihre Hände in den letzten sieben Tagen gezittert?',
		translations: {
					en: 'Did you feel tremors in your hands in the past seven days?',
					de: 'Haben Ihre Hände in den letzten sieben Tagen gezittert?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-movement-disorder', 'antidepressant reduction', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0104-daily',
		type:     'integer',
		meaning:    'Waren Ihre Beine heute unruhig oder haben Sie nicht ruhig sitzen können?',
		translations: {
					en: 'Were your legs restless or were you unable to sit still today?',
					de: 'Waren Ihre Beine heute unruhig oder haben Sie nicht ruhig sitzen können?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering (nur Gefühl der Unruhe)' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig (stehe häufiger auf)' } },
							{ value:3  , translations: { en: 'a lot',                                              de: 'stark (kann nur kurzzeitig sitzen)' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-leg-restlessness', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0104-weekly',
		type:     'integer',
		meaning:    'Waren Ihre Beine in den letzten sieben Tagen unruhig oder haben Sie nicht ruhig sitzen können?',
		translations: {
					en: 'Were your legs restless or were you unable to sit still in the past seven days?',
					de: 'Waren Ihre Beine in den letzten sieben Tagen unruhig oder haben Sie nicht ruhig sitzen können?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering (nur Gefühl der Unruhe)' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig (stehe häufiger auf)' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark (kann nur kurzzeitig sitzen)' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-leg-restlessness', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0105-daily',
		type:     'integer',
		meaning:    'Haben Sie heute vermehrten Speichelfluss bemerkt?',
		translations: {
					en: 'Did you notice increased salivation today?',
					de: 'Haben Sie heute vermehrten Speichelfluss bemerkt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig' } },
							{ value:3  , translations: { en: 'a lot',                                              de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-salivation', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0105-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen vermehrten Speichelfluss bemerkt?',
		translations: {
					en: 'Did you notice increased salivation in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen vermehrten Speichelfluss bemerkt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-salivation', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0106-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute langsamer als sonst bewegt, z. B. beim Laufen?',
		translations: {
					en: 'Did you move slower than usual today, for example while walking?',
					de: 'Haben Sie sich heute langsamer als sonst bewegt, z. B. beim Laufen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat slower',                                    de: 'ein wenig langsamer' } },
							{ value:2  , translations: { en: 'moderately slower',                                  de: 'deutlich langsamer' } },
							{ value:3  , translations: { en: 'a lot slower',                                       de: 'sehr viel langsamer' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-movement-disorder', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0106-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen langsamer als sonst bewegt, z. B. beim Laufen?',
		translations: {
					en: 'Did you move slower than usual in the past seven days, for example while walking?',
					de: 'Haben Sie sich in den letzten sieben Tagen langsamer als sonst bewegt, z. B. beim Laufen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat slower',                                   de: 'ein wenig langsamer' } },
								{ value:2  , translations: { en: 'moderately slower',                                 de: 'deutlich langsamer' } },
								{ value:3  , translations: { en: 'a lot slower',                                      de: 'sehr viel langsamer' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-movement-disorder', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0107-daily',
		type:     'integer',
		meaning:    'Hatten Sie heute das Gefühl, dass Sie verschwommen sehen?',
		translations: {
					en: 'Did you feel like your vision was blurred today?',
					de: 'Hatten Sie heute das Gefühl, dass Sie verschwommen sehen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig' } },
							{ value:3  , translations: { en: 'very',                                               de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-impaired-vision', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0107-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, verschwommen zu sehen?',
		translations: {
					en: 'Did you feel like your vision was blurred in the past seven days?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, verschwommen zu sehen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-impaired-vision', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0108-daily',
		type:     'integer',
		meaning:    'War Ihr Mund heute vermehrt trocken?',
		translations: {
					en: 'Has your mouth been more dry than usual today?',
					de: 'War Ihr Mund heute vermehrt trocken?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig' } },
							{ value:3  , translations: { en: 'a lot',                                              de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0108-weekly',
		type:     'integer',
		meaning:    'War Ihr Mund in den letzten sieben Tagen vermehrt trocken?',
		translations: {
					en: 'Has your mouth been more dry than usual in the past seven days?',
					de: 'War Ihr Mund in den letzten sieben Tagen vermehrt trocken?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0109-daily',
		type:     'integer',
		meaning:    'Haben Sie heute Schwierigkeiten, Wasser zu lassen bzw. Ihre Blase zu entleeren?',
		translations: {
					en: 'Did you have difficulties urinating or emptying your bladder today?',
					de: 'Haben Sie heute Schwierigkeiten, Wasser zu lassen bzw. Ihre Blase zu entleeren?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'sometimes',                                          de: 'gering' } },
							{ value:2  , translations: { en: 'often',                                              de: 'mäßig' } },
							{ value:3  , translations: { en: 'constantly',                                         de: 'stark (nur mit medizinischer Hilfe)' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0109-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in der letzten Woche Schwierigkeiten, Wasser zu lassen bzw. Ihre Blase zu entleeren?',
		translations: {
					en: 'Did you have difficulties urinating or emptying your bladder in the past week?',
					de: 'Hatten Sie in der letzten Woche Schwierigkeiten, Wasser zu lassen bzw. Ihre Blase zu entleeren?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'sometimes',                                         de: 'gering' } },
								{ value:2  , translations: { en: 'often',                                             de: 'mäßig' } },
								{ value:3  , translations: { en: 'constantly',                                        de: 'stark (nur mit medizinischer Hilfe)' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0110-daily',
		type:     'integer',
		meaning:    'Ist Ihnen heute übel?',
		translations: {
					en: 'Did you feel nauseous today?',
					de: 'Ist Ihnen heute übel?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig' } },
							{ value:3  , translations: { en: 'very',                                               de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0110-weekly',
		type:     'integer',
		meaning:    'War Ihnen in der letzten Woche übel?',
		translations: {
					en: 'Did you feel nauseous in the past week?',
					de: 'War Ihnen in der letzten Woche übel?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0111-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute übergeben müssen?',
		translations: {
					en: 'Did you need to vomit today?',
					de: 'Haben Sie sich heute übergeben müssen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot',                                              de: 'ständig' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0111-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen übergeben müssen?',
		translations: {
					en: 'Did you need to vomit in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen übergeben müssen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0112-daily',
		type:     'integer',
		meaning:    'Haben Sie heute Verstopfung beim Stuhlgang bemerkt?',
		translations: {
					en: 'Did you feel constipated today?',
					de: 'Haben Sie heute Verstopfung beim Stuhlgang bemerkt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'sometimes',                                          de: 'gering' } },
							{ value:2  , translations: { en: 'often',                                              de: 'mäßig' } },
							{ value:3  , translations: { en: 'constantly',                                         de: 'stark (Ich habe Abführmittel eingenommen)' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0112-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche Verstopfung beim Stuhlgang bemerkt?',
		translations: {
					en: 'Did you feel constipated in the past week?',
					de: 'Haben Sie in der letzten Woche Verstopfung beim Stuhlgang bemerkt?'
				},
		options:    [

						{ value:0  , translations: { en: 'not at all',                                          de: 'nein' } },
						{ value:1  , translations: { en: 'sometimes',                                           de: 'gering' } },
						{ value:2  , translations: { en: 'often',                                               de: 'mäßig' } },
						{ value:3  , translations: { en: 'constantly',                                          de: 'stark (Ich habe Abführmittel eingenommen)' } },
					],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0113-daily',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Nacht unwillkürlich Urin gelassen?',
		translations: {
					en: 'Did you pass urine involuntarily past night?',
					de: 'Haben Sie in der letzten Nacht unwillkürlich Urin gelassen?'
				},
		options:    [

						{ value:0  , translations: { en: 'not at all',                                          de: 'nein' } },
						{ value:1  , translations: { en: 'sometimes',                                           de: 'selten' } },
						{ value:2  , translations: { en: 'often',                                               de: 'manchmal' } },
						{ value:3  , translations: { en: 'constantly',                                          de: 'häufig' } },
					],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0113-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche unwillkürlich Nachts Urin gelassen?',
		translations: {
					en: 'Did you pass urine involuntarily during nighttime in the past week?',
					de: 'Haben Sie in der letzten Woche unwillkürlich Nachts Urin gelassen?'
				},
		options:    [

						{ value:0  , translations: { en: 'not at all',                                          de: 'nein' } },
						{ value:1  , translations: { en: 'sometimes',                                           de: 'selten' } },
						{ value:2  , translations: { en: 'often',                                               de: 'manchmal' } },
						{ value:3  , translations: { en: 'constantly',                                          de: 'häufig' } },
					],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0114-daily',
		type:     'integer',
		meaning:    'Haben Sie heute vermehrt Durst?',
		translations: {
					en: 'Did you feel more thirsty than usual today?',
					de: 'Haben Sie heute vermehrt Durst?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat more',                                      de: 'gering' } },
							{ value:2  , translations: { en: 'moderately more',                                    de: 'mäßig' } },
							{ value:3  , translations: { en: 'a lot more',                                         de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0114-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche vermehrt Durst gehabt?',
		translations: {
					en: 'Did you feel more thirsty than usual in the past week?',
					de: 'Haben Sie in der letzten Woche vermehrt Durst gehabt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',                                     de: 'gering' } },
								{ value:2  , translations: { en: 'moderately more',                                   de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot more',                                        de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0115-daily',
		type:     'integer',
		meaning:    'Haben Sie heute häufiger Wasser gelassen?',
		translations: {
					en: 'Did you urinate more often than usual today?',
					de: 'Haben Sie heute häufiger Wasser gelassen?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat more',                                      de: 'manchmal' } },
							{ value:2  , translations: { en: 'moderately more',                                    de: 'häufig' } },
							{ value:3  , translations: { en: 'a lot more',                                         de: 'ständig' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0115-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche häufiger Wasser gelassen?',
		translations: {
					en: 'Did you urinate more often than usual in the past week?',
					de: 'Haben Sie in der letzten Woche häufiger Wasser gelassen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',                                     de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately more',                                   de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot more',                                        de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0116-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche Schwellungen in der Brust oder der Brustwarze bemerkt?',
		translations: {
					en: 'Have you noticed swelling in your breasts or nipples in the past week?',
					de: 'Haben Sie in der letzten Woche Schwellungen in der Brust oder der Brustwarze bemerkt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'leichtes Ziehen' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'starkes Ziehen/sichtbare Schwellung' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'schmerzhafte Schwellung' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-hyperprolactinaemia', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0117-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche bemerkt, dass milchige Flüssigkeit aus Ihren Brustwarzen austritt?',
		translations: {
					en: 'Have you noticed milky fluid leaking from your nipples in the past week?',
					de: 'Haben Sie in der letzten Woche bemerkt, dass milchige Flüssigkeit aus Ihren Brustwarzen austritt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'minimal',                                           de: 'minimal' } },
								{ value:2  , translations: { en: 'noticeable',                                        de: 'deutlich' } },
								{ value:3  , translations: { en: 'I had to change clothes',                           de: 'ich musste Kleidungsstücke wechseln' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-hyperprolactinaemia', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0118-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche weniger sexuelles Verlangen verspürt?',
		translations: {
					en: 'Was your sex drive lower in the past week?',
					de: 'Haben Sie in der letzten Woche weniger sexuelles Verlangen verspürt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat lower',                                    de: 'etwas weniger' } },
								{ value:2  , translations: { en: 'moderately lower',                                  de: 'deutlich weniger' } },
								{ value:3  , translations: { en: 'a lot lower',                                       de: 'sehr viel weniger' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-sexual-dysfunction', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0119-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche Schwierigkeiten gehabt, eine Erektion zu bekommen?',
		translations: {
					en: 'Did you have trouble getting an erection in the past week?',
					de: 'Haben Sie in der letzten Woche Schwierigkeiten gehabt, eine Erektion zu bekommen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-sexual-dysfunction', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0120-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche Schwierigkeiten gehabt, feucht zu werden?',
		translations: {
					en: 'Did you have difficulties getting wet in the past week?',
					de: 'Haben Sie in der letzten Woche Schwierigkeiten gehabt, feucht zu werden?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-sexual-dysfunction', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0121-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in der letzten Woche vermehrt Schwierigkeiten, einen Orgasmus zu bekommen?',
		translations: {
					en: 'Did you have more difficulties having an orgasm in the past week?',
					de: 'Hatten Sie in der letzten Woche vermehrt Schwierigkeiten, einen Orgasmus zu bekommen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',                                     de: 'etwas schwieriger' } },
								{ value:2  , translations: { en: 'moderately more',                                   de: 'deutlich schwieriger' } },
								{ value:3  , translations: { en: 'a lot more',                                        de: 'kein Orgasmus mehr bekommen' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-symptom-area-sexual-dysfunction', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0122-weekly',
		type:     'integer',
		meaning:    'Haben Sie in der letzten Woche das Interesse an Intimität verloren?',
		translations: {
					en: 'Did you lose interest in intimacy in the past week?',
					de: 'Haben Sie in der letzten Woche das Interesse an Intimität verloren?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-category-weekly']
	},
	{
		id:       'rcc-curated-extended-0123-daily',
		type:     'integer',
		meaning:    'Haben Sie sich heute unbeteiligt gefühlt oder Dinge als weniger bedeutsam für sich erlebt?',
		translations: {
					en: 'Have you felt uninvolved or like things were less meaningful to you today?',
					de: 'Haben Sie sich heute unbeteiligt gefühlt oder Dinge als weniger bedeutsam für sich erlebt?'
				},
		options:    [

							{ value:0  , translations: { en: 'not at all',                                         de: 'nein' } },
							{ value:1  , translations: { en: 'somewhat',                                           de: 'gering' } },
							{ value:2  , translations: { en: 'moderately',                                         de: 'mäßig' } },
							{ value:3  , translations: { en: 'very',                                               de: 'stark' } },
							],
				tags:     ['rcc-category-use', 'use', 'rcc-category-daily']
		},
		{
		id:       'rcc-curated-extended-0123-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in der letzten Woche unbeteiligt gefühlt oder Dinge als weniger bedeutsam für sich erlebt?',
		translations: {
					en: 'In the past week, have you felt uninvolved or like things were less meaningful to you?',
					de: 'Haben Sie sich in der letzten Woche unbeteiligt gefühlt oder Dinge als weniger bedeutsam für sich erlebt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'use', 'rcc-category-weekly']
	},
	// TODO
	//	{
	// 	id:       'rcc-curated-extended-0124-weekly',
	// 	type:     'integer',
	// 	meaning:    "Haben Sie in den letzten sieben Tagen folgende Symptome verspürt?",
	// 	translations: {
	// 				en: "Did you feel the following symptoms in the past seven days?",
	// 		     	de: "Haben Sie in den letzten sieben Tagen folgende Symptome verspürt?"
	// 			},
	// 	options:    [
	//
	// 							{ value:0  , translations: { en: 'not at all',               de 'nein' } },
	// 							{ value:1  , translations: { en: 'somewhat',                                       de: 'manchmal' } },
	// 							{ value:2  , translations: { en: 'often',                                          de: 'häufig' } },
	// 							{ value:3  , translations: { en: 'constantly',                                     de: 'ständig' } },
	// 							],
	// 				tags:     ['rcc-category-use']
	// },
		{
		id:       'rcc-curated-extended-0125-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen vermehrt nervös oder ängstlich gefühlt?',
		translations: {
					en: 'In the past seven days, did you feel more nervous or anxious?',
					de: 'Haben Sie sich in den letzten sieben Tagen vermehrt nervös oder ängstlich gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0126-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen vermehrt euphorisch oder hochgestimmt gefühlt?',
		translations: {
					en: 'In the past seven days, was your mood elevated or did you feel \'high\' compared to usual?',
					de: 'Haben Sie sich in den letzten sieben Tagen vermehrt euphorisch oder hochgestimmt gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0127-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen vermehrt reizbar gefühlt?',
		translations: {
					en: 'In the past seven days, did you feel more irritable than usual?',
					de: 'Haben Sie sich in den letzten sieben Tagen vermehrt reizbar gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat more',                                     de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately more',                                   de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot more',                                        de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0128-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen plötzliche Stimmungseinbrüche bemerkt?',
		translations: {
					en: 'In the past seven days, did you notice sudden worsenings in mood?',
					de: 'Haben Sie in den letzten sieben Tagen plötzliche Stimmungseinbrüche bemerkt?'
				},
		options:    [

								{ value:0  , translations: { en: 'no',                                                de: 'nein' } },
								{ value:1  , translations: { en: 'sometimes',                                         de: 'einmalig' } },
								{ value:2  , translations: { en: 'often',                                             de: 'mehrmals' } },
								{ value:3  , translations: { en: 'constantly',                                        de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0129-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen plötzliche Wutausbrüche gehabt?',
		translations: {
					en: 'In the past seven days, did you have sudden outbursts of anger?',
					de: 'Haben Sie in den letzten sieben Tagen plötzliche Wutausbrüche gehabt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'einmalig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mehrmals' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0130-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen Panik oder Angstattacken erlitten?',
		translations: {
					en: 'In the past seven days, did you have panic or anxiety attacks?',
					de: 'Haben Sie in den letzten sieben Tagen Panik oder Angstattacken erlitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'einmalig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mehrmals' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-sleep', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0131-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen Anfälle von Weinen erlitten?',
		translations: {
					en: 'In the past seven days, did you suffer from bouts of crying or tearfulness?',
					de: 'Haben Sie in den letzten sieben Tagen Anfälle von Weinen erlitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'einmalig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mehrmals' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0132-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen vermehrt unruhig gefühlt?',
		translations: {
					en: 'Did you feel more agitated in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen vermehrt unruhig gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-inner-restlessness', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0133-weekly',
		type:     'integer',
		meaning:    'Haben Sie sich in den letzten sieben Tagen unwirklich oder fremd gefühlt?',
		translations: {
					en: 'Did you feel unreal or strange in the past seven days?',
					de: 'Haben Sie sich in den letzten sieben Tagen unwirklich oder fremd gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'very',                                              de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-depersonalisation', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0134-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Verwirrung oder Konzentrationsstörungen gelitten?',
		translations: {
					en: 'Did you suffer from confusion or difficulty concentrating in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Verwirrung oder Konzentrationsstörungen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'no',                                                de: 'nein' } },
								{ value:1  , translations: { en: 'sometimes',                                         de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                             de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                                        de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-concentration-disorders', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0135-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Vergesslichkeit oder Gedächtnisstörungen gelitten?',
		translations: {
					en: 'Did you suffer from forgetfulness or memory problems in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Vergesslichkeit oder Gedächtnisstörungen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'no',                                                de: 'nein' } },
								{ value:1  , translations: { en: 'sometimes',                                         de: 'manchmal' } },
								{ value:2  , translations: { en: 'often',                                             de: 'häufig' } },
								{ value:3  , translations: { en: 'constantly',                                        de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-memory-disorders', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0136-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen vermehrt unter Stimmungsschwankungen gelitten?',
		translations: {
					en: 'Did you suffer from increased mood swings in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen vermehrt unter Stimmungsschwankungen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-mood', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0137-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen vermehrt unter Schlafstörungen oder Schlaflosigkeit gelitten?',
		translations: {
					en: 'Did you suffer from increased sleep disturbances or insomnia in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen vermehrt unter Schlafstörungen oder Schlaflosigkeit gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-sleep', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0138-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen intensiver geträumt oder vermehrt unter Alpträumen gelitten?',
		translations: {
					en: 'Did you suffer from more intense dreaming or increased nightmares in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen intensiver geträumt oder vermehrt unter Alpträumen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-sleep', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0139-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen vermehrt geschwitzt?',
		translations: {
					en: 'Were you sweating more in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen vermehrt geschwitzt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig (musste Kleidung wechseln)' } },
								],
					tags:     ['rcc-category-use', 'antidepressant reduction', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0140-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen vermehrt gezittert?',
		translations: {
					en: 'Did you shiver more in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen vermehrt gezittert?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-movement-disorder', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0141-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Muskelverspannungen oder Steifheit gelitten?',
		translations: {
					en: 'Did you suffer from muscle tension or stiffness in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Muskelverspannungen oder Steifheit gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'antidepressant reduction', 'use', 'rcc-symptom-area-muscle-stiffness', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0142-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Muskelschmerzen gelitten?',
		translations: {
					en: 'Did you suffer from muscle pain in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Muskelschmerzen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-muscle-pain', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0143-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Unruhe in den Beinen gelitten?',
		translations: {
					en: 'Did you suffer from restlessness in your legs in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Unruhe in den Beinen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'antidepressant reduction', 'use', 'rcc-symptom-area-movement-restlessness', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0144-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Muskelkrämpfen oder Zuckungen gelitten?',
		translations: {
					en: 'Did you suffer from muscle cramps or twitching in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Muskelkrämpfen oder Zuckungen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'antidepressant reduction', 'use', 'rcc-symptom-area-movement-disorder', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0145-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen vermehrt erschöpft oder müde gefühlt?',
		translations: {
					en: 'Did you feel increased fatigue or tiredness in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen vermehrt erschöpft oder müde gefühlt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-drive', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0146-weekly',
		type:     'integer',
		meaning:    'Hat sich in den letzten sieben Tagen Ihr Gang unsicher angefühlt oder haben Sie an Schwierigkeiten gelitten, Ihre Bewegungen zu koordinieren?',
		translations: {
					en: 'In the past seven days, did your gait feel unsteady or have you suffered from difficulties coordinating your movements?',
					de: 'Hat sich in den letzten sieben Tagen Ihr Gang unsicher angefühlt oder haben Sie an Schwierigkeiten gelitten, Ihre Bewegungen zu koordinieren?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-gait-disorder', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0147-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen verschwommen gesehen?',
		translations: {
					en: 'Did you experience blurred vision in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen verschwommen gesehen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-use', 'antidepressant reduction', 'use', 'rcc-symptom-area-impaired-vision', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0148-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter trockenen Augen gelitten?',
		translations: {
					en: 'Did you suffer from dry eyes in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter trockenen Augen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'geringfügig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0149-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter unkontrollierbaren Mund- oder Zungenbewegungen gelitten?',
		translations: {
					en: 'Did you suffer from uncontrollable mouth or tongue movements in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter unkontrollierbaren Mund- oder Zungenbewegungen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'geringfügig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-movement-disorders', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0150-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen Probleme beim Sprechen gehabt?',
		translations: {
					en: 'Did you have trouble speaking in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen Probleme beim Sprechen gehabt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'geringfügig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-speech', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0151-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen an Kopfschmerzen gelitten?',
		translations: {
					en: 'Did you suffer from headaches in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen an Kopfschmerzen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-headache', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0152-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter vermehrtem Speichelfluss gelitten?',
		translations: {
					en: 'Did you suffer from increased salivation in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter vermehrtem Speichelfluss gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'geringfügig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'mäßig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-use', 'antidepressant reduction', 'use', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0153-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Schwindel, Benommenheit oder einem Drehgefühl gelitten?',
		translations: {
					en: 'Did you suffer from dizziness, lightheadedness, or a spinning sensation in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Schwindel, Benommenheit oder einem Drehgefühl gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-dizziness', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0154-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter einen laufenden Nase gelitten?',
		translations: {
					en: 'Did you suffer from a runny nose in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter einen laufenden Nase gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0155-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Kurzatmigkeit und Schnappen nach Luft gelitten?',
		translations: {
					en: 'Did you suffer from shortness of breath and gasping for air in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Kurzatmigkeit und Schnappen nach Luft gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0156-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen an Schüttelfrost gelitten?',
		translations: {
					en: 'Did you suffer from chills in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen an Schüttelfrost gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0157-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen Fieber?',
		translations: {
					en: 'Did you have a fever in the past seven days?',
					de: 'Hatten Sie in den letzten sieben Tagen Fieber?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'elevated temperature (37.5°-38°)',                  de: 'manchmal' } },
								{ value:2  , translations: { en: 'low-grade fever (38.1°-38.5°)',                     de: 'häufig' } },
								{ value:3  , translations: { en: 'moderate to high-grade fever (>38.5°)',             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0158-weekly',
		type:     'integer',
		meaning:    'Hatten Sie in den letzten sieben Tagen das Gefühl, sich übergeben zu müssen?',
		translations: {
					en: 'Did you feel the need to vomit in the past seven days?',
					de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, sich übergeben zu müssen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'ein wenig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'moderat' } },
								{ value:3  , translations: { en: 'significantly (I vomited at least once)',           de: 'Ich habe mich ein- oder mehrmals übergeben müssen' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0159-weekly',
		type:     'integer',
		meaning:    'War Ihnen in den letzten sieben Tagen vermehrt übel?',
		translations: {
					en: 'Did you feel nauseous more often in the past seven days?',
					de: 'War Ihnen in den letzten sieben Tagen vermehrt übel?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'ein wenig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'deutlich' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0160-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Durchfall gelitten?',
		translations: {
					en: 'Did you suffer from diarrhea in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Durchfall gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'ein wenig' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'deutlich' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'stark' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0161-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Magenkrämpfen gelitten?',
		translations: {
					en: 'Did you suffer from stomach cramps in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Magenkrämpfen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0162-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Magenblähungen gelitten?',
		translations: {
					en: 'Did you suffer from stomach bloating in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Magenblähungen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-vegetative-symptoms', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0163-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen ungewöhnliche visuelle Empfindungen wahrgenommen?',
		translations: {
					en: 'Did you notice any unusual visual sensations in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen ungewöhnliche visuelle Empfindungen wahrgenommen?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-perceptual-disturbance', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0164-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen auf Geräusche ungewöhnlich empfindlich reagiert?',
		translations: {
					en: 'Were you unusually sensitive to noise in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen auf Geräusche ungewöhnlich empfindlich reagiert?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-perceptual-disturbance', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0165-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen Brennen, Taubheitsgefühle oder Kribbeln auf der Haut verspürt?',
		translations: {
					en: 'Did you experience burning, numbness, or tingling of the skin in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen Brennen, Taubheitsgefühle oder Kribbeln auf der Haut verspürt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-perceptual-disturbance', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0166-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen unter Ohrklingeln (Tinnitus) oder Ohrgeräuschen gelitten?',
		translations: {
					en: 'Did you suffer from ringing in the ears (tinnitus) or ringing in the ears in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen unter Ohrklingeln (Tinnitus) oder Ohrgeräuschen gelitten?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-perceptual-disturbance', 'rcc-category-weekly']
	},
		{
		id:       'rcc-curated-extended-0167-weekly',
		type:     'integer',
		meaning:    'Haben Sie in den letzten sieben Tagen ungewöhnliche Geschmacks- oder Geruchsempfindungen gehabt?',
		translations: {
					en: 'Did you have any unusual taste or smelling sensations in the past seven days?',
					de: 'Haben Sie in den letzten sieben Tagen ungewöhnliche Geschmacks- oder Geruchsempfindungen gehabt?'
				},
		options:    [

								{ value:0  , translations: { en: 'not at all',                                        de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                          de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                                        de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                                             de: 'ständig' } },
								],
					tags:     ['rcc-category-dess', 'antidepressant reduction', 'rcc-symptom-area-perceptual-disturbance', 'rcc-category-weekly']
	},




		{
			id:       'C',
			type:     'integer',
			meaning:   'How was your appetite today?',
			translations: {
								en: 'How is your appetite today?',
								de: 'Wie ist Ihr Appetit heute?'
							},
			options:    [
								{ value:0  , translations: { en: 'very bad',                                          de: 'sehr schlecht'   } },
								{ value:1  , meaning: '' },
								{ value:2  , meaning: '' },
								{ value:3  , meaning: '' },
								{ value:4  , meaning: '' },
								{ value:5  , meaning: '' },
								{ value:6  , meaning: '' },
								{ value:7  , meaning: '' },
								{ value:8  , meaning: '' },
								{ value:9  , meaning: '' },
								{ value:10 , translations: { en: 'very good',                                         de: 'sehr gut'  } }
							],
			tags:     ['scale', 'rcc-category-example-B']
		},

		{
			id:       'D',
			type:     'boolean',
			meaning:    'Did you have breakfast today?',
			translations: {
								en: 'Did you have breakfast today?',
								de: 'Haben Sie heute gefrühstückt?'
							},
		},
		{
			id:       'E',
			type:     'integer',
			meaning:    'Have you taken your medication in the agreed quantity?',
			translations: {
								en: 'Have you taken your medication in the agreed quantity?',
								de: 'Haben Sie Ihre Medikamente in der vereinbarten Menge genommen?'
							},
			options:    [
								{ value:0  , boolean:false  , translations: { en: 'no, not at all',                   de: 'nein, überhaupt nicht' } },
								{ value:1  , boolean:false  , translations: { en: 'no, less than agreed',             de: 'nein, weniger' } },
								{ value:2  , boolean:true   , translations: { en: 'yes, exactly',                     de: 'ja, exakt' } },
								{ value:3  , boolean:false  , translations: { en: 'no, more than agreed',             de: 'nein, mehr ' } }
							],
			tags:     ['meds', 'complex-yes-no']
		},

]
