import	{	Injectable				}	from '@angular/core'
import	{
			QuestionStore,
			QuestionConfig
		}								from '@rcc/core'

// Question Config in: lib/core/src/items/questions/questions.commons.ts


@Injectable()
export class CuratedDefaultQuestionStoreService extends QuestionStore {

	public readonly name = 'CURATED.QUESTIONS.CURATED_DEFAULT_NAME'

	public constructor(){
		super(staticStorage)
	}
}


export const 	questionCategories = [
					'rcc-category-default',
					'rcc-category-daily',
					'rcc-category-weekly',

					'rcc-category-depression',
					'rcc-category-bipolar-disorder',
					'rcc-category-psychosis',
					'rcc-category-schizoaffective-disorders',

					'rcc-symptom-area-mood',
					'rcc-symptom-area-drive',
					'rcc-symptom-area-activity',
					'rcc-symptom-area-daily-function',
					'rcc-symptom-area-cognitive-abilities',
					'rcc-symptom-area-delusion',
					'rcc-symptom-area-paranoid-thoughts',
				]


const staticStorage = { getAll: () => Promise.resolve(configs) }


const configs:QuestionConfig[] = [

		{
			id:       'rcc-curated-default-01-daily',
			type:     'integer',
			meaning:    'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?',
			translations: {
								en: 'How well were you able to manage your usual daily tasks today?',
								de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
							},
			options:    [

								{ value:0  , translations: { en: 'yes',                                                   de: 'ja, vollkommen' } },
								{ value:1  , translations: { en: 'somewhat worse',                                        de: 'ja, mit leichten Einschränkungen' } },
								{ value:2  , translations: { en: 'moderately worse',                                      de: 'mit deutlichen Einschränkungen' } },
								{ value:3  , translations: { en: 'a lot worse',                                           de: 'kaum' } },
							],
			tags:     ['rcc-category-psychosis', 'rcc-category-depression', 'rcc-category-bipolar-disorder', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'icd-10-f31', 'icd-10-f33', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-daily-function', 'rcc-category-daily', 'rcc-category-default']
		},

			{
			id:       'rcc-curated-default-02-daily',
			type:     'integer',
			meaning:    'Haben Sie sich heute niedergeschlagen gefühlt?',
			translations: {
								en: 'Did you feel down today?',
								de: 'Haben Sie sich heute niedergeschlagen gefühlt?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',                                                   de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                                                     de: 'etwas' } },
								{ value:2  , translations: { en: 'moderately',                                                   de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                                         de: 'stark' } },
							],
			tags:     ['rcc-category-psychosis', 'rcc-category-depression', 'rcc-category-bipolar-disorder', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'icd-10-f31', 'icd-10-f33', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-mood', 'rcc-category-daily', 'rcc-category-default']
		},

		{
			id:       'rcc-curated-default-03-daily',
			type:     'integer',
			meaning:    'Sind Sie antriebslos?',
			translations: {
								en: 'Was your energy low today?',
								de: 'Sind Sie antriebslos?'
							},
			options:    [

								{ value:0  , translations: { en: 'not at all',                             de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                               de: 'etwas' } },
								{ value:2  , translations: { en: 'moderately',                             de: 'deutlich' } },
								{ value:3  , translations: { en: 'very',                                   de: 'stark' } },
							],
			tags:     ['rcc-category-psychosis', 'rcc-category-depression', 'rcc-category-bipolar-disorder', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'icd-10-f31', 'icd-10-f33', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-drive', 'rcc-category-daily', 'rcc-category-default']

		},

		{
			id:       'rcc-curated-default-04-daily',
			type:     'integer',
			meaning:    'Haben Sie das Gefühl, dass etwas Seltsames vor sich geht?',
			translations: {
						en: 'Did you feel like something strange was going on today?',
						de: 'Haben Sie das Gefühl, dass etwas Seltsames vor sich geht?'
					},
			options:    [

								{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
								{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
								{ value:3  , translations: { en: 'a lot',                          de: 'ständig' } },
								],
					tags:     ['rcc-category-psychosis', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily', 'rcc-category-default']
			},

		{
			id:       'rcc-curated-default-05-daily',
			type:     'integer',
			meaning:    'Sind Sie heute misstrauisch?',
			translations: {
						en: 'Did you feel suspicious today?',
						de: 'Sind Sie heute misstrauisch?'
					},
			options:    [

								{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
								{ value:1  , translations: { en: 'somewhat',                              de: 'gering' } },
								{ value:2  , translations: { en: 'moderately',                            de: 'mäßig' } },
								{ value:3  , translations: { en: 'very',                                  de: 'stark' } },
								],
					tags:     ['rcc-category-psychosis', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-daily', 'rcc-category-default']
		},

		{
			id:       'rcc-curated-default-06-daily',
			type:     'integer',
			meaning:    'Waren Sie voller Energie und motiviert, viele verschiedene Dinge zu tun?',
			translations: {
						en: 'Were you full of energy and motivated to do lots of different things today?',
						de: 'Waren Sie voller Energie und motiviert, viele verschiedene Dinge zu tun?'
					},
			options:    [

						{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
						{ value:1  , translations: { en: 'somewhat',                  de: 'gering' } },
						{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
						{ value:3  , translations: { en: 'very',                      de: 'stark' } },
					],
							tags:     ['rcc-category-schizoaffective-disorders', 'rcc-category-bipolar-disorder', 'icd-10-f31', 'icd-10-f33', 'bipolar disorder', 'schizoaffective disorders', 'rcc-symptom-area-drive-activity', 'rcc-category-daily', 'rcc-category-default']
		},

		{
				id:       'rcc-curated-default-07-daily',
				type:     'integer',
				meaning:    'Haben Sie heute den Eindruck, die Gedanken rasen Ihnen durch den Kopf?',
				translations: {
							en: 'Did you have the impression that thoughts were racing through your head today?',
							de: 'Haben Sie heute den Eindruck, die Gedanken rasen Ihnen durch den Kopf?'
						},
				options:    [

										{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
										{ value:1  , translations: { en: 'rarely',                de: 'manchmal' } },
										{ value:2  , translations: { en: 'often',                 de: 'häufig' } },
										{ value:3  , translations: { en: 'constantly',            de: 'ständig' } },
										],
							tags:     ['rcc-category-schizoaffective-disorders', 'rcc-category-bipolar-disorder', 'icd-10-f31', 'icd-10-f33', 'bipolar disorder', 'schizoaffective disorders', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily', 'rcc-category-default']
		},
]
