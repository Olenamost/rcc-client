import  {
			Injectable,
			NgModule
		}                              	 	from '@angular/core'

import  {
			QuestionConfig,
			QuestionStore,
			SymptomCheckConfig,
			SymptomCheckStore
		}                               	from '@rcc/core'

import  {
			provideTranslationMap,
		}                               	from '@rcc/common'

import  {
			QuestionnaireServiceModule,
			DayQueryService,
			DayQueryModule,
			SymptomCheckMetaStoreServiceModule,
		}                               	from '@rcc/features'



const en =  {
				'NAME':     'Example: Day Questions'
			}

const de =  {
				'NAME':     'Beispiel: TagesFragen'
			}




@Injectable()
export class ExampleDayQuestionStore extends QuestionStore {

	public name = 'EXAMPLE_DAY_QUESTION_STORE.NAME'

	public constructor(){
	super(ExampleQuestionStorage)
	}
}

const ExampleQuestionStorage = { getAll: () => Promise.resolve(example_configs) }

const example_configs:QuestionConfig[] = [

	{
		id:         'weekly-A',
		type:       'integer',
		meaning:    'rate last week',
		translations: {
			'en': 'How did the last 7 days go for you? [weekly]',
			'de': 'Wie sind die vergangen 7 Tage für Dich gelaufen? [weekly]'
		},

		min:	0,
		max:	3,
		tags:       []
	},

	{
		id:         'weekly-B',
		type:       'boolean',
		meaning:    'did something good happen to you in the past 7 days',
		translations: {
			'en': 'Did something good happen to you, during the past 7 days? [weekly]',
			'de': 'Ist Dir in den letzten 7 Tagen etwas Schönes passiert? [weekly]'
		},

		tags:	[]
	},


	{
		id:         'daily-A',
		type:       'integer',
		meaning:    'favorite number < 10',       // This property will be used to display the question text,
															// if translations are missing,
															// but it's main purpose is to give the intent of the question.
															// (maybe we should drop this property)
		translations: {
			'en': 'What\'s your favorite number below 10? [daily]',
			'de': 'Was ist Deine Lieblingszahl unter 10? [daily]'
		},

		min:    0,
		max:    9,
		tags:   ['scale']
	},


	{
		id:       'daily-B',
		type:     'integer',
		meaning:   'What is your overall mood today?',
		translations: {
			en: 'What is your overall mood today? [daily]',
			de: 'Wie ist Ihre allgemeine Stimmung heute? [daily]'
		},

		options:    [
			{
				value: 0,
				translations:{
					en: 'depressed',
					de: 'bedrückt'
				}
			},
			{
				value: 1,
				translations:{
					en: 'okay',
					de: 'okay'
				}
			},
			{
				value: 2,
				translations:{
					en: 'good',
					de: 'gut'
				}
			},
			{
				value: 3,
				translations: {
					en: 'excited',
					de: 'enthusiastisch'
				}
			}
		]

	},



]





@Injectable()
export class ExampleDaySymptomCheckStore extends SymptomCheckStore {

	public name = 'EXAMPLE_DAY_QUESTION_STORE.NAME'

	public constructor(){
	super(ExampleSymptomCheckStorage)
	}
}

const ExampleSymptomCheckStorage = { getAll: () => Promise.resolve(sc_example_configs) }

const sc_example_configs: SymptomCheckConfig[] = [

	{
		meta:		{
						label: 				'Example: Dr. Tuesday',
						defaultSchedule:	[
												[], // every day
												[]	// ignore time of day
											],
						creationDate:		'2020-02-28T16:13:00.0Z'
					},
		questions:	[
						{ id: 'weekly-A', schedule: [ [1], [] ] }, // Tuesdays, ignore day of time
						{ id: 'weekly-B', schedule: [ [1], [] ] }, // Tuesdays, ignore day of time
						'daily-A',
						'daily-B'
					]

	},


	{
		meta:		{
						label: 				'Example: Dr. Friday',
						defaultSchedule:	[
												[], // every day
												[]	// ignore time of day
											],
						creationDate:		'2020-02-28T16:13:00.0Z'
					},
		questions:	[
						{ id: 'weekly-A', schedule: [ [5], [] ] }, // Friday, ignore day of time
						'daily-A',
						'daily-B'
					]

	},



	{
		meta:		{
						label: 				'Example: Dr. Physical',
						defaultSchedule:	[
												[],
												[]
											],
						creationDate:		'2020-02-28T16:13:00.0Z'
					},
		questions:	[
						'physical-state-000',
						'physical-state-001',
						'physical-state-002'
					]
	}

]





@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([ExampleDaySymptomCheckStore]),
		QuestionnaireServiceModule.forChild([ExampleDayQuestionStore]),
		DayQueryModule,
	],
	providers: [
		ExampleDayQuestionStore,
		ExampleDaySymptomCheckStore,
		provideTranslationMap('EXAMPLE_DAY_QUESTION_STORE', { de ,en }),
	]
})
export class ExampleDayQuestionsSetupModule {

	public constructor(
		// importSymptomCheckStoreService		: ImportSymptomCheckStoreService,		// <- the real one, that's probably empty at the time
		exampleDaySymptomCheckStore	: ExampleDaySymptomCheckStore,		// <- the fake one, so we don't pollute indexDB with example data
		dayQueryService				: DayQueryService
	){

		dayQueryService.ready.then( async () => {

			console.info('ExampleDayQuestionsSetupModule:')
			console.info('DayQueryService:', dayQueryService)

			console.info('QueryControls for January and February:')

			// await importSymptomCheckStoreService.ready
			await exampleDaySymptomCheckStore.ready

			// const symptomChecks 		= importSymptomCheckStoreService.items
			const symptomChecks 		= exampleDaySymptomCheckStore.items
			const queryControlsRecord	= await dayQueryService.getQueryControls(symptomChecks)

			return queryControlsRecord
		})
		.then(
			console.info,
			console.error
		)
	}

}
