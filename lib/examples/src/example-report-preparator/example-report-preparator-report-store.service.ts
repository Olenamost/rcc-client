import	{   Injectable          }	from '@angular/core'
import	{
			ReportConfig,
			ReportStore,
			EntryConfig,
			CalendarDateString,
			Schedule

		}							from '@rcc/core'


@Injectable()
export class ExampleReportPreparatorReportStoreService extends ReportStore {

		public readonly name = 'EXAMPLE_REPORT_PREPARATOR.REPORT_STORE_NAME'

		public constructor(){
				super(staticStorage)
		}
}

function randomValue(last = 1): number {

	if( Math.random() < 0.25) 	return 1
	if( Math.random() < 0.3) 	return last

	if(last === 0) return Math.random() < 0.8 ? 1 :3
	if(last === 1) return Math.random() < 0.8 ? 2 :0
	if(last === 2) return Math.random() < 0.8 ? 3 :1
	if(last === 3) return Math.random() < 0.8 ? 2 :1
}

function getValueRun(length: number): number[]{

	const result = new Array<number>()

	let last_value = Math.floor( Math.random()*2 )+1

	for(let i = 0; i<length; i++)
		result[i] = last_value = randomValue(last_value)


	return result
}

function getBooleanRun(length: number): boolean[] {
	return new Array(length).fill(false).map( () => Math.random() < 0.5 )
}

const staticStorage = { getAll: () => Promise.resolve([config]) }
const now			= Schedule.getIsoStringWithTimezone()
const value_runs	= Array(3).fill(0).map( () => getValueRun(200) )
const boolean_run	= getBooleanRun(200)

const endDate	: string = CalendarDateString.today()
const startDate	: string = CalendarDateString.daysBefore(endDate, 28)

const config:  ReportConfig = [
		'Example Report (preparators)',
		now,
		CalendarDateString.range(startDate, endDate).map(
			(dateStr,i)	=>	{
				return [
						...[0,1,2].map( num => [
							`physical-state-00${num}`,
							value_runs[num][i],
							now,
							null,
							dateStr
						] as EntryConfig),

						[
							'example-breakfast',
							boolean_run[i],
							now,
							null,
							dateStr
						] as EntryConfig
					]
			}
		)
		.flat()
		.filter( () => Math.random() > 0.12)
]
