import 	{	NgModule 						} 	from '@angular/core'
import	{
			DevModule,
			provideTranslationMap,
		}										from '@rcc/common'

import	{
			ReportMetaStoreServiceModule,
		}										from '@rcc/features'

import	{	StaticReportStoreService				}	from './static-report-store.service'


import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		ReportMetaStoreServiceModule.forChild([StaticReportStoreService]),
		DevModule.note('StaticReportStoreServiceModule'),
	],
	providers: [
		StaticReportStoreService,
		provideTranslationMap('STATIC_REPORT_STORE', { en, de })
	]
})
export class StaticReportStoreServiceModule{}
