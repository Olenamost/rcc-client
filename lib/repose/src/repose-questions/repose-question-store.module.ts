import 	{	NgModule						}	from '@angular/core'
import	{
			provideTranslationMap,
			provideItemSelectionFilter
		}										from '@rcc/common'
import	{ QuestionnaireServiceModule 		}	from '@rcc/features'
import	{
			ReposeQuestionStoreService,
			ReposeQuestionCategoryFilters
		}										from './repose-question-store.service'

import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	imports:[
		QuestionnaireServiceModule.forChild([ReposeQuestionStoreService])
	],
	providers:[
		ReposeQuestionStoreService,
		provideTranslationMap('QUESTION_STORE.REPOSE_QUESTIONS', { en,de }),
		...ReposeQuestionCategoryFilters.map(provideItemSelectionFilter)
	]
})
export class ReposeQuestionStoreServiceModule {

}
