import	{	Injectable				}	from '@angular/core'
import	{
			Item,
			Question,
			QuestionStore,
			QuestionConfig
		}								from '@rcc/core'

// Question Config in: lib/core/src/items/questions/questions.commons.ts


@Injectable()
export class ReposeQuestionStoreService extends QuestionStore {

	public readonly name = 'QUESTION_STORE.REPOSE_QUESTIONS.NAME'

	public constructor(){
		super(staticStorage)
	}
}


const 	questionCategories = [
					'repose-category-depression',
					'repose-category-example-B',
				]

export const	ReposeQuestionCategoryFilters = questionCategories.map( tag => ({
					filter: (item:Item) => (item instanceof Question) && item.tags?.includes(tag),
					representation: {
						label: `QUESTION_STORE.REPOSE_QUESTIONS.CATEGORIES.${tag.toUpperCase()}`,
						icon: 'question'
					}
				}))

const staticStorage = { getAll: () => Promise.resolve(configs) }


// Depression  f32, f33  Stimmung  depressive Stimmung
// Haben Sie sich heute deprimiert gefühlt?  Haben Sie sich in den letzten sieben Tagen deprimiert gefühlt?  nein, gering, mäßig, stark  default Intensität  depression, negativsymptomatik    Did you feel depressed, sad or down today?    Did you feel depressed, sad or down during the last seven days? none, mild, moderate, severe  no, mildly, moderately, very much default Erleben Sie sich als häufiger deprimiert/ traurig/ niedergeschlagen?  nicht vorhanden, gering, mäßig, stark



const configs:QuestionConfig[] = [

		// Vorgeschlagen wurde ein Erfassungszeitraum über die APP von 6 Monaten für ePROMS (6 Items alle 3 Tage = 61 Assessments). In Folge ergäben sich folgende Messzeitpunkte:
		// T0 (baseline) = 0 Monate
		// T1-T59 (interim assessments) = alle 3 Tage
		// T60 (final assessment) = nach 6 Monaten

		// 0, 6 Monate Komplettes RDoC Assessment = full assessment
		// 2, 4 Monate abgespecktes kurzes RDoC Assessment = short assessment


		// BSI-53: Brief Symptom Inventory

		// Sie finden nachstehend eine Liste von Problemen und Beschwerden, die man manchmal hat. Bitte lesen Sie jede Frage einzeln sorgfältig durch und entscheiden Sie, wie stark Sie durch diese Beschwerden gestört oder bedrängt worden sind, und zwar während der vergangenen 7 Tage bis heute. Überlegen Sie bitte nicht erst, welche Antwort „den besten Eindruck“ machen könnte, sondern antworten Sie so, wie es für Sie persönlich zutrifft. Machen Sie bitte hinter jeder Frage nur ein Kreuz in das Kästchen unter der für Sie am besten zutreffenden Antwort. Streichen Sie versehentliche Antworten bitte dick durch und kreuzen Sie danach das richtige Kästchen an. Überhaupt nicht,	Ein wenig,	Ziemlich,	Stark,	Sehr stark

		// Wie sehr litten Sie in den letzten 7 Tagen unter Nervosität oder innerem Zittern?
		{

			id:       'BSI53_1',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Nervosität oder innerem Zittern?',
			translations: {
					en: 'How much were you distressed by nervousness or shakiness inside during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Nervosität oder innerem Zittern?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_1', 'item1', 'full-assessment', 'block1', 'full-assessment-block1_item1', 'short-assessment', 'short-assessment-block1_item1']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Ohnmachts- und Schwindelgefühlen?
		{

			id:       'BSI53_2',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Ohnmachts- und Schwindelgefühlen?',
			translations: {
					en: 'How much were you distressed by faintness or dizziness during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Ohnmachts- und Schwindelgefühlen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_2', 'item2', 'full-assessment', 'block1', 'full-assessment-block1_item2',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter der Idee, dass irgendjemand Macht über Ihre Gedanken hat?
		{

			id:       'BSI53_3',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter der Idee, dass irgendjemand Macht über Ihre Gedanken hat?',
			translations: {
					en: 'How much were you distressed by the idea that someone else can control your thoughts during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter der Idee, dass irgendjemand Macht über Ihre Gedanken hat?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_3', 'item3', 'full-assessment', 'block1', 'full-assessment-block1_item3',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass andere an den meisten Ihrer Schwierigkeiten Schuld sind?
		{

			id:       'BSI53_4',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass andere an den meisten Ihrer Schwierigkeiten Schuld sind?',
			translations: {
					en: 'How much were you distressed by the feeling others are to blame for most of your troubles during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass andere an den meisten Ihrer Schwierigkeiten Schuld sind?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_4', 'item4', 'full-assessment', 'block1', 'full-assessment-block1_item4',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Gedächtnisschwierigkeiten?
		{

			id:       'BSI53_5',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Gedächtnisschwierigkeiten?',
			translations: {
					en: 'How much were you distressed by trouble remembering things during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Gedächtnisschwierigkeiten?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_5', 'item5', 'full-assessment', 'block1', 'full-assessment-block1_item5',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, leicht reizbar oder verärgerbar zu sein?
		{

			id:       'BSI53_6',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, leicht reizbar oder verärgerbar zu sein?',
			translations: {
					en: 'How much were you distressed by feeling easily annoyed or irritated during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, leicht reizbar oder verärgerbar zu sein?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_6', 'item6', 'full-assessment', 'block1', 'full-assessment-block1_item6',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Herz- oder Brustschmerzen?
		{

			id:       'BSI53_7',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Herz- oder Brustschmerzen?',
			translations: {
					en: 'How much were you distressed by pains in the heart of chest during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Herz- oder Brustschmerzen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_7', 'item7', 'full-assessment', 'block1', 'full-assessment-block1_item7',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Furcht auf offenen Plätzen oder auf der Straße?
		{

			id:       'BSI53_8',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Furcht auf offenen Plätzen oder auf der Straße?',
			translations: {
					en: 'How much were you distressed by feeling afraid in open spaces during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Furcht auf offenen Plätzen oder auf der Straße?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:      ['BSI53', 'BSI53_8', 'item8', 'full-assessment', 'block1', 'full-assessment-block1_item8', 'short-assessment', 'short-assessment-block1_item8']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Gedanken, sich das Leben zu nehmen?
		{

			id:       'BSI53_9',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Gedanken, sich das Leben zu nehmen?',
			translations: {
					en: 'How much were you distressed by thoughts of ending your life during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Gedanken, sich das Leben zu nehmen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_9', 'item9', 'full-assessment', 'block1', 'full-assessment-block1_item9',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass man den meisten Menschen nicht trauen kann?
		{

			id:       'BSI53_10',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass man den meisten Menschen nicht trauen kann?',
			translations: {
					en: 'How much were you distressed by feeling that most people cannot be trusted during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass man den meisten Menschen nicht trauen kann?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_10', 'item10', 'full-assessment', 'block1', 'full-assessment-block1_item10',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter schlechtem Appetit?
		{

			id:       'BSI53_11',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter schlechtem Appetit?',
			translations: {
					en: 'How much were you distressed by poor appetite during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter schlechtem Appetit?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_11', 'item11', 'full-assessment', 'block1', 'full-assessment-block1_item11',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter plötzlichem Erschrecken ohne Grund?
		{

			id:       'BSI53_12',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter plötzlichem Erschrecken ohne Grund?',
			translations: {
					en: 'How much were you distressed by suddenly scared for no reason during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter plötzlichem Erschrecken ohne Grund?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_12', 'item12', 'full-assessment', 'block1', 'full-assessment-block1_item12', 'short-assessment', 'short-assessment-block1_item12']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Gefühlsausbrüchen, denen gegenüber Sie machtlos waren?
		{

			id:       'BSI53_13',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Gefühlsausbrüchen, denen gegenüber Sie machtlos waren?',
			translations: {
					en: 'How much were you distressed by temper outbursts that you could not control during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Gefühlsausbrüchen, denen gegenüber Sie machtlos waren?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_12', 'item12', 'full-assessment', 'block1', 'full-assessment-block1_item12',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Einsamkeitsgefühlen, selbst wenn Sie in Gesellschaft sind?
		{

			id:       'BSI53_14',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Einsamkeitsgefühlen, selbst wenn Sie in Gesellschaft sind?',
			translations: {
					en: 'How much were you distressed by feeling lonely even when you are with people during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Einsamkeitsgefühlen, selbst wenn Sie in Gesellschaft sind?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_14', 'item14', 'full-assessment', 'block1', 'full-assessment-block1_item14', 'short-assessment', 'short-assessment-block1_item14']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass es Ihnen schwerfällt, etwas anzufangen?
		{

			id:       'BSI53_15',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass es Ihnen schwerfällt, etwas anzufangen?',
			translations: {
					en: 'How much were you distressed by feeling blocked in getting things done during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass es Ihnen schwerfällt, etwas anzufangen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_15', 'item15', 'full-assessment', 'block1', 'full-assessment-block1_item15',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Einsamkeitsgefühlen?
		{

			id:       'BSI53_16',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Einsamkeitsgefühlen?',
			translations: {
					en: 'How much were you distressed by feeling lonely during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Einsamkeitsgefühlen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_16', 'item16', 'full-assessment', 'block1', 'full-assessment-block1_item16',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Schwermut?
		{

			id:       'BSI53_17',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Schwermut?',
			translations: {
					en: 'How much were you distressed by feeling blue during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Schwermut?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_17', 'item17', 'full-assessment', 'block1', 'full-assessment-block1_item17',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, sich für nichts zu interessieren?
		{

			id:       'BSI53_18',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, sich für nichts zu interessieren?',
			translations: {
					en: 'How much were you distressed by feeling no interest in things during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, sich für nichts zu interessieren?'
			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['BSI53', 'BSI53_18', 'item18', 'full-assessment', 'block1', 'full-assessment-block1_item18', 'short-assessment', 'short-assessment-block1_item18']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Furchtsamkeit?
		{

			id:       'BSI53_19',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Furchtsamkeit?',
			translations: {
					en: 'How much were you distressed by feeling fearful during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Furchtsamkeit?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['BSI53', 'BSI53_19', 'item19', 'full-assessment', 'block1', 'full-assessment-block1_item19', 'short-assessment', 'short-assessment-block1_item19']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Verletzlichkeit in Gefühlsdingen?
		{

			id:       'BSI53_20',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Verletzlichkeit in Gefühlsdingen?',
			translations: {
					en: 'How much were you distressed by your feelings being easily hurt during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Verletzlichkeit in Gefühlsdingen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['BSI53', 'BSI53_20', 'item20', 'full-assessment', 'block1', 'full-assessment-block1_item20',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass die Leute unfreundlich sind oder Sie nicht leiden können?
		{

			id:       'BSI53_21',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass die Leute unfreundlich sind oder Sie nicht leiden können?',
			translations: {
					en: 'How much were you distressed by feeling that people are unfriendly or dislike you during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass die Leute unfreundlich sind oder Sie nicht leiden können?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },
			],
			tags:     ['BSI53', 'BSI53_21', 'item21', 'full-assessment', 'block1', 'full-assessment-block1_item21',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Minderwertigkeitsgefühlen gegenüber anderen?
		{

			id:       'BSI53_22',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Minderwertigkeitsgefühlen gegenüber anderen?',
			translations: {
					en: 'How much were you distressed by feeling inferior to others during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Minderwertigkeitsgefühlen gegenüber anderen?'
			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },
			],
			tags:     ['BSI53', 'BSI53_22', 'item22', 'full-assessment', 'block1', 'full-assessment-block1_item22',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Übelkeit oder Magenverstimmung?
		{

			id:       'BSI53_23',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Übelkeit oder Magenverstimmung?',
			translations: {
					en: 'How much were you distressed by nausea or upset stomach during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Übelkeit oder Magenverstimmung?'
			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },
			],
			tags:     ['BSI53', 'BSI53_23', 'item23', 'full-assessment', 'block1', 'full-assessment-block1_item23',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass andere Sie beobachten oder über Sie reden?
		{

			id:       'BSI53_24',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass andere Sie beobachten oder über Sie reden?',
			translations: {
					en: 'How much were you distressed by feeling that you are watched or talked about by others during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass andere Sie beobachten oder über Sie reden?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_24', 'item24', 'full-assessment', 'block1', 'full-assessment-block1_item24',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Einschlafschwierigkeiten?
		{

			id:       'BSI53_25',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Einschlafschwierigkeiten?',
			translations: {
					en: 'How much were you distressed by trouble falling asleep during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Einschlafschwierigkeiten?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_25', 'item25', 'full-assessment', 'block1', 'full-assessment-block1_item25',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Zwang, wieder und wieder nachzukontrollieren, was Sie tun?
		{

			id:       'BSI53_26',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Zwang, wieder und wieder nachzukontrollieren, was Sie tun?',
			translations: {
					en: 'How much were you distressed by having to check and double check what you do during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Zwang, wieder und wieder nachzukontrollieren, was Sie tun?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_26', 'item26', 'full-assessment', 'block1', 'full-assessment-block1_item26',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Schwierigkeiten, sich zu entscheiden?
		{

			id:       'BSI53_27',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Schwierigkeiten, sich zu entscheiden?',
			translations: {
					en: 'How much were you distressed by difficulty making decisions during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Schwierigkeiten, sich zu entscheiden?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_27', 'item27', 'full-assessment', 'block1', 'full-assessment-block1_item27',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Furcht vor Fahrten in Bus, Straßenbahn, U-Bahn oder Zug?
		{

			id:       'BSI53_28',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Furcht vor Fahrten in Bus, Straßenbahn, U-Bahn oder Zug?',
			translations: {
					en: 'How much were you distressed by feeling afraid to travel on buses, subways, or trains during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Furcht vor Fahrten in Bus, Straßenbahn, U-Bahn oder Zug?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_28', 'item28', 'full-assessment', 'block1', 'full-assessment-block1_item28', 'short-assessment', 'short-assessment-block1_item28']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Schwierigkeiten beim Atmen?
		{

			id:       'BSI53_29',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Schwierigkeiten beim Atmen?',
			translations: {
					en: 'How much were you distressed by trouble getting your breath during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Schwierigkeiten beim Atmen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_29', 'item29', 'full-assessment', 'block1', 'full-assessment-block1_item29',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Hitzewallungen oder Kälteschauern?
		{

			id:       'BSI53_30',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Hitzewallungen oder Kälteschauern?',
			translations: {
					en: 'How much were you distressed by hot or cold spells during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Hitzewallungen oder Kälteschauern?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_30', 'item30', 'full-assessment', 'block1', 'full-assessment-block1_item30',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter der Notwendigkeit, bestimmte Dinge, Orte oder Tätigkeiten zu meiden, weil Sie durch diese erschreckt werden?
		{

			id:       'BSI53_31',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter der Notwendigkeit, bestimmte Dinge, Orte oder Tätigkeiten zu meiden, weil Sie durch diese erschreckt werden?',
			translations: {
					en: 'How much were you distressed by having to avoid certain things, places, or activities because they frighten you during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter der Notwendigkeit, bestimmte Dinge, Orte oder Tätigkeiten zu meiden, weil Sie durch diese erschreckt werden?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_31', 'item31', 'full-assessment', 'block1', 'full-assessment-block1_item31', 'short-assessment', 'short-assessment-block1_item31']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Leere im Kopf?
		{

			id:       'BSI53_32',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Leere im Kopf?',
			translations: {
					en: 'How much were you distressed by your mind going blank during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Leere im Kopf?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_32', 'item32', 'full-assessment', 'block1', 'full-assessment-block1_item32',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Taubheit oder Kribbeln in einzelnen Körperteilen?
		{

			id:       'BSI53_33',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Taubheit oder Kribbeln in einzelnen Körperteilen?',
			translations: {
					en: 'How much were you distressed by numbness or tingling in parts of your body during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Taubheit oder Kribbeln in einzelnen Körperteilen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_33', 'item33', 'full-assessment', 'block1', 'full-assessment-block1_item33',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gedanken, dass Sie für Ihre Sünden bestraft werden sollen?
		{

			id:       'BSI53_34',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gedanken, dass Sie für Ihre Sünden bestraft werden sollen?',
			translations: {
					en: 'How much were you distressed by the idea that you should be punished for your sins during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gedanken, dass Sie für Ihre Sünden bestraft werden sollen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_34', 'item34', 'full-assessment', 'block1', 'full-assessment-block1_item34',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter einem Gefühl der Hoffnungslosigkeit angesichts der Zukunft?
		{

			id:       'BSI53_35',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter einem Gefühl der Hoffnungslosigkeit angesichts der Zukunft?',
			translations: {
					en: 'How much were you distressed by feeling hopeless about the future during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter einem Gefühl der Hoffnungslosigkeit angesichts der Zukunft?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_35', 'item35', 'full-assessment', 'block1', 'full-assessment-block1_item35',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Konzentrationsschwierigkeiten?
		{

			id:       'BSI53_36',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Konzentrationsschwierigkeiten?',
			translations: {
					en: 'How much were you distressed by trouble concentrating during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Konzentrationsschwierigkeiten?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_36', 'item36', 'full-assessment', 'block1', 'full-assessment-block1_item36',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Schwächegefühl in einzelnen Körperteilen?
		{

			id:       'BSI53_37',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Schwächegefühl in einzelnen Körperteilen?',
			translations: {
					en: 'How much were you distressed by feeling weak in parts of your body during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Schwächegefühl in einzelnen Körperteilen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_37', 'item37', 'full-assessment', 'block1', 'full-assessment-block1_item37',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, gespannt oder aufgeregt zu sein?
		{

			id:       'BSI53_38',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, gespannt oder aufgeregt zu sein?',
			translations: {
					en: 'How much were you distressed by feeling tense or keyed up during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, gespannt oder aufgeregt zu sein?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_38', 'item38', 'full-assessment', 'block1', 'full-assessment-block1_item38', 'short-assessment', 'short-assessment-block1_item38']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Gedanken an den Tod oder ans Sterben?
		{

			id:       'BSI53_39',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Gedanken an den Tod oder ans Sterben?',
			translations: {
					en: 'How much were you distressed by thoughts of death or dying during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Gedanken an den Tod oder ans Sterben?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_39', 'item39', 'full-assessment', 'block1', 'full-assessment-block1_item39',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Drang, jemanden zu schlagen, zu verletzen oder ihm Schmerzen zuzufügen?
		{

			id:       'BSI53_40',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Drang, jemanden zu schlagen, zu verletzen oder ihm Schmerzen zuzufügen?',
			translations: {
					en: 'How much were you distressed by having urges to beat, injure, or harm someone during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Drang, jemanden zu schlagen, zu verletzen oder ihm Schmerzen zuzufügen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_40', 'item40', 'full-assessment', 'block1', 'full-assessment-block1_item40',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Drang, Dinge zu zerbrechen oder zu zerschmettern?
		{

			id:       'BSI53_41',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Drang, Dinge zu zerbrechen oder zu zerschmettern?',
			translations: {
					en: 'How much were you distressed by having urges to break or smash things during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Drang, Dinge zu zerbrechen oder zu zerschmettern?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_41', 'item41', 'full-assessment', 'block1', 'full-assessment-block1_item41',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter starker Befangenheit im Umgang mit anderen?
		{

			id:       'BSI53_42',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter starker Befangenheit im Umgang mit anderen?',
			translations: {
					en: 'How much were you distressed by feeling very self-conscious with others during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter starker Befangenheit im Umgang mit anderen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_42', 'item42', 'full-assessment', 'block1', 'full-assessment-block1_item42',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Abneigung gegen Menschenmengen, z.B. beim Einkaufen oder im Kino?
		{

			id:       'BSI53_43',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Abneigung gegen Menschenmengen, z.B. beim Einkaufen oder im Kino?',
			translations: {
					en: 'How much were you distressed by feeling uneasy in crowds during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Abneigung gegen Menschenmengen, z.B. beim Einkaufen oder im Kino?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_43', 'item43', 'full-assessment', 'block1', 'full-assessment-block1_item43', 'short-assessment', 'short-assessment-block1_item43']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Eindruck, sich einer anderen Person nie so richtig nahe fühlen zu können?
		{

			id:       'BSI53_44',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Eindruck, sich einer anderen Person nie so richtig nahe fühlen zu können?',
			translations: {
					en: 'How much were you distressed by never feeling close to another person during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Eindruck, sich einer anderen Person nie so richtig nahe fühlen zu können?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_44', 'item44', 'full-assessment', 'block1', 'full-assessment-block1_item44',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Schreck- oder Panikanfällen?
		{

			id:       'BSI53_45',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Schreck- oder Panikanfällen?',
			translations: {
					en: 'How much were you distressed by spells of terror or panic during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Schreck- oder Panikanfällen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_45', 'item45', 'full-assessment', 'block1', 'full-assessment-block1_item45', 'short-assessment', 'short-assessment-block1_item45']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter der Neigung, immer wieder in Erörterungen und Auseinandersetzungen zu geraten?
		{

			id:       'BSI53_46',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter der Neigung, immer wieder in Erörterungen und Auseinandersetzungen zu geraten?',
			translations: {
					en: 'How much were you distressed by getting into frequent arguments during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter der Neigung, immer wieder in Erörterungen und Auseinandersetzungen zu geraten?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_46', 'item46', 'full-assessment', 'block1', 'full-assessment-block1_item46',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Nervosität, wenn Sie allein gelassen werden?
		{

			id:       'BSI53_47',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Nervosität, wenn Sie allein gelassen werden?',
			translations: {
					en: 'How much were you distressed by feeling nervous when you are left alone during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Nervosität, wenn Sie allein gelassen werden?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_47', 'item47', 'full-assessment', 'block1', 'full-assessment-block1_item47', 'short-assessment', 'short-assessment-block1_item47']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter mangelnder Anerkennung Ihrer Leistungen durch andere?
		{

			id:       'BSI53_48',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter mangelnder Anerkennung Ihrer Leistungen durch andere?',
			translations: {
					en: 'How much were you distressed by other not giving you proper credit for your achievements during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter mangelnder Anerkennung Ihrer Leistungen durch andere?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_48', 'item48', 'full-assessment', 'block1', 'full-assessment-block1_item48',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter so starker Ruhelosigkeit, dass Sie nicht stillsitzen können?
		{

			id:       'BSI53_49',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter so starker Ruhelosigkeit, dass Sie nicht stillsitzen können?',
			translations: {
					en: 'How much were you distressed by feeling so restless you couldnt sit still during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter so starker Ruhelosigkeit, dass Sie nicht stillsitzen können?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_49', 'item49', 'full-assessment', 'block1', 'full-assessment-block1_item49', 'short-assessment', 'short-assessment-block1_item49']
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, wertlos zu sein?
		{

			id:       'BSI53_50',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, wertlos zu sein?',
			translations: {
					en: 'How much were you distressed by feelings of worthlessness during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, wertlos zu sein?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_50', 'item50', 'full-assessment', 'block1', 'full-assessment-block1_item50',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass die Leute Sie ausnutzen, wenn Sie es zulassen würden?
		{

			id:       'BSI53_51',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass die Leute Sie ausnutzen, wenn Sie es zulassen würden?',
			translations: {
					en: 'How much were you distressed by feeling that people will take advantage of you if you let them during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gefühl, dass die Leute Sie ausnutzen, wenn Sie es zulassen würden?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_51', 'item51', 'full-assessment', 'block1', 'full-assessment-block1_item51',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter Schuldgefühlen?
		{

			id:       'BSI53_52',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter Schuldgefühlen?',
			translations: {
					en: 'How much were you distressed by feeling of guilt during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter Schuldgefühlen?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_52', 'item52', 'full-assessment', 'block1', 'full-assessment-block1_item52',]
		},
		// Wie sehr litten Sie in den letzten 7 Tagen unter dem Gedanken, dass irgendetwas mit Ihrem Verstand nicht in Ordnung ist?
		{

			id:       'BSI53_53',
			type:     'integer',
			meaning:  'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gedanken, dass irgendetwas mit Ihrem Verstand nicht in Ordnung ist?',
			translations: {
					en: 'How much were you distressed by the idea that something is wrong with your mind during the past 7 days?',
					de: 'Wie sehr litten Sie in den letzten 7 Tagen unter dem Gedanken, dass irgendetwas mit Ihrem Verstand nicht in Ordnung ist?'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all',  de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'a little bit', de: 'ein wenig' } },
					{ value:2  , translations: { en: 'moderately', de: 'ziemlich' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'stark' } },
					{ value:4  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['BSI53', 'BSI53_53', 'item53', 'full-assessment', 'block1', 'full-assessment-block1_item53',]
		},


		// BIS/BAS: Behavioral Inhibition System/Behavior Activation System Scales

		// Der folgende Fragebogen enthält eine Reihe von Feststellungen, mit denen man sich selbst beschreiben kann. Diese Feststellungen können genau auf Sie zutreffen, eher zutreffen, eher nicht oder gar nicht auf Sie zutreffen. Zur Beantwortung des Fragebogens setzen Sie ein Kreuz in den entsprechenden Kreis. Bitte beantworten Sie jede Feststellung, auch wenn Sie einmal nicht sicher sind, welche Antwort für Sie zutrifft. Kreuzen Sie dann diejenige Antwort an, die noch am ehesten auf Sie zutrifft.

		// Eine eigene Familie ist die wichtigste Sache im Leben.
		{

			id:       'BISBAS_1',
			type:     'integer',
			meaning:  'Eine eigene Familie ist die wichtigste Sache im Leben.',
			translations: {
					en: 'A person’s family is the most important thing in life.',
					de: 'Eine eigene Familie ist die wichtigste Sache im Leben.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_1', 'item1', 'full-assessment', 'block2', 'full-assessment-block2_item1']
		},
		// Sogar wenn mir etwas Schlimmes bevorsteht, bin ich selten nervös oder ängstlich.
		{

			id:       'BISBAS_2',
			type:     'integer',
			meaning:  'Sogar wenn mir etwas Schlimmes bevorsteht, bin ich selten nervös oder ängstlich.',
			translations: {
					en: 'Even if something bad is about to happen to me, I rarely experience fear or nervousness.',
					de: 'Sogar wenn mir etwas Schlimmes bevorsteht, bin ich selten nervös oder ängstlich.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:      ['BISBAS', 'BISBAS_2', 'item2', 'full-assessment', 'block2', 'full-assessment-block2_item2', 'short-assessment', 'short-assessment-block2_item2']
		},
		// Ich strenge mich besonders an, damit ich erreiche, was ich möchte.
		{

			id:       'BISBAS_3',
			type:     'integer',
			meaning:  'Ich strenge mich besonders an, damit ich erreiche, was ich möchte.',
			translations: {
					en: 'I go out of my way to get things I want.',
					de: 'Ich strenge mich besonders an, damit ich erreiche, was ich möchte.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_3', 'item3', 'full-assessment', 'block2', 'full-assessment-block2_item3']
		},
		// Wenn etwas Gutes, das ich erwarte, nicht eintritt, bin ich eine Zeit lang weniger begeistert vom Leben.
		{

			id:       'BISBAS_29',
			type:     'integer',
			meaning:  'Wenn etwas Gutes, das ich erwarte, nicht eintritt, bin ich eine Zeit lang weniger begeistert vom Leben.',
			translations: {
					en: 'When something good I am expecting doesn’t happen, I feel less enthusiastic about life for a while.',
					de: 'Wenn etwas Gutes, das ich erwarte, nicht eintritt, bin ich eine Zeit lang weniger begeistert vom Leben.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_29', 'item29', 'full-assessment', 'block2', 'full-assessment-block2_item29']
		},
		// Wenn mir etwas gut gelingt, bleibe ich sehr gern bei der Sache.
		{

			id:       'BISBAS_4',
			type:     'integer',
			meaning:  'Wenn mir etwas gut gelingt, bleibe ich sehr gern bei der Sache.',
			translations: {
					en: 'When I’m doing well at something I love to keep at it.',
					de: 'Wenn mir etwas gut gelingt, bleibe ich sehr gern bei der Sache.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_4', 'item4', 'full-assessment', 'block2', 'full-assessment-block2_item4']
		},
		// Ich bin immer bereit, etwas Neues zu versuchen, wenn ich denke, dass es Spaß machen wird.
		{

			id:       'BISBAS_5',
			type:     'integer',
			meaning:  'Ich bin immer bereit, etwas Neues zu versuchen, wenn ich denke, dass es Spaß machen wird.',
			translations: {
					en: 'I’m always willing to try something new if I think it will be fun.',
					de: 'Ich bin immer bereit, etwas Neues zu versuchen, wenn ich denke, dass es Spaß machen wird.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_5', 'item5', 'full-assessment', 'block2', 'full-assessment-block2_item5']
		},

		// Es ist wichtig für mich, wie ich gekleidet bin.
		{

			id:       'BISBAS_6',
			type:     'integer',
			meaning:  'Es ist wichtig für mich, wie ich gekleidet bin.',
			translations: {
					en: 'How I dress is important to me.',
					de: 'Es ist wichtig für mich, wie ich gekleidet bin.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_6', 'item6', 'full-assessment', 'block2', 'full-assessment-block2_item6']
		},
		// Wenn ich erreiche, was ich will, bin ich voller Energie und Spannung.
		{

			id:       'BISBAS_7',
			type:     'integer',
			meaning:  'Wenn ich erreiche, was ich will, bin ich voller Energie und Spannung.',
			translations: {
					en: 'When I get something I want, I feel excited and energised.',
					de: 'Wenn ich erreiche, was ich will, bin ich voller Energie und Spannung.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_7', 'item7', 'full-assessment', 'block2', 'full-assessment-block2_item7']
		},
		// Kritik oder Beschimpfungen verletzen mich ziemlich stark.
		{

			id:       'BISBAS_8',
			type:     'integer',
			meaning:  'Kritik oder Beschimpfungen verletzen mich ziemlich stark.',
			translations: {
					en: 'Criticism or scolding hurts me quite a bit.',
					de: 'Kritik oder Beschimpfungen verletzen mich ziemlich stark.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_8', 'item8', 'full-assessment', 'block2', 'full-assessment-block2_item8', 'short-assessment', 'short-assessment-block2_item8']
		},
		// Wenn ich etwas haben will, tue ich gewöhnlich alles, um es zu bekommen.
		{

			id:       'BISBAS_9',
			type:     'integer',
			meaning:  'Wenn ich etwas haben will, tue ich gewöhnlich alles, um es zu bekommen.',
			translations: {
					en: 'When I want something I usually go all-out to get it.',
					de: 'Wenn ich etwas haben will, tue ich gewöhnlich alles, um es zu bekommen.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },
			],
			tags:     ['BISBAS', 'BISBAS_9', 'item9', 'full-assessment', 'block2', 'full-assessment-block2_item9']
		},
		// Ich werde oft Dinge nur deshalb tun, weil sie Spaß machen könnten.
		{

			id:       'BISBAS_10',
			type:     'integer',
			meaning:  'Ich werde oft Dinge nur deshalb tun, weil sie Spaß machen könnten.',
			translations: {
					en: 'I will often do things for no other reason than they might be fun.',
					de: 'Ich werde oft Dinge nur deshalb tun, weil sie Spaß machen könnten.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },
			],
			tags:     ['BISBAS', 'BISBAS_10', 'item10', 'full-assessment', 'block2', 'full-assessment-block2_item10']
		},
		// Wenn ich nicht bekomme, was ich will, verliere ich Interesse an meinen alltäglichen Aufgaben.
		{

			id:       'BISBAS_27',
			type:     'integer',
			meaning:  'Wenn ich nicht bekomme, was ich will, verliere ich Interesse an meinen alltäglichen Aufgaben.',
			translations: {
					en: 'When I don’t get what I want, I lose interest in my day-to-day tasks.',
					de: 'Wenn ich nicht bekomme, was ich will, verliere ich Interesse an meinen alltäglichen Aufgaben.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },
			],
			tags:     ['BISBAS', 'BISBAS_27', 'item27', 'full-assessment', 'block2', 'full-assessment-block2_item27']
		},
		// Es ist schwierig für mich, Zeit für solche Dinge wie Friseurbesuche zu finden.
		{

			id:       'BISBAS_11',
			type:     'integer',
			meaning:  'Es ist schwierig für mich, Zeit für solche Dinge wie Friseurbesuche zu finden.',
			translations: {
					en: 'It’s hard for me to find the time to do things such as get a haircut.',
					de: 'Es ist schwierig für mich, Zeit für solche Dinge wie Friseurbesuche zu finden.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_11', 'item11', 'full-assessment', 'block2', 'full-assessment-block2_item11']
		},
		// Wenn ich eine Chance sehe, etwas Erwünschtes zu bekommen, versuche ich sofort mein Glück.
		{

			id:       'BISBAS_12',
			type:     'integer',
			meaning:  'Wenn ich eine Chance sehe, etwas Erwünschtes zu bekommen, versuche ich sofort mein Glück.',
			translations: {
					en: 'If I see a chance to get something I want I move in on it right away.',
					de: 'Wenn ich eine Chance sehe, etwas Erwünschtes zu bekommen, versuche ich sofort mein Glück.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_12', 'item12', 'full-assessment', 'block2', 'full-assessment-block2_item12']
		},
		// Ich bin ziemlich besorgt oder verstimmt, wenn ich glaube oder weiß, dass jemand wütend auf mich ist.
		{

			id:       'BISBAS_13',
			type:     'integer',
			meaning:  'Ich bin ziemlich besorgt oder verstimmt, wenn ich glaube oder weiß, dass jemand wütend auf mich ist.',
			translations: {
					en: 'I feel pretty worried or upset when I know somebody is angry at me.',
					de: 'Ich bin ziemlich besorgt oder verstimmt, wenn ich glaube oder weiß, dass jemand wütend auf mich ist.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_13', 'item13', 'full-assessment', 'block2', 'full-assessment-block2_item13']
		},
		// Wenn ich eine Gelegenheit für etwas sehe, das ich mag, bin ich sofort voller Spannung.
		{

			id:       'BISBAS_14',
			type:     'integer',
			meaning:  'Wenn ich eine Gelegenheit für etwas sehe, das ich mag, bin ich sofort voller Spannung.',
			translations: {
					en: 'When I see an opportunity for something I like I get excited right away.',
					de: 'Wenn ich eine Gelegenheit für etwas sehe, das ich mag, bin ich sofort voller Spannung.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_14', 'item14', 'full-assessment', 'block2', 'full-assessment-block2_item14']
		},
		// Wenn ich hart an etwas gearbeitet habe, verliere ich die Motivation, wenn ich nicht so belohnt werde, wie ich es verdient hätte.
		{

			id:       'BISBAS_28',
			type:     'integer',
			meaning:  'Wenn ich hart an etwas gearbeitet habe, verliere ich die Motivation, wenn ich nicht so belohnt werde, wie ich es verdient hätte.',
			translations: {
					en: 'If I have been working hard at something, I lose motivation if I don’t get the reward I deserve.',
					de: 'Wenn ich hart an etwas gearbeitet habe, verliere ich die Motivation, wenn ich nicht so belohnt werde, wie ich es verdient hätte.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_28', 'item28', 'full-assessment', 'block2', 'full-assessment-block2_item28']
		},
		// Ich handle oft so, wie es mir gerade in den Sinn kommt.
		{

			id:       'BISBAS_15',
			type:     'integer',
			meaning:  'Ich handle oft so, wie es mir gerade in den Sinn kommt.',
			translations: {
					en: 'I often act on the spur of the moment.',
					de: 'Ich handle oft so, wie es mir gerade in den Sinn kommt.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_15', 'item15', 'full-assessment', 'block2', 'full-assessment-block2_item15']
		},
		// Wenn ich glaube, dass mir etwas Unangenehmes bevorsteht, bin ich gewöhnlich ziemlich unruhig.
		{

			id:       'BISBAS_16',
			type:     'integer',
			meaning:  'Wenn ich glaube, dass mir etwas Unangenehmes bevorsteht, bin ich gewöhnlich ziemlich unruhig.',
			translations: {
					en: 'If I think something unpleasant is going to happen I usually get pretty worked up.',
					de: 'Wenn ich glaube, dass mir etwas Unangenehmes bevorsteht, bin ich gewöhnlich ziemlich unruhig.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },
			],
			tags:     ['BISBAS', 'BISBAS_16', 'item16', 'full-assessment', 'block2', 'full-assessment-block2_item16']
		},
		// Ich wundere mich oft über das menschliche Verhalten.
		{

			id:       'BISBAS_17',
			type:     'integer',
			meaning:  'Ich wundere mich oft über das menschliche Verhalten.',
			translations: {
					en: 'I often wonder why people act the way they do.',
					de: 'Ich wundere mich oft über das menschliche Verhalten.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_17', 'item17', 'full-assessment', 'block2', 'full-assessment-block2_item17']
		},
		// Wenn mir etwas Schönes passiert, berührt mich das sehr stark.
		{

			id:       'BISBAS_18',
			type:     'integer',
			meaning:  'Wenn mir etwas Schönes passiert, berührt mich das sehr stark.',
			translations: {
					en: 'When good things happen to me, it affects me strongly.',
					de: 'Wenn mir etwas Schönes passiert, berührt mich das sehr stark.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_18', 'item18', 'full-assessment', 'block2', 'full-assessment-block2_item18']
		},
		// Ich bin besorgt, wenn ich glaube, dass ich eine wichtige Sache schlecht gemacht habe.
		{

			id:       'BISBAS_19',
			type:     'integer',
			meaning:  'Ich bin besorgt, wenn ich glaube, dass ich eine wichtige Sache schlecht gemacht habe.',
			translations: {
					en: 'I feel worried when I think I have done poorly at something.',
					de: 'Ich bin besorgt, wenn ich glaube, dass ich eine wichtige Sache schlecht gemacht habe.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_19', 'item19', 'full-assessment', 'block2', 'full-assessment-block2_item19']
		},
		// Wenn ein Ereignis abgesagt wird, auf das ich mich gefreut habe, verliere ich die Energie um eine Alternative zu arrangieren.
		{

			id:       'BISBAS_26',
			type:     'integer',
			meaning:  'Wenn ein Ereignis abgesagt wird, auf das ich mich gefreut habe, verliere ich die Energie um eine Alternative zu arrangieren.',
			translations: {
					en: 'When an event I am looking forward to is cancelled, I lose the energyto arrange an alternative.',
					de: 'Wenn ein Ereignis abgesagt wird, auf das ich mich gefreut habe, verliere ich die Energie um eine Alternative zu arrangieren.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_26', 'item26', 'full-assessment', 'block2', 'full-assessment-block2_item26']
		},
		// Ich brauche Abwechslung und neue Erfahrungen.
		{

			id:       'BISBAS_20',
			type:     'integer',
			meaning:  'Ich brauche Abwechslung und neue Erfahrungen.',
			translations: {
					en: 'I crave excitement and new sensations.',
					de: 'Ich brauche Abwechslung und neue Erfahrungen.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_20', 'item20', 'full-assessment', 'block2', 'full-assessment-block2_item20']
		},
		// Wenn ich etwas erreichen will, verfolge ich hartnäckig mein Ziel.
		{

			id:       'BISBAS_21',
			type:     'integer',
			meaning:  'Wenn ich etwas erreichen will, verfolge ich hartnäckig mein Ziel.',
			translations: {
					en: 'When I go after something I use a no holds barred approach.',
					de: 'Wenn ich etwas erreichen will, verfolge ich hartnäckig mein Ziel.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_21', 'item21', 'full-assessment', 'block2', 'full-assessment-block2_item21']
		},
		// Verglichen mit meinen Freunden habe ich sehr wenig Ängste.
		{

			id:       'BISBAS_22',
			type:     'integer',
			meaning:  'Verglichen mit meinen Freunden habe ich sehr wenig Ängste.',
			translations: {
					en: 'I have very few fears compared to my friends.',
					de: 'Verglichen mit meinen Freunden habe ich sehr wenig Ängste.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_22', 'item22', 'full-assessment', 'block2', 'full-assessment-block2_item22', 'short-assessment', 'short-assessment-block2_item22']
		},
		// Ich fände es sehr aufregend, einen Wettbewerb zu gewinnen.
		{

			id:       'BISBAS_23',
			type:     'integer',
			meaning:  'Ich fände es sehr aufregend, einen Wettbewerb zu gewinnen.',
			translations: {
					en: 'It would excite me to win a contest.',
					de: 'Ich fände es sehr aufregend, einen Wettbewerb zu gewinnen.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_23', 'item23', 'full-assessment', 'block2', 'full-assessment-block2_item23']
		},
		// Wenn die Umstände mich am Erreichen eines wichtigen Ziels hindern, fällt es mir schwer es weiterhin zu versuchen.
		{

			id:       'BISBAS_25',
			type:     'integer',
			meaning:  'Wenn die Umstände mich am Erreichen eines wichtigen Ziels hindern, fällt es mir schwer es weiterhin zu versuchen.',
			translations: {
					en: 'When circumstances prevent me from achieving an important goal, I find it hard to keep trying.',
					de: 'Wenn die Umstände mich am Erreichen eines wichtigen Ziels hindern, fällt es mir schwer es weiterhin zu versuchen.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },

			],
			tags:     ['BISBAS', 'BISBAS_25', 'item25', 'full-assessment', 'block2', 'full-assessment-block2_item25']
		},
		// Ich habe Angst, Fehler zu machen.
		{

			id:       'BISBAS_24',
			type:     'integer',
			meaning:  'Ich habe Angst, Fehler zu machen.',
			translations: {
					en: 'I worry about making mistakes.',
					de: 'Ich habe Angst, Fehler zu machen.'

			},
			options:    [
					{ value:1  , translations: { en: 'very true for me', de: 'trifft für mich genau zu' } },
					{ value:2  , translations: { en: 'somewhat true for me', de: 'trifft für mich eher zu' } },
					{ value:3  , translations: { en: 'somewhat false for me', de: 'trifft für mich eher nicht zu' } },
					{ value:4  , translations: { en: 'very false for me', de: 'trifft für mich gar nicht zu' } },
			],
			tags:     ['BISBAS', 'BISBAS_24', 'item24', 'full-assessment', 'block2', 'full-assessment-block2_item24', 'short-assessment', 'short-assessment-block2_item24']
		},

		// SUD-Skala: Subjective Units of Discomfort/Distress/Disturbance - Skala

		// Denken Sie an die schlimmste Angst, die Sie je erlebt haben oder die Sie sich vorstellen können, und geben Sie dieser die Zahl 100. Denken Sie nun an einen Zustand der absoluten Ruhe und geben Sie diesem die Zahl Null. Nun haben Sie eine Skala. Wie schätzen Sie sich auf dieser Skala in diesem Moment ein?

		{

			id:       'SUD',
			type:     'integer',
			meaning:   'Denken Sie an die schlimmste Angst, die Sie je erlebt haben oder die Sie sich vorstellen können, und geben Sie dieser die Zahl 100. Denken Sie nun an einen Zustand der absoluten Ruhe und geben Sie diesem die Zahl Null. Nun haben Sie eine Skala. Wie schätzen Sie sich auf dieser Skala in diesem Moment ein?',
			translations: {
					en: 'Think of the worst anxiety you have ever experienced or can imagine experiencing, and assign to this the number 100. Now think of the state of being absolutely calm, and call this zero. Now you have a scale. On this scale how do you rate yourself at this moment?',
					de: 'Denken Sie an die schlimmste Angst, die Sie je erlebt haben oder die Sie sich vorstellen können, und geben Sie dieser die Zahl 100. Denken Sie nun an einen Zustand der absoluten Ruhe und geben Sie diesem die Zahl Null. Nun haben Sie eine Skala. Wie schätzen Sie sich auf dieser Skala in diesem Moment ein?'

			},
			options:    [
					{ value:0  , translations: { en: 'totally relaxed',  de: 'völlig entspannt'   } },
					{ value:10  ,translations: { en: 'alert and awake, concentrating well',  de: 'aufmerksam und wach, mit guter Konzentration'   } },
					{ value:20  , translations: { en: 'minimal anxiety/distress',  de: 'geringfügig ängstlich/besorgt'   } },
					{ value:30  , translations: { en: 'mild anxiety/distress, no inteference with performance',  de: 'leicht ängstlich/gestresst, keine Beeinträchtigung der Leistungsfähigkeit'   } },
					{ value:40  , meaning: '' },
					{ value:50  , translations: { en: 'moderate anxiety/distress, uncomfortable but can continue to perform',  de: 'mäßig ängstlich/gestresst, sich unwohl fühlen, jedoch weiterarbeiten können'   } },
					{ value:60  , meaning: '' },
					{ value:70  , translations: { en: 'quite anxious/distressed, interfering with performance', de: 'ziemlich ängstlich/gestresst, was die Leistungsfähigkeit beeinträchtigt'  } },
					{ value:80  , translations: { en: 'very anxious/distressed, can not concentrate', de: 'sehr ängstlich/gestresst, kann sich nicht konzentrieren'  } },
					{ value:90  , translations: { en: 'extremely anxious/distressed', de: 'extrem ängstlich/verzweifelt'  } },
					{ value:100 , translations: { en: 'highest distress/fear/anxiety/discomfort that you have ever felt', de: 'höchste Belastung/Angst// Beunruhigung, die Sie je empfunden haben'  } }

			],
			tags:     ['SUD', 'scale', 'full-assessment', 'block3', 'full-assessment-block3', 'short-assessment', 'short-assessment-block3', 'T1-T59']
		},
		// CTS: Childhood Trauma Screener

		// Die folgenden Fragen beziehen sich auf Ihre Kindheit und Jugend. Auch wenn die Fragen sehr persönlich sind, möchten wir Sie bitten, so ehrlich wie möglich zu antworten. Es gibt keine richtigen oder falschen Antworten. Bitte kreuzen Sie hinter jeder Frage die Antwort an, die auf Sie am besten zutrifft.

		// Als ich aufwuchs hatte ich das Gefühl, geliebt zu werden.
		{

			id:       'CTS_1',
			type:     'integer',
			meaning:  'Als ich aufwuchs hatte ich das Gefühl, geliebt zu werden.',
			translations: {
					en: 'Growing up I felt loved.',
					de: 'Als ich aufwuchs hatte ich das Gefühl, geliebt zu werden.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'gar nicht' } },
					{ value:1  , translations: { en: 'rarely', de: 'selten' } },
					{ value:2  , translations: { en: 'sometimes', de: 'einige Male' } },
					{ value:3  , translations: { en: 'often', de: 'häufig' } },
					{ value:4  , translations: { en: 'very often', de: 'sehr häufig' } },

			],
			tags:     ['CTS', 'CTS_1', 'item1', 'full-assessment-baseline-T0', 'block3']
		},
		// Als ich aufwuchs schlugen mich Personen aus meiner Familie so stark, dass ich blaue Flecken oder Schrammen davontrug.
		{

			id:       'CTS_2',
			type:     'integer',
			meaning:  'Als ich aufwuchs schlugen mich Personen aus meiner Familie so stark, dass ich blaue Flecken oder Schrammen davontrug.',
			translations: {
					en: 'When I was growing up, people in my family would beat me so badly that I would get bruises or scrapes.',
					de: 'Als ich aufwuchs schlugen mich Personen aus meiner Familie so stark, dass ich blaue Flecken oder Schrammen davontrug.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'gar nicht' } },
					{ value:1  , translations: { en: 'rarely', de: 'selten' } },
					{ value:2  , translations: { en: 'sometimes', de: 'einige Male' } },
					{ value:3  , translations: { en: 'often', de: 'häufig' } },
					{ value:4  , translations: { en: 'very often', de: 'sehr häufig' } },

			],
			tags:     ['CTS', 'CTS_2', 'item2', 'full-assessment-baseline-T0', 'block3']
		},
		// Als ich aufwuchs hatte ich das Gefühl, es hasst mich jemand aus meiner Familie.
		{

			id:       'CTS_3',
			type:     'integer',
			meaning:  'Als ich aufwuchs hatte ich das Gefühl, es hasst mich jemand aus meiner Familie.',
			translations: {
					en: 'Growing up I felt like someone in my family hated me.',
					de: 'Als ich aufwuchs hatte ich das Gefühl, es hasst mich jemand aus meiner Familie.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'gar nicht' } },
					{ value:1  , translations: { en: 'rarely', de: 'selten' } },
					{ value:2  , translations: { en: 'sometimes', de: 'einige Male' } },
					{ value:3  , translations: { en: 'often', de: 'häufig' } },
					{ value:4  , translations: { en: 'very often', de: 'sehr häufig' } },

			],
			tags:     ['CTS', 'CTS_3', 'item3', 'full-assessment-baseline-T0', 'block3']

		}, // Als ich aufwuchs belästigte mich jemand sexuell.
		{

			id:       'CTS_4',
			type:     'integer',
			meaning:  'Als ich aufwuchs belästigte mich jemand sexuell.',
			translations: {
					en: 'When I was growing up someone sexually molested me.',
					de: 'Als ich aufwuchs belästigte mich jemand sexuell.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'gar nicht' } },
					{ value:1  , translations: { en: 'rarely', de: 'selten' } },
					{ value:2  , translations: { en: 'sometimes', de: 'einige Male' } },
					{ value:3  , translations: { en: 'often', de: 'häufig' } },
					{ value:4  , translations: { en: 'very often', de: 'sehr häufig' } },

			],
			tags:     ['CTS', 'CTS_4', 'item4', 'full-assessment-baseline-T0', 'block3']

		}, // Als ich aufwuchs gab es jemanden, der mich zum Arzt brachte, wenn ich es brauchte.
		{

			id:       'CTS_5',
			type:     'integer',
			meaning:  'Als ich aufwuchs gab es jemanden, der mich zum Arzt brachte, wenn ich es brauchte.',
			translations: {
					en: 'Growing up there was someone who would take me to the doctor when I needed it.',
					de: 'Als ich aufwuchs gab es jemanden, der mich zum Arzt brachte, wenn ich es brauchte.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'gar nicht' } },
					{ value:1  , translations: { en: 'rarely', de: 'selten' } },
					{ value:2  , translations: { en: 'sometimes', de: 'einige Male' } },
					{ value:3  , translations: { en: 'often', de: 'häufig' } },
					{ value:4  , translations: { en: 'very often', de: 'sehr häufig' } },

			],
			tags:     ['CTS', 'CTS_5', 'item5', 'full-assessment-baseline-T0', 'block3']
		},
		// SRRS:
		// Um Ihr Stressniveau zu ermitteln, entscheiden Sie für jedes dieser Lebensereignisse, ob es bei Ihnen in den letzten 12 Monaten eingetreten ist. Kreuzen Sie bitte Ja oder Nein an. 

		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Tod des Partners?

			id:       'SRRS_1',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Tod des Partners?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Death of a spouse?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Tod des Partners?'

			},
			tags:     ['SRRS', 'SRRS_1', 'item1', 'full-assessment', 'block2', 'full-assessment-block2_item1']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Scheidung?

			id:       'SRRS_2',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Scheidung?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Divorce?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Scheidung?'

			},
			tags: ['SRRS', 'SRRS_2', 'item2', 'full-assessment', 'block2', 'full-assessment-block2_item2']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Trennung vom Ehepartner?

			id:       'SRRS_3',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Trennung vom Ehepartner?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Marital Separation?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Trennung vom Ehepartner?'

			},
			tags: ['SRRS', 'SRRS_3', 'item3', 'full-assessment', 'block2', 'full-assessment-block2_item3']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Gefängnisstrafe/Haftstrafe?

			id:       'SRRS_4',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Gefängnisstrafe/Haftstrafe?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Jail Term?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Gefängnisstrafe/Haftstrafe?'

			},
			tags: ['SRRS', 'SRRS_4', 'item4', 'full-assessment', 'block2', 'full-assessment-block2_item4']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Tod eines nahen Familienangehörigen?

			id:       'SRRS_5',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Tod eines nahen Familienangehörigen?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Death of a close family member?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Tod eines nahen Familienangehörigen?'

			},
			tags:  ['SRRS', 'SRRS_5', 'item5', 'full-assessment', 'block2', 'full-assessment-block2_item5']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Eigene Verletzung oder Krankheit?

			id:       'SRRS_6',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Eigene Verletzung oder Krankheit?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Personal injury or illness ?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: ?Eigene Verletzung oder Krankheit'

			},
			tags: ['SRRS', 'SRRS_6', 'item6', 'full-assessment', 'block2', 'full-assessment-block2_item6']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Heirat?

			id:       'SRRS_7',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Heirat?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Marriage?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Heirat?'

			},
			tags: ['SRRS', 'SRRS_7', 'item7', 'full-assessment', 'block2', 'full-assessment-block2_item7']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Verlust des Arbeitsplatzes?

			id:       'SRRS_8',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Verlust des Arbeitsplatzes?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Fired at work?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Verlust des Arbeitsplatzes?'

			},
			tags: ['SRRS', 'SRRS_8', 'item8', 'full-assessment', 'block2', 'full-assessment-block2_item8']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Versöhnung mit dem Ehepartner?

			id:       'SRRS_9',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Versöhnung mit dem Ehepartner?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Marital reconciliation?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Versöhnung mit dem Ehepartner?'

			},
			tags: ['SRRS', 'SRRS_9', 'item9', 'full-assessment', 'block2', 'full-assessment-block2_item9']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Pensionierung/Ruhestand?

			id:       'SRRS_10',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Pensionierung/Ruhestand?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Retirement?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Pensionierung/Ruhestand?'

			},
			tags: ['SRRS', 'SRRS_10', 'item10', 'full-assessment', 'block2', 'full-assessment-block2_item10']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung im Gesundheitszustand eines Familienmitglieds?

			id:       'SRRS_11',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung im Gesundheitszustand eines Familienmitglieds?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in health of family member?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung im Gesundheitszustand eines Familienmitglieds?'

			},
			tags: ['SRRS', 'SRRS_11', 'item11', 'full-assessment', 'block2', 'full-assessment-block2_item11']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schwangerschaft?

			id:       'SRRS_12',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schwangerschaft?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Pregnancy?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schwangerschaft?'

			},
			tags: ['SRRS', 'SRRS_12', 'item12', 'full-assessment', 'block2', 'full-assessment-block2_item12']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Sexuelle Schwierigkeiten?

			id:       'SRRS_13',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Sexuelle Schwierigkeiten?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Sex difficulties?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Sexuelle Schwierigkeiten?'

			},
			tags: ['SRRS', 'SRRS_13', 'item13', 'full-assessment', 'block2', 'full-assessment-block2_item13']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Familienzuwachs?

			id:       'SRRS_14',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Familienzuwachs?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Gain of new family member?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Familienzuwachs?'

			},
			tags: ['SRRS', 'SRRS_14', 'item14', 'full-assessment', 'block2', 'full-assessment-block2_item14']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Geschäftliche Veränderung?

			id:       'SRRS_15',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Geschäftliche Veränderung?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Business readjustment?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Geschäftliche Veränderung?'

			},
			tags: ['SRRS', 'SRRS_15', 'item15', 'full-assessment', 'block2', 'full-assessment-block2_item15']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Erhebliche Einkommensveränderung?

			id:       'SRRS_16',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Erhebliche Einkommensveränderung?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in financial state?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Erhebliche Einkommensveränderung?'

			},
			tags: ['SRRS', 'SRRS_16', 'item16', 'full-assessment', 'block2', 'full-assessment-block2_item16']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Tod eines nahen Freundes?

			id:       'SRRS_17',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Tod eines nahen Freundes?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Death of close friend?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Tod eines nahen Freundes?'

			},
			tags: ['SRRS', 'SRRS_17', 'item17', 'full-assessment', 'block2', 'full-assessment-block2_item17']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Berufswechsel?

			id:       'SRRS_18',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Berufswechsel?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change to a different line of work?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Berufswechsel?'

			},
			tags: ['SRRS', 'SRRS_18', 'item18', 'full-assessment', 'block2', 'full-assessment-block2_item18']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Häufigkeit der Auseinandersetzungen mit dem Partner?

			id:       'SRRS_19',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Häufigkeit der Auseinandersetzungen mit dem Partner?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in number of arguments with spouse?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Häufigkeit der Auseinandersetzungen mit dem Partner?'

			},
			tags: ['SRRS', 'SRRS_19', 'item19', 'full-assessment', 'block2', 'full-assessment-block2_item19']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schulden über 51.000.- €?

			id:       'SRRS_20',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schulden über 51.000.- €?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: A large mortgage or loan?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schulden über 51.000.- €?'

			},
			tags: ['SRRS', 'SRRS_20', 'item20', 'full-assessment', 'block2', 'full-assessment-block2_item20']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Kündigung eines Darlehens?

			id:       'SRRS_21',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Kündigung eines Darlehens?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Foreclosure of mortgage or loan?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Kündigung eines Darlehens?'

			},
			tags: ['SRRS', 'SRRS_21', 'item21', 'full-assessment', 'block2', 'full-assessment-block2_item21']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Veränderung des beruflichen Verantwortungsbereich?

			id:       'SRRS_22',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Veränderung des beruflichen Verantwortungsbereich?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in responsibilities at work?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Veränderung des beruflichen Verantwortungsbereich?'

			},
			tags: ['SRRS', 'SRRS_22', 'item22', 'full-assessment', 'block2', 'full-assessment-block2_item22']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Kinder verlassen das Elternhaus?

			id:       'SRRS_23',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Kinder verlassen das Elternhaus?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Son or daughter leaving home?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Kinder verlassen das Elternhaus?'

			},
			tags: ['SRRS', 'SRRS_23', 'item23', 'full-assessment', 'block2', 'full-assessment-block2_item23']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Probleme mit angeheirateten Verwandten?

			id:       'SRRS_24',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Probleme mit angeheirateten Verwandten?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Trouble with in-laws?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Probleme mit angeheirateten Verwandten?'

			},
			tags: ['SRRS', 'SRRS_24', 'item24', 'full-assessment', 'block2', 'full-assessment-block2_item24']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Großer persönlicher Erfolg?

			id:       'SRRS_25',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Großer persönlicher Erfolg?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Outstanding personal achievement?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Großer persönlicher Erfolg?'

			},
			tags: ['SRRS', 'SRRS_25', 'item25', 'full-assessment', 'block2', 'full-assessment-block2_item25']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Anfang oder Ende der Berufstätigkeit des Partners?

			id:       'SRRS_26',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Anfang oder Ende der Berufstätigkeit des Partners?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Spouse begins or stops work?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Anfang oder Ende der Berufstätigkeit des Partners?'

			},
			tags: ['SRRS', 'SRRS_26', 'item26', 'full-assessment', 'block2', 'full-assessment-block2_item26']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schulbeginn oder -abschluss?

			id:       'SRRS_27',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schulbeginn oder -abschluss?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Begin or end school/college?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schulbeginn oder -abschluss?'

			},
			tags: ['SRRS', 'SRRS_27', 'item27', 'full-assessment', 'block2', 'full-assessment-block2_item27']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung des Lebensstandards?

			id:       'SRRS_28',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung des Lebensstandards?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in living conditions?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung des Lebensstandards?'

			},
			tags: ['SRRS', 'SRRS_28', 'item28', 'full-assessment', 'block2', 'full-assessment-block2_item28']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung persönlicher Gewohnheiten?

			id:       'SRRS_29',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung persönlicher Gewohnheiten?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Revision of personal habits?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung persönlicher Gewohnheiten?'

			},
			tags: ['SRRS', 'SRRS_29', 'item29', 'full-assessment', 'block2', 'full-assessment-block2_item29']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Probleme mit dem Vorgesetzten?

			id:       'SRRS_30',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Probleme mit dem Vorgesetzten?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Trouble with boss?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Probleme mit dem Vorgesetzten?'

			},
			tags: ['SRRS', 'SRRS_30', 'item30', 'full-assessment', 'block2', 'full-assessment-block2_item30']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung von Arbeitszeit oder -bedingungen?

			id:       'SRRS_31',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung von Arbeitszeit oder -bedingungen?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in work hours or conditions?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung von Arbeitszeit oder -bedingungen?'

			},
			tags: ['SRRS', 'SRRS_31', 'item31', 'full-assessment', 'block2', 'full-assessment-block2_item31']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Wohnungswechsel?

			id:       'SRRS_32',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Wohnungswechsel?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in residence?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Wohnungswechsel?'

			},
			tags: ['SRRS', 'SRRS_32', 'item32', 'full-assessment', 'block2', 'full-assessment-block2_item32']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Wechsel der Schule/Ausbildungsstätte/Universität?

			id:       'SRRS_33',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Wechsel der Schule/Ausbildungsstätte/Universität?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in school/college?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Wechsel der Schule/Ausbildungsstätte/Universität?'

			},
			tags: ['SRRS', 'SRRS_33', 'item33', 'full-assessment', 'block2', 'full-assessment-block2_item33']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Freizeitgewohnheiten?

			id:       'SRRS_34',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Freizeitgewohnheiten?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in recreation?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Freizeitgewohnheiten?'

			},
			tags: ['SRRS', 'SRRS_34', 'item34', 'full-assessment', 'block2', 'full-assessment-block2_item34']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der religiösen Gewohnheiten?

			id:       'SRRS_35',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der religiösen Gewohnheiten?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in church activities?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der religiösen Gewohnheiten?'

			},
			tags: ['SRRS', 'SRRS_35', 'item35', 'full-assessment', 'block2', 'full-assessment-block2_item35']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der sozialen Gewohnheiten?

			id:       'SRRS_36',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der sozialen Gewohnheiten?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in social activities?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der sozialen Gewohnheiten?'

			},
			tags: ['SRRS', 'SRRS_36', 'item36', 'full-assessment', 'block2', 'full-assessment-block2_item36']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schulden unter 51.000.- €?

			id:       'SRRS_37',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schulden unter 51.000.- €?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: A moderate loan or mortgage?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Schulden unter 51.000.- €?'

			},
			tags: ['SRRS', 'SRRS_37', 'item37', 'full-assessment', 'block2', 'full-assessment-block2_item37']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Schlafgewohnheiten?

			id:       'SRRS_38',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Schlafgewohnheiten?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in sleeping habits?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Schlafgewohnheiten?'

			},
			tags: ['SRRS', 'SRRS_38', 'item38', 'full-assessment', 'block2', 'full-assessment-block2_item38']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Häufigkeit familiärer Zusammenkünfte?

			id:       'SRRS_39',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Häufigkeit familiärer Zusammenkünfte?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in number of family get-togethers?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Häufigkeit familiärer Zusammenkünfte?'

			},
			tags: ['SRRS', 'SRRS_39', 'item39', 'full-assessment', 'block2', 'full-assessment-block2_item39']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Essgewohnheiten?

			id:       'SRRS_40',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Essgewohnheiten?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Change in eating habits?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Änderung der Essgewohnheiten?'

			},
			tags: ['SRRS', 'SRRS_40', 'item40', 'full-assessment', 'block2', 'full-assessment-block2_item40']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Urlaub ?

			id:       'SRRS_41',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Urlaub ?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months:  Vacation?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Urlaub ?'

			},
			tags: ['SRRS', 'SRRS_41', 'item41', 'full-assessment', 'block2', 'full-assessment-block2_item41']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Feiertage?

			id:       'SRRS_42',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Feiertage?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: holidays?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Feiertage?'

			},
			tags: ['SRRS', 'SRRS_42', 'item42', 'full-assessment', 'block2', 'full-assessment-block2_item42']
		},
		{  // Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Geringfügige Gesetzesüberschreitungen?

			id:       'SRRS_43',
			type:     'boolean',
			meaning:    'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Geringfügige Gesetzesüberschreitungen?',
			translations: {
					en: 'Which of the following life events have occurred in your life in the last 12 months: Minor violations of the law?',
					de: 'Welche der folgenden Lebensereignisse sind in den letzten 12 Monaten in Ihrem Leben eingetreten: Geringfügige Gesetzesüberschreitungen?'

			},
			tags: ['SRRS', 'SRRS_43', 'item43', 'full-assessment', 'block2', 'full-assessment-block2_item43']
		},

		// PANAS: Positive and Negative Affect Schedule (POSAFF Subscale)

		// Dieser Fragebogen enthält eine Reihe von Wörtern, die unterschiedliche Gefühle und Empfindungen beschreiben. Lesen Sie jedes Wort und tragen dann in die Skala neben jedem Wort die Intensität ein. Sie haben die Möglichkeit, zwischen fünf Abstufungen zu wählen.

		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben.

		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: aktiv
		{

			id:       'PANAS_active',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: aktiv',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: active',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: aktiv'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_19', 'PANAS_aktive', 'item_active', 'full-assessment', 'block3', 'full-assessment-block3-item_active', 'short-assessment', 'short-assessment-block3-item_active', 'short-assessment-hedonic']
		},
		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: interessiert
		{

			id:       'PANAS_interessiert',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: interessiert',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: interested',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: interessiert'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_01', 'PANAS_interested', 'item_interested', 'full-assessment', 'block3', 'full-assessment-block3-item_interested', 'short-assessment', 'short-assessment-block3-item_interested', 'short-assessment-hedonic']
		},
		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: freudig erregt
		{

			id:       'PANAS_excited',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: freudig erregt',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: excited',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: freudig erregt'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_03', 'PANAS_excited', 'item_excited', 'full-assessment', 'block3', 'full-assessment-block3-item_excited']
		},
		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: stark
		{

			id:       'PANAS_strong',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: stark',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: strong',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: stark'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_05', 'PANAS_strong', 'item_strong', 'full-assessment', 'block3', 'full-assessment-block3-item_strong' ]
		},
		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: angeregt
		{

			id:       'PANAS_inspired',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: angeregt',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: inspired',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: angeregt'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_14', 'PANAS_inspired', 'item_inspired', 'full-assessment', 'block3', 'full-assessment-block3-item_inspired']
		},
		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: stolz
		{

			id:       'PANAS_proud',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: stolz',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: proud',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: stolz'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_10', 'PANAS_proud', 'item_proud', 'full-assessment', 'block3', 'full-assessment-block3-item_proud']
		},
		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: begeistert
		{

			id:       'PANAS_enthusiastic',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: begeistert',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: enthusiastic',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: begeistert'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_09', 'PANAS_enthusiastic', 'item_enthusiastic', 'full-assessment', 'block3', 'full-assessment-block3-item_enthusiastic', 'short-assessment', 'short-assessment-block3-item_enthusiastic', 'short-assessment-hedonic']
		},
		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: wach
		{

			id:       'PANAS_alert',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: wach',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: alert',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: wach'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_12', 'PANAS_alert', 'item_alert', 'full-assessment', 'block3', 'full-assessment-block3-item_alert']
		},
		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: entschlossen
		{

			id:       'PANAS_determined',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: entschlossen',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: determined',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: entschlossen'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_16', 'PANAS_determined', 'item_determined', 'full-assessment', 'block3', 'full-assessment-block3-item_determined', 'short-assessment', 'short-assessment-block3-item_determined', 'short-assessment-hedonic']
		},
		// Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: aufmerksam
		{

			id:       'PANAS_attentive',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: aufmerksam',
			translations: {
					en: 'Indicate the extent you have felt this way over the past week: attentive',
					de: 'Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben: aufmerksam'

			},
			options:    [
					{ value:0  , translations: { en: 'very slightly or not at all', de: 'ganz wenig oder gar nicht' } },
					{ value:1  , translations: { en: 'a little', de: 'ein bisschen' } },
					{ value:2  , translations: { en: 'moderately', de: 'einigermaßen' } },
					{ value:3  , translations: { en: 'quite a bit', de: 'erheblich' } },
					{ value:4  , translations: { en: 'extremely', de: 'äußerst' } },

			],
			tags:     ['PANAS', 'PANAS_17', 'PANAS_attentive', 'item_attentive', 'full-assessment', 'block3', 'full-assessment-block3-item_attentive' ]
		},
		// PVSS-21: Positive Valence Systems Scale Short Form

		// Geben Sie bitte an, inwieweit die folgenden Aussagen Ihre Reaktionen in den letzten zwei Wochen (einschließlich heute) beschreiben. Wenn eines der Erlebnisse NICHT auf Sie in den letzten zwei Wochen zutraf: kein Problem. Geben Sie in dem Fall bitte an, wie Sie in den letzten zwei Wochen reagiert hätten, wenn Sie die Situation erlebt hätten. Bitte berücksichtigen Sie dabei nur den in der Frage beschriebenen Aspekt der Situation, welcher durch eine Unterstreichung hervorgehoben ist. Wenn die Aussage beispielweise besagt „Ich wollte neue Menschen kennenlernen“, bewerten Sie bitte wie sehr Sie in den letzten zwei Wochen neue Menschen kennen lernen wollten oder es gewollt hätten, wenn sich die Gelegenheit ergeben hätte. Berücksichtigen Sie dabei nicht, was die Situation Ihnen abverlangt hätte oder, ob es für Sie möglich gewesen wäre neue Menschen kennen zu lernen.

		// Ich habe meinen ersten Bissen Essen genossen, wenn ich mich hungrig gefühlt hatte.
		{

			id:       'PVSS21_1',
			type:     'integer',
			meaning:  'Ich habe meinen ersten Bissen Essen genossen, wenn ich mich hungrig gefühlt hatte.',
			translations: {
					en: 'I savored my first bite of food after feeling hungry.',
					de: 'Ich habe meinen ersten Bissen Essen genossen, wenn ich mich hungrig gefühlt hatte.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_1', 'item1', 'full-assessment', 'block3', 'full-assessment-block3-item1']
		},
		// Ich habe Energie in Aktivitäten gesteckt, die mir Spaß machen.
		{

			id:       'PVSS21_2',
			type:     'integer',
			meaning:  'Ich habe Energie in Aktivitäten gesteckt, die mir Spaß machen.',
			translations: {
					en: 'I put energy into activities I enjoy.',
					de: 'Ich habe Energie in Aktivitäten gesteckt, die mir Spaß machen.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_2', 'item2', 'full-assessment', 'block3', 'full-assessment-block3-item2']
		},
		// Ich habe mich gefreut, draußen frische Luft zu schnappen.
		{

			id:       'PVSS21_3',
			type:     'integer',
			meaning:  'Ich habe mich gefreut, draußen frische Luft zu schnappen.',
			translations: {
					en: 'I was delighted to catch a breath of fresh air outdoors.',
					de: 'Ich habe mich gefreut, draußen frische Luft zu schnappen.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },
			],
			tags:     ['PVSS21', 'PVSS21_3', 'item3', 'full-assessment', 'block3', 'full-assessment-block3-item3']
		},
		// Ich wollte Zeit mit Menschen verbringen, die ich kenne.
		{

			id:       'PVSS21_4',
			type:     'integer',
			meaning:  'Ich wollte Zeit mit Menschen verbringen, die ich kenne.',
			translations: {
					en: 'I wanted to spend time with people I know.',
					de: 'Ich wollte Zeit mit Menschen verbringen, die ich kenne.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_4', 'item4', 'full-assessment', 'block3', 'full-assessment-block3-item4']
		},
		// Eine vergnügliche Aktivität am Wochenende hat meine gute Stimmung über die darauffolgende Woche aufrechterhalten.
		{

			id:       'PVSS21_5',
			type:     'integer',
			meaning:  'Eine vergnügliche Aktivität am Wochenende hat meine gute Stimmung über die darauffolgende Woche aufrechterhalten.',
			translations: {
					en: 'A fun activity during the weekend sustained my good mood.',
					de: 'Eine vergnügliche Aktivität am Wochenende hat meine gute Stimmung über die darauffolgende Woche aufrechterhalten.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_5', 'item5', 'full-assessment', 'block3', 'full-assessment-block3-item5']
		},
		// Es hat sich gut angefühlt körperlichen Kontakt mit jemandem zu haben, dem ich mich nahe fühlte.
		{

			id:       'PVSS21_6',
			type:     'integer',
			meaning:  'Es hat sich gut angefühlt körperlichen Kontakt mit jemandem zu haben, dem ich mich nahe fühlte.',
			translations: {
					en: 'It felt good to have physical contact with someone I felt close to.',
					de: 'Es hat sich gut angefühlt körperlichen Kontakt mit jemandem zu haben, dem ich mich nahe fühlte.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_6', 'item6', 'full-assessment', 'block3', 'full-assessment-block3-item6']
		},
		// Ich habe erwartet einen kurzen Moment im Freien zu genießen.
		{

			id:       'PVSS21_7',
			type:     'integer',
			meaning:  'Ich habe erwartet einen kurzen Moment im Freien zu genießen.',
			translations: {
					en: 'I expected to enjoy a brief moment outdoors.',
					de: 'Ich habe erwartet einen kurzen Moment im Freien zu genießen.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_7', 'item7', 'full-assessment', 'block3', 'full-assessment-block3-item7']
		},
		// Ich habe mich darauf gefreut Feedback zu meiner Arbeit zu bekommen.
		{

			id:       'PVSS21_8',
			type:     'integer',
			meaning:  'Ich habe mich darauf gefreut Feedback zu meiner Arbeit zu bekommen.',
			translations: {
					en: 'I looked forward to hearing feedback on my work.',
					de: 'Ich habe mich darauf gefreut Feedback zu meiner Arbeit zu bekommen.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_8', 'item8', 'full-assessment', 'block3', 'full-assessment-block3-item8']
		},
		// Ich habe erwartet meine Mahlzeiten zu genießen.
		{

			id:       'PVSS21_9',
			type:     'integer',
			meaning:  'Ich habe erwartet meine Mahlzeiten zu genießen.',
			translations: {
					en: 'I expected to enjoy my meals.',
					de: 'Ich habe erwartet meine Mahlzeiten zu genießen.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_9', 'item9', 'full-assessment', 'block3', 'full-assessment-block3-item9']
		},
		// Wenn ich für meine Arbeit gelobt wurde, war ich für den Rest des Tages zufrieden.
		{

			id:       'PVSS21_10',
			type:     'integer',
			meaning:  'Wenn ich für meine Arbeit gelobt wurde, war ich für den Rest des Tages zufrieden.',
			translations: {
					en: 'Receiving praise about my work made me feel pleased for the rest of the day.',
					de: 'Wenn ich für meine Arbeit gelobt wurde, war ich für den Rest des Tages zufrieden.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_10', 'item10', 'full-assessment', 'block3', 'full-assessment-block3-item10']
		},
		// Ich habe mich gefreut Zeit mit anderen zu verbringen.
		{

			id:       'PVSS21_11',
			type:     'integer',
			meaning:  'Ich habe mich gefreut Zeit mit anderen zu verbringen.',
			translations: {
					en: 'I looked forward to spending time with others.',
					de: 'Ich habe mich gefreut Zeit mit anderen zu verbringen.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_11', 'item11', 'full-assessment', 'block3', 'full-assessment-block3-item11']
		},
		// Ich wollte Ziele erreichen, die ich mir selbst gesetzt habe.
		{

			id:       'PVSS21_12',
			type:     'integer',
			meaning:  'Ich wollte Ziele erreichen, die ich mir selbst gesetzt habe.',
			translations: {
					en: 'I wanted to accomplish goals I set for myself.',
					de: 'Ich wollte Ziele erreichen, die ich mir selbst gesetzt habe.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_12', 'item12', 'full-assessment', 'block3', 'full-assessment-block3-item12']
		},
		// Ich habe erwartet es zu genießen, von jemanden umarmt zu werden, den ich liebe.
		{

			id:       'PVSS21_13',
			type:     'integer',
			meaning:  'Ich habe erwartet es zu genießen, von jemanden umarmt zu werden, den ich liebe.',
			translations: {
					en: 'I expected to enjoy being hugged by someone I love.',
					de: 'Ich habe erwartet es zu genießen, von jemanden umarmt zu werden, den ich liebe.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },
			],
			tags:     ['PVSS21', 'PVSS21_13', 'item13', 'full-assessment', 'block3', 'full-assessment-block3-item13']
		},
		// Ich wollte an spaßigen Aktivitäten mit Freunden teilnehmen.
		{

			id:       'PVSS21_14',
			type:     'integer',
			meaning:  'Ich wollte an spaßigen Aktivitäten mit Freunden teilnehmen.',
			translations: {
					en: 'I wanted to participate in a fun activity with friends.',
					de: 'Ich wollte an spaßigen Aktivitäten mit Freunden teilnehmen.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },
			],
			tags:     ['PVSS21', 'PVSS21_14', 'item14', 'full-assessment', 'block3', 'full-assessment-block3-item14']
		},
		// Ich habe hart gearbeitet, um positive Rückmeldungen für meine Projekte zu erhalten.
		{

			id:       'PVSS21_15',
			type:     'integer',
			meaning:  'Ich habe hart gearbeitet, um positive Rückmeldungen für meine Projekte zu erhalten.',
			translations: {
					en: 'I worked hard to earn positive feedback on my projects.',
					de: 'Ich habe hart gearbeitet, um positive Rückmeldungen für meine Projekte zu erhalten.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },
			],
			tags:     ['PVSS21', 'PVSS21_15', 'item15', 'full-assessment', 'block3', 'full-assessment-block3-item15']
		},
		// Ich habe mich auf eine bevorstehende Mahlzeit gefreut.
		{

			id:       'PVSS21_16',
			type:     'integer',
			meaning:  'Ich habe mich auf eine bevorstehende Mahlzeit gefreut.',
			translations: {
					en: 'I looked forward to an upcoming meal.',
					de: 'Ich habe mich auf eine bevorstehende Mahlzeit gefreut.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_16', 'item16', 'full-assessment', 'block3', 'full-assessment-block3-item16']
		},
		// Ich habe mich gefreut, wenn ich ein selbst gesetztes Ziel erreicht hatte.
		{

			id:       'PVSS21_17',
			type:     'integer',
			meaning:  'Ich habe mich gefreut, wenn ich ein selbst gesetztes Ziel erreicht hatte.',
			translations: {
					en: 'I felt pleased when I reached a goal I set for myself.',
					de: 'Ich habe mich gefreut, wenn ich ein selbst gesetztes Ziel erreicht hatte.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_17', 'item17', 'full-assessment', 'block3', 'full-assessment-block3-item17']
		},
		// Eine Umarmung von einer mir nahestehenden Person zu bekommen, hat mich sogar nach dem Abschied noch glücklich gemacht.
		{

			id:       'PVSS21_18',
			type:     'integer',
			meaning:  'Eine Umarmung von einer mir nahestehenden Person zu bekommen, hat mich sogar nach dem Abschied noch glücklich gemacht.',
			translations: {
					en: 'Getting a hug from someone close to me made me happy even after we parted.',
					de: 'Eine Umarmung von einer mir nahestehenden Person zu bekommen, hat mich sogar nach dem Abschied noch glücklich gemacht.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_18', 'item18', 'full-assessment', 'block3', 'full-assessment-block3-item18']
		},
		// Ich habe erwartet die Aufgaben zu meistern, die ich übernommen habe.
		{

			id:       'PVSS21_19',
			type:     'integer',
			meaning:  'Ich habe erwartet die Aufgaben zu meistern, die ich übernommen habe.',
			translations: {
					en: 'I expected to master the tasks I undertook.',
					de: 'Ich habe erwartet die Aufgaben zu meistern, die ich übernommen habe.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_19', 'item19', 'full-assessment', 'block3', 'full-assessment-block3-item19']
		},
		// Ich habe aktiv Aktivitäten verfolgt, von denen ich dachte, sie würden Spaß machen.
		{

			id:       'PVSS21_20',
			type:     'integer',
			meaning:  'Ich habe aktiv Aktivitäten verfolgt, von denen ich dachte, sie würden Spaß machen.',
			translations: {
					en: 'I actively pursued activities that I thought would be fun.',
					de: 'Ich habe aktiv Aktivitäten verfolgt, von denen ich dachte, sie würden Spaß machen.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_20', 'item20', 'full-assessment', 'block3', 'full-assessment-block3-item20']
		},
		// Ich habe mir große Mühe gegeben, die Schönheit um mich herum zu bewundern.
		{

			id:       'PVSS21_21',
			type:     'integer',
			meaning:  'Ich habe mir große Mühe gegeben, die Schönheit um mich herum zu bewundern.',
			translations: {
					en: 'I went out of my way to admire the beauty around me.',
					de: 'Ich habe mir große Mühe gegeben, die Schönheit um mich herum zu bewundern.'

			},
			options:    [
					{ value:1  , translations: { en: 'extremely untrue of me', de: 'extrem unzutreffend für mich' } },
					{ value:2  , translations: { en: 'very untrue of me', de: 'sehr unzutreffend für mich' } },
					{ value:3  , translations: { en: 'moderately untrue of me', de: 'moderat unzutreffend für mich' } },
					{ value:4  , translations: { en: 'slightly untrue of me', de: 'etwas unzutreffend für mich' } },
					{ value:5  , translations: { en: 'neutral', de: 'neutral' } },
					{ value:6  , translations: { en: 'slightly true of me', de: 'etwas zutreffend für mich' } },
					{ value:7  , translations: { en: 'moderately true of me', de: 'moderat zutreffend für mich' } },
					{ value:8  , translations: { en: 'very true of me', de: 'sehr zutreffend für mich' } },
					{ value:9  , translations: { en: 'extremely true of me', de: 'extrem zutreffend für mich' } },

			],
			tags:     ['PVSS21', 'PVSS21_21', 'item21', 'full-assessment', 'block3', 'full-assessment-block3-item21']
		},

		// WHODAS 2.0: WHO Disability Assessment Schedule 2.0 (single item friendship)

		// Bitte beantworten Sie die folgende Frage im Hinblick darauf, wie viele Schwierigkeiten, die aufgrund von Gesundheitsproblemen entstehen können, Sie in den letzten 30 Tagen bei dieser nachfolgenden Aktivität hatten.

		// Wie viele Schwierigkeiten hatten Sie in den letzten 30 Tagen:
		{

			id:       'WHODAS20_11',
			type:     'integer',
			meaning:  'Wie viele Schwierigkeiten hatten Sie in den letzten 30 Tagen eine Freundschaft aufrecht zu erhalten?',
			translations: {
					en: 'In the past 30 days, how much difficulty did you have in maintaining a friendship?',
					de: 'Wie viele Schwierigkeiten hatten Sie in den letzten 30 Tagen eine Freundschaft aufrecht zu erhalten?'

			},
			options:    [
					{ value:0  , translations: { en: 'none', de: 'keine' } },
					{ value:1  , translations: { en: 'low', de: 'geringe' } },
					{ value:2  , translations: { en: 'moderate', de: 'mäßige' } },
					{ value:3  , translations: { en: 'strong', de: 'starke' } },
					{ value:4  , translations: { en: 'very strong/ not possible', de: 'sehr starke/ nicht möglich' } },

			],
			tags:     ['WHODAS20', 'WHODAS20_11', 'item11', 'full-assessment', 'block3', 'full-assessment-block3-item11', 'short-assessment', 'short-assessment-block3-item11']
		},
		// TAS-20: Toronto Alexithymia Scale

		// Bitte schätzen Sie ein, inwieweit folgende Aussagen auf Sie zutreffen.

		// Mir ist oft unklar, welche Gefühle ich gerade habe.
		{

			id:       'TAS20_1',
			type:     'integer',
			meaning:  'Mir ist oft unklar, welche Gefühle ich gerade habe.',
			translations: {
					en: 'I am often confused about what emotion I am feeling.',
					de: 'Mir ist oft unklar, welche Gefühle ich gerade habe.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_1', 'item1', 'full-assessment', 'block4', 'full-assessment-block4-item1']
		},
		// Es fällt mir schwer, die richtigen Worte für meine Gefühle zu finden.
		{

			id:       'TAS20_2',
			type:     'integer',
			meaning:  'Es fällt mir schwer, die richtigen Worte für meine Gefühle zu finden.',
			translations: {
					en: 'It is difficult for me to find the right words for my feelings.',
					de: 'Es fällt mir schwer, die richtigen Worte für meine Gefühle zu finden.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_2', 'item2', 'full-assessment', 'block4', 'full-assessment-block4-item2']
		},
		// Ich habe körperliche Empfindungen, die sogar die Ärzte nicht verstehen.
		{

			id:       'TAS20_3',
			type:     'integer',
			meaning:  'Ich habe körperliche Empfindungen, die sogar die Ärzte nicht verstehen.',
			translations: {
					en: 'I have physical sensations that even doctors don’t understand.',
					de: 'Ich habe körperliche Empfindungen, die sogar die Ärzte nicht verstehen.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_3', 'item3', 'full-assessment', 'block4', 'full-assessment-block4-item3']

			},

		// Wenn mich etwas aus der Fassung gebracht hat, weiß ich oft nicht, ob ich traurig, ängstlich oder wütend bin.
		{

			id:       'TAS20_6',
			type:     'integer',
			meaning:  'Wenn mich etwas aus der Fassung gebracht hat, weiß ich oft nicht, ob ich traurig, ängstlich oder wütend bin.',
			translations: {
					en: 'When I am upset, I don’t know if I am sad, frightened, or angry.',
					de: 'Wenn mich etwas aus der Fassung gebracht hat, weiß ich oft nicht, ob ich traurig, ängstlich oder wütend bin.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_6', 'item6', 'full-assessment', 'block4', 'full-assessment-block4-item6']

			},

		// Ich bin oft über Vorgänge in meinem Körper verwirrt.
		{

			id:       'TAS20_7',
			type:     'integer',
			meaning:  'Ich bin oft über Vorgänge in meinem Körper verwirrt.',
			translations: {
					en: 'I am often puzzled by sensations in my body.',
					de: 'Ich bin oft über Vorgänge in meinem Körper verwirrt.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_7', 'item7', 'full-assessment', 'block4', 'full-assessment-block4-item7']

			},

		// Einige meiner Gefühle kann ich gar nicht richtig benennen.
		{

			id:       'TAS20_9',
			type:     'integer',
			meaning:  'Einige meiner Gefühle kann ich gar nicht richtig benennen.',
			translations: {
					en: 'I have feelings that I can’t quite identify.',
					de: 'Einige meiner Gefühle kann ich gar nicht richtig benennen.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_9', 'item9', 'full-assessment', 'block4', 'full-assessment-block4-item9']

			},

		// Ich finde es schwierig zu beschreiben, was ich für andere Menschen empfinde.
		{

			id:       'TAS20_11',
			type:     'integer',
			meaning:  'Ich finde es schwierig zu beschreiben, was ich für andere Menschen empfinde.',
			translations: {
					en: 'I find it hard to describe how I feel about people.',
					de: 'Ich finde es schwierig zu beschreiben, was ich für andere Menschen empfinde.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_11', 'item11', 'full-assessment', 'block4', 'full-assessment-block4-item11']

			},

		// Andere fordern mich auf, meine Gefühle zu beschreiben.
		{

			id:       'TAS20_12',
			type:     'integer',
			meaning:  'Andere fordern mich auf, meine Gefühle zu beschreiben.',
			translations: {
					en: 'People tell me to describe my feelings more.',
					de: 'Andere fordern mich auf, meine Gefühle zu beschreiben.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_12', 'item12', 'full-assessment', 'block4', 'full-assessment-block4-item12']

			},

		// Ich weiß nicht, was in mir vorgeht.
		{

			id:       'TAS20_13',
			type:     'integer',
			meaning:  'Ich weiß nicht, was in mir vorgeht.',
			translations: {
					en: 'I don’t know what’s going on inside me.',
					de: 'Ich weiß nicht, was in mir vorgeht.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_13', 'item13', 'full-assessment', 'block4', 'full-assessment-block4-item13']

			},

		// Ich weiß oft nicht, warum ich wütend bin.
		{

			id:       'TAS20_14',
			type:     'integer',
			meaning:  'Ich weiß oft nicht, warum ich wütend bin.',
			translations: {
					en: 'I often don’t know why I am angry.',
					de: 'Ich weiß oft nicht, warum ich wütend bin.'

			},
			options:    [
					{ value:1  , translations: { en: 'strongly disagree', de: 'trifft überhaupt nicht zu' } },
					{ value:2  , translations: { en: 'disagree', de: 'trifft eher nicht zu' } },
					{ value:3  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:4  , translations: { en: 'agree', de: 'trifft eher zu' } },
					{ value:5  , translations: { en: 'strongly agree', de: 'trifft vollständig zu' } },

			],
			tags:     ['TAS20', 'TAS20_14', 'item14', 'full-assessment', 'block4', 'full-assessment-block4-item14']

			},
		// IRI: Interpersonal Reactivity Index

		// Die folgenden Aussagen fragen Sie nach Ihren Gedanken und Gefühlen in einer Vielzahl von Situationen. Geben Sie für jede Aussage an, wie gut sie Sie beschreibt, indem Sie das entsprechende Kästchen ankreuzen. LESEN SIE JEDE AUFGABE SORGFÄLTIG DURCH BEVOR SIE ANTWORTEN. Antworten Sie so ehrlich wie möglich. Vielen Dank!

		// Ich empfinde oft warmherzige, sorgende Gefühle für Leute, denen es weniger gut geht als mir.
		{

			id:       'IRI_2',
			type:     'integer',
			meaning:  'Ich empfinde oft warmherzige, sorgende Gefühle für Leute, denen es weniger gut geht als mir.',
			translations: {
					en: 'I often have tender, concerned feelings for people less fortunate than me.',
					de: 'Ich empfinde oft warmherzige, sorgende Gefühle für Leute, denen es weniger gut geht als mir.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_2', 'item2', 'full-assessment', 'block4', 'full-assessment-block4-item2', 'short-assessment', 'short-assessment-block4-item2']

			},

		// Mir fällt es manchmal schwer, Dinge aus Sicht der anderen Person zu sehen.
		{

			id:       'IRI_3',
			type:     'integer',
			meaning:  'Mir fällt es manchmal schwer, Dinge aus Sicht der anderen Person zu sehen.',
			translations: {
					en: 'I sometimes find it difficult to see things from the other guy\'s point of view.',
					de: 'Mir fällt es manchmal schwer, Dinge aus Sicht der anderen Person zu sehen.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_3', 'item3', 'full-assessment', 'block4', 'full-assessment-block4-item3']

			},

		// Manchmal habe ich wenig Mitleid für andere Menschen, die gerade Probleme habe.
		{

			id:       'IRI_4',
			type:     'integer',
			meaning:  'Manchmal habe ich wenig Mitleid für andere Menschen, die gerade Probleme habe.',
			translations: {
					en: 'Sometimes I don\'t feel very sorry for other people when they are having problems.',
					de: 'Manchmal habe ich wenig Mitleid für andere Menschen, die gerade Probleme habe.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_4', 'item4', 'full-assessment', 'block4', 'full-assessment-block4-item4']

			},

		// Ich versuche bei Meinungsverschiedenheiten zuerst alle Ansichten zu betrachten, bevor ich eine Entscheidung treffe.
		{

			id:       'IRI_8',
			type:     'integer',
			meaning:  'Ich versuche bei Meinungsverschiedenheiten zuerst alle Ansichten zu betrachten, bevor ich eine Entscheidung treffe.',
			translations: {
					en: 'I try to look at everybody\'s side of a disagreement before I make a decision.',
					de: 'Ich versuche bei Meinungsverschiedenheiten zuerst alle Ansichten zu betrachten, bevor ich eine Entscheidung treffe.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_8', 'item8', 'full-assessment', 'block4', 'full-assessment-block4-item8']

			},

		// Wenn ich sehe, wie jemand ausgenutzt wird, habe ich das Gefühl, ihn schützen zu müssen.
		{

			id:       'IRI_9',
			type:     'integer',
			meaning:  'Wenn ich sehe, wie jemand ausgenutzt wird, habe ich das Gefühl, ihn schützen zu müssen.',
			translations: {
					en: 'When I see someone being taken advantage of, I feel kind of protective towards them.',
					de: 'Wenn ich sehe, wie jemand ausgenutzt wird, habe ich das Gefühl, ihn schützen zu müssen.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_9', 'item9', 'full-assessment', 'block4', 'full-assessment-block4-item9']

			},

		// Ich versuche manchmal, meine Freunde besser zu verstehen, indem ich mir vorstelle, wie die Dinge aus ihrer Sicht aussehen könnten.
		{

			id:       'IRI_11',
			type:     'integer',
			meaning:  'Ich versuche manchmal, meine Freunde besser zu verstehen, indem ich mir vorstelle, wie die Dinge aus ihrer Sicht aussehen könnten.',
			translations: {
					en: 'I sometimes try to understand my friends better by imagining how things look from their perspective.',
					de: 'Ich versuche manchmal, meine Freunde besser zu verstehen, indem ich mir vorstelle, wie die Dinge aus ihrer Sicht aussehen könnten.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_11', 'item11', 'full-assessment', 'block4', 'full-assessment-block4-item11']

			},

		// Das Unglück anderer lässt mich normalerweise weitgehend unberührt.
		{

			id:       'IRI_14',
			type:     'integer',
			meaning:  'Das Unglück anderer lässt mich normalerweise weitgehend unberührt.',
			translations: {
					en: 'Other people\'s misfortunes do not usually disturb me a great deal.',
					de: 'Das Unglück anderer lässt mich normalerweise weitgehend unberührt.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_14', 'item14', 'full-assessment', 'block4', 'full-assessment-block4-item14']

			},

		// Wenn ich mir sicher bin, dass ich Recht habe, vergeude ich nicht viel Zeit damit, mir die Argumente von anderen anzuhören.
		{

			id:       'IRI_15',
			type:     'integer',
			meaning:  'Wenn ich mir sicher bin, dass ich Recht habe, vergeude ich nicht viel Zeit damit, mir die Argumente von anderen anzuhören.',
			translations: {
					en: 'If I\'m sure I\'m right about something, I don\'t waste much time listening to other people\'s arguments.',
					de: 'Wenn ich mir sicher bin, dass ich Recht habe, vergeude ich nicht viel Zeit damit, mir die Argumente von anderen anzuhören.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_15', 'item15', 'full-assessment', 'block4', 'full-assessment-block4-item15']

			},

		// Wenn ich eine Person sehe, die unfair behandelt wird, empfinde ich manchmal nur wenig Mitleid.
		{

			id:       'IRI_18',
			type:     'integer',
			meaning:  'Wenn ich eine Person sehe, die unfair behandelt wird, empfinde ich manchmal nur wenig Mitleid.',
			translations: {
					en: 'When I see someone being treated unfairly, I sometimes don\'t feel very much pity for them.',
					de: 'Wenn ich eine Person sehe, die unfair behandelt wird, empfinde ich manchmal nur wenig Mitleid.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_18', 'item18', 'full-assessment', 'block4', 'full-assessment-block4-item18']

			},

		// Ich bin oft ziemlich berührt von Dingen, die ich mitbekomme.
		{

			id:       'IRI_20',
			type:     'integer',
			meaning:  'Ich bin oft ziemlich berührt von Dingen, die ich mitbekomme.',
			translations: {
					en: 'I am often quite touched by things that I see happen.',
					de: 'Ich bin oft ziemlich berührt von Dingen, die ich mitbekomme.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_20', 'item20', 'full-assessment', 'block4', 'full-assessment-block4-item20']

			},

		// Ich glaube, dass jedes Problem zwei Seiten hat, und versuche deshalb, beide zu sehen.
		{

			id:       'IRI_21',
			type:     'integer',
			meaning:  'Ich glaube, dass jedes Problem zwei Seiten hat, und versuche deshalb, beide zu sehen.',
			translations: {
					en: 'I believe that there are two sides to every question and try to look at them both.',
					de: 'Ich glaube, dass jedes Problem zwei Seiten hat, und versuche deshalb, beide zu sehen.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_21', 'item21', 'full-assessment', 'block4', 'full-assessment-block4-item21']

			},

		// Ich würde mich selbst als eine gutmütige Person bezeichnen.
		{

			id:       'IRI_22',
			type:     'integer',
			meaning:  'Ich würde mich selbst als eine gutmütige Person bezeichnen.',
			translations: {
					en: 'I would describe myself as a pretty soft-hearted person.',
					de: 'Ich würde mich selbst als eine gutmütige Person bezeichnen.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_22', 'item22', 'full-assessment', 'block4', 'full-assessment-block4-item22']

			},

		// Wenn ich wütend auf jemanden bin, versuche ich normalerweise, mich für eine Weile in die Lage der anderen Person zu versetzen.
		{

			id:       'IRI_25',
			type:     'integer',
			meaning:  'Wenn ich wütend auf jemanden bin, versuche ich normalerweise, mich für eine Weile in die Lage der anderen Person zu versetzen.',
			translations: {
					en: 'When I\'m upset at someone, I usually try to put myself in his shoes for a while.',
					de: 'Wenn ich wütend auf jemanden bin, versuche ich normalerweise, mich für eine Weile in die Lage der anderen Person zu versetzen.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_25', 'item25', 'full-assessment', 'block4', 'full-assessment-block4-item25']

			},

		// Bevor ich jemanden kritisiere, versuche ich mir vorzustellen, wie ich mich an seiner Stelle fühlen würde.
		{

			id:       'IRI_28',
			type:     'integer',
			meaning:  'Bevor ich jemanden kritisiere, versuche ich mir vorzustellen, wie ich mich an seiner Stelle fühlen würde.',
			translations: {
					en: 'Before criticizing somebody, I try to imagine how I would feel if I were in their place.',
					de: 'Bevor ich jemanden kritisiere, versuche ich mir vorzustellen, wie ich mich an seiner Stelle fühlen würde.'

			},
			options:    [
					{ value:0  , translations: { en: 'strongly disagree', de: 'beschreibt mich überhaupt nicht' } },
					{ value:1  , translations: { en: 'disagree', de: 'beschreibt mich kaum' } },
					{ value:2  , translations: { en: 'neither agree nor disagree', de: 'weder noch ' } },
					{ value:3  , translations: { en: 'agree', de: 'beschreibt mich etwas' } },
					{ value:4  , translations: { en: 'strongly agree', de: 'beschreibt mich sehr gut' } },

			],
			tags:     ['IRI', 'IRI_28', 'item28', 'full-assessment', 'block4', 'full-assessment-block4-item28']

			},

		// POMS: Profile of Mood states

		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt.

		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: abgeschlafft.
		{

			id:       'POMS_abgeschlafft',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: abgeschlafft.',
			translations: {
					en: 'In the last 24 hours, I have been feling worn out',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: abgeschlafft.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_abgeschlafft', 'item_abgeschafft', 'full-assessment', 'block4', 'full-assessment-block4-item_abgeschafft', 'short-assessment', 'short-assessment-block4-item_abgeschafft']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: lebhaft.
		{

			id:       'POMS_lebhaft',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: lebhaft.',
			translations: {
					en: 'In the last 24 hours, I have been feeling lively',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: lebhaft.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_lebhaft', 'item_lebhaft', 'full-assessment', 'block4', 'full-assessment-block4-item_lebhaft']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: lustlos.
		{

			id:       'POMS_lustlos',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: lustlos.',
			translations: {
					en: 'In the last 24 hours, I have been feeling listless',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: lustlos.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_lustlos', 'item_lustlos', 'full-assessment', 'block4', 'full-assessment-block4-item_lustlos']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: aktiv.
		{

			id:       'POMS_aktiv',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: aktiv.',
			translations: {
					en: 'In the last 24 hours, I have been feeling active',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: aktiv.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_aktiv', 'item_aktiv', 'full-assessment', 'block4', 'full-assessment-block4-item_aktiv']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: energisch.
		{

			id:       'POMS_energisch',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: energisch.',
			translations: {
					en: 'In the last 24 hours, I have been feeling energetic',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: energisch.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_energisch', 'item_energisch', 'full-assessment', 'block4', 'full-assessment-block4-item_energisch']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: müde.
		{

			id:       'POMS_müde',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: müde.',
			translations: {
					en: 'In the last 24 hours, I have been feeling fatigued',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: müde.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_müde', 'item_müde', 'full-assessment', 'block4', 'full-assessment-block4-item_müde']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: fröhlich.
		{

			id:       'POMS_fröhlich',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: fröhlich.',
			translations: {
					en: 'In the last 24 hours, I have been feeling cheerful.',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: fröhlich.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_fröhlich', 'item_fröhlich', 'full-assessment', 'block4', 'full-assessment-block4-item_fröhlich']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: erschöpft.
		{

			id:       'POMS_erschöpft',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: erschöpft.',
			translations: {
					en: 'In the last 24 hours, I have been feeling exhausted',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: erschöpft.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['POMS', 'POMS_erschöpft', 'item_erschöpft', 'full-assessment', 'block4', 'full-assessment-block4-item_erschöpft']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: träge.
		{

			id:       'POMS_träge',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: träge.',
			translations: {
					en: 'In the last 24 hours, I have been feeling sluggish',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: träge.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['POMS', 'POMS_träge', 'item_träge', 'full-assessment', 'block4', 'full-assessment-block4-item_träge']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: ermattet.
		{

			id:       'POMS_ermattet',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: ermattet.',
			translations: {
					en: 'In the last 24 hours, I have been feeling weary',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: ermattet.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },


			],
			tags:     ['POMS', 'POMS_ermattet', 'item_ermattet', 'full-assessment', 'block4', 'full-assessment-block4-item_ermattet']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: munter.
		{

			id:       'POMS_munter',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: munter.',
			translations: {
					en: 'In the last 24 hours, I have been feeling alert.',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: munter.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_munter', 'item_munter', 'full-assessment', 'block4', 'full-assessment-block4-item_munter']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: schwungvoll.
		{

			id:       'POMS_schwungvoll',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: schwungvoll.',
			translations: {
					en: 'In the last 24 hours, I have been feeling full of pep',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: schwungvoll.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_schwungvoll', 'item_schwungvoll', 'full-assessment', 'block4', 'full-assessment-block4-item_schwungvoll']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: tatkräftig.
		{

			id:       'POMS_tatkräftig',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: tatkräftig.',
			translations: {
					en: 'In the last 24 hours, I have been feeling vigorous',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: tatkräftig.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_tatkräftig', 'item_tatkräftig', 'full-assessment', 'block4', 'full-assessment-block4-item_tatkräftig']
		},
		// Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: entkräftet.
		{

			id:       'POMS_entkräftet',
			type:     'integer',
			meaning:  'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: entkräftet.',
			translations: {
					en: 'In the last 24 hours, I have been feeling bushed',
					de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt: entkräftet.'

			},
			options:    [
					{ value:0  , translations: { en: 'not at all', de: 'überhaupt nicht' } },
					{ value:1  , translations: { en: 'very slightly', de: 'sehr schwach' } },
					{ value:2  , translations: { en: 'slightly', de: 'schwach' } },
					{ value:3  , translations: { en: 'moderately', de: 'etwas' } },
					{ value:4  , translations: { en: 'quite a bit', de: 'ziemlich' } },
					{ value:5  , translations: { en: 'very', de: 'stark' } },
					{ value:6  , translations: { en: 'extremely', de: 'sehr stark' } },

			],
			tags:     ['POMS', 'POMS_entkräftet', 'item_entkräftet', 'full-assessment', 'block4', 'full-assessment-block4-item_entkräftet']
		},
		// Fragen zum Chronotyp (D-rMEQ):

		// 1. Bitte lesen Sie jede Frage sorgfältig durch, bevor Sie antworten.
		// 2. Beantworten Sie bitte alle Fragen, auch dann, wenn Sie sich bei einer Frage unsicher sind.
		// 3. Beantworten Sie die Fragen in der vorgegebenen Reihenfolge.
		// 4. Beantworten Sie die Fragen so schnell wie möglich. Es sind die ersten Reaktionen auf die Fragen, die uns mehr interessieren als eine lange überlegte Antwort.
		// 5. Beantworten Sie jede Frage ehrlich. Es gibt keine richtige oder falsche Antwort.

		// Wenn es nur nach Ihrem Wohlbefinden ginge und Sie Ihren Tag völlig frei einteilen könnten, wann würden Sie dann aufstehen?
		{

			id:       'RMEQ_1',
			type:     'integer',
			meaning:  'Wenn es nur nach Ihrem Wohlbefinden ginge und Sie Ihren Tag völlig frei einteilen könnten, wann würden Sie dann aufstehen?',
			translations: {
					en: 'Considering only your own feeling beat rhythm, at what time would you get up if you were entirely free to plan your day?',
					de: 'Wenn es nur nach Ihrem Wohlbefinden ginge und Sie Ihren Tag völlig frei einteilen könnten, wann würden Sie dann aufstehen?'

			},
			options:    [
					{ value:5  , translations: { en: '5:00-6:30 AM', de: '5:00-6:30' } },
					{ value:4  , translations: { en: '6:30-7:45 AM', de: '6:30-7:45' } },
					{ value:3  , translations: { en: '7:45-9:45 AM', de: '7:45-9:45' } },
					{ value:2  , translations: { en: '9:45-11:00 AM', de: '9:45-11:00' } },
					{ value:1  , translations: { en: '11:00-12:00 noon', de: '11:00-12:00' } },

			],
			tags:     ['RMEQ', 'RMEQ_1', 'item1', 'full-assessment', 'block4', 'full-assessment-block4-item1', 'short-assessment', 'short-assessment-block4-item1']
		},
		// Wie müde fühlen Sie sich morgens in der ersten halben Stunde nach dem Aufwachen?
		{

			id:       'RMEQ_2',
			type:     'integer',
			meaning:  'Wie müde fühlen Sie sich morgens in der ersten halben Stunde nach dem Aufwachen?',
			translations: {
					en: 'During the first half hour after having woken in the morning, how tired do you feel?',
					de: 'Wie müde fühlen Sie sich morgens in der ersten halben Stunde nach dem Aufwachen?'

			},
			options:    [
					{ value:0  , translations: { en: 'very tired', de: 'sehr müde' } },
					{ value:1  , translations: { en: 'fairly tired', de: 'ziemlich müde' } },
					{ value:2  , translations: { en: 'fairly refreshed', de: 'ziemlich frisch' } },
					{ value:3  , translations: { en: 'very refreshed', de: 'sehr frisch' } },

			],
			tags:     ['RMEQ', 'RMEQ_2', 'item2', 'full-assessment', 'block4', 'full-assessment-block4-item2']
		},
		// Um wie viel Uhr werden Sie abends müde und haben das Bedürfnis, schlafen zu gehen?
		{

			id:       'RMEQ_3',
			type:     'integer',
			meaning:  'Um wie viel Uhr werden Sie abends müde und haben das Bedürfnis, schlafen zu gehen?',
			translations: {
					en: 'At what time in the evening do you feel tired and as a result in need of sleep?',
					de: 'Um wie viel Uhr werden Sie abends müde und haben das Bedürfnis, schlafen zu gehen?'

			},
			options:    [
					{ value:5  , translations: { en: '8:00-9:00 PM', de: '20:00-21:00' } },
					{ value:4  , translations: { en: '9:00-10:15 PM', de: '21:00-22:15' } },
					{ value:3  , translations: { en: '10:15-12:45 PM', de: '22:15-00:45' } },
					{ value:2  , translations: { en: '12:45-2:00 AM', de: '00:45-2:00' } },
					{ value:1  , translations: { en: '2:00-3:00 AM', de: '2:00-3:00' } },

			],
			tags:     ['RMEQ', 'RMEQ_3', 'item3', 'full-assessment', 'block4', 'full-assessment-block4-item3']
		},
		// Zu welcher Tageszeit fühlen Sie sich Ihrer Meinung nach am besten?
		{

			id:       'RMEQ_4',
			type:     'integer',
			meaning:  'Zu welcher Tageszeit fühlen Sie sich Ihrer Meinung nach am besten?',
			translations: {
					en: 'At what time of the day do you think that you reach your feeling best peak?',
					de: 'Zu welcher Tageszeit fühlen Sie sich Ihrer Meinung nach am besten?'

			},
			options:    [
					{ value:5  , translations: { en: '5:00-8:00 AM', de: '5:00-8:00' } },
					{ value:4  , translations: { en: '8:00-10:00 AM', de: '8:00-10:00' } },
					{ value:3  , translations: { en: '10:00 AM-5:00 PM', de: '10:00-17:00' } },
					{ value:2  , translations: { en: '5:00-10:00 PM', de: '17:00-22:00' } },
					{ value:1  , translations: { en: '10:00 PM-5:00 AM', de: '22:00-5:00' } },

			],
			tags:     ['RMEQ', 'RMEQ_4', 'item4', 'full-assessment', 'block4', 'full-assessment-block4-item4']
		},
		// Man spricht bei Menschen von „Morgen-“ und „Abendtypen“. Zu welchem der folgenden Typen zählen Sie sich?
		{

			id:       'RMEQ_5',
			type:     'integer',
			meaning:  'Man spricht bei Menschen von „Morgen-“ und „Abendtypen“. Zu welchem der folgenden Typen zählen Sie sich?',
			translations: {
					en: 'One hears about morning and evening types of people. Which one of these types do you consider yourself to be?',
					de: 'Man spricht bei Menschen von „Morgen-“ und „Abendtypen“. Zu welchem der folgenden Typen zählen Sie sich?'

			},
			options:    [
					{ value:6  , translations: { en: 'definitely a morning-type', de: 'eindeutig Morgentyp' } },
					{ value:4  , translations: { en: 'rather more a morning-type than an evening-type', de: 'eher Morgen- als Abendtyp' } },
					{ value:2  , translations: { en: 'rather more an evening-type than a morning-type', de: 'eher Abend- als Morgentyp' } },
					{ value:0  , translations: { en: 'definitely an evening-type', de: 'eindeutig Abendtyp' } },

			],
			tags:     ['RMEQ', 'RMEQ_5', 'item5', 'full-assessment', 'block4', 'full-assessment-block4-item5']
		},

		// AES:  Apathy Evaluation Scale

		// Bitte kreuzen Sie jeweils die Antwort an, die Ihre Gedanken, Gefühle und Verhaltensweisen während der letzten vier Wochen am besten beschreibt.

		// Ich erledige Dinge während des Tages.
		{

			id:       'AES_2',
			type:     'integer',
			meaning:  'Ich erledige Dinge während des Tages.',
			translations: {
					en: 'I get things done during the day.',
					de: 'Ich erledige Dinge während des Tages.'

			},
			options:    [
					{ value:1  , translations: { en: 'not at all true', de: 'trifft gar nicht zu' } },
					{ value:2  , translations: { en: 'slightly true', de: 'trifft etwas zu' } },
					{ value:3  , translations: { en: 'somewhat true', de: 'trifft ziemlich zu' } },
					{ value:4  , translations: { en: 'very true', de: 'trifft sehr zu' } },

			],
			tags:     ['AES', 'AES_2', 'item2', 'full-assessment', 'block4', 'full-assessment-block4-item2', 'short-assessment', 'short-assessment-block4-item2']
		},
		// Mir ist es wichtig, Dinge selbst zu beginnen.
		{

			id:       'AES_3',
			type:     'integer',
			meaning:  'Mir ist es wichtig, Dinge selbst zu beginnen.',
			translations: {
					en: 'It is important to me to get things started on my own.',
					de: 'Mir ist es wichtig, Dinge selbst zu beginnen.'

			},
			options:    [
					{ value:1  , translations: { en: 'not at all true', de: 'trifft gar nicht zu' } },
					{ value:2  , translations: { en: 'slightly true', de: 'trifft etwas zu' } },
					{ value:3  , translations: { en: 'somewhat true', de: 'trifft ziemlich zu' } },
					{ value:4  , translations: { en: 'very true', de: 'trifft sehr zu' } },

			],
			tags:     ['AES', 'AES_3', 'item3', 'full-assessment', 'block4', 'full-assessment-block4-item3']
		},
		// Jemand muss mir sagen, was ich täglich tun soll.
		{

			id:       'AES_10',
			type:     'integer',
			meaning:  'Jemand muss mir sagen, was ich täglich tun soll.',
			translations: {
					en: 'Someone has to tell me what to do each day.',
					de: 'Jemand muss mir sagen, was ich täglich tun soll.'

			},
			options:    [
					{ value:1  , translations: { en: 'not at all true', de: 'trifft gar nicht zu' } },
					{ value:2  , translations: { en: 'slightly true', de: 'trifft etwas zu' } },
					{ value:3  , translations: { en: 'somewhat true', de: 'trifft ziemlich zu' } },
					{ value:4  , translations: { en: 'very true', de: 'trifft sehr zu' } },

			],
			tags:     ['AES', 'AES_10', 'item10', 'full-assessment', 'block4', 'full-assessment-block4-item10']
		},
		// Während des Tages Dinge zu erleben ist mir wichtig.
		{

			id:       'AES_16',
			type:     'integer',
			meaning:  'Während des Tages Dinge zu erleben ist mir wichtig.',
			translations: {
					en: 'Getting things done during the day is important to me.',
					de: 'Während des Tages Dinge zu erleben ist mir wichtig.'

			},
			options:    [
					{ value:1  , translations: { en: 'not at all true', de: 'trifft gar nicht zu' } },
					{ value:2  , translations: { en: 'slightly true', de: 'trifft etwas zu' } },
					{ value:3  , translations: { en: 'somewhat true', de: 'trifft ziemlich zu' } },
					{ value:4  , translations: { en: 'very true', de: 'trifft sehr zu' } },

			],
			tags:     ['AES', 'AES_16', 'item16', 'full-assessment', 'block4', 'full-assessment-block4-item16']
		},
		// Ich habe Eigeninitiative.
		{

			id:       'AES_17',
			type:     'integer',
			meaning:  'Ich habe Eigeninitiative.',
			translations: {
					en: 'I have initiative.',
					de: 'Ich habe Eigeninitiative.'

			},
			options:    [
					{ value:1  , translations: { en: 'not at all true', de: 'trifft gar nicht zu' } },
					{ value:2  , translations: { en: 'slightly true', de: 'trifft etwas zu' } },
					{ value:3  , translations: { en: 'somewhat true', de: 'trifft ziemlich zu' } },
					{ value:4  , translations: { en: 'very true', de: 'trifft sehr zu' } },
			],
			tags:     ['AES', 'AES_17', 'item17', 'full-assessment', 'block4', 'full-assessment-block4-item17']
		},
	]
