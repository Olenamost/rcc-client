// this module contains anything to do with the REPOSE study
import en from './i18n/en.json'
import de from './i18n/de.json'
import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { provideMainMenuEntry, provideTranslationMap, SharedModule } from '@rcc/common'
import { FhirExternalRequestService } from './fhir-requests.service'
import { ReposeExportService } from './repose-export.service'
import { ReposePageComponent } from './repose-page/repose-page.component'
import { ReposeRespondentModule } from './repose-respondent/repose-respondent.module'

const ReposeMenuEntry = {
	position: 2,
	label: 'REPOSE-Studie',
	icon: 'fill',
	path: 'repose'
}

const routes = [
	{ path: 'repose', component: ReposePageComponent }
]

@NgModule({
	declarations: [
		ReposePageComponent						// landing page
	],
	imports: [
		HttpClientModule,
		ReposeRespondentModule,
		SharedModule,							// ionic icons
		RouterModule.forChild(routes)			// routing
	],
	providers: [
		provideMainMenuEntry(ReposeMenuEntry),	// menu bar entry
		provideTranslationMap('REPOSE', { en,de }),
		ReposeExportService,					// export data
		FhirExternalRequestService
	]
})
export class ReposeModule {}
