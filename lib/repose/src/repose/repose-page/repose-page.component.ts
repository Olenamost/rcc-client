// REPOSE study landing page
import { Component } from '@angular/core'
import { R4 } from '@rcc/core'
import { FhirExternalRequestService } from '../fhir-requests.service'
import { ReposeExportService } from '../repose-export.service'
import { ReposeRespondentService } from '../repose-respondent'

@Component({
	templateUrl:	'./repose-page.component.html'
})
export class ReposePageComponent {
	// user data in FHIR format
	public fhir 		: R4.IQuestionnaireResponse
	public fhir_json 	: string
	public ready 		: Promise<void>

	public constructor(
		private reposeExportService: ReposeExportService,
		private fhirExternalRequestService: FhirExternalRequestService,
		private reposeRespondentService: ReposeRespondentService
	) {
		this.ready = this.setup()
	}

	public async setup(): Promise<void> {
		const respondentId = await this.reposeRespondentService.getRespondentId()
		this.fhir = this.reposeExportService.exportToFHIR(respondentId)
		this.fhir_json = JSON.stringify(this.fhir, null, 2)
	}

	public onSendData(): void {
		this.fhirExternalRequestService.store(this.fhir)
	}
}
