import { Injectable } from '@angular/core'
import { Entry, Question, R4, Report, report2fhir } from '@rcc/core'
import { JournalService, QuestionnaireService } from '@rcc/features'

/**
 * This service exports questions + user entries from app storage to some other database
 */
@Injectable()
export class ReposeExportService {
	// Journal => get access to user entries
	// Questionnaire => get access to questions in app
	public constructor(
		private journalService: JournalService,
		private questionnaireService: QuestionnaireService
	) {}

	// returns all entries from the journal
	private getEntries(): Entry[] {
		return this.journalService.items
	}

	// returns all questions from the questionnaire
	// TODO für beide query params spezifizieren
	private getQuestions(): Question[] {
		return this.questionnaireService.items
	}

	/**
	 * gets all questions and entries from the journal and generates a report in FHIR format
	 * @param {string} id		report respondent ID
	 * @param {string} lang		language code for export, defaults to English ('en')
	 * @returns all questions and entries in FHIR format
	 */
	public exportToFHIR(id: string, lang='en'): R4.IQuestionnaireResponse {
		const questions = this.getQuestions()
		const entries = this.getEntries()
		const report = Report.from(entries)						// report2fhir expects Report type
		const fhir = report2fhir(report, questions, lang, id)	// no need for exporter
		return fhir
	}
}
