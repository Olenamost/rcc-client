import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { assertProperty } from '@rcc/core'
import { R4	} 	from '@ahryman40k/ts-fhir-types'
import { RccToastController } from '@rcc/common'

@Injectable()
export class FhirExternalRequestService {
	public constructor(
		private http: HttpClient,
		private rccToastController: RccToastController
	) {}

	// QUESTION VoidStorage..?
	public store(data: R4.IQuestionnaireResponse): void {
		// all transmissions much have report ID and respondent ID
		assertProperty(data, 'id', 'Missing report ID')
		// FIXME
		// assertProperty(data, 'source.identifier.value', 'Missing respondent ID (source.identifier.value)')

		this.http.put('https://hapi.fhir.org/baseR4/QuestionnaireResponse/' + data.id, data)
		.subscribe({ next: () => {
			void this.rccToastController.success('REPOSE.HTTP_REQUEST.SUCCESS')
		}, error: error => {
			void this.rccToastController.failure('REPOSE.HTTP_REQUEST.FAILURE')
			if (error instanceof Error) throw error
			throw new Error(String(error))
		} })
	}

}

