import { NgModule } from '@angular/core'
import { IncomingDataService, IncomingDataServiceModule, provideSettingsEntry, SettingsEntry, SettingsModule } from '@rcc/common'
import { uuidv4 } from '@rcc/core'
import { ReposeRespondentService } from './repose-respondent.service'

// FIXME problem mit id: hat aktuell noch nichts mit report/entries zu tun
// dh beim export generiere ich fhir questionnaireresponse aus den entries
// diese sind unabhängig von id!!! einfach was im speicher ist
// und tackere an diesen report die aktuell eingestellte id
// dh in dem report könnten responses von unterschiedlichen ids sein
const settingsEntry: SettingsEntry<unknown> =	{
	id:				'repose-active-respondent-id',
	label: 			'REPOSE.SETTINGS.RESPONDENT_ID.LABEL',
	description:	'REPOSE.SETTINGS.RESPONDENT_ID.DESCRIPTION',
	icon:			'finger-print-outline',
	type:			'string' as const,
	defaultValue:	null,
}

/**
 * Example data module for RespondentData, used by ReposeRespondentService
 */
@NgModule({
	imports: [IncomingDataServiceModule]
})
export class ExampleRespondentId {
	public constructor(
		private incomingDataService: IncomingDataService,
		private reposeRespondentService: ReposeRespondentService
	) {
		this.incomingDataService.next( { type: 'repose-respondent-id', id: uuidv4() } )
	}
}

@NgModule({
	imports:	[ExampleRespondentId, SettingsModule],
	providers:	[ReposeRespondentService,
				provideSettingsEntry(settingsEntry)]
})
export class ReposeRespondentModule {
}
