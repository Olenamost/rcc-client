import { Injectable } from '@angular/core'
import { IncomingDataService, RccAlertController, RccSettingsService, RccToastController } from '@rcc/common'
import { assert, assertProperty, isErrorFree, Item, ItemStorage } from '@rcc/core'
import { filter } from 'rxjs'

/**
 * Interface for respondent data, currently we're only interested in their ID
 */
export interface RespondentData {
	id 			: string	// respondent ID
	type		: 'repose-respondent-id'
}

export function assertRespondentData(data: unknown): asserts data is RespondentData {
	assertProperty(data, 'id', 'assertRespondentData: missing .id')
	assertProperty(data, 'type', 'assertRespondentData: missing .type')
	assert(typeof data.id === 'string', 'assertRespondentData: .id must be a string')
	assert(data.type === 'repose-respondent-id', 'assertRespondentData: type must be \'repose-respondent-id\'')
}

/**
 * Checks if incoming data is respondent data
 * @param data Data from IncomingData
 * @returns true iff data fits RespondentData interface
 */
export function isRespondentData(data: unknown): data is RespondentData {
	return isErrorFree(() => assertRespondentData(data))
}

/**
 * This service manages the study respondent's ID and its storage in RccStorage.
 */
@Injectable()
export class ReposeRespondentService {
	public settingsId 		: string = 'repose-active-respondent-id'

	// Create ItemStorage respose-respondent in setup
	protected dataStorage 	: ItemStorage<Item<RespondentData>>

	public constructor(
		private incomingDataService: IncomingDataService,
		private rccSettingsService: RccSettingsService,
		private rccAlertController: RccAlertController,
		private rccToastController: RccToastController
	) {
		this.incomingDataService
		.pipe(
			filter(isRespondentData)
		)
		.subscribe(data => void this.handleIncomingRespondentData(data))
	}

	/**
	 * Async setup function since constructor cannot be async
	 * Creates a new ItemStorage 'repose-respondent',
	 * listens for RespondentData from IncomingData
	 * and puts it in storage
	 */
	public setup(): void {
		this.incomingDataService
		.pipe(
			filter(isRespondentData)
		)
		.subscribe(data => void this.handleIncomingRespondentData(data))
	}

	public async setRespondentId(newId: string): Promise<void> {
		// Don't await promise here to not block rccSettingsService
		const toastPromise = this.rccToastController.info('Saving new ID...')
		try {
			await this.rccSettingsService.set(this.settingsId, newId)
			void toastPromise.then(() => this.rccToastController.success('ID saved'))
		} catch (e) {
			void this.rccToastController.failure('Could not save ID')
			throw e
		}
	}

	public async getRespondentId(): Promise<string> {
		const settingsId = await this.rccSettingsService.get(this.settingsId)
		return settingsId.value as string
	}

	/**
	 * Asks whether an existing ID should be overwritten and stores the incoming ID.
	 * @param data Filtered RespondentData from IncomingData
	 */
	public async handleIncomingRespondentData(data: RespondentData): Promise<void> {
		const currentId = await this.getRespondentId()

		// if no ID exists yet
		if (!currentId)
			await this.rccAlertController.confirm(`Found new ID ${data.id}, save?`)
			.then(
				() => this.setRespondentId(data.id),
				() => this.rccToastController.present({ message: 'CANCELED' })
			)

		// if an ID already exists
		if (currentId)
			await this.rccAlertController.confirm(`Found new ID ${data.id}, overwrite existing ID ${currentId}?`)
			.then(
				() => this.setRespondentId(data.id),
				() => this.rccToastController.present({ message: 'CANCELED' })
			)
	}
}
