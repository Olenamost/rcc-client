import	{	NgModule					}	from '@angular/core'
import	{	BrowserModule 				} 	from '@angular/platform-browser'
import	{	RouteReuseStrategy,RouterModule 			}	from '@angular/router'

import 	{
			IonicModule,
			IonicRouteStrategy
		}									from '@ionic/angular'

import	{	IonicBaseAppComponent		}	from './base-app.component'
import	{	MainMenuModule				}	from '@rcc/common'

import	{	IonicModalsModule			}	from '../ionic-modals'
import	{	IonicIconsModule			}	from '../ionic-icons'

@NgModule({
	imports :[
		BrowserModule,
		RouterModule,
		IonicModule.forRoot(),
		MainMenuModule.forRoot({ menuId: 'rcc-main-menu', contentId: 'rcc-main-content' }),
		IonicModalsModule,
		IonicIconsModule,

	],
	declarations: [
		IonicBaseAppComponent
	],
	exports :[
		IonicBaseAppComponent
	],
	providers: [
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
	],

})
export class IonicBaseAppModule {

}


