import	{
			Component,
			ViewEncapsulation
		} 							from '@angular/core'

// import	{	Platform } 				from '@ionic/angular'

@Component({
	selector: 		'app-root',
	templateUrl: 	'base-app.component.html',
	encapsulation:	ViewEncapsulation.None
})
export class IonicBaseAppComponent {
	// constructor(
	// 	private platform		: Platform,
	// ) {
	// 	this.initializeApp()
	// }

	// initializeApp() : void {}
}
