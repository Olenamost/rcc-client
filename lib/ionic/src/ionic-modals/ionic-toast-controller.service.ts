import	{	Injectable				}	from '@angular/core'

import	{	ToastController			}	from '@ionic/angular'

import	{
			ToastConfig,
			RccTranslationService
		}								from '@rcc/common'



@Injectable()
export class IonicToastController {

	public constructor(
		public toastController			: ToastController,
		public rccTranslationService	: RccTranslationService
	) {}

	public async present(config: ToastConfig) :Promise<unknown> {

		let resolve		: 	(...args:unknown[]) => unknown = () => { throw new Error('IonicToastController.present(): resolve not initialized.') }

		const promise 	= 	new Promise( solve => resolve = solve )

		const toast		= 	await this.toastController.create({
								message: 	this.rccTranslationService.translate(config.message),
								duration:	1000,
								color:		config.color,
								position:	'top'
							})

		void toast.onWillDismiss().then( () => resolve('dismissed'))
		void toast.present()

		return await promise
	}

	public async success(message: string): Promise<unknown>{
		return await this.present({ message: message, color: 'success' })
	}

	public async failure(message: string): Promise<unknown>{
		return await this.present({ message: message, color: 'warning' })
	}

	public async info(message: string): Promise<unknown>{
		return await this.present({ message: message, color: 'medium' })
	}
}
