import	{
			Injectable,
			Type

		}								from '@angular/core'

import	{
			ModalController,
		}								from '@ionic/angular'


@Injectable()
export class IonicModalController {

	public constructor(
		public modalController: ModalController
	){}

	public modals : string[] = []
	public lastId = 0

	public async present(component: Type<unknown>, data?: unknown ) : Promise<unknown>{

		this.lastId++

		const id 		= 'modal-'+ this.lastId.toString()
		const modal		= await this.modalController.create({ component, componentProps: data, id })

		this.modals.push(id)

		await modal.present()

		const result	= await modal.onDidDismiss()

		this.modals 	= this.modals.filter( modal => modal !== id )

		if(result.data instanceof Error) throw result.data

		return result.data as unknown

	}

	public async dismiss(data:unknown): Promise<void>{
		await this.modalController.dismiss(data, '', this.modals[this.modals.length-1])
	}

}
