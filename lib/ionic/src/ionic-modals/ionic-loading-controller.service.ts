import	{	Injectable				}	from '@angular/core'
import	{	LoadingController		}	from '@ionic/angular'
import	{
			LoadingConfig,
			LoadingInstance
		}								from '@rcc/common'


@Injectable()
export class IonicLoadingController {

	public constructor(
		public loadingController	: LoadingController,
	) {}


	public async present(loadingConfig: LoadingConfig) :Promise<LoadingInstance> {

		const message 	= loadingConfig.message

		const loading = await this.loadingController.create({ message })

		loading.present()

		return 	{ close: async () => void loading.dismiss()	}

	}

}
