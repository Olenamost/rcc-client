import	{
			Injectable,
		}								from '@angular/core'

import	{
			AlertConfig,
			ButtonConfig,
			RccTranslationService,
			RccAlertController
		}								from '@rcc/common'

import	{
			AlertController,
			AlertOptions,
			AlertButton
		}								from '@ionic/angular'



/**
 * This Service is meant to replace RccAlertController.
 * The only relevant method is .present() since all the other methods
 * will make use of it (as of the original RccAlertController extend below).
 *
 * The idea here is to re-use the AlertController that comes with Ionic and
 * make it conform to the interfaces we defined in @rcc/common.
 */

@Injectable()
export class IonicAlertController extends RccAlertController {

	public constructor(
		public alertController			: AlertController,
		public rccTranslationService	: RccTranslationService
	) { super () }

	public async present(config: AlertConfig) :Promise<unknown> {


		// Creating a promise that can be resolved with resolve() and rejected with reject()
		let 	resolve	: (...args:unknown[]) => unknown = null
		let 	reject	: (...args:unknown[]) => unknown = null

		const 	promise : Promise<unknown>			= 	new Promise<unknown>( (solve, ject) => { resolve = solve; reject = ject })

		// The following could also be placed directly into the Promise constructor above.
		// But extracting the resolve/reject methods like above, saves us one indention level :D


		let 	alert			: HTMLIonAlertElement	= 	null // Type live on the global scope for some reason.
		let		alertOptions	: AlertOptions			=	null
		let		buttons 		: AlertButton[]			= 	[]

		// Buttons as they come with config for RccAlertController or default buttons:
		config.buttons	=	config.buttons || [{ label: 'OKAY', resolveAs: 'okay' }]


		// Translations have to be done here, because we cannot change the
		// template of HTMLIonAlertElement to e.g. insert the translate pipe.

		// Ionic AlertButtons:
		buttons			=	(config.buttons||[]).map( (button: ButtonConfig) => ({

								text:		this.rccTranslationService.translate(button.label),
								handler: 	() => {

												// Here is were the promise resolution / rejection
												// for the above promise is happening:
												if(button.resolveAs	!== undefined) resolve(button.resolveAs)
												if(button.rejectAs 	!== undefined) reject(button.rejectAs)
											}
							}))

		// Ionic AlertOptions:
		alertOptions	= 	{
								cssClass: 	config.cssClass 	&& config.cssClass,
								header:		config.header		&& this.rccTranslationService.translate(config.header),
								subHeader:	config.subHeader	&& this.rccTranslationService.translate(config.subHeader),
								message:	config.message		&& this.rccTranslationService.translate(config.message),

								buttons
							}

		// Creating an HTMLIonAlertElement wit the Ionic Service
		alert 			=	await this.alertController.create(alertOptions)

		// The promise will also be rejected when the alert is dismissed
		// by means others than clicking the buttons (e.g. clicking outside the alert)
		void alert.onDidDismiss().then( () => reject(this.dismissedMessage))

		// Show the alert:
		void alert.present()

		// Wait for reject or resolve to be called:
		return await promise
	}

}
