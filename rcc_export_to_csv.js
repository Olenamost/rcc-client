import	yargs 					from 'yargs'
import	assert					from 'assert'


import	{ 	hideBin 		} 	from 'yargs/helpers'
import	{
			readFile,
			writeFile
		}						from 'fs/promises'


const argv	=	yargs(hideBin(process.argv))

				.options({

					json: {
						describe:		'Path or glob pattern to exported json file(s).',
						dempandOption:	true,
						demandOption:	true,
						array:			true
					},

					language: {
						describe:		'Languages to be used in the output; generates a new file for each language',
						alias:			'l',
						array:			true,
						demandOption:	true,
						coerce:			(languages) => languages.map( lang => lang.toLowerCase() )

					},

					output: {
						describe:		'Target directory to store the generated CSV files in.',
						default:		'./'
					},

					debug: {
						describe:		'Output extra debug information and more detailed errors.'
					}

				})
				.argv



async function chainAsync(array, callback){

	return array.reduce( (last, value)=> {
			return last.then( async sum => [...sum, await callback(value) ])
		},
		Promise.resolve([])
	)
}



/**
 * Handles errors depending on debug mode.
 * @param {Error|string} error
 */
function handleErrors( error ){

	if(argv.debug) 	throw error
	else			console.error(error.message)

}


/**
 * Tries to read json data from provide file.
 * Throws if file does not conrain valid json data, or if that data does not
 * contain entries for 'report' or 'questions'.
 *
 * @param 	{string} path		Path to JSON file *
 *
 * @returns {object}			Parsed JSON data
 */
async function getExportedData(filename){


	const exportDataContent = 	await readFile(filename, 'utf8')
	const exportData		=	JSON.parse(exportDataContent)

	assert('report' 	in exportData, `Missing report data in JSON file.`)
	assert('questions' 	in exportData, `Missing question data in JSON file.`)

	return exportData
}




/**
 * Convert report and question data to a human readable CSV string.
 *
 * @param {array} 	questions 	Array of questions refered to in the report.
 * @param {object} 	report		Report data.
 * @param {string} 	lang		The language (in lowercase) to be used in the CSV string.
 *
 * @returns {string} The CSV string.
 */
function getCSV(origin, questions, report, lang){

	const [id, creationDate, entries] 	= report
	const answeredQuestionIds			= Array.from( new Set(entries.map( ([id]) => id)) )

	const rows							= []


	//Questions:
	answeredQuestionIds.forEach( questionId => {

		const question			= 	questions.find( q => q.id == questionId)
		const questionWording 	= 	question.translations[lang] || question.meaning
		const relevantEntries	= 	entries.filter( ([qid]) => qid == questionId)
									.sort( (entry1, entry2)	=> {

										// compare target days
										if(entry1[4] > entry2[4]) return  1
										if(entry1[4] < entry2[4]) return -1

										return 0
									})

		// Entries:

		relevantEntries.forEach( ([questionId, value, answerDate, note, targetDay]) => {

			const answerRaw 	= value
			const option		= question.options.find( option => option.value == value)
			const answerWording = option && (option.translations[lang] || option.meaning ) || ''

			rows.push([creationDate, origin, questionWording, targetDay, answerRaw, answerWording])
		})


	})

	const numberOfColumns	= 	Math.max(...rows.map( cols => cols.length))
	const paddedRows 		= 	rows.map( row => [...row, ...Array(numberOfColumns-row.length).fill('')] )

	const escapedRows		= 	rows.map( row => row.map( col => {

									if(typeof col == 'number') return col

									const escapedCol = 	String(col).replace('"', '""')

									return `"${escapedCol}"`
								}))

	return escapedRows.map( row => row.join(',') ).join('\n')

}


/**
 * Creates a .CSV file.
 * @param	{string}	csvData	The data to be stored in the file
 * @param	{string}	path	Where to store the file
 * @param	{string}	prefix	Filename prefix
 * @param	{string}	lang	Language of the CSV data (will be appended to the filename)
 *
 * @returns {string}	Filename
 */
async function storeCSVData( csvData, path, prefix, lang ) {

	const filename		= `RCC_Report_${prefix}_${lang}.csv`
	const destination 	= `${path}/${filename}`.replaceAll("//", "/")



	await writeFile(destination, csvData, 'utf8')

	return filename
}





const filenames		=	argv.json
const languages		=	argv.language


//####
process.stdout.write('\nProcessing:\n')

const CSV			=	{}

await chainAsync( filenames, async filename => {

	process.stdout.write(`\n    ${filename} ... \n`)

	const data 	= 	await getExportedData(filename).catch(handleErrors)

	await chainAsync( languages, async language => {

		process.stdout.write(`      [${language}`)

		const questions		=	data.questions
		const report		=	data.report
		const languages		=	argv.language
		const csvData		=	getCSV(filename, questions, report, language)

		CSV[language]		=	(CSV[language] || [])

		CSV[language].push(csvData)

		process.stdout.write(` -> ok]\n`)
	})

})

//####
process.stdout.write('\nStoring:\n\n')

const path			=	argv.output

await chainAsync( languages, async language => {

	process.stdout.write(`    -> `)

	const filename = await storeCSVData(CSV[language].join('\n'), path, 'Combined', language)

	process.stdout.write(`${filename}\n`)
})


process.stdout.write('\n')

