// const fs					= 	require('fs')
import path from 'path'
import fs	from 'fs'

const baseDir			=	process.argv[2] || '.'
const subDirs			=	getDirs(baseDir)
const auto_fix_broken 	= 	!!process.argv.find( arg => ['--fix', 		'--fix-all'].includes(arg))
const auto_fix_sus 		= 	!!process.argv.find( arg => ['--fix-sus', 	'--fix-all'].includes(arg))

function readRdirSync( base = '.') {
	return	fs.readdirSync(base)
			.map( filename => {
				const path_to_file = path.join(base, filename)
				return 	fs.statSync(path_to_file).isDirectory()
						?	readRdirSync(path_to_file)
						:	path_to_file

			})
			.flat()
}

function getDirs( base ){
	return	[
				base,
				...	fs.readdirSync(base)
					.filter( 	filename 	=>  fs.statSync( path.resolve(base, filename) ).isDirectory())
					.map( 		dir 		=>	getDirs(path.resolve(base, dir) ) )
					.flat()
			]
}



function getRelativeImports(str=''){
	const matches	= str.match(/(?<=from\s*('|"))\..*(?=('|"))/mgi)

	return matches || []

}

function isPresent(dir, source){

	const result		= 	path.resolve(dir, source)
	const json			= 	source.match(/\.json$/)
	const json_exists	= 	json && fs.existsSync(result)
	const index_exists	= 	fs.existsSync(result+'/index.ts')
	const ts_exists		= 	fs.existsSync(result+'.ts')

	return json_exists || index_exists || ts_exists || false

}

function simplifySource(dir, source){
	const resolved_path		=	path.join(dir, source)
	const relative_path		=	path.relative(dir, resolved_path)
	const simplified_path	=	getBackSteps(relative_path)
							?	relative_path
							:	'.'+path.sep+relative_path

	return simplified_path
}


function checkFile(path_to_file, auto_fix_relative = false){

	const content				= fs.readFileSync(path_to_file, 'utf8')
	const sources				= getRelativeImports(content)
	const dir					= path.dirname(path_to_file)
	const missing_sources		= sources.filter( source => !isPresent(dir, source) )
	const suspecious_sources	= sources.filter( source => simplifySource(dir, source) !== source )


	if(missing_sources.length === 0 && suspecious_sources.length === 0) return null

	console.log( ' * '+path.relative(baseDir, path_to_file), '\n' )



	suspecious_sources.forEach( source => {

		console.log('\t\x1b[33m%s\x1b[0m \t%s','[suspicious]', source)

		const simplified_path 	= 	simplifySource(dir, source)
		const easy_fix			= 	!!path.normalize(simplified_path)

		const message			=	easy_fix
									?	"[auto-fix]"
									:	"[suggestion]"

		const color 			= 	easy_fix
									?	'\x1b[32m'
									:	'\x1b[2m'

		if(auto_fix_sus) {

			if(easy_fix){

				fs.writeFileSync(path_to_file, content.split(source).join(simplified_path))
				console.log('\t\x1b[32m[auto fix]\x1b[0m \t%s \x1b[32m[done]\x1b[0m', simplified_path)

			} else {
				console.log('\t\x1b[2m[failed]\x1b[0m \t%s', 'unable to fix automatically.')
			}


		} else {
			console.log('\t%s%s\x1b[0m \t%s', message, color, simplified_path)
		}
		console.log('\n')

	})

	missing_sources.forEach( source  => {

		console.log('\t\x1b[31m%s\x1b[0m \t%s','[missing]', source)

		const fixes = getFixes(path_to_file, source)

		if(auto_fix_broken){

			if(!fixes[0]){
				console.log('\t\x1b[2m[failed]\x1b[0m \t%s', 'fix not found.')
			} else {
				fs.writeFileSync(path_to_file, content.split(source).join(fixes[0]))
				console.log('\t\x1b[32m[auto fix]\x1b[0m \t%s \x1b[32m[done]\x1b[0m', fixes[0])
			}

		} else {

			fixes.forEach( (fix, index) => {
				const fix_type	=	index === 0
									?	'[auto fix]'
									:	'[alt  fix] '

				const color 	= 	index === 0
									?	'\x1b[32m'
									:	'\x1b[2m'

				console.log('\t%s%s\x1b[0m \t%s', color, fix_type , fix)
			})

		}

		console.log('\n')

	})

	console.log('\n')

	return path_to_file
}

function getBackSteps(path_str){

	const normalized 	= path.normalize(path_str)
	const back_steps	= normalized.split(path.sep).findIndex( segment => segment !== '..')

	return back_steps
}

function getPathStub(path_str){

	const normalized 	= path.normalize(path_str)
	const path_stub		= normalized.split(path.sep).slice(getBackSteps(normalized)).join(path.sep)

	return path_stub
}

function getFixes(path_to_parent, source){


	const parent_dir 	= path.dirname(path_to_parent)
	const source_stub	= getPathStub(source)


	return 	subDirs
			.filter(	(dir)			=> 	isPresent(dir, source_stub) )
			.map(		(dir)			=>	path.relative(parent_dir, dir) )
			.map(		(dir)			=>	path.join(dir, source_stub) )
			.sort(		(dir_1, dir_2)	=>	{
												const depth_1 = getBackSteps(dir_1)
												const depth_2 = getBackSteps(dir_2)

												if(depth_1 === depth_2) return  0
												if(depth_1 >  depth_2) return  1
												if(depth_1 <  depth_2) return -1

											}
			)

}










process.stdout.write('\n')
process.stdout.write(`\x1b[36mLooking for relative imports in\x1b[0m ${baseDir} ...\n\n`)

const ts_files 		= 	readRdirSync(baseDir)
						.filter(	filename => filename.match(/\.ts$/) )

const broken_files	=	ts_files.filter( filename => checkFile(filename, auto_fix_broken) )

if(broken_files.length && !auto_fix_broken) console.log("Use '--fix' to try and automatically fix files with broken relative imports.")
if(broken_files.length && !auto_fix_broken) console.log("Use '--fix-sus' to try and automatically fix files with suspicious relative imports.")
if(broken_files.length && !auto_fix_broken) console.log("Use '--fix-all' to try and automatically fix files with suspicious or broken relative imports.")
if(broken_files.length === 0) console.log("\x1b[36m[ok]\x1b[0m No broken relative imports.")

process.stdout.write('\n')
